#!/usr/bin/env bash
# Script for generating entities
#yo jhipster:entity Contact
#yo jhipster:entity Address
yo jhipster:entity Age
yo jhipster:entity Characteristic
yo jhipster:entity Concept
yo jhipster:entity Intervention
yo jhipster:entity IdInfo
yo jhipster:entity Organisation
yo jhipster:entity OutcomeMeasure
yo jhipster:entity Practitioner
yo jhipster:entity Role
yo jhipster:entity SecondaryId
yo jhipster:entity Trial
yo jhipster:entity TrialCentre