package com.trialslink.factories;

import com.trialslink.domain.TrialImport;
import com.trialslink.domain.enumeration.TrialImportSource;
import com.trialslink.trialimport.CTgovImporter;
import com.trialslink.trialimport.NIHRImporter;
import io.advantageous.boon.core.Sys;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;

/**
 * Created by pigiotisk on 17/08/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class TrialImporterFactoryTest {

    @InjectMocks
    private TrialImporterFactory trialImporterFactory;

    @Mock
    private CTgovImporter cTgovImporter;

    @Mock
    private NIHRImporter nihrImporter;

    @Test
    public void shouldReturnctGovImporterInstance() throws Exception {
        TrialImport trialImport = new TrialImport();
        trialImport.setSource(TrialImportSource.CLINICALTRIALS_GOV);

        assert (trialImporterFactory.getTrialImporter(trialImport) instanceof CTgovImporter);
    }

    @Test
    public void shouldReturnnihrImporterInstance() throws Exception {
        TrialImport trialImport = new TrialImport();
        trialImport.setSource(TrialImportSource.NIHR);

        assert (trialImporterFactory.getTrialImporter(trialImport) instanceof NIHRImporter);
    }
}
