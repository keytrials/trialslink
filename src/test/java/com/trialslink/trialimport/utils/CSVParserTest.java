package com.trialslink.trialimport.utils;

import com.csvreader.CsvWriter;
import com.trialslink.TrialslinkApp;
import com.trialslink.repository.TrialCentreRepository;
import com.trialslink.service.CharacteristicService;
import com.trialslink.service.OrganisationService;
import com.trialslink.service.TrialVisitorService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class CSVParserTest {
    private final Logger log = LoggerFactory.getLogger(CSVParser.class);
    private TrialVisitorService trialVisitorService;
    private CharacteristicService characteristicService;
    private CSVParser parser = new CSVParser(trialVisitorService, characteristicService);
    private CSVParserResponse response = new CSVParserResponse();
    private final String TEST_FILE = "data/test.csv";
    private final String FAILED_TEST_FILE = "data/test_with_failed.csv";
    private final String EMPTY_TEST_FILE = "data/test_blank.csv";

    @Before
    public void setup() {

        // before we open the file check to see if it already exists
        try {
            // use FileWriter constructor that specifies open for appending
            CsvWriter csvOutputTest = new CsvWriter(new FileWriter(TEST_FILE, true), ',');
            CsvWriter csvOutputFailed = new CsvWriter(new FileWriter(FAILED_TEST_FILE, true), ',');
            CsvWriter csvOutputEmp = new CsvWriter(new FileWriter(EMPTY_TEST_FILE, true), ',');

            // if the file didn't already exist then we need to write out the header line

            csvOutputTest.write("Title");
            csvOutputTest.write("Research Summary");
            csvOutputTest.endRecord();

            csvOutputFailed.write("Title");
            csvOutputFailed.write("Research Summary");
            csvOutputFailed.endRecord();

            csvOutputEmp.write("");
            csvOutputEmp.endRecord();
            csvOutputEmp.close();

            // else assume that the file already has the correct header line

            // write out a few records
            csvOutputTest.write("title1");
            csvOutputTest.write("summary1");
            csvOutputTest.endRecord();

            csvOutputTest.write("title2");
            csvOutputTest.write(" Summary2");
            csvOutputTest.endRecord();

            csvOutputFailed.write("title1");
            csvOutputFailed.write("summary1");
            csvOutputFailed.endRecord();

            csvOutputFailed.write("");
            csvOutputFailed.write("summary2");
            csvOutputFailed.endRecord();

            csvOutputTest.close();
            csvOutputFailed.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() {
        // delete test files after the test is complete
        new File(TEST_FILE).delete();
        new File(FAILED_TEST_FILE).delete();
        new File(EMPTY_TEST_FILE).delete();
    }

    @Test
    public void testParse() {
        response = parser.parse(TEST_FILE);
        assertFalse(response.getImportedTrials().isEmpty());
        assertTrue(response.getFailedTrials().isEmpty());
    }

    /**
     * Only test empty file and file with failed, the corresponding method has caught the IOException already.
     * @throws Exception
     */
    @Test
    public void testParseFailed() {
        response = parser.parse(EMPTY_TEST_FILE);
        assertTrue(response.getImportedTrials().isEmpty());
        assertTrue(response.getFailedTrials().isEmpty());

        response = parser.parse(FAILED_TEST_FILE);
        assertFalse(response.getImportedTrials().isEmpty());
        assertFalse(response.getFailedTrials().isEmpty());
    }
}
