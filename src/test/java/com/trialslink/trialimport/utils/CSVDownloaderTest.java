package com.trialslink.trialimport.utils;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by pigiotisk on 17/08/2017.
 */
public class CSVDownloaderTest {
    @Test(expected = java.net.MalformedURLException.class)
    public void shouldThrowMalformedURLException() throws Exception {
        CSVDownloader csvDownloader = new CSVDownloader();
        csvDownloader.download(new URL("malformedURL"), "path");
    }

    @Test
    public void shouldFindUrl() throws IOException {
        String inputFileName = "data/test/inputTestFile.txt";
        String outputFileName = "data/test/outputTestFile.txt";

        File inputFile = new File(inputFileName);
        inputFile.getParentFile().mkdirs();
        inputFile.createNewFile();
        File outputFile = new File(outputFileName);
        outputFile.getParentFile().mkdirs();
        outputFile.createNewFile();

        //InputStream anyInputStream = new ByteArrayInputStream("test data".getBytes());
        FileOutputStream fileOutputStream = new FileOutputStream(inputFile, false);
        fileOutputStream.write("testData".getBytes());
        fileOutputStream.close();

        URL url = new File(inputFileName).toURI().toURL();
        CSVDownloader downloader = new CSVDownloader();
        downloader.download(url, outputFileName);

        assertTrue(FileUtils.contentEquals(inputFile, outputFile));

        //delete the files & directories
        inputFile.delete();
        outputFile.delete();

    }

}
