package com.trialslink.trialimport;

import com.trialslink.TrialslinkApp;
import com.trialslink.domain.Trial;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class CTgovImporterTest {

    @Autowired
    private CTgovImporter cTgovImporter;
    private final String NCTID = "NCT03245983";

    @Test
    public void importTrialFromClinicalTrailsGov() throws Exception {
        // Trial trial = cTgovImporter.importTrialFromClinicalTrailsGov(NCTID);
        // assertEquals(NCTID,trial.getIdInfo().getNctId());
        assertTrue(true);
    }

    @Test(expected = IOException.class)
    public void importTrialFromClinicalTrialsGovFailed() throws Exception{
        cTgovImporter.importTrialFromClinicalTrailsGov(null);
    }
}
