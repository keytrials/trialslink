package com.trialslink.web.rest;

import com.trialslink.TrialslinkApp;

import com.trialslink.domain.TrialImport;
import com.trialslink.repository.TrialImportRepository;
import com.trialslink.service.TrialImportService;
import com.trialslink.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.trialslink.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.trialslink.domain.enumeration.TrialImportSource;
import com.trialslink.domain.enumeration.TrialImportStatus;
/**
 * Test class for the TrialImportResource REST controller.
 *
 * @see TrialImportResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class TrialImportResourceIntTest {

    private static final TrialImportSource DEFAULT_SOURCE = TrialImportSource.NIHR;
    private static final TrialImportSource UPDATED_SOURCE = TrialImportSource.CLINICALTRIALS_GOV;

    private static final String DEFAULT_SOURCE_IDENTIFIER = "AAAAAAAAAA";
    private static final String UPDATED_SOURCE_IDENTIFIER = "BBBBBBBBBB";

    private static final TrialImportStatus DEFAULT_STATUS = TrialImportStatus.IN_PROGRESS;
    private static final TrialImportStatus UPDATED_STATUS = TrialImportStatus.COMPLETE;

    private static final String DEFAULT_FILE_PATH = "AAAAAAAAAA";
    private static final String UPDATED_FILE_PATH = "BBBBBBBBBB";

    @Autowired
    private TrialImportRepository trialImportRepository;

    @Autowired
    private TrialImportService trialImportService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restTrialImportMockMvc;

    private TrialImport trialImport;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TrialImportResource trialImportResource = new TrialImportResource(trialImportService);
        this.restTrialImportMockMvc = MockMvcBuilders.standaloneSetup(trialImportResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TrialImport createEntity() {
        TrialImport trialImport = new TrialImport()
            .source(DEFAULT_SOURCE)
            .sourceIdentifier(DEFAULT_SOURCE_IDENTIFIER)
            .status(DEFAULT_STATUS)
            .filePath(DEFAULT_FILE_PATH);
        return trialImport;
    }

    @Before
    public void initTest() {
        trialImportRepository.deleteAll();
        trialImport = createEntity();
    }

    @Test
    public void createTrialImport() throws Exception {
        int databaseSizeBeforeCreate = trialImportRepository.findAll().size();

        // Create the TrialImport
        restTrialImportMockMvc.perform(post("/api/trial-imports")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trialImport)))
            .andExpect(status().isCreated());

        // Validate the TrialImport in the database
        List<TrialImport> trialImportList = trialImportRepository.findAll();
        assertThat(trialImportList).hasSize(databaseSizeBeforeCreate + 1);
        TrialImport testTrialImport = trialImportList.get(trialImportList.size() - 1);
        assertThat(testTrialImport.getSource()).isEqualTo(DEFAULT_SOURCE);
        assertThat(testTrialImport.getSourceIdentifier()).isEqualTo(DEFAULT_SOURCE_IDENTIFIER);
        assertThat(testTrialImport.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testTrialImport.getFilePath()).isEqualTo(DEFAULT_FILE_PATH);
    }

    @Test
    public void createTrialImportWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = trialImportRepository.findAll().size();

        // Create the TrialImport with an existing ID
        trialImport.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restTrialImportMockMvc.perform(post("/api/trial-imports")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trialImport)))
            .andExpect(status().isBadRequest());

        // Validate the TrialImport in the database
        List<TrialImport> trialImportList = trialImportRepository.findAll();
        assertThat(trialImportList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkSourceIsRequired() throws Exception {
        int databaseSizeBeforeTest = trialImportRepository.findAll().size();
        // set the field null
        trialImport.setSource(null);

        // Create the TrialImport, which fails.

        restTrialImportMockMvc.perform(post("/api/trial-imports")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trialImport)))
            .andExpect(status().isBadRequest());

        List<TrialImport> trialImportList = trialImportRepository.findAll();
        assertThat(trialImportList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllTrialImports() throws Exception {
        // Initialize the database
        trialImportRepository.save(trialImport);

        // Get all the trialImportList
        restTrialImportMockMvc.perform(get("/api/trial-imports?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trialImport.getId())))
            .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE.toString())))
            .andExpect(jsonPath("$.[*].sourceIdentifier").value(hasItem(DEFAULT_SOURCE_IDENTIFIER.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].filePath").value(hasItem(DEFAULT_FILE_PATH.toString())));
    }

    @Test
    public void getTrialImport() throws Exception {
        // Initialize the database
        trialImportRepository.save(trialImport);

        // Get the trialImport
        restTrialImportMockMvc.perform(get("/api/trial-imports/{id}", trialImport.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(trialImport.getId()))
            .andExpect(jsonPath("$.source").value(DEFAULT_SOURCE.toString()))
            .andExpect(jsonPath("$.sourceIdentifier").value(DEFAULT_SOURCE_IDENTIFIER.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.filePath").value(DEFAULT_FILE_PATH.toString()));
    }

    @Test
    public void getNonExistingTrialImport() throws Exception {
        // Get the trialImport
        restTrialImportMockMvc.perform(get("/api/trial-imports/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateTrialImport() throws Exception {
        // Initialize the database
        trialImportService.save(trialImport);

        int databaseSizeBeforeUpdate = trialImportRepository.findAll().size();

        // Update the trialImport
        TrialImport updatedTrialImport = trialImportRepository.findOne(trialImport.getId());
        updatedTrialImport
            .source(UPDATED_SOURCE)
            .sourceIdentifier(UPDATED_SOURCE_IDENTIFIER)
            .status(UPDATED_STATUS)
            .filePath(UPDATED_FILE_PATH);

        restTrialImportMockMvc.perform(put("/api/trial-imports")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTrialImport)))
            .andExpect(status().isOk());

        // Validate the TrialImport in the database
        List<TrialImport> trialImportList = trialImportRepository.findAll();
        assertThat(trialImportList).hasSize(databaseSizeBeforeUpdate);
        TrialImport testTrialImport = trialImportList.get(trialImportList.size() - 1);
        assertThat(testTrialImport.getSource()).isEqualTo(UPDATED_SOURCE);
        assertThat(testTrialImport.getSourceIdentifier()).isEqualTo(UPDATED_SOURCE_IDENTIFIER);
        assertThat(testTrialImport.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testTrialImport.getFilePath()).isEqualTo(UPDATED_FILE_PATH);
    }

    @Test
    public void updateNonExistingTrialImport() throws Exception {
        int databaseSizeBeforeUpdate = trialImportRepository.findAll().size();

        // Create the TrialImport

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTrialImportMockMvc.perform(put("/api/trial-imports")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trialImport)))
            .andExpect(status().isCreated());

        // Validate the TrialImport in the database
        List<TrialImport> trialImportList = trialImportRepository.findAll();
        assertThat(trialImportList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteTrialImport() throws Exception {
        // Initialize the database
        trialImportService.save(trialImport);

        int databaseSizeBeforeDelete = trialImportRepository.findAll().size();

        // Get the trialImport
        restTrialImportMockMvc.perform(delete("/api/trial-imports/{id}", trialImport.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<TrialImport> trialImportList = trialImportRepository.findAll();
        assertThat(trialImportList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TrialImport.class);
        TrialImport trialImport1 = new TrialImport();
        trialImport1.setId("id1");
        TrialImport trialImport2 = new TrialImport();
        trialImport2.setId(trialImport1.getId());
        assertThat(trialImport1).isEqualTo(trialImport2);
        trialImport2.setId("id2");
        assertThat(trialImport1).isNotEqualTo(trialImport2);
        trialImport1.setId(null);
        assertThat(trialImport1).isNotEqualTo(trialImport2);
    }
}
