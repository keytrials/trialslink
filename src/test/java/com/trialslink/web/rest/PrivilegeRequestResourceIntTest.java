package com.trialslink.web.rest;

import com.trialslink.TrialslinkApp;
import com.trialslink.domain.PrivilegeRequest;
import com.trialslink.domain.enumeration.PrivilegeRequestStatus;
import com.trialslink.repository.PrivilegeRequestRepository;
import com.trialslink.service.PrivilegeRequestService;
import com.trialslink.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.Instant;
import java.util.List;

import static com.trialslink.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.theInstance;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
 * Test class for the PrivilegeRequestResource REST controller.
 *
 * @see PrivilegeRequestResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class PrivilegeRequestResourceIntTest {

    private static final PrivilegeRequestStatus DEFAULT_STATUS = PrivilegeRequestStatus.PENDING;
    private static final PrivilegeRequestStatus UPDATED_STATUS = PrivilegeRequestStatus.APPROVED;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_AT = Instant.now();

    private static final Instant DEFAULT_UPDATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_AT = Instant.now();

    @Autowired
    private PrivilegeRequestRepository privilegeRequestRepository;

    @Autowired
    private PrivilegeRequestService privilegeRequestService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restPrivilegeRequestMockMvc;

    private PrivilegeRequest privilegeRequest;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PrivilegeRequestResource privilegeRequestResource = new PrivilegeRequestResource(privilegeRequestService);
        this.restPrivilegeRequestMockMvc = MockMvcBuilders.standaloneSetup(privilegeRequestResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PrivilegeRequest createEntity() {
        PrivilegeRequest privilegeRequest = new PrivilegeRequest()
            .status(DEFAULT_STATUS);

        privilegeRequest.setCreatedBy(DEFAULT_CREATED_BY);
        privilegeRequest.setLastModifiedBy(DEFAULT_UPDATED_BY);
        privilegeRequest.setCreatedDate(DEFAULT_CREATED_AT);
        privilegeRequest.setLastModifiedDate(DEFAULT_UPDATED_AT);
        return privilegeRequest;
    }

    @Before
    public void initTest() {
        privilegeRequestRepository.deleteAll();
        privilegeRequest = createEntity();
    }

    @Test
    public void createPrivilegeRequest() throws Exception {
        int databaseSizeBeforeCreate = privilegeRequestRepository.findAll().size();

        // Create the PrivilegeRequest
        restPrivilegeRequestMockMvc.perform(post("/api/privilege-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(privilegeRequest)))
            .andExpect(status().isCreated());

        // Validate the PrivilegeRequest in the database
        List<PrivilegeRequest> privilegeRequestList = privilegeRequestRepository.findAll();
        assertThat(privilegeRequestList).hasSize(databaseSizeBeforeCreate + 1);
        PrivilegeRequest testPrivilegeRequest = privilegeRequestList.get(privilegeRequestList.size() - 1);
        assertThat(testPrivilegeRequest.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testPrivilegeRequest.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testPrivilegeRequest.getLastModifiedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testPrivilegeRequest.getCreatedDate()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testPrivilegeRequest.getLastModifiedDate()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    public void createPrivilegeRequestWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = privilegeRequestRepository.findAll().size();

        // Create the PrivilegeRequest with an existing ID
        privilegeRequest.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restPrivilegeRequestMockMvc.perform(post("/api/privilege-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(privilegeRequest)))
            .andExpect(status().isBadRequest());

        // Validate the PrivilegeRequest in the database
        List<PrivilegeRequest> privilegeRequestList = privilegeRequestRepository.findAll();
        assertThat(privilegeRequestList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = privilegeRequestRepository.findAll().size();
        // set the field null
        privilegeRequest.setStatus(null);

        // Create the PrivilegeRequest, which fails.

        restPrivilegeRequestMockMvc.perform(post("/api/privilege-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(privilegeRequest)))
            .andExpect(status().isBadRequest());

        List<PrivilegeRequest> privilegeRequestList = privilegeRequestRepository.findAll();
        assertThat(privilegeRequestList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllPrivilegeRequests() throws Exception {
        // Initialize the database
        privilegeRequestRepository.save(privilegeRequest);

        // Get all the privilegeRequestList
        restPrivilegeRequestMockMvc.perform(get("/api/privilege-requests?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(privilegeRequest.getId())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].created_by").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].updated_by").value(hasItem(DEFAULT_UPDATED_BY.toString())))
            .andExpect(jsonPath("$.[*].created_at").value(hasItem(theInstance(DEFAULT_CREATED_AT))))
            .andExpect(jsonPath("$.[*].updated_at").value(hasItem(theInstance(DEFAULT_UPDATED_AT))));
    }

    @Test
    public void getPrivilegeRequest() throws Exception {
        // Initialize the database
        privilegeRequestRepository.save(privilegeRequest);

        // Get the privilegeRequest
        restPrivilegeRequestMockMvc.perform(get("/api/privilege-requests/{id}", privilegeRequest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(privilegeRequest.getId()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.created_by").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.updated_by").value(DEFAULT_UPDATED_BY.toString()))
            .andExpect(jsonPath("$.created_at").value(theInstance(DEFAULT_CREATED_AT)))
            .andExpect(jsonPath("$.updated_at").value(theInstance(DEFAULT_UPDATED_AT)));
    }

    @Test
    public void getNonExistingPrivilegeRequest() throws Exception {
        // Get the privilegeRequest
        restPrivilegeRequestMockMvc.perform(get("/api/privilege-requests/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updatePrivilegeRequest() throws Exception {
        // Initialize the database
        privilegeRequestService.save(privilegeRequest);

        int databaseSizeBeforeUpdate = privilegeRequestRepository.findAll().size();

        // Update the privilegeRequest
        PrivilegeRequest updatedPrivilegeRequest = privilegeRequestRepository.findOne(privilegeRequest.getId());
        updatedPrivilegeRequest
            .status(UPDATED_STATUS);

        updatedPrivilegeRequest.setCreatedBy(UPDATED_CREATED_BY);
        updatedPrivilegeRequest.setLastModifiedBy(UPDATED_UPDATED_BY);
        updatedPrivilegeRequest.setCreatedDate(UPDATED_CREATED_AT);
        updatedPrivilegeRequest.setLastModifiedDate(UPDATED_UPDATED_AT);

        restPrivilegeRequestMockMvc.perform(put("/api/privilege-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPrivilegeRequest)))
            .andExpect(status().isOk());

        // Validate the PrivilegeRequest in the database
        List<PrivilegeRequest> privilegeRequestList = privilegeRequestRepository.findAll();
        assertThat(privilegeRequestList).hasSize(databaseSizeBeforeUpdate);
        PrivilegeRequest testPrivilegeRequest = privilegeRequestList.get(privilegeRequestList.size() - 1);
        assertThat(testPrivilegeRequest.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testPrivilegeRequest.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPrivilegeRequest.getLastModifiedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testPrivilegeRequest.getCreatedDate()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testPrivilegeRequest.getLastModifiedDate()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    public void updateNonExistingPrivilegeRequest() throws Exception {
        int databaseSizeBeforeUpdate = privilegeRequestRepository.findAll().size();

        // Create the PrivilegeRequest

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPrivilegeRequestMockMvc.perform(put("/api/privilege-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(privilegeRequest)))
            .andExpect(status().isCreated());

        // Validate the PrivilegeRequest in the database
        List<PrivilegeRequest> privilegeRequestList = privilegeRequestRepository.findAll();
        assertThat(privilegeRequestList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deletePrivilegeRequest() throws Exception {
        // Initialize the database
        privilegeRequestService.save(privilegeRequest);

        int databaseSizeBeforeDelete = privilegeRequestRepository.findAll().size();

        // Get the privilegeRequest
        restPrivilegeRequestMockMvc.perform(delete("/api/privilege-requests/{id}", privilegeRequest.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PrivilegeRequest> privilegeRequestList = privilegeRequestRepository.findAll();
        assertThat(privilegeRequestList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PrivilegeRequest.class);
        PrivilegeRequest privilegeRequest1 = new PrivilegeRequest();
        privilegeRequest1.setId("id1");
        PrivilegeRequest privilegeRequest2 = new PrivilegeRequest();
        privilegeRequest2.setId(privilegeRequest1.getId());
        assertThat(privilegeRequest1).isEqualTo(privilegeRequest2);
        privilegeRequest2.setId("id2");
        assertThat(privilegeRequest1).isNotEqualTo(privilegeRequest2);
        privilegeRequest1.setId(null);
        assertThat(privilegeRequest1).isNotEqualTo(privilegeRequest2);
    }
}
