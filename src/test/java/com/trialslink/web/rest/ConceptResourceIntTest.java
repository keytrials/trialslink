package com.trialslink.web.rest;

import com.trialslink.TrialslinkApp;
import com.trialslink.domain.Concept;
import com.trialslink.service.ConceptService;
import com.trialslink.service.util.TermlexRestClient;
import com.trialslink.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.trialslink.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ConceptResource REST controller.
 *
 * @see ConceptResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class ConceptResourceIntTest {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_SYSTEM = "AAAAAAAAAA";
    private static final String UPDATED_SYSTEM = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    @Autowired
    private ConceptService conceptRepository;
    @Autowired
    private TermlexRestClient termlexRestClient;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restConceptMockMvc;

    private Concept concept;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ConceptResource conceptResource = new ConceptResource(conceptRepository, termlexRestClient);
        this.restConceptMockMvc = MockMvcBuilders.standaloneSetup(conceptResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Concept createEntity() {
        Concept concept = new Concept()
            .label(DEFAULT_LABEL)
            .system(DEFAULT_SYSTEM)
            .code(DEFAULT_CODE)
            .type(DEFAULT_TYPE);
        return concept;
    }

    @Before
    public void initTest() {
        conceptRepository.deleteAll();
        concept = createEntity();
    }

    @Test
    public void createConcept() throws Exception {
        int databaseSizeBeforeCreate = conceptRepository.findAllAsList().size();

        // Create the Concept
        restConceptMockMvc.perform(post("/api/concepts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(concept)))
            .andExpect(status().isCreated());

        // Validate the Concept in the database
        List<Concept> conceptList = conceptRepository.findAllAsList();
        assertThat(conceptList).hasSize(databaseSizeBeforeCreate + 1);
        Concept testConcept = conceptList.get(conceptList.size() - 1);
        assertThat(testConcept.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testConcept.getSystem()).isEqualTo(DEFAULT_SYSTEM);
        assertThat(testConcept.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testConcept.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    public void createConceptWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = conceptRepository.findAllAsList().size();

        // Create the Concept with an existing ID
        concept.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restConceptMockMvc.perform(post("/api/concepts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(concept)))
            .andExpect(status().isBadRequest());

        // Validate the Concept in the database
        List<Concept> conceptList = conceptRepository.findAllAsList();
        assertThat(conceptList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkLabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = conceptRepository.findAllAsList().size();
        // set the field null
        concept.setLabel(null);

        // Create the Concept, which fails.

        restConceptMockMvc.perform(post("/api/concepts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(concept)))
            .andExpect(status().isBadRequest());

        List<Concept> conceptList = conceptRepository.findAllAsList();
        assertThat(conceptList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllConcepts() throws Exception {
        // Initialize the database
        conceptRepository.save(concept);

        // Get all the conceptList
        restConceptMockMvc.perform(get("/api/concepts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(concept.getId())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL.toString())))
            .andExpect(jsonPath("$.[*].system").value(hasItem(DEFAULT_SYSTEM.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    public void getConcept() throws Exception {
        // Initialize the database
        conceptRepository.save(concept);

        // Get the concept
        restConceptMockMvc.perform(get("/api/concepts/{id}", concept.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(concept.getId()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL.toString()))
            .andExpect(jsonPath("$.system").value(DEFAULT_SYSTEM.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    public void getNonExistingConcept() throws Exception {
        // Get the concept
        restConceptMockMvc.perform(get("/api/concepts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateConcept() throws Exception {
        // Initialize the database
        conceptRepository.save(concept);
        int databaseSizeBeforeUpdate = conceptRepository.findAllAsList().size();

        // Update the concept
        Concept updatedConcept = conceptRepository.findOne(concept.getId());
        updatedConcept
            .label(UPDATED_LABEL)
            .system(UPDATED_SYSTEM)
            .code(UPDATED_CODE)
            .type(UPDATED_TYPE);

        restConceptMockMvc.perform(put("/api/concepts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedConcept)))
            .andExpect(status().isOk());

        // Validate the Concept in the database
        List<Concept> conceptList = conceptRepository.findAllAsList();
        assertThat(conceptList).hasSize(databaseSizeBeforeUpdate);
        Concept testConcept = conceptList.get(conceptList.size() - 1);
        assertThat(testConcept.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testConcept.getSystem()).isEqualTo(UPDATED_SYSTEM);
        assertThat(testConcept.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testConcept.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    public void updateNonExistingConcept() throws Exception {
        int databaseSizeBeforeUpdate = conceptRepository.findAllAsList().size();

        // Create the Concept

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restConceptMockMvc.perform(put("/api/concepts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(concept)))
            .andExpect(status().isCreated());

        // Validate the Concept in the database
        List<Concept> conceptList = conceptRepository.findAllAsList();
        assertThat(conceptList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteConcept() throws Exception {
        // Initialize the database
        conceptRepository.save(concept);
        int databaseSizeBeforeDelete = conceptRepository.findAllAsList().size();

        // Get the concept
        restConceptMockMvc.perform(delete("/api/concepts/{id}", concept.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Concept> conceptList = conceptRepository.findAllAsList();
        assertThat(conceptList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Concept.class);
        Concept concept1 = new Concept();
        concept1.setId("id1");
        Concept concept2 = new Concept();
        concept2.setId(concept1.getId());
        assertThat(concept1).isEqualTo(concept2);
        concept2.setId("id2");
        assertThat(concept1).isNotEqualTo(concept2);
        concept1.setId(null);
        assertThat(concept1).isNotEqualTo(concept2);
    }
}
