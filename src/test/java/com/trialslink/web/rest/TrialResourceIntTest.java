package com.trialslink.web.rest;

import com.trialslink.TrialslinkApp;
import com.trialslink.domain.Concept;
import com.trialslink.domain.Condition;
import com.trialslink.domain.Trial;
import com.trialslink.domain.enumeration.Phase;
import com.trialslink.domain.enumeration.StudyType;
import com.trialslink.domain.enumeration.TrialStatus;
import com.trialslink.repository.ConceptRepository;
import com.trialslink.repository.TrialRepository;
import com.trialslink.service.AnnotationService;
import com.trialslink.service.TrialImportService;
import com.trialslink.service.TrialService;
import com.trialslink.web.rest.errors.ExceptionTranslator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;

import static com.trialslink.web.rest.TestUtil.createFormattingConversionService;
import static com.trialslink.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
 * Test class for the TrialResource REST controller.
 *
 * @see TrialResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class TrialResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_BRIEF_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_BRIEF_TITLE = "BBBBBBBBBB";

    private static final TrialStatus DEFAULT_STATUS = TrialStatus.RECRUITING;
    private static final TrialStatus UPDATED_STATUS = TrialStatus.SUSPENDED;

    private static final StudyType DEFAULT_TYPE = StudyType.INTERVENTIONAL;
    private static final StudyType UPDATED_TYPE = StudyType.OBSERVATIONAL;

    private static final Phase DEFAULT_PHASE = Phase.ONE;
    private static final Phase UPDATED_PHASE = Phase.TWO;

    private static final Boolean DEFAULT_HAS_EXPANDED_ACCESS = false;
    private static final Boolean UPDATED_HAS_EXPANDED_ACCESS = true;

    private static final String DEFAULT_CATEGORY = "AAAAAAAAAA";
    private static final String UPDATED_CATEGORY = "BBBBBBBBBB";

    private static final String DEFAULT_FOCUS = "AAAAAAAAAA";
    private static final String UPDATED_FOCUS = "BBBBBBBBBB";

    private static final Set<String> DEFAULT_KEYWORD = Collections.singleton("AAAAAAAAAA");
    private static final Set<String> UPDATED_KEYWORD = Collections.singleton("BBBBBBBBBB");

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_RECORD_VERIFICATION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_RECORD_VERIFICATION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_START_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_START_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_PRIMARY_COMPLETION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_PRIMARY_COMPLETION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_STUDY_COMPLETION_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_STUDY_COMPLETION_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_REASON_STOPPED = "AAAAAAAAAA";
    private static final String UPDATED_REASON_STOPPED = "BBBBBBBBBB";

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final String DEFAULT_BRIEF_SUMMARY = "AAAAAAAAAA";
    private static final String UPDATED_BRIEF_SUMMARY = "BBBBBBBBBB";

    private static final String DEFAULT_DETAILED_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DETAILED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private TrialRepository trialRepository;

    @Autowired
    private TrialService trialService;

    @Autowired
    private ConceptRepository conceptRepository;
    @Autowired
    private AnnotationService annotationService;
    @Autowired
    private TrialImportService trialImportService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restTrialMockMvc;

    private Trial trial;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TrialResource trialResource = new TrialResource(trialService, annotationService, trialImportService);
        this.restTrialMockMvc = MockMvcBuilders.standaloneSetup(trialResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Trial createEntity() {
        Trial trial = new Trial()
            .title(DEFAULT_TITLE)
            .briefTitle(DEFAULT_BRIEF_TITLE)
            .status(DEFAULT_STATUS)
            .type(DEFAULT_TYPE)
            .phase(Collections.singleton(DEFAULT_PHASE))
            .hasExpandedAccess(DEFAULT_HAS_EXPANDED_ACCESS)
            .category(DEFAULT_CATEGORY)
            .focus(DEFAULT_FOCUS)
            .keyword(DEFAULT_KEYWORD)
            .description(DEFAULT_DESCRIPTION)
            .recordVerificationDate(DEFAULT_RECORD_VERIFICATION_DATE)
            .startDate(DEFAULT_START_DATE)
            .primaryCompletionDate(DEFAULT_PRIMARY_COMPLETION_DATE)
            .studyCompletionDate(DEFAULT_STUDY_COMPLETION_DATE)
            .reasonStopped(DEFAULT_REASON_STOPPED)
            .note(DEFAULT_NOTE)
            .briefSummary(DEFAULT_BRIEF_SUMMARY)
            .detailedDescription(DEFAULT_DETAILED_DESCRIPTION);
        return trial;
    }

    @Before
    public void initTest() {
        trialRepository.deleteAll();
        trial = createEntity();
    }

    @Test
    public void createTrial() throws Exception {
        int databaseSizeBeforeCreate = trialRepository.findAll().size();

        // Create the Trial
        restTrialMockMvc.perform(post("/api/trials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trial)))
            .andExpect(status().isCreated());

        // Validate the Trial in the database
        List<Trial> trialList = trialRepository.findAll();
        assertThat(trialList).hasSize(databaseSizeBeforeCreate + 1);
        Trial testTrial = trialList.get(trialList.size() - 1);
        assertThat(testTrial.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testTrial.getBriefTitle()).isEqualTo(DEFAULT_BRIEF_TITLE);
        assertThat(testTrial.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testTrial.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testTrial.getPhases()).isEqualTo(DEFAULT_PHASE);
        assertThat(testTrial.isHasExpandedAccess()).isEqualTo(DEFAULT_HAS_EXPANDED_ACCESS);
        assertThat(testTrial.getCategory()).isEqualTo(DEFAULT_CATEGORY);
        assertThat(testTrial.getFocus()).isEqualTo(DEFAULT_FOCUS);
        assertThat(testTrial.getKeywords()).isEqualTo(DEFAULT_KEYWORD);
        assertThat(testTrial.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTrial.getRecordVerificationDate()).isEqualTo(DEFAULT_RECORD_VERIFICATION_DATE);
        assertThat(testTrial.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testTrial.getPrimaryCompletionDate()).isEqualTo(DEFAULT_PRIMARY_COMPLETION_DATE);
        assertThat(testTrial.getStudyCompletionDate()).isEqualTo(DEFAULT_STUDY_COMPLETION_DATE);
        assertThat(testTrial.getReasonStopped()).isEqualTo(DEFAULT_REASON_STOPPED);
        assertThat(testTrial.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testTrial.getBriefSummary()).isEqualTo(DEFAULT_BRIEF_SUMMARY);
        assertThat(testTrial.getDetailedDescription()).isEqualTo(DEFAULT_DETAILED_DESCRIPTION);
    }

    @Test
    public void createTrialWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = trialRepository.findAll().size();

        // Create the Trial with an existing ID
        trial.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restTrialMockMvc.perform(post("/api/trials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trial)))
            .andExpect(status().isBadRequest());

        // Validate the Trial in the database
        List<Trial> trialList = trialRepository.findAll();
        assertThat(trialList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = trialRepository.findAll().size();
        // set the field null
        trial.setTitle(null);

        // Create the Trial, which fails.

        restTrialMockMvc.perform(post("/api/trials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trial)))
            .andExpect(status().isBadRequest());

        List<Trial> trialList = trialRepository.findAll();
        assertThat(trialList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkBriefTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = trialRepository.findAll().size();
        // set the field null
        trial.setBriefTitle(null);

        // Create the Trial, which fails.

        restTrialMockMvc.perform(post("/api/trials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trial)))
            .andExpect(status().isBadRequest());

        List<Trial> trialList = trialRepository.findAll();
        assertThat(trialList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = trialRepository.findAll().size();
        // set the field null
        trial.setStatus(null);

        // Create the Trial, which fails.

        restTrialMockMvc.perform(post("/api/trials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trial)))
            .andExpect(status().isBadRequest());

        List<Trial> trialList = trialRepository.findAll();
        assertThat(trialList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = trialRepository.findAll().size();
        // set the field null
        trial.setType(null);

        // Create the Trial, which fails.

        restTrialMockMvc.perform(post("/api/trials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trial)))
            .andExpect(status().isBadRequest());

        List<Trial> trialList = trialRepository.findAll();
        assertThat(trialList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkBriefSummaryIsRequired() throws Exception {
        int databaseSizeBeforeTest = trialRepository.findAll().size();
        // set the field null
        trial.setBriefSummary(null);

        // Create the Trial, which fails.

        restTrialMockMvc.perform(post("/api/trials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trial)))
            .andExpect(status().isBadRequest());

        List<Trial> trialList = trialRepository.findAll();
        assertThat(trialList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkInfoButtonModelIsRead() throws Exception {

        // create QueryModel with conditions - we use concept repository to get relevant condition.code
        Page<Concept> conceptsPage = conceptRepository.findAllByType(new PageRequest(0, 5), "condition");
        List<String> conditions = new ArrayList<>();
        conceptsPage.getContent().forEach(concept -> {
            conditions.add(concept.getCode());
        });
        conditions.add("5c7ce73101b5830470234223");
        conditions.add("5c7ce73201b5830470234226");
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("mainSearchCriteria.v.c", conditions);
        queryParams.put("age", 30);

        // Post query model to search endpoint
        MockHttpServletResponse response = restTrialMockMvc.perform(post("/api/_search/trials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(queryParams))).andDo(print())
            .andExpect(status().isOk()).andReturn().getResponse();

        // verify header in response has link containing the conditions
        assertThat(response.getHeaders("Link").toString().contains("5c7ce73201b5830470234226"));
    }

    @Test
    public void checkSearchShouldNotBreakOnMissingCondition() throws Exception {

        // create QueryModel with conditions
        List<String> conditions = new ArrayList<>();
        conditions.add("hello");
        conditions.add("world");
        conditions.add(null);
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("mainSearchCriteria.v.c", conditions);
        queryParams.put("age", 30);

        // Post query model to search endpoint
        restTrialMockMvc.perform(post("/api/_search/trials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(queryParams))).andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    public void checkSearchShouldNotBreakOnFreetextSearch() throws Exception {

        // create QueryModel with conditions
        String token = "cancer";
        Map<String, Object> queryParams = new HashMap<>();
        queryParams.put("token", token);

        // Post query model to search endpoint
        restTrialMockMvc.perform(post("/api/_search/trials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(queryParams))).andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    public void getAllTrials() throws Exception {
        // Initialize the database
        trialRepository.save(trial);

        // Get all the trialList
        restTrialMockMvc.perform(get("/api/trials?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trial.getId())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].briefTitle").value(hasItem(DEFAULT_BRIEF_TITLE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].phase").value(hasItem(DEFAULT_PHASE.toString())))
            .andExpect(jsonPath("$.[*].hasExpandedAccess").value(hasItem(DEFAULT_HAS_EXPANDED_ACCESS.booleanValue())))
            .andExpect(jsonPath("$.[*].category").value(hasItem(DEFAULT_CATEGORY.toString())))
            .andExpect(jsonPath("$.[*].focus").value(hasItem(DEFAULT_FOCUS.toString())))
            .andExpect(jsonPath("$.[*].keyword").value(hasItem(DEFAULT_KEYWORD.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].recordVerificationDate").value(hasItem(sameInstant(DEFAULT_RECORD_VERIFICATION_DATE))))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(sameInstant(DEFAULT_START_DATE))))
            .andExpect(jsonPath("$.[*].primaryCompletionDate").value(hasItem(sameInstant(DEFAULT_PRIMARY_COMPLETION_DATE))))
            .andExpect(jsonPath("$.[*].studyCompletionDate").value(hasItem(sameInstant(DEFAULT_STUDY_COMPLETION_DATE))))
            .andExpect(jsonPath("$.[*].reasonStopped").value(hasItem(DEFAULT_REASON_STOPPED.toString())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())))
            .andExpect(jsonPath("$.[*].briefSummary").value(hasItem(DEFAULT_BRIEF_SUMMARY.toString())))
            .andExpect(jsonPath("$.[*].detailedDescription").value(hasItem(DEFAULT_DETAILED_DESCRIPTION.toString())));
    }

    @Test
    public void getTrial() throws Exception {
        // Initialize the database
        trialRepository.save(trial);

        // Get the trial
        restTrialMockMvc.perform(get("/api/trials/{id}", trial.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(trial.getId()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.briefTitle").value(DEFAULT_BRIEF_TITLE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.phase").value(DEFAULT_PHASE.toString()))
            .andExpect(jsonPath("$.hasExpandedAccess").value(DEFAULT_HAS_EXPANDED_ACCESS.booleanValue()))
            .andExpect(jsonPath("$.category").value(DEFAULT_CATEGORY.toString()))
            .andExpect(jsonPath("$.focus").value(DEFAULT_FOCUS.toString()))
            .andExpect(jsonPath("$.keyword").value(DEFAULT_KEYWORD.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.recordVerificationDate").value(sameInstant(DEFAULT_RECORD_VERIFICATION_DATE)))
            .andExpect(jsonPath("$.startDate").value(sameInstant(DEFAULT_START_DATE)))
            .andExpect(jsonPath("$.primaryCompletionDate").value(sameInstant(DEFAULT_PRIMARY_COMPLETION_DATE)))
            .andExpect(jsonPath("$.studyCompletionDate").value(sameInstant(DEFAULT_STUDY_COMPLETION_DATE)))
            .andExpect(jsonPath("$.reasonStopped").value(DEFAULT_REASON_STOPPED.toString()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()))
            .andExpect(jsonPath("$.briefSummary").value(DEFAULT_BRIEF_SUMMARY.toString()))
            .andExpect(jsonPath("$.detailedDescription").value(DEFAULT_DETAILED_DESCRIPTION.toString()));
    }

    @Test
    public void getNonExistingTrial() throws Exception {
        // Get the trial
        restTrialMockMvc.perform(get("/api/trials/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateTrial() throws Exception {
        // Initialize the database
        trialService.save(trial);

        int databaseSizeBeforeUpdate = trialRepository.findAll().size();

        // Update the trial
        Trial updatedTrial = trialRepository.findOne(trial.getId());
        updatedTrial
            .title(UPDATED_TITLE)
            .briefTitle(UPDATED_BRIEF_TITLE)
            .status(UPDATED_STATUS)
            .type(UPDATED_TYPE)
            .phase(Collections.singleton(UPDATED_PHASE))
            .hasExpandedAccess(UPDATED_HAS_EXPANDED_ACCESS)
            .category(UPDATED_CATEGORY)
            .focus(UPDATED_FOCUS)
            .keyword(UPDATED_KEYWORD)
            .description(UPDATED_DESCRIPTION)
            .recordVerificationDate(UPDATED_RECORD_VERIFICATION_DATE)
            .startDate(UPDATED_START_DATE)
            .primaryCompletionDate(UPDATED_PRIMARY_COMPLETION_DATE)
            .studyCompletionDate(UPDATED_STUDY_COMPLETION_DATE)
            .reasonStopped(UPDATED_REASON_STOPPED)
            .note(UPDATED_NOTE)
            .briefSummary(UPDATED_BRIEF_SUMMARY)
            .detailedDescription(UPDATED_DETAILED_DESCRIPTION);

        restTrialMockMvc.perform(put("/api/trials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTrial)))
            .andExpect(status().isOk());

        // Validate the Trial in the database
        List<Trial> trialList = trialRepository.findAll();
        assertThat(trialList).hasSize(databaseSizeBeforeUpdate);
        Trial testTrial = trialList.get(trialList.size() - 1);
        assertThat(testTrial.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testTrial.getBriefTitle()).isEqualTo(UPDATED_BRIEF_TITLE);
        assertThat(testTrial.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testTrial.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testTrial.getPhases()).isEqualTo(UPDATED_PHASE);
        assertThat(testTrial.isHasExpandedAccess()).isEqualTo(UPDATED_HAS_EXPANDED_ACCESS);
        assertThat(testTrial.getCategory()).isEqualTo(UPDATED_CATEGORY);
        assertThat(testTrial.getFocus()).isEqualTo(UPDATED_FOCUS);
        assertThat(testTrial.getKeywords()).isEqualTo(UPDATED_KEYWORD);
        assertThat(testTrial.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTrial.getRecordVerificationDate()).isEqualTo(UPDATED_RECORD_VERIFICATION_DATE);
        assertThat(testTrial.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testTrial.getPrimaryCompletionDate()).isEqualTo(UPDATED_PRIMARY_COMPLETION_DATE);
        assertThat(testTrial.getStudyCompletionDate()).isEqualTo(UPDATED_STUDY_COMPLETION_DATE);
        assertThat(testTrial.getReasonStopped()).isEqualTo(UPDATED_REASON_STOPPED);
        assertThat(testTrial.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testTrial.getBriefSummary()).isEqualTo(UPDATED_BRIEF_SUMMARY);
        assertThat(testTrial.getDetailedDescription()).isEqualTo(UPDATED_DETAILED_DESCRIPTION);
    }

    @Test
    public void updateNonExistingTrial() throws Exception {
        int databaseSizeBeforeUpdate = trialRepository.findAll().size();

        // Create the Trial

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTrialMockMvc.perform(put("/api/trials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trial)))
            .andExpect(status().isCreated());

        // Validate the Trial in the database
        List<Trial> trialList = trialRepository.findAll();
        assertThat(trialList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteTrial() throws Exception {
        // Initialize the database
        trialService.save(trial);

        int databaseSizeBeforeDelete = trialRepository.findAll().size();

        // Get the trial
        restTrialMockMvc.perform(delete("/api/trials/{id}", trial.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Trial> trialList = trialRepository.findAll();
        assertThat(trialList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void sanitiseTrials() throws Exception {
        // Initialize the database
        Condition condition = new Condition();
        condition.setCode("64572001");
        trial.addConditions(condition);
        trial = trialService.save(trial);

        // Get the trial
        restTrialMockMvc.perform(get("/api/_sanitise/trials")
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        //@todo - please do this
        // Validate the database has trial that does not have above condition
        Trial sanitisedTrial = trialRepository.findOne(trial.getId());
        assertThat(sanitisedTrial.getConditions().isEmpty());
    }

    @Test
    public void importTrials() throws Exception {
        restTrialMockMvc.perform(
                fileUpload("/api/_import/trials")
                        .file(new MockMultipartFile("file", "test-import.xls", "application/xls",
                                getClass().getResourceAsStream("/test-import.xls"))))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Trial.class);
        Trial trial1 = new Trial();
        trial1.setId("id1");
        Trial trial2 = new Trial();
        trial2.setId(trial1.getId());
        assertThat(trial1).isEqualTo(trial2);
        trial2.setId("id2");
        assertThat(trial1).isNotEqualTo(trial2);
        trial1.setId(null);
        assertThat(trial1).isNotEqualTo(trial2);
    }
}
