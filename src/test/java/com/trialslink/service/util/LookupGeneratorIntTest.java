package com.trialslink.service.util;

import com.trialslink.TrialslinkApp;
import com.trialslink.domain.Concept;
import com.trialslink.service.ConceptService;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;

/**
 * A test for {@link LookupGenerator}
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TrialslinkApp.class)
public class LookupGeneratorIntTest extends TestCase {

    private final Logger log = LoggerFactory.getLogger(LookupGeneratorIntTest.class);

    @Autowired
    private LookupGenerator lookupGenerator;
    @Autowired
    private ConceptService conceptService;
    Concept conceptA;
    Concept conceptB;
    Concept conceptC;
    Concept conceptD;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.lookupGenerator = new LookupGenerator(this.conceptService);
        // set up concept hierarchy: A --> B --> C & D ; A is parent of B and B is parent of C and D
        conceptA = new Concept("A");
        conceptService.save(conceptA);
        conceptB = new Concept("B");
        conceptService.save(conceptB);
        conceptC = new Concept("C");
        conceptService.save(conceptC);
        conceptD = new Concept("D");
        conceptService.save(conceptD);

        conceptB.addParent(conceptA);
        // always save child which automatically add parent
        conceptService.save(conceptB);

        // now add children for conceptB
        conceptC.addParent(conceptB);
        conceptService.save(conceptC);
        conceptD.addParent(conceptB);
        conceptService.save(conceptD);
    }

    @Test
    public void testParentsAreUpdated() throws Exception {
        // parent conceptA should have conceptB as child
        assertTrue("conceptA should have conceptB as child", conceptA.getChildren().contains(conceptB.getId()));
        // parent conceptB should have conceptC as child
        assertTrue("conceptB should have conceptC as child", conceptB.getChildren().contains(conceptC.getId()));
        // parent conceptB should have conceptD as child
        assertTrue("conceptB should have conceptD as child", conceptB.getChildren().contains(conceptD.getId()));
    }

    @Test
    public void testGetDescendants() throws Exception {
        // lookup generator must return B,C and D as descendants of A
        Set<String> descendants = lookupGenerator.getDescendants(conceptA.getId());
        assertTrue("conceptA should have conceptB as descendant", descendants.contains(conceptB.getId()));
        assertTrue("conceptA should have conceptC as descendant", descendants.contains(conceptC.getId()));
        assertTrue("conceptA should have conceptD as descendant", descendants.contains(conceptD.getId()));
    }

    @Test
    public void testGetAncestors() throws Exception {
        // lookup generator must return B and A as ancestors of C, but not D
        Set<String> ancestors = lookupGenerator.getAncestors(conceptC.getId());
        log.info("ancestors = {}" , ancestors);
        assertTrue("conceptC should have conceptA as ancestor", ancestors.contains(conceptA.getId()));
        assertTrue("conceptC should have conceptB as ancestor", ancestors.contains(conceptB.getId()));
        assertTrue("conceptC should NOT have concept as ancestor", !ancestors.contains(conceptD.getId()));
    }
}