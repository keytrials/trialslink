import { JhiAlertService } from 'ng-jhipster';
import { Sanitizer } from '@angular/core';

export class MockAlertService {

    getMock(sanitizer: Sanitizer) {
        return new JhiAlertService(sanitizer, null, null);
    }

    init() {}
}
