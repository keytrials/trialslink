/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { TrialslinkTestModule } from '../../../test.module';
import { TrialCentreComponent } from '../../../../../../main/webapp/app/entities/trial-centre/trial-centre.component';
import { TrialCentreService } from '../../../../../../main/webapp/app/entities/trial-centre/trial-centre.service';
import { TrialCentre } from '../../../../../../main/webapp/app/entities/trial-centre/trial-centre.model';

describe('Component Tests', () => {

    describe('TrialCentre Management Component', () => {
        let comp: TrialCentreComponent;
        let fixture: ComponentFixture<TrialCentreComponent>;
        let service: TrialCentreService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [TrialCentreComponent],
                providers: [
                    TrialCentreService
                ]
            })
            .overrideTemplate(TrialCentreComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TrialCentreComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TrialCentreService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new TrialCentre('123')],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.trialCentres[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
