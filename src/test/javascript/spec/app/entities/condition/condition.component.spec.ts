/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { TrialslinkTestModule } from '../../../test.module';
import { ConditionComponent } from '../../../../../../main/webapp/app/entities/condition/condition.component';
import { ConditionService } from '../../../../../../main/webapp/app/entities/condition/condition.service';
import { Condition } from '../../../../../../main/webapp/app/entities/condition/condition.model';

describe('Component Tests', () => {

    describe('Condition Management Component', () => {
        let comp: ConditionComponent;
        let fixture: ComponentFixture<ConditionComponent>;
        let service: ConditionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [ConditionComponent],
                providers: [
                    ConditionService
                ]
            })
            .overrideTemplate(ConditionComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ConditionComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConditionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new Condition('123')],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.conditions[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
