/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';

import { TrialslinkTestModule } from '../../../test.module';
import { OutcomeMeasureDetailComponent } from '../../../../../../main/webapp/app/entities/outcome-measure/outcome-measure-detail.component';
import { OutcomeMeasureService } from '../../../../../../main/webapp/app/entities/outcome-measure/outcome-measure.service';
import { OutcomeMeasure } from '../../../../../../main/webapp/app/entities/outcome-measure/outcome-measure.model';

describe('Component Tests', () => {

    describe('OutcomeMeasure Management Detail Component', () => {
        let comp: OutcomeMeasureDetailComponent;
        let fixture: ComponentFixture<OutcomeMeasureDetailComponent>;
        let service: OutcomeMeasureService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [OutcomeMeasureDetailComponent],
                providers: [
                    OutcomeMeasureService
                ]
            })
            .overrideTemplate(OutcomeMeasureDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(OutcomeMeasureDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OutcomeMeasureService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new OutcomeMeasure('123')));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.outcomeMeasure).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
