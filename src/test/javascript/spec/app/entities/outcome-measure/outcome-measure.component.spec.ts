/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { TrialslinkTestModule } from '../../../test.module';
import { OutcomeMeasureComponent } from '../../../../../../main/webapp/app/entities/outcome-measure/outcome-measure.component';
import { OutcomeMeasureService } from '../../../../../../main/webapp/app/entities/outcome-measure/outcome-measure.service';
import { OutcomeMeasure } from '../../../../../../main/webapp/app/entities/outcome-measure/outcome-measure.model';

describe('Component Tests', () => {

    describe('OutcomeMeasure Management Component', () => {
        let comp: OutcomeMeasureComponent;
        let fixture: ComponentFixture<OutcomeMeasureComponent>;
        let service: OutcomeMeasureService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [OutcomeMeasureComponent],
                providers: [
                    OutcomeMeasureService
                ]
            })
            .overrideTemplate(OutcomeMeasureComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(OutcomeMeasureComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(OutcomeMeasureService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new OutcomeMeasure('123')],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.outcomeMeasures[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
