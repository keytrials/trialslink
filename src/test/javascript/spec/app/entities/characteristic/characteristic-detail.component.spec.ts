/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';

import { TrialslinkTestModule } from '../../../test.module';
import { CharacteristicDetailComponent } from '../../../../../../main/webapp/app/entities/characteristic/characteristic-detail.component';
import { CharacteristicService } from '../../../../../../main/webapp/app/entities/characteristic/characteristic.service';
import { Characteristic } from '../../../../../../main/webapp/app/entities/characteristic/characteristic.model';

describe('Component Tests', () => {

    describe('Characteristic Management Detail Component', () => {
        let comp: CharacteristicDetailComponent;
        let fixture: ComponentFixture<CharacteristicDetailComponent>;
        let service: CharacteristicService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [CharacteristicDetailComponent],
                providers: [
                    CharacteristicService
                ]
            })
            .overrideTemplate(CharacteristicDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CharacteristicDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CharacteristicService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new Characteristic('123')));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.characteristic).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
