/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { TrialslinkTestModule } from '../../../test.module';
import { CharacteristicComponent } from '../../../../../../main/webapp/app/entities/characteristic/characteristic.component';
import { CharacteristicService } from '../../../../../../main/webapp/app/entities/characteristic/characteristic.service';
import { Characteristic } from '../../../../../../main/webapp/app/entities/characteristic/characteristic.model';

describe('Component Tests', () => {

    describe('Characteristic Management Component', () => {
        let comp: CharacteristicComponent;
        let fixture: ComponentFixture<CharacteristicComponent>;
        let service: CharacteristicService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [CharacteristicComponent],
                providers: [
                    CharacteristicService
                ]
            })
            .overrideTemplate(CharacteristicComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CharacteristicComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CharacteristicService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new Characteristic('123')],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.characteristics[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
