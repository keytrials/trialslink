/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TrialslinkTestModule } from '../../../test.module';
import { CharacteristicDialogComponent } from '../../../../../../main/webapp/app/entities/characteristic/characteristic-dialog.component';
import { CharacteristicService } from '../../../../../../main/webapp/app/entities/characteristic/characteristic.service';
import { Characteristic } from '../../../../../../main/webapp/app/entities/characteristic/characteristic.model';

describe('Component Tests', () => {

    describe('Characteristic Management Dialog Component', () => {
        let comp: CharacteristicDialogComponent;
        let fixture: ComponentFixture<CharacteristicDialogComponent>;
        let service: CharacteristicService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [CharacteristicDialogComponent],
                providers: [
                    CharacteristicService
                ]
            })
            .overrideTemplate(CharacteristicDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CharacteristicDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CharacteristicService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Characteristic('123');
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.characteristic = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'characteristicListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Characteristic();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.characteristic = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'characteristicListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
