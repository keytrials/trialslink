/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { TrialslinkTestModule } from '../../../test.module';
import { SecondaryIdComponent } from '../../../../../../main/webapp/app/entities/secondary-id/secondary-id.component';
import { SecondaryIdService } from '../../../../../../main/webapp/app/entities/secondary-id/secondary-id.service';
import { SecondaryId } from '../../../../../../main/webapp/app/entities/secondary-id/secondary-id.model';

describe('Component Tests', () => {

    describe('SecondaryId Management Component', () => {
        let comp: SecondaryIdComponent;
        let fixture: ComponentFixture<SecondaryIdComponent>;
        let service: SecondaryIdService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [SecondaryIdComponent],
                providers: [
                    SecondaryIdService
                ]
            })
            .overrideTemplate(SecondaryIdComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SecondaryIdComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SecondaryIdService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new SecondaryId('123')],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.secondaryIds[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
