/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TrialslinkTestModule } from '../../../test.module';
import { SecondaryIdDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/secondary-id/secondary-id-delete-dialog.component';
import { SecondaryIdService } from '../../../../../../main/webapp/app/entities/secondary-id/secondary-id.service';

describe('Component Tests', () => {

    describe('SecondaryId Management Delete Component', () => {
        let comp: SecondaryIdDeleteDialogComponent;
        let fixture: ComponentFixture<SecondaryIdDeleteDialogComponent>;
        let service: SecondaryIdService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [SecondaryIdDeleteDialogComponent],
                providers: [
                    SecondaryIdService
                ]
            })
            .overrideTemplate(SecondaryIdDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SecondaryIdDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SecondaryIdService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete('123');
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith('123');
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
