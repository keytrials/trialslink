/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';

import { TrialslinkTestModule } from '../../../test.module';
import { SecondaryIdDetailComponent } from '../../../../../../main/webapp/app/entities/secondary-id/secondary-id-detail.component';
import { SecondaryIdService } from '../../../../../../main/webapp/app/entities/secondary-id/secondary-id.service';
import { SecondaryId } from '../../../../../../main/webapp/app/entities/secondary-id/secondary-id.model';

describe('Component Tests', () => {

    describe('SecondaryId Management Detail Component', () => {
        let comp: SecondaryIdDetailComponent;
        let fixture: ComponentFixture<SecondaryIdDetailComponent>;
        let service: SecondaryIdService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [SecondaryIdDetailComponent],
                providers: [
                    SecondaryIdService
                ]
            })
            .overrideTemplate(SecondaryIdDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SecondaryIdDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SecondaryIdService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new SecondaryId('123')));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.secondaryId).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
