/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { TrialslinkTestModule } from '../../../test.module';
import { TrialComponent } from '../../../../../../main/webapp/app/entities/trial/trial.component';
import { TrialService } from '../../../../../../main/webapp/app/entities/trial/trial.service';
import { Trial } from '../../../../../../main/webapp/app/entities/trial/trial.model';

describe('Component Tests', () => {

    describe('Trial Management Component', () => {
        let comp: TrialComponent;
        let fixture: ComponentFixture<TrialComponent>;
        let service: TrialService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [TrialComponent],
                providers: [
                    TrialService
                ]
            })
            .overrideTemplate(TrialComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TrialComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TrialService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new Trial('123')],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.trials[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
