import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { Http, BaseRequestOptions } from '@angular/http';
import { OnInit, Sanitizer } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils } from 'ng-jhipster';
import { JhiEventManager, JhiAlertService, JhiLanguageService } from 'ng-jhipster';
import { MockLanguageService } from '../../../helpers/mock-language.service';
import { MockAlertService } from '../../../helpers/mock-alert.service';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';

import { TrialslinkTestModule } from '../../../test.module';
import { TrialDetailComponent } from '../../../../../../main/webapp/app/entities/trial/trial-detail.component';
import { TrialService } from '../../../../../../main/webapp/app/entities/trial/trial.service';
import { TrialCentreService } from '../../../../../../main/webapp/app/entities/trial-centre/trial-centre.service';
import { Trial } from '../../../../../../main/webapp/app/entities/trial/trial.model';
import {Principal} from "../../../../../../main/webapp/app/shared/auth/principal.service";
import {MockPrincipal} from "../../../helpers/mock-principal.service";

describe('Component Tests', () => {

    describe('Trial Management Detail Component', () => {
        let comp: TrialDetailComponent;
        let fixture: ComponentFixture<TrialDetailComponent>;
        let service: TrialService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [TrialDetailComponent],
                providers: [
                    MockBackend,
                    BaseRequestOptions,
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Http,
                        useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
                            return new Http(backendInstance, defaultOptions);
                        },
                        deps: [MockBackend, BaseRequestOptions]
                    },
                    {
                        provide: JhiLanguageService,
                        useClass: MockLanguageService
                    },
                    {
                        provide: JhiAlertService,
                        useFactory: MockAlertService, deps: [Sanitizer]
                    },
                    {
                        provide: Principal,
                        useClass: MockPrincipal
                    },
                    TrialService,
                    TrialCentreService,
                    JhiEventManager
                ]
            })
            .overrideTemplate(TrialDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TrialDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TrialService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new Trial('123')));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.trial).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
