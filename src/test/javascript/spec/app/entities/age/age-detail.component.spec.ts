/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';

import { TrialslinkTestModule } from '../../../test.module';
import { AgeDetailComponent } from '../../../../../../main/webapp/app/entities/age/age-detail.component';
import { AgeService } from '../../../../../../main/webapp/app/entities/age/age.service';
import { Age } from '../../../../../../main/webapp/app/entities/age/age.model';

describe('Component Tests', () => {

    describe('Age Management Detail Component', () => {
        let comp: AgeDetailComponent;
        let fixture: ComponentFixture<AgeDetailComponent>;
        let service: AgeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [AgeDetailComponent],
                providers: [
                    AgeService
                ]
            })
            .overrideTemplate(AgeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AgeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AgeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new Age('123')));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.age).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
