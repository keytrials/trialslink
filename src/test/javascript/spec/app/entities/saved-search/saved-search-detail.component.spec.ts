/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TrialslinkTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { SavedSearchDetailComponent } from '../../../../../../main/webapp/app/entities/saved-search/saved-search-detail.component';
import { SavedSearchService } from '../../../../../../main/webapp/app/entities/saved-search/saved-search.service';
import { SavedSearch } from '../../../../../../main/webapp/app/entities/saved-search/saved-search.model';

describe('Component Tests', () => {

    describe('SavedSearch Management Detail Component', () => {
        let comp: SavedSearchDetailComponent;
        let fixture: ComponentFixture<SavedSearchDetailComponent>;
        let service: SavedSearchService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [SavedSearchDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    SavedSearchService,
                    JhiEventManager
                ]
            }).overrideComponent(SavedSearchDetailComponent, {
                set: {
                    template: ''
                }
            })
//=======
//                    SavedSearchService
//                ]
//            })
//            .overrideTemplate(SavedSearchDetailComponent, '')
//>>>>>>> jhipster_upgrade
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SavedSearchDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SavedSearchService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new SavedSearch('123')));

                // WHEN
                comp.ngOnInit();

//<<<<<<< HEAD
            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.savedSearch).toEqual(jasmine.objectContaining({id: 'aaa'}));
//=======
//                // THEN
//                expect(service.find).toHaveBeenCalledWith('123');
//                expect(comp.savedSearch).toEqual(jasmine.objectContaining({id: '123'}));
//>>>>>>> jhipster_upgrade
            });
        });
    });

});
