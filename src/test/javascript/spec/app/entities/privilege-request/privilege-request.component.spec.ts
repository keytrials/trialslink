/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { TrialslinkTestModule } from '../../../test.module';
import { PrivilegeRequestComponent } from '../../../../../../main/webapp/app/entities/privilege-request/privilege-request.component';
import { PrivilegeRequestService } from '../../../../../../main/webapp/app/entities/privilege-request/privilege-request.service';
import { PrivilegeRequest } from '../../../../../../main/webapp/app/entities/privilege-request/privilege-request.model';

describe('Component Tests', () => {

    describe('PrivilegeRequest Management Component', () => {
        let comp: PrivilegeRequestComponent;
        let fixture: ComponentFixture<PrivilegeRequestComponent>;
        let service: PrivilegeRequestService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [PrivilegeRequestComponent],
                providers: [
                    PrivilegeRequestService
                ]
            })
            .overrideTemplate(PrivilegeRequestComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PrivilegeRequestComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PrivilegeRequestService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new PrivilegeRequest('123')],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.privilegeRequests[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
