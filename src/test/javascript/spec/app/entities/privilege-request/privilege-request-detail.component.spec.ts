/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TrialslinkTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';

import { PrivilegeRequestDetailComponent } from '../../../../../../main/webapp/app/entities/privilege-request/privilege-request-detail.component';
import { PrivilegeRequestService } from '../../../../../../main/webapp/app/entities/privilege-request/privilege-request.service';
import { PrivilegeRequest } from '../../../../../../main/webapp/app/entities/privilege-request/privilege-request.model';

describe('Component Tests', () => {

    describe('PrivilegeRequest Management Detail Component', () => {
        let comp: PrivilegeRequestDetailComponent;
        let fixture: ComponentFixture<PrivilegeRequestDetailComponent>;
        let service: PrivilegeRequestService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [PrivilegeRequestDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PrivilegeRequestService,
                    JhiEventManager
                ]
            })
            .overrideTemplate(PrivilegeRequestDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PrivilegeRequestDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PrivilegeRequestService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new PrivilegeRequest('123')));

                // WHEN
                comp.ngOnInit();

//<<<<<<< HEAD
            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.privilegeRequest).toEqual(jasmine.objectContaining({id: 'aaa'}));
//=======
//                // THEN
//                expect(service.find).toHaveBeenCalledWith('123');
//                expect(comp.privilegeRequest).toEqual(jasmine.objectContaining({id: '123'}));
//>>>>>>> jhipster_upgrade
            });
        });
    });

});
