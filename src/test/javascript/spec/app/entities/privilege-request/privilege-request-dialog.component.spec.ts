/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TrialslinkTestModule } from '../../../test.module';
import { PrivilegeRequestDialogComponent } from '../../../../../../main/webapp/app/entities/privilege-request/privilege-request-dialog.component';
import { PrivilegeRequestService } from '../../../../../../main/webapp/app/entities/privilege-request/privilege-request.service';
import { PrivilegeRequest } from '../../../../../../main/webapp/app/entities/privilege-request/privilege-request.model';

describe('Component Tests', () => {

    describe('PrivilegeRequest Management Dialog Component', () => {
        let comp: PrivilegeRequestDialogComponent;
        let fixture: ComponentFixture<PrivilegeRequestDialogComponent>;
        let service: PrivilegeRequestService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [PrivilegeRequestDialogComponent],
                providers: [
                    PrivilegeRequestService
                ]
            })
            .overrideTemplate(PrivilegeRequestDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PrivilegeRequestDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PrivilegeRequestService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new PrivilegeRequest('123');
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.privilegeRequest = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'privilegeRequestListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new PrivilegeRequest();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.privilegeRequest = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'privilegeRequestListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
