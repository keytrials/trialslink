/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { TrialslinkTestModule } from '../../../test.module';
import { ConceptComponent } from '../../../../../../main/webapp/app/entities/concept/concept.component';
import { ConceptService } from '../../../../../../main/webapp/app/entities/concept/concept.service';
import { Concept } from '../../../../../../main/webapp/app/entities/concept/concept.model';

describe('Component Tests', () => {

    describe('Concept Management Component', () => {
        let comp: ConceptComponent;
        let fixture: ComponentFixture<ConceptComponent>;
        let service: ConceptService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [ConceptComponent],
                providers: [
                    ConceptService
                ]
            })
            .overrideTemplate(ConceptComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ConceptComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConceptService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new Concept('123')],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.concepts[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
