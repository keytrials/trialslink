/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TrialslinkTestModule } from '../../../test.module';
import { ConceptDialogComponent } from '../../../../../../main/webapp/app/entities/concept/concept-dialog.component';
import { ConceptService } from '../../../../../../main/webapp/app/entities/concept/concept.service';
import { Concept } from '../../../../../../main/webapp/app/entities/concept/concept.model';

describe('Component Tests', () => {

    describe('Concept Management Dialog Component', () => {
        let comp: ConceptDialogComponent;
        let fixture: ComponentFixture<ConceptDialogComponent>;
        let service: ConceptService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [ConceptDialogComponent],
                providers: [
                    ConceptService
                ]
            })
            .overrideTemplate(ConceptDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ConceptDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConceptService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Concept('123');
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.concept = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'conceptListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Concept();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.concept = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'conceptListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
