/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';

import { TrialslinkTestModule } from '../../../test.module';
import { ConceptDetailComponent } from '../../../../../../main/webapp/app/entities/concept/concept-detail.component';
import { ConceptService } from '../../../../../../main/webapp/app/entities/concept/concept.service';
import { Concept } from '../../../../../../main/webapp/app/entities/concept/concept.model';

describe('Component Tests', () => {

    describe('Concept Management Detail Component', () => {
        let comp: ConceptDetailComponent;
        let fixture: ComponentFixture<ConceptDetailComponent>;
        let service: ConceptService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [ConceptDetailComponent],
                providers: [
                    ConceptService
                ]
            })
            .overrideTemplate(ConceptDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ConceptDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ConceptService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new Concept('123')));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.concept).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
