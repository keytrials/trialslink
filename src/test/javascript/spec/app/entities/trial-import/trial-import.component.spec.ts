/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { TrialslinkTestModule } from '../../../test.module';
import { TrialImportComponent } from '../../../../../../main/webapp/app/entities/trial-import/trial-import.component';
import { TrialImportService } from '../../../../../../main/webapp/app/entities/trial-import/trial-import.service';
import { TrialImport } from '../../../../../../main/webapp/app/entities/trial-import/trial-import.model';

describe('Component Tests', () => {

    describe('TrialImport Management Component', () => {
        let comp: TrialImportComponent;
        let fixture: ComponentFixture<TrialImportComponent>;
        let service: TrialImportService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [TrialImportComponent],
                providers: [
                    TrialImportService
                ]
            })
            .overrideTemplate(TrialImportComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TrialImportComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TrialImportService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new TrialImport('123')],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.trialImports[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
