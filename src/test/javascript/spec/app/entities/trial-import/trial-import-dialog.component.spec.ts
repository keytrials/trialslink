/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TrialslinkTestModule } from '../../../test.module';
import { TrialImportDialogComponent } from '../../../../../../main/webapp/app/entities/trial-import/trial-import-dialog.component';
import { TrialImportService } from '../../../../../../main/webapp/app/entities/trial-import/trial-import.service';
import { TrialImport } from '../../../../../../main/webapp/app/entities/trial-import/trial-import.model';

describe('Component Tests', () => {

    describe('TrialImport Management Dialog Component', () => {
        let comp: TrialImportDialogComponent;
        let fixture: ComponentFixture<TrialImportDialogComponent>;
        let service: TrialImportService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [TrialImportDialogComponent],
                providers: [
                    TrialImportService
                ]
            })
            .overrideTemplate(TrialImportDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TrialImportDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TrialImportService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TrialImport('123');
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.trialImport = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'trialImportListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TrialImport();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.trialImport = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'trialImportListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
