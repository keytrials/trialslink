/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TrialslinkTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TrialImportDetailComponent } from '../../../../../../main/webapp/app/entities/trial-import/trial-import-detail.component';
import { TrialImportService } from '../../../../../../main/webapp/app/entities/trial-import/trial-import.service';
import {
    TrialImport,
    TrialImportStatus
} from '../../../../../../main/webapp/app/entities/trial-import/trial-import.model';

describe('Component Tests', () => {

    describe('TrialImport Management Detail Component', () => {
        let comp: TrialImportDetailComponent;
        let fixture: ComponentFixture<TrialImportDetailComponent>;
        let service: TrialImportService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [TrialImportDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Router,
                        useValue: null
                    },
                    TrialImportService,
                    JhiEventManager
                ]
            })
            .overrideTemplate(TrialImportDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TrialImportDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TrialImportService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                let testTrialImport = new TrialImport('aaa');
                testTrialImport.status = TrialImportStatus.COMPLETE;

                spyOn(service, 'find').and.returnValue(Observable.of(testTrialImport));
//=======
//
//                spyOn(service, 'find').and.returnValue(Observable.of(new TrialImport('123')));
//>>>>>>> jhipster_upgrade

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.trialImport).toEqual(jasmine.objectContaining({id: 'aaa'}));
//=======
//                expect(service.find).toHaveBeenCalledWith('123');
//                expect(comp.trialImport).toEqual(jasmine.objectContaining({id: '123'}));
//>>>>>>> jhipster_upgrade
            });
        });
    });

});
