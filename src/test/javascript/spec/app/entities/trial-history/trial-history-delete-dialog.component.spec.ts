/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TrialslinkTestModule } from '../../../test.module';
import { TrialHistoryDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/trial-history/trial-history-delete-dialog.component';
import { TrialHistoryService } from '../../../../../../main/webapp/app/entities/trial-history/trial-history.service';

describe('Component Tests', () => {

    describe('TrialHistory Management Delete Component', () => {
        let comp: TrialHistoryDeleteDialogComponent;
        let fixture: ComponentFixture<TrialHistoryDeleteDialogComponent>;
        let service: TrialHistoryService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [TrialHistoryDeleteDialogComponent],
                providers: [
                    TrialHistoryService
                ]
            })
            .overrideTemplate(TrialHistoryDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TrialHistoryDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TrialHistoryService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete('123');
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith('123');
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
