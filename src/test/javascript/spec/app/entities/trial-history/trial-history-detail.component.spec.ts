/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { TrialslinkTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TrialHistoryDetailComponent } from '../../../../../../main/webapp/app/entities/trial-history/trial-history-detail.component';
import { TrialHistoryService } from '../../../../../../main/webapp/app/entities/trial-history/trial-history.service';
import { TrialHistory } from '../../../../../../main/webapp/app/entities/trial-history/trial-history.model';

describe('Component Tests', () => {

    describe('TrialHistory Management Detail Component', () => {
        let comp: TrialHistoryDetailComponent;
        let fixture: ComponentFixture<TrialHistoryDetailComponent>;
        let service: TrialHistoryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [TrialHistoryDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TrialHistoryService,
                    JhiEventManager
                ]
            })
            .overrideTemplate(TrialHistoryDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TrialHistoryDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TrialHistoryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new TrialHistory('123')));

                // WHEN
                comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.trialHistory).toEqual(jasmine.objectContaining({id: 'aaa'}));
//=======
//                // THEN
//                expect(service.find).toHaveBeenCalledWith('123');
//                expect(comp.trialHistory).toEqual(jasmine.objectContaining({id: '123'}));
//>>>>>>> jhipster_upgrade
            });
        });
    });

});
