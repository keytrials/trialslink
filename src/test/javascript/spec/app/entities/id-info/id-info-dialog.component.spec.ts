/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TrialslinkTestModule } from '../../../test.module';
import { IdInfoDialogComponent } from '../../../../../../main/webapp/app/entities/id-info/id-info-dialog.component';
import { IdInfoService } from '../../../../../../main/webapp/app/entities/id-info/id-info.service';
import { IdInfo } from '../../../../../../main/webapp/app/entities/id-info/id-info.model';

describe('Component Tests', () => {

    describe('IdInfo Management Dialog Component', () => {
        let comp: IdInfoDialogComponent;
        let fixture: ComponentFixture<IdInfoDialogComponent>;
        let service: IdInfoService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [IdInfoDialogComponent],
                providers: [
                    IdInfoService
                ]
            })
            .overrideTemplate(IdInfoDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(IdInfoDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(IdInfoService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new IdInfo('123');
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.idInfo = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'idInfoListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new IdInfo();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.idInfo = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'idInfoListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
