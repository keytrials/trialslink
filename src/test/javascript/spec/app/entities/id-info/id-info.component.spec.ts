/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { TrialslinkTestModule } from '../../../test.module';
import { IdInfoComponent } from '../../../../../../main/webapp/app/entities/id-info/id-info.component';
import { IdInfoService } from '../../../../../../main/webapp/app/entities/id-info/id-info.service';
import { IdInfo } from '../../../../../../main/webapp/app/entities/id-info/id-info.model';

describe('Component Tests', () => {

    describe('IdInfo Management Component', () => {
        let comp: IdInfoComponent;
        let fixture: ComponentFixture<IdInfoComponent>;
        let service: IdInfoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [IdInfoComponent],
                providers: [
                    IdInfoService
                ]
            })
            .overrideTemplate(IdInfoComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(IdInfoComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(IdInfoService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new IdInfo('123')],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.idInfos[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
