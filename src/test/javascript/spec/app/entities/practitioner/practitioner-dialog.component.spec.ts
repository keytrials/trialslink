/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TrialslinkTestModule } from '../../../test.module';
import { PractitionerDialogComponent } from '../../../../../../main/webapp/app/entities/practitioner/practitioner-dialog.component';
import { PractitionerService } from '../../../../../../main/webapp/app/entities/practitioner/practitioner.service';
import { Practitioner } from '../../../../../../main/webapp/app/entities/practitioner/practitioner.model';

describe('Component Tests', () => {

    describe('Practitioner Management Dialog Component', () => {
        let comp: PractitionerDialogComponent;
        let fixture: ComponentFixture<PractitionerDialogComponent>;
        let service: PractitionerService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [PractitionerDialogComponent],
                providers: [
                    PractitionerService
                ]
            })
            .overrideTemplate(PractitionerDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PractitionerDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PractitionerService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Practitioner('123');
                        spyOn(service, 'update').and.returnValue(Observable.of(entity));
                        comp.practitioner = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'practitionerListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Practitioner();
                        spyOn(service, 'create').and.returnValue(Observable.of(entity));
                        comp.practitioner = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'practitionerListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
