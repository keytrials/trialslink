/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { Headers } from '@angular/http';

import { TrialslinkTestModule } from '../../../test.module';
import { PractitionerComponent } from '../../../../../../main/webapp/app/entities/practitioner/practitioner.component';
import { PractitionerService } from '../../../../../../main/webapp/app/entities/practitioner/practitioner.service';
import { Practitioner } from '../../../../../../main/webapp/app/entities/practitioner/practitioner.model';

describe('Component Tests', () => {

    describe('Practitioner Management Component', () => {
        let comp: PractitionerComponent;
        let fixture: ComponentFixture<PractitionerComponent>;
        let service: PractitionerService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [PractitionerComponent],
                providers: [
                    PractitionerService
                ]
            })
            .overrideTemplate(PractitionerComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PractitionerComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PractitionerService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new Headers();
                headers.append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of({
                    json: [new Practitioner('123')],
                    headers
                }));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.practitioners[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
