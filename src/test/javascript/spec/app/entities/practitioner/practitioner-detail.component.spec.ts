/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';

import { TrialslinkTestModule } from '../../../test.module';
import { PractitionerDetailComponent } from '../../../../../../main/webapp/app/entities/practitioner/practitioner-detail.component';
import { PractitionerService } from '../../../../../../main/webapp/app/entities/practitioner/practitioner.service';
import { Practitioner } from '../../../../../../main/webapp/app/entities/practitioner/practitioner.model';

describe('Component Tests', () => {

    describe('Practitioner Management Detail Component', () => {
        let comp: PractitionerDetailComponent;
        let fixture: ComponentFixture<PractitionerDetailComponent>;
        let service: PractitionerService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TrialslinkTestModule],
                declarations: [PractitionerDetailComponent],
                providers: [
                    PractitionerService
                ]
            })
            .overrideTemplate(PractitionerDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PractitionerDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PractitionerService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new Practitioner('123')));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.practitioner).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
