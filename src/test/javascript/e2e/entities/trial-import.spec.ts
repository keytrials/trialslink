import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

export class TrialImportComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-trial-import div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TrialImportDialogPage {
    modalTitle = element(by.css('h4#myTrialImportLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    sourceSelect = element(by.css('select#field_source'));
    sourceIdentifierInput = element(by.css('input#field_sourceIdentifier'));
    statusSelect = element(by.css('select#field_status'));
    filePathInput = element(by.css('input#field_filePath'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setSourceSelect = function(source) {
        this.sourceSelect.sendKeys(source);
    }

    getSourceSelect = function() {
        return this.sourceSelect.element(by.css('option:checked')).getText();
    }

    sourceSelectLastOption = function() {
        this.sourceSelect.all(by.tagName('option')).last().click();
    }
    setSourceIdentifierInput = function(sourceIdentifier) {
        this.sourceIdentifierInput.sendKeys(sourceIdentifier);
    }

    getSourceIdentifierInput = function() {
        return this.sourceIdentifierInput.getAttribute('value');
    }

    setStatusSelect = function(status) {
        this.statusSelect.sendKeys(status);
    }

    getStatusSelect = function() {
        return this.statusSelect.element(by.css('option:checked')).getText();
    }

    statusSelectLastOption = function() {
        this.statusSelect.all(by.tagName('option')).last().click();
    }
    setFilePathInput = function(filePath) {
        this.filePathInput.sendKeys(filePath);
    }

    getFilePathInput = function() {
        return this.filePathInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}

describe('TrialImport e2e test', () => {

    let navBarPage: NavBarPage;
    let trialImportDialogPage: TrialImportDialogPage;
    let trialImportComponentsPage: TrialImportComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load TrialImports', () => {
        navBarPage.goToEntity('trial-import');
        trialImportComponentsPage = new TrialImportComponentsPage();
        expect(trialImportComponentsPage.getTitle()).toMatch(/trialslinkApp.trialImport.home.title/);

    });

    it('should load create TrialImport dialog', () => {
//<<<<<<< HEAD
//        element(by.css('button.create-trial-import')).click().then(() => {
//            const expectVal = /trialslinkApp.trialImport.home.createOrEditLabel/;
//            element.all(by.css('h4.modal-title')).first().getAttribute('jhiTranslate').then((value) => {
//                expect(value).toMatch(expectVal);
//            });
//=======
        trialImportComponentsPage.clickOnCreateButton();
        trialImportDialogPage = new TrialImportDialogPage();
        expect(trialImportDialogPage.getModalTitle()).toMatch(/trialslinkApp.trialImport.home.createOrEditLabel/);
        trialImportDialogPage.close();
    });
//>>>>>>> jhipster_upgrade

    it('should create and save TrialImports', () => {
        trialImportComponentsPage.clickOnCreateButton();
        trialImportDialogPage.sourceSelectLastOption();
        trialImportDialogPage.setSourceIdentifierInput('sourceIdentifier');
        expect(trialImportDialogPage.getSourceIdentifierInput()).toMatch('sourceIdentifier');
        trialImportDialogPage.statusSelectLastOption();
        trialImportDialogPage.setFilePathInput('filePath');
        expect(trialImportDialogPage.getFilePathInput()).toMatch('filePath');
        trialImportDialogPage.save();
        expect(trialImportDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
//<<<<<<< HEAD
//        accountMenu.click();
//        logout.click();
//=======
        navBarPage.autoSignOut();
//>>>>>>> jhipster_upgrade
    });
});
