import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Characteristic e2e test', () => {

    let navBarPage: NavBarPage;
    let characteristicDialogPage: CharacteristicDialogPage;
    let characteristicComponentsPage: CharacteristicComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Characteristics', () => {
        navBarPage.goToEntity('characteristic');
        characteristicComponentsPage = new CharacteristicComponentsPage();
        expect(characteristicComponentsPage.getTitle()).toMatch(/trialslinkApp.characteristic.home.title/);

    });

    it('should load create Characteristic dialog', () => {
        characteristicComponentsPage.clickOnCreateButton();
        characteristicDialogPage = new CharacteristicDialogPage();
        expect(characteristicDialogPage.getModalTitle()).toMatch(/trialslinkApp.characteristic.home.createOrEditLabel/);
        characteristicDialogPage.close();
    });

    it('should create and save Characteristics', () => {
        characteristicComponentsPage.clickOnCreateButton();
        characteristicDialogPage.setCriteriaInput('criteria');
        expect(characteristicDialogPage.getCriteriaInput()).toMatch('criteria');
        characteristicDialogPage.setCodeInput('code');
        expect(characteristicDialogPage.getCodeInput()).toMatch('code');
        characteristicDialogPage.sexSelectLastOption();
        characteristicDialogPage.getBasedOnGenderInput().isSelected().then((selected) => {
            if (selected) {
                characteristicDialogPage.getBasedOnGenderInput().click();
                expect(characteristicDialogPage.getBasedOnGenderInput().isSelected()).toBeFalsy();
            } else {
                characteristicDialogPage.getBasedOnGenderInput().click();
                expect(characteristicDialogPage.getBasedOnGenderInput().isSelected()).toBeTruthy();
            }
        });
        characteristicDialogPage.getAcceptHealthyVolunteersInput().isSelected().then((selected) => {
            if (selected) {
                characteristicDialogPage.getAcceptHealthyVolunteersInput().click();
                expect(characteristicDialogPage.getAcceptHealthyVolunteersInput().isSelected()).toBeFalsy();
            } else {
                characteristicDialogPage.getAcceptHealthyVolunteersInput().click();
                expect(characteristicDialogPage.getAcceptHealthyVolunteersInput().isSelected()).toBeTruthy();
            }
        });
        characteristicDialogPage.save();
        expect(characteristicDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class CharacteristicComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-characteristic div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class CharacteristicDialogPage {
    modalTitle = element(by.css('h4#myCharacteristicLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    criteriaInput = element(by.css('input#field_criteria'));
    codeInput = element(by.css('input#field_code'));
    sexSelect = element(by.css('select#field_sex'));
    basedOnGenderInput = element(by.css('input#field_basedOnGender'));
    acceptHealthyVolunteersInput = element(by.css('input#field_acceptHealthyVolunteers'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setCriteriaInput = function(criteria) {
        this.criteriaInput.sendKeys(criteria);
    }

    getCriteriaInput = function() {
        return this.criteriaInput.getAttribute('value');
    }

    setCodeInput = function(code) {
        this.codeInput.sendKeys(code);
    }

    getCodeInput = function() {
        return this.codeInput.getAttribute('value');
    }

    setSexSelect = function(sex) {
        this.sexSelect.sendKeys(sex);
    }

    getSexSelect = function() {
        return this.sexSelect.element(by.css('option:checked')).getText();
    }

    sexSelectLastOption = function() {
        this.sexSelect.all(by.tagName('option')).last().click();
    }
    getBasedOnGenderInput = function() {
        return this.basedOnGenderInput;
    }
    getAcceptHealthyVolunteersInput = function() {
        return this.acceptHealthyVolunteersInput;
    }
    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
