import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Practitioner e2e test', () => {

    let navBarPage: NavBarPage;
    let practitionerDialogPage: PractitionerDialogPage;
    let practitionerComponentsPage: PractitionerComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Practitioners', () => {
        navBarPage.goToEntity('practitioner');
        practitionerComponentsPage = new PractitionerComponentsPage();
        expect(practitionerComponentsPage.getTitle()).toMatch(/trialslinkApp.practitioner.home.title/);

    });

    it('should load create Practitioner dialog', () => {
        practitionerComponentsPage.clickOnCreateButton();
        practitionerDialogPage = new PractitionerDialogPage();
        expect(practitionerDialogPage.getModalTitle()).toMatch(/trialslinkApp.practitioner.home.createOrEditLabel/);
        practitionerDialogPage.close();
    });

    it('should create and save Practitioners', () => {
        practitionerComponentsPage.clickOnCreateButton();
        practitionerDialogPage.setNameInput('name');
        expect(practitionerDialogPage.getNameInput()).toMatch('name');
        practitionerDialogPage.genderSelectLastOption();
        practitionerDialogPage.save();
        expect(practitionerDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class PractitionerComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-practitioner div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class PractitionerDialogPage {
    modalTitle = element(by.css('h4#myPractitionerLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    genderSelect = element(by.css('select#field_gender'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    }

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    }

    setGenderSelect = function(gender) {
        this.genderSelect.sendKeys(gender);
    }

    getGenderSelect = function() {
        return this.genderSelect.element(by.css('option:checked')).getText();
    }

    genderSelectLastOption = function() {
        this.genderSelect.all(by.tagName('option')).last().click();
    }
    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
