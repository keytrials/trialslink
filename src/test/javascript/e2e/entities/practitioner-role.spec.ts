import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('PractitionerRole e2e test', () => {

    let navBarPage: NavBarPage;
    let practitionerRoleDialogPage: PractitionerRoleDialogPage;
    let practitionerRoleComponentsPage: PractitionerRoleComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load PractitionerRoles', () => {
        navBarPage.goToEntity('practitioner-role');
        practitionerRoleComponentsPage = new PractitionerRoleComponentsPage();
        expect(practitionerRoleComponentsPage.getTitle()).toMatch(/trialslinkApp.practitionerRole.home.title/);

    });

    it('should load create PractitionerRole dialog', () => {
        practitionerRoleComponentsPage.clickOnCreateButton();
        practitionerRoleDialogPage = new PractitionerRoleDialogPage();
        expect(practitionerRoleDialogPage.getModalTitle()).toMatch(/trialslinkApp.practitionerRole.home.createOrEditLabel/);
        practitionerRoleDialogPage.close();
    });

    it('should create and save PractitionerRoles', () => {
        practitionerRoleComponentsPage.clickOnCreateButton();
        practitionerRoleDialogPage.practitionerSelectLastOption();
        practitionerRoleDialogPage.roleSelectLastOption();
        practitionerRoleDialogPage.centreSelectLastOption();
        practitionerRoleDialogPage.save();
        expect(practitionerRoleDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class PractitionerRoleComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-practitioner-role div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class PractitionerRoleDialogPage {
    modalTitle = element(by.css('h4#myPractitionerRoleLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    practitionerSelect = element(by.css('select#field_practitioner'));
    roleSelect = element(by.css('select#field_role'));
    centreSelect = element(by.css('select#field_centre'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    practitionerSelectLastOption = function() {
        this.practitionerSelect.all(by.tagName('option')).last().click();
    }

    practitionerSelectOption = function(option) {
        this.practitionerSelect.sendKeys(option);
    }

    getPractitionerSelect = function() {
        return this.practitionerSelect;
    }

    getPractitionerSelectedOption = function() {
        return this.practitionerSelect.element(by.css('option:checked')).getText();
    }

    roleSelectLastOption = function() {
        this.roleSelect.all(by.tagName('option')).last().click();
    }

    roleSelectOption = function(option) {
        this.roleSelect.sendKeys(option);
    }

    getRoleSelect = function() {
        return this.roleSelect;
    }

    getRoleSelectedOption = function() {
        return this.roleSelect.element(by.css('option:checked')).getText();
    }

    centreSelectLastOption = function() {
        this.centreSelect.all(by.tagName('option')).last().click();
    }

    centreSelectOption = function(option) {
        this.centreSelect.sendKeys(option);
    }

    getCentreSelect = function() {
        return this.centreSelect;
    }

    getCentreSelectedOption = function() {
        return this.centreSelect.element(by.css('option:checked')).getText();
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
