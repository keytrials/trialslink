import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('SecondaryId e2e test', () => {

    let navBarPage: NavBarPage;
    let secondaryIdDialogPage: SecondaryIdDialogPage;
    let secondaryIdComponentsPage: SecondaryIdComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load SecondaryIds', () => {
        navBarPage.goToEntity('secondary-id');
        secondaryIdComponentsPage = new SecondaryIdComponentsPage();
        expect(secondaryIdComponentsPage.getTitle()).toMatch(/trialslinkApp.secondaryId.home.title/);

    });

    it('should load create SecondaryId dialog', () => {
        secondaryIdComponentsPage.clickOnCreateButton();
        secondaryIdDialogPage = new SecondaryIdDialogPage();
        expect(secondaryIdDialogPage.getModalTitle()).toMatch(/trialslinkApp.secondaryId.home.createOrEditLabel/);
        secondaryIdDialogPage.close();
    });

    it('should create and save SecondaryIds', () => {
        secondaryIdComponentsPage.clickOnCreateButton();
        secondaryIdDialogPage.setTypeInput('type');
        expect(secondaryIdDialogPage.getTypeInput()).toMatch('type');
        secondaryIdDialogPage.setValueInput('value');
        expect(secondaryIdDialogPage.getValueInput()).toMatch('value');
        secondaryIdDialogPage.save();
        expect(secondaryIdDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class SecondaryIdComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-secondary-id div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class SecondaryIdDialogPage {
    modalTitle = element(by.css('h4#mySecondaryIdLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    typeInput = element(by.css('input#field_type'));
    valueInput = element(by.css('input#field_value'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setTypeInput = function(type) {
        this.typeInput.sendKeys(type);
    }

    getTypeInput = function() {
        return this.typeInput.getAttribute('value');
    }

    setValueInput = function(value) {
        this.valueInput.sendKeys(value);
    }

    getValueInput = function() {
        return this.valueInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
