import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';


export class TrialHistoryComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-trial-history div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TrialHistoryDialogPage {
    modalTitle = element(by.css('h4#myTrialHistoryLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}

describe('TrialHistory e2e test', () => {

    let navBarPage: NavBarPage;
    let trialHistoryDialogPage: TrialHistoryDialogPage;
    let trialHistoryComponentsPage: TrialHistoryComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load TrialHistories', () => {
        navBarPage.goToEntity('trial-history');
        trialHistoryComponentsPage = new TrialHistoryComponentsPage();
        expect(trialHistoryComponentsPage.getTitle()).toMatch(/trialslinkApp.trialHistory.home.title/);

    });

    it('should load create TrialHistory dialog', () => {
//<<<<<<< HEAD
//        element(by.css('button.create-trial-history')).click().then(() => {
//            const expectVal = /trialslinkApp.trialHistory.home.createOrEditLabel/;
//            element.all(by.css('h4.modal-title')).first().getAttribute('jhiTranslate').then((value) => {
//                expect(value).toMatch(expectVal);
//            });
//=======
        trialHistoryComponentsPage.clickOnCreateButton();
        trialHistoryDialogPage = new TrialHistoryDialogPage();
        expect(trialHistoryDialogPage.getModalTitle()).toMatch(/trialslinkApp.trialHistory.home.createOrEditLabel/);
        trialHistoryDialogPage.close();
    });
//>>>>>>> jhipster_upgrade

    it('should create and save TrialHistories', () => {
        trialHistoryComponentsPage.clickOnCreateButton();
        trialHistoryDialogPage.save();
        expect(trialHistoryDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
//<<<<<<< HEAD
//        accountMenu.click();
//        logout.click();
//=======
        navBarPage.autoSignOut();
//>>>>>>> jhipster_upgrade
    });
});
