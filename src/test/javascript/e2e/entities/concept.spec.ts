import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Concept e2e test', () => {

    let navBarPage: NavBarPage;
    let conceptDialogPage: ConceptDialogPage;
    let conceptComponentsPage: ConceptComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Concepts', () => {
        navBarPage.goToEntity('concept');
        conceptComponentsPage = new ConceptComponentsPage();
        expect(conceptComponentsPage.getTitle()).toMatch(/trialslinkApp.concept.home.title/);

    });

    it('should load create Concept dialog', () => {
        conceptComponentsPage.clickOnCreateButton();
        conceptDialogPage = new ConceptDialogPage();
        expect(conceptDialogPage.getModalTitle()).toMatch(/trialslinkApp.concept.home.createOrEditLabel/);
        conceptDialogPage.close();
    });

    it('should create and save Concepts', () => {
        conceptComponentsPage.clickOnCreateButton();
        conceptDialogPage.setLabelInput('label');
        expect(conceptDialogPage.getLabelInput()).toMatch('label');
        conceptDialogPage.setSystemInput('system');
        expect(conceptDialogPage.getSystemInput()).toMatch('system');
        conceptDialogPage.setCodeInput('code');
        expect(conceptDialogPage.getCodeInput()).toMatch('code');
        conceptDialogPage.setTypeInput('type');
        expect(conceptDialogPage.getTypeInput()).toMatch('type');
        conceptDialogPage.save();
        expect(conceptDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ConceptComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-concept div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ConceptDialogPage {
    modalTitle = element(by.css('h4#myConceptLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    labelInput = element(by.css('input#field_label'));
    systemInput = element(by.css('input#field_system'));
    codeInput = element(by.css('input#field_code'));
    typeInput = element(by.css('input#field_type'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setLabelInput = function(label) {
        this.labelInput.sendKeys(label);
    }

    getLabelInput = function() {
        return this.labelInput.getAttribute('value');
    }

    setSystemInput = function(system) {
        this.systemInput.sendKeys(system);
    }

    getSystemInput = function() {
        return this.systemInput.getAttribute('value');
    }

    setCodeInput = function(code) {
        this.codeInput.sendKeys(code);
    }

    getCodeInput = function() {
        return this.codeInput.getAttribute('value');
    }

    setTypeInput = function(type) {
        this.typeInput.sendKeys(type);
    }

    getTypeInput = function() {
        return this.typeInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
