import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Contact e2e test', () => {

    let navBarPage: NavBarPage;
    let contactDialogPage: ContactDialogPage;
    let contactComponentsPage: ContactComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Contacts', () => {
        navBarPage.goToEntity('contact');
        contactComponentsPage = new ContactComponentsPage();
        expect(contactComponentsPage.getTitle()).toMatch(/trialslinkApp.contact.home.title/);

    });

    it('should load create Contact dialog', () => {
        contactComponentsPage.clickOnCreateButton();
        contactDialogPage = new ContactDialogPage();
        expect(contactDialogPage.getModalTitle()).toMatch(/trialslinkApp.contact.home.createOrEditLabel/);
        contactDialogPage.close();
    });

    it('should create and save Contacts', () => {
        contactComponentsPage.clickOnCreateButton();
        contactDialogPage.setPhoneInput('5');
        expect(contactDialogPage.getPhoneInput()).toMatch('5');
        contactDialogPage.setPersonNameInput('personName');
        expect(contactDialogPage.getPersonNameInput()).toMatch('personName');
        contactDialogPage.setEmailInput('email');
        expect(contactDialogPage.getEmailInput()).toMatch('email');
        contactDialogPage.setPurposeInput('purpose');
        expect(contactDialogPage.getPurposeInput()).toMatch('purpose');
        contactDialogPage.save();
        expect(contactDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ContactComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-contact div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ContactDialogPage {
    modalTitle = element(by.css('h4#myContactLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    phoneInput = element(by.css('input#field_phone'));
    personNameInput = element(by.css('input#field_personName'));
    emailInput = element(by.css('input#field_email'));
    purposeInput = element(by.css('input#field_purpose'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setPhoneInput = function(phone) {
        this.phoneInput.sendKeys(phone);
    }

    getPhoneInput = function() {
        return this.phoneInput.getAttribute('value');
    }

    setPersonNameInput = function(personName) {
        this.personNameInput.sendKeys(personName);
    }

    getPersonNameInput = function() {
        return this.personNameInput.getAttribute('value');
    }

    setEmailInput = function(email) {
        this.emailInput.sendKeys(email);
    }

    getEmailInput = function() {
        return this.emailInput.getAttribute('value');
    }

    setPurposeInput = function(purpose) {
        this.purposeInput.sendKeys(purpose);
    }

    getPurposeInput = function() {
        return this.purposeInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
