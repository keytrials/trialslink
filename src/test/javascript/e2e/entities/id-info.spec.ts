import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('IdInfo e2e test', () => {

    let navBarPage: NavBarPage;
    let idInfoDialogPage: IdInfoDialogPage;
    let idInfoComponentsPage: IdInfoComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load IdInfos', () => {
        navBarPage.goToEntity('id-info');
        idInfoComponentsPage = new IdInfoComponentsPage();
        expect(idInfoComponentsPage.getTitle()).toMatch(/trialslinkApp.idInfo.home.title/);

    });

    it('should load create IdInfo dialog', () => {
        idInfoComponentsPage.clickOnCreateButton();
        idInfoDialogPage = new IdInfoDialogPage();
        expect(idInfoDialogPage.getModalTitle()).toMatch(/trialslinkApp.idInfo.home.createOrEditLabel/);
        idInfoDialogPage.close();
    });

    it('should create and save IdInfos', () => {
        idInfoComponentsPage.clickOnCreateButton();
        idInfoDialogPage.setProtocolIdInput('protocolId');
        expect(idInfoDialogPage.getProtocolIdInput()).toMatch('protocolId');
        idInfoDialogPage.setNctIdInput('nctId');
        expect(idInfoDialogPage.getNctIdInput()).toMatch('nctId');
        idInfoDialogPage.save();
        expect(idInfoDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class IdInfoComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-id-info div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class IdInfoDialogPage {
    modalTitle = element(by.css('h4#myIdInfoLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    protocolIdInput = element(by.css('input#field_protocolId'));
    nctIdInput = element(by.css('input#field_nctId'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setProtocolIdInput = function(protocolId) {
        this.protocolIdInput.sendKeys(protocolId);
    }

    getProtocolIdInput = function() {
        return this.protocolIdInput.getAttribute('value');
    }

    setNctIdInput = function(nctId) {
        this.nctIdInput.sendKeys(nctId);
    }

    getNctIdInput = function() {
        return this.nctIdInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
