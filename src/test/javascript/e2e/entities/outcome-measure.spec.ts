import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('OutcomeMeasure e2e test', () => {

    let navBarPage: NavBarPage;
    let outcomeMeasureDialogPage: OutcomeMeasureDialogPage;
    let outcomeMeasureComponentsPage: OutcomeMeasureComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load OutcomeMeasures', () => {
        navBarPage.goToEntity('outcome-measure');
        outcomeMeasureComponentsPage = new OutcomeMeasureComponentsPage();
        expect(outcomeMeasureComponentsPage.getTitle()).toMatch(/trialslinkApp.outcomeMeasure.home.title/);

    });

    it('should load create OutcomeMeasure dialog', () => {
        outcomeMeasureComponentsPage.clickOnCreateButton();
        outcomeMeasureDialogPage = new OutcomeMeasureDialogPage();
        expect(outcomeMeasureDialogPage.getModalTitle()).toMatch(/trialslinkApp.outcomeMeasure.home.createOrEditLabel/);
        outcomeMeasureDialogPage.close();
    });

    it('should create and save OutcomeMeasures', () => {
        outcomeMeasureComponentsPage.clickOnCreateButton();
        outcomeMeasureDialogPage.setTitleInput('title');
        expect(outcomeMeasureDialogPage.getTitleInput()).toMatch('title');
        outcomeMeasureDialogPage.setDescriptionInput('description');
        expect(outcomeMeasureDialogPage.getDescriptionInput()).toMatch('description');
        outcomeMeasureDialogPage.setTimeFrameInput('timeFrame');
        expect(outcomeMeasureDialogPage.getTimeFrameInput()).toMatch('timeFrame');
        outcomeMeasureDialogPage.save();
        expect(outcomeMeasureDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class OutcomeMeasureComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-outcome-measure div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class OutcomeMeasureDialogPage {
    modalTitle = element(by.css('h4#myOutcomeMeasureLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    titleInput = element(by.css('input#field_title'));
    descriptionInput = element(by.css('input#field_description'));
    timeFrameInput = element(by.css('input#field_timeFrame'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setTitleInput = function(title) {
        this.titleInput.sendKeys(title);
    }

    getTitleInput = function() {
        return this.titleInput.getAttribute('value');
    }

    setDescriptionInput = function(description) {
        this.descriptionInput.sendKeys(description);
    }

    getDescriptionInput = function() {
        return this.descriptionInput.getAttribute('value');
    }

    setTimeFrameInput = function(timeFrame) {
        this.timeFrameInput.sendKeys(timeFrame);
    }

    getTimeFrameInput = function() {
        return this.timeFrameInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
