import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Trial e2e test', () => {

    let navBarPage: NavBarPage;
    let trialDialogPage: TrialDialogPage;
    let trialComponentsPage: TrialComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Trials', () => {
        navBarPage.goToEntity('trial');
        trialComponentsPage = new TrialComponentsPage();
        expect(trialComponentsPage.getTitle()).toMatch(/trialslinkApp.trial.home.title/);

    });

    it('should load create Trial dialog', () => {
        trialComponentsPage.clickOnCreateButton();
        trialDialogPage = new TrialDialogPage();
        expect(trialDialogPage.getModalTitle()).toMatch(/trialslinkApp.trial.home.createOrEditLabel/);
        trialDialogPage.close();
    });

    it('should create and save Trials', () => {
        trialComponentsPage.clickOnCreateButton();
        trialDialogPage.setTitleInput('title');
        expect(trialDialogPage.getTitleInput()).toMatch('title');
        trialDialogPage.setBriefTitleInput('briefTitle');
        expect(trialDialogPage.getBriefTitleInput()).toMatch('briefTitle');
        trialDialogPage.statusSelectLastOption();
        trialDialogPage.typeSelectLastOption();
        trialDialogPage.setPhaseInput('phase');
        expect(trialDialogPage.getPhaseInput()).toMatch('phase');
        trialDialogPage.getHasExpandedAccessInput().isSelected().then((selected) => {
            if (selected) {
                trialDialogPage.getHasExpandedAccessInput().click();
                expect(trialDialogPage.getHasExpandedAccessInput().isSelected()).toBeFalsy();
            } else {
                trialDialogPage.getHasExpandedAccessInput().click();
                expect(trialDialogPage.getHasExpandedAccessInput().isSelected()).toBeTruthy();
            }
        });
        trialDialogPage.setCategoryInput('category');
        expect(trialDialogPage.getCategoryInput()).toMatch('category');
        trialDialogPage.setFocusInput('focus');
        expect(trialDialogPage.getFocusInput()).toMatch('focus');
        trialDialogPage.setKeywordInput('keyword');
        expect(trialDialogPage.getKeywordInput()).toMatch('keyword');
        trialDialogPage.setDescriptionInput('description');
        expect(trialDialogPage.getDescriptionInput()).toMatch('description');
        trialDialogPage.setRecordVerificationDateInput(12310020012301);
        expect(trialDialogPage.getRecordVerificationDateInput()).toMatch('2001-12-31T02:30');
        trialDialogPage.setStartDateInput(12310020012301);
        expect(trialDialogPage.getStartDateInput()).toMatch('2001-12-31T02:30');
        trialDialogPage.setPrimaryCompletionDateInput(12310020012301);
        expect(trialDialogPage.getPrimaryCompletionDateInput()).toMatch('2001-12-31T02:30');
        trialDialogPage.setStudyCompletionDateInput(12310020012301);
        expect(trialDialogPage.getStudyCompletionDateInput()).toMatch('2001-12-31T02:30');
        trialDialogPage.setReasonStoppedInput('reasonStopped');
        expect(trialDialogPage.getReasonStoppedInput()).toMatch('reasonStopped');
        trialDialogPage.setNoteInput('note');
        expect(trialDialogPage.getNoteInput()).toMatch('note');
        trialDialogPage.setBriefSummaryInput('briefSummary');
        expect(trialDialogPage.getBriefSummaryInput()).toMatch('briefSummary');
        trialDialogPage.setDetailedDescriptionInput('detailedDescription');
        expect(trialDialogPage.getDetailedDescriptionInput()).toMatch('detailedDescription');
        trialDialogPage.save();
        expect(trialDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TrialComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-trial div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TrialDialogPage {
    modalTitle = element(by.css('h4#myTrialLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    titleInput = element(by.css('input#field_title'));
    briefTitleInput = element(by.css('input#field_briefTitle'));
    statusSelect = element(by.css('select#field_status'));
    typeSelect = element(by.css('select#field_type'));
    phaseInput = element(by.css('input#field_phase'));
    hasExpandedAccessInput = element(by.css('input#field_hasExpandedAccess'));
    categoryInput = element(by.css('input#field_category'));
    focusInput = element(by.css('input#field_focus'));
    keywordInput = element(by.css('input#field_keyword'));
    descriptionInput = element(by.css('input#field_description'));
    recordVerificationDateInput = element(by.css('input#field_recordVerificationDate'));
    startDateInput = element(by.css('input#field_startDate'));
    primaryCompletionDateInput = element(by.css('input#field_primaryCompletionDate'));
    studyCompletionDateInput = element(by.css('input#field_studyCompletionDate'));
    reasonStoppedInput = element(by.css('input#field_reasonStopped'));
    noteInput = element(by.css('input#field_note'));
    briefSummaryInput = element(by.css('input#field_briefSummary'));
    detailedDescriptionInput = element(by.css('input#field_detailedDescription'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setTitleInput = function(title) {
        this.titleInput.sendKeys(title);
    }

    getTitleInput = function() {
        return this.titleInput.getAttribute('value');
    }

    setBriefTitleInput = function(briefTitle) {
        this.briefTitleInput.sendKeys(briefTitle);
    }

    getBriefTitleInput = function() {
        return this.briefTitleInput.getAttribute('value');
    }

    setStatusSelect = function(status) {
        this.statusSelect.sendKeys(status);
    }

    getStatusSelect = function() {
        return this.statusSelect.element(by.css('option:checked')).getText();
    }

    statusSelectLastOption = function() {
        this.statusSelect.all(by.tagName('option')).last().click();
    }
    setTypeSelect = function(type) {
        this.typeSelect.sendKeys(type);
    }

    getTypeSelect = function() {
        return this.typeSelect.element(by.css('option:checked')).getText();
    }

    typeSelectLastOption = function() {
        this.typeSelect.all(by.tagName('option')).last().click();
    }
    setPhaseInput = function(phase) {
        this.phaseInput.sendKeys(phase);
    }

    getPhaseInput = function() {
        return this.phaseInput.getAttribute('value');
    }

    getHasExpandedAccessInput = function() {
        return this.hasExpandedAccessInput;
    }
    setCategoryInput = function(category) {
        this.categoryInput.sendKeys(category);
    }

    getCategoryInput = function() {
        return this.categoryInput.getAttribute('value');
    }

    setFocusInput = function(focus) {
        this.focusInput.sendKeys(focus);
    }

    getFocusInput = function() {
        return this.focusInput.getAttribute('value');
    }

    setKeywordInput = function(keyword) {
        this.keywordInput.sendKeys(keyword);
    }

    getKeywordInput = function() {
        return this.keywordInput.getAttribute('value');
    }

    setDescriptionInput = function(description) {
        this.descriptionInput.sendKeys(description);
    }

    getDescriptionInput = function() {
        return this.descriptionInput.getAttribute('value');
    }

    setRecordVerificationDateInput = function(recordVerificationDate) {
        this.recordVerificationDateInput.sendKeys(recordVerificationDate);
    }

    getRecordVerificationDateInput = function() {
        return this.recordVerificationDateInput.getAttribute('value');
    }

    setStartDateInput = function(startDate) {
        this.startDateInput.sendKeys(startDate);
    }

    getStartDateInput = function() {
        return this.startDateInput.getAttribute('value');
    }

    setPrimaryCompletionDateInput = function(primaryCompletionDate) {
        this.primaryCompletionDateInput.sendKeys(primaryCompletionDate);
    }

    getPrimaryCompletionDateInput = function() {
        return this.primaryCompletionDateInput.getAttribute('value');
    }

    setStudyCompletionDateInput = function(studyCompletionDate) {
        this.studyCompletionDateInput.sendKeys(studyCompletionDate);
    }

    getStudyCompletionDateInput = function() {
        return this.studyCompletionDateInput.getAttribute('value');
    }

    setReasonStoppedInput = function(reasonStopped) {
        this.reasonStoppedInput.sendKeys(reasonStopped);
    }

    getReasonStoppedInput = function() {
        return this.reasonStoppedInput.getAttribute('value');
    }

    setNoteInput = function(note) {
        this.noteInput.sendKeys(note);
    }

    getNoteInput = function() {
        return this.noteInput.getAttribute('value');
    }

    setBriefSummaryInput = function(briefSummary) {
        this.briefSummaryInput.sendKeys(briefSummary);
    }

    getBriefSummaryInput = function() {
        return this.briefSummaryInput.getAttribute('value');
    }

    setDetailedDescriptionInput = function(detailedDescription) {
        this.detailedDescriptionInput.sendKeys(detailedDescription);
    }

    getDetailedDescriptionInput = function() {
        return this.detailedDescriptionInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
