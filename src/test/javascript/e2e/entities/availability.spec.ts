import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Availability e2e test', () => {

    let navBarPage: NavBarPage;
    let availabilityDialogPage: AvailabilityDialogPage;
    let availabilityComponentsPage: AvailabilityComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Availabilities', () => {
        navBarPage.goToEntity('availability');
        availabilityComponentsPage = new AvailabilityComponentsPage();
        expect(availabilityComponentsPage.getTitle()).toMatch(/trialslinkApp.availability.home.title/);

    });

    it('should load create Availability dialog', () => {
        availabilityComponentsPage.clickOnCreateButton();
        availabilityDialogPage = new AvailabilityDialogPage();
        expect(availabilityDialogPage.getModalTitle()).toMatch(/trialslinkApp.availability.home.createOrEditLabel/);
        availabilityDialogPage.close();
    });

    it('should create and save Availabilities', () => {
        availabilityComponentsPage.clickOnCreateButton();
        availabilityDialogPage.getAllDayInput().isSelected().then((selected) => {
            if (selected) {
                availabilityDialogPage.getAllDayInput().click();
                expect(availabilityDialogPage.getAllDayInput().isSelected()).toBeFalsy();
            } else {
                availabilityDialogPage.getAllDayInput().click();
                expect(availabilityDialogPage.getAllDayInput().isSelected()).toBeTruthy();
            }
        });
        availabilityDialogPage.setStartTimeInput(12310020012301);
        expect(availabilityDialogPage.getStartTimeInput()).toMatch('2001-12-31T02:30');
        availabilityDialogPage.setEndTimeInput(12310020012301);
        expect(availabilityDialogPage.getEndTimeInput()).toMatch('2001-12-31T02:30');
        availabilityDialogPage.setDaysInput('days');
        expect(availabilityDialogPage.getDaysInput()).toMatch('days');
        availabilityDialogPage.save();
        expect(availabilityDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class AvailabilityComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-availability div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class AvailabilityDialogPage {
    modalTitle = element(by.css('h4#myAvailabilityLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    allDayInput = element(by.css('input#field_allDay'));
    startTimeInput = element(by.css('input#field_startTime'));
    endTimeInput = element(by.css('input#field_endTime'));
    daysInput = element(by.css('input#field_days'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    getAllDayInput = function() {
        return this.allDayInput;
    }
    setStartTimeInput = function(startTime) {
        this.startTimeInput.sendKeys(startTime);
    }

    getStartTimeInput = function() {
        return this.startTimeInput.getAttribute('value');
    }

    setEndTimeInput = function(endTime) {
        this.endTimeInput.sendKeys(endTime);
    }

    getEndTimeInput = function() {
        return this.endTimeInput.getAttribute('value');
    }

    setDaysInput = function(days) {
        this.daysInput.sendKeys(days);
    }

    getDaysInput = function() {
        return this.daysInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
