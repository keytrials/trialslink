import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('TrialCentre e2e test', () => {

    let navBarPage: NavBarPage;
    let trialCentreDialogPage: TrialCentreDialogPage;
    let trialCentreComponentsPage: TrialCentreComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load TrialCentres', () => {
        navBarPage.goToEntity('trial-centre');
        trialCentreComponentsPage = new TrialCentreComponentsPage();
        expect(trialCentreComponentsPage.getTitle()).toMatch(/trialslinkApp.trialCentre.home.title/);

    });

    it('should load create TrialCentre dialog', () => {
        trialCentreComponentsPage.clickOnCreateButton();
        trialCentreDialogPage = new TrialCentreDialogPage();
        expect(trialCentreDialogPage.getModalTitle()).toMatch(/trialslinkApp.trialCentre.home.createOrEditLabel/);
        trialCentreDialogPage.close();
    });

    it('should create and save TrialCentres', () => {
        trialCentreComponentsPage.clickOnCreateButton();
        trialCentreDialogPage.setNameInput('name');
        expect(trialCentreDialogPage.getNameInput()).toMatch('name');
        trialCentreDialogPage.getAllDayInput().isSelected().then((selected) => {
            if (selected) {
                trialCentreDialogPage.getAllDayInput().click();
                expect(trialCentreDialogPage.getAllDayInput().isSelected()).toBeFalsy();
            } else {
                trialCentreDialogPage.getAllDayInput().click();
                expect(trialCentreDialogPage.getAllDayInput().isSelected()).toBeTruthy();
            }
        });
        trialCentreDialogPage.setStartDateInput(12310020012301);
        expect(trialCentreDialogPage.getStartDateInput()).toMatch('2001-12-31T02:30');
        trialCentreDialogPage.setEndDateInput(12310020012301);
        expect(trialCentreDialogPage.getEndDateInput()).toMatch('2001-12-31T02:30');
        trialCentreDialogPage.setDaysInput('days');
        expect(trialCentreDialogPage.getDaysInput()).toMatch('days');
        trialCentreDialogPage.save();
        expect(trialCentreDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class TrialCentreComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-trial-centre div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class TrialCentreDialogPage {
    modalTitle = element(by.css('h4#myTrialCentreLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    allDayInput = element(by.css('input#field_allDay'));
    startDateInput = element(by.css('input#field_startDate'));
    endDateInput = element(by.css('input#field_endDate'));
    daysInput = element(by.css('input#field_days'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function(name) {
        this.nameInput.sendKeys(name);
    }

    getNameInput = function() {
        return this.nameInput.getAttribute('value');
    }

    getAllDayInput = function() {
        return this.allDayInput;
    }
    setStartDateInput = function(startDate) {
        this.startDateInput.sendKeys(startDate);
    }

    getStartDateInput = function() {
        return this.startDateInput.getAttribute('value');
    }

    setEndDateInput = function(endDate) {
        this.endDateInput.sendKeys(endDate);
    }

    getEndDateInput = function() {
        return this.endDateInput.getAttribute('value');
    }

    setDaysInput = function(days) {
        this.daysInput.sendKeys(days);
    }

    getDaysInput = function() {
        return this.daysInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
