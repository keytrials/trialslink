import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Criteria e2e test', () => {

    let navBarPage: NavBarPage;
    let criteriaDialogPage: CriteriaDialogPage;
    let criteriaComponentsPage: CriteriaComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Criteria', () => {
        navBarPage.goToEntity('criteria');
        criteriaComponentsPage = new CriteriaComponentsPage();
        expect(criteriaComponentsPage.getTitle()).toMatch(/trialslinkApp.criteria.home.title/);

    });

    it('should load create Criteria dialog', () => {
        criteriaComponentsPage.clickOnCreateButton();
        criteriaDialogPage = new CriteriaDialogPage();
        expect(criteriaDialogPage.getModalTitle()).toMatch(/trialslinkApp.criteria.home.createOrEditLabel/);
        criteriaDialogPage.close();
    });

    it('should create and save Criteria', () => {
        criteriaComponentsPage.clickOnCreateButton();
        criteriaDialogPage.typeSelectLastOption();
        criteriaDialogPage.save();
        expect(criteriaDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class CriteriaComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-criteria div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class CriteriaDialogPage {
    modalTitle = element(by.css('h4#myCriteriaLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    typeSelect = element(by.css('select#field_type'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setTypeSelect = function(type) {
        this.typeSelect.sendKeys(type);
    }

    getTypeSelect = function() {
        return this.typeSelect.element(by.css('option:checked')).getText();
    }

    typeSelectLastOption = function() {
        this.typeSelect.all(by.tagName('option')).last().click();
    }
    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
