import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Condition e2e test', () => {

    let navBarPage: NavBarPage;
    let conditionDialogPage: ConditionDialogPage;
    let conditionComponentsPage: ConditionComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Conditions', () => {
        navBarPage.goToEntity('condition');
        conditionComponentsPage = new ConditionComponentsPage();
        expect(conditionComponentsPage.getTitle()).toMatch(/trialslinkApp.condition.home.title/);

    });

    it('should load create Condition dialog', () => {
        conditionComponentsPage.clickOnCreateButton();
        conditionDialogPage = new ConditionDialogPage();
        expect(conditionDialogPage.getModalTitle()).toMatch(/trialslinkApp.condition.home.createOrEditLabel/);
        conditionDialogPage.close();
    });

    it('should create and save Conditions', () => {
        conditionComponentsPage.clickOnCreateButton();
        conditionDialogPage.setLabelInput('label');
        expect(conditionDialogPage.getLabelInput()).toMatch('label');
        conditionDialogPage.setSystemInput('system');
        expect(conditionDialogPage.getSystemInput()).toMatch('system');
        conditionDialogPage.setCodeInput('code');
        expect(conditionDialogPage.getCodeInput()).toMatch('code');
        conditionDialogPage.setStatusInput('status');
        expect(conditionDialogPage.getStatusInput()).toMatch('status');
        conditionDialogPage.setStatusCodeInput('statusCode');
        expect(conditionDialogPage.getStatusCodeInput()).toMatch('statusCode');
        conditionDialogPage.save();
        expect(conditionDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class ConditionComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-condition div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class ConditionDialogPage {
    modalTitle = element(by.css('h4#myConditionLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    labelInput = element(by.css('input#field_label'));
    systemInput = element(by.css('input#field_system'));
    codeInput = element(by.css('input#field_code'));
    statusInput = element(by.css('input#field_status'));
    statusCodeInput = element(by.css('input#field_statusCode'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setLabelInput = function(label) {
        this.labelInput.sendKeys(label);
    }

    getLabelInput = function() {
        return this.labelInput.getAttribute('value');
    }

    setSystemInput = function(system) {
        this.systemInput.sendKeys(system);
    }

    getSystemInput = function() {
        return this.systemInput.getAttribute('value');
    }

    setCodeInput = function(code) {
        this.codeInput.sendKeys(code);
    }

    getCodeInput = function() {
        return this.codeInput.getAttribute('value');
    }

    setStatusInput = function(status) {
        this.statusInput.sendKeys(status);
    }

    getStatusInput = function() {
        return this.statusInput.getAttribute('value');
    }

    setStatusCodeInput = function(statusCode) {
        this.statusCodeInput.sendKeys(statusCode);
    }

    getStatusCodeInput = function() {
        return this.statusCodeInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
