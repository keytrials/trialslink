# ===================================================================
# Spring Boot configuration for the "dev" profile.
#
# This configuration overrides the application.yml file.
#
# More information on profiles: http://www.jhipster.tech/profiles/
# More information on configuration properties: http://www.jhipster.tech/common-application-properties/
# ===================================================================

# ===================================================================
# Standard Spring Boot properties.
# Full reference is available at:
# http://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html
# ===================================================================

logging:
    level:
        ROOT: DEBUG
        com.trialslink: DEBUG
        io.github.jhipster: DEBUG

spring:
    profiles:
        active: dev
        include: swagger
    devtools:
        restart:
            enabled: true
        livereload:
            enabled: false # we use gulp + BrowserSync for livereload
    jackson:
        serialization.indent_output: true
    rabbitmq:
        host: localhost
        port: 5672
        username: guest
        password: guest
   #     listener:
  #          simple:
 #               retry:
#                    enabled: false # Whether or not publishing retries are enabled.
#                    initial-interval: 1000 # Interval between the first and second attempt to deliver a message.
#                    max-attempts: 0 # Maximum number of attempts to deliver a message.
#                    max-interval: 86400000 # Maximum interval between attempts.
#                    multiplier: 1.0 # A multiplier to apply to the previous delivery retry interval.
#                    stateless: true # Whether or not retry is stateless or stateful.
    data:
        mongodb:
            uri: mongodb://localhost:27017
            database: trialslink
        elasticsearch:
            cluster-name:
            cluster-nodes:
            properties:
                indices:
                    query:
                        bool:
                            max_clause_count: 10240
                http:
                    enabled: true
                path:
                  logs: target/elasticsearch/log
                  data: target/elasticsearch/data
    mail:
        host: smtp.gmail.com
        port: 587
        username:   #Replace this field with your Gmail username.
        password:            #Replace this field with your Gmail password.
        protocol: smtp
        tls: true
        properties.mail.smtp:
            auth: true
            starttls.enable: true
            ssl.trust: smtp.gmail.com
    messages:
        cache-seconds: 1
    thymeleaf:
        cache: false

# ===================================================================
# To enable SSL, generate a certificate using:
# keytool -genkey -alias trialslink -storetype PKCS12 -keyalg RSA -keysize 2048 -keystore keystore.p12 -validity 3650
#
# You can also use Let's Encrypt:
# https://maximilian-boehm.com/hp2121/Create-a-Java-Keystore-JKS-from-Let-s-Encrypt-Certificates.htm
#
# Then, modify the server.ssl properties so your "server" configuration looks like:
#
# server:
#    port: 8443
#    ssl:
#        key-store: keystore.p12
#        key-store-password: <your-password>
#        key-store-type: PKCS12
#        key-alias: trialslink
# ===================================================================
server:
    port: 8080

# ===================================================================
# JHipster specific properties
#
# Full reference is available at: http://www.jhipster.tech/common-application-properties/
# ===================================================================

jhipster:
    http:
        version: V_1_1 # To use HTTP/2 you will need SSL support (see above the "server.ssl" configuration)
    # CORS is only enabled by default with the "dev" profile, so BrowserSync can access the API
    cors:
        allowed-origins: "*"
        allowed-methods: "*"
        allowed-headers: "*"
        exposed-headers: "Authorization,Link,X-Total-Count"
        allow-credentials: true
        max-age: 1800
    security:
        authentication:
            jwt:
                secret: my-secret-token-to-change-in-production
                # Token is valid 24 hours
                token-validity-in-seconds: 86400
                token-validity-in-seconds-for-remember-me: 2592000
    mail: # specific JHipster mail property, for standard properties see MailProperties
        from: trialslink@localhost
        base-url: http://127.0.0.1:8080
    metrics: # DropWizard Metrics configuration, used by MetricsConfiguration
        jmx.enabled: true
        graphite: # Use the "graphite" Maven profile to have the Graphite dependencies
            enabled: false
            host: localhost
            port: 2003
            prefix: trialslink
        prometheus: # Use the "prometheus" Maven profile to have the Prometheus dependencies
            enabled: false
            endpoint: /prometheusMetrics
        logs: # Reports Dropwizard metrics in the logs
            enabled: false
            report-frequency: 60 # in seconds
    logging:
        logstash: # Forward logs to logstash over a socket, used by LoggingConfiguration
            enabled: false
            host: localhost
            port: 5000
            queue-size: 512


# ===================================================================
# Application specific properties
# Add your own application properties here, see the ApplicationProperties class
# to have type-safe configuration, like in the JHipsterProperties above
#
# More documentation is available at:
# http://www.jhipster.tech/common-application-properties/
# ===================================================================

application:
    importTrials: false
    importCodes: false
    importFilePath: src/main/resources/config/all-open-trials.tsv
    branding: uclh
    dateFormat: dd/MM/yy
    preferOmnisearch: true
    allowRegistrations: false
    showLanguageSwitcher: false
    ribbon:
        display: true
        text: Beta
    useLocalStorage: false
    contactDetails:
        mask: false
        defaultMask: uclh.research@nhs.net
    singleSiteInstance: true
    allowedFilters:
        - phases
        - statuses
        - types
        - areas
        - siteStatuses

notifier:
    enabled: false

annotator:
    maxTrials: 10
    processOnlyEmptyTrials: true
    schedule: 0 0 1 * * ?
    sanitiseSchedule: 0 0 4 * * ?
    conceptsToPurge:
        - 64572001

termlexclient:
    baseUrl: http://localhost:8221/api/
    userName:
    password:
    apiToken: eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTU1NzgzOTA2N30.-1vaNITo2QZHxS4WMzfLYdb6NmoXFZyVmf7KY8ASn8nLDKk5BLa3xErDo44iYml-tb-uPzRNKf8xz7UQIEEHsw

excelImport:
    filter: true   # flag that indicates if filtering for the import excel is needed or not
    filterColumnNumber: 41   # Column number of the excel which you want to filter the rows of the excel
    filterValues:           # values with which you want to filter the rows of the excel
        - Open
        - Closed to recruitment - in follow up
    sheetNumber: 0
