* Haematological Malignancies
	* Myelodysplastic Syndromes (MDS)
		* MDS 5q- Syndrome
		* Chronic Myelomonocytic Leukaemia (CMML)
	* Acute Leukaemia
		* Acute Myeloid Leukaemia (AML)
			* Acute Promyelocytic Leukaemia (APML)
			* Core Binding Factor Leukaemia
		* Acute Lymphoblastic Leukaemia (ALL)
			* B-Lymphoblastic Leukaemia (B-ALL)
			* T-Lymphoblastic Leukaemia (T-ALL)
	* Plasma Cell Neoplasm
		* Multiple Myeloma (MM)
			* Smouldering Myeloma (MM)
			* Symptomatic Myeloma (MM)
		* Plasmacytoma
		* Amyloidosis
	* Chronic Myeloid Leukaemia
		* BcR-ABL positive CML
		* Chronic Neutrophilic Leukaemia (CNL)
	* Lymphoma
		* Hodgkin Lymphoma
			* Hodgkin lymphoma, nodular lymphocyte predominance
			* Classical Hodgkin Lymphoma, nodular sclerosis
		* Non-Hodgkin Lymphoma (NHL)
			* B-cell Non-Hodgkin Lymphoma (B-NHL)
				* Follicular Lymphoma (FL)
					* Grade 1/2 Follicular Lymphoma
					* Grade 3 Follicular Lymphoma
						* Grade 3a Follicular Lymphoma
						* Grade 3b Follicular Lymphoma
				* Diffuse Large B-cell Lymphoma (DLBCL)
					* Germinal Centre DLBCL
					* Non-Germinal Centre DBLCL
				* Mantle Cell Lymphoma (MCL)
				* Marginal Zone Lymphoma
					* Nodal Marginal Zone Lymphoma
					* Splenic Marginal Zone lymphoma
					* Extranodal Marginal Zone Lymphoma
				* Burkitt Lymphoma (BL)
				* Waldenstrom macroglobulinaemia / Lymphoplasmacytic Lymphoma (LPL)
				* Primary Mediastinal B-cell Lymphoma (PMBCL)
			* T-cell Non-Hodgkin Lymphoma (T-NHL)
				* Anaplastic Large Cell Lymphoma T-cell (ALCL)
				* Adult T-Cell Leukemia/Lymphoma (ATLL)
				* Intestinal T-cell Lymphoma
				* Hepatosplenic T-cell Lymphoma
			* NK/T cell Lymphoma
			* Post-transplant Lymphoproliferative Disorder (PTLD)
		* CNS Lymphoma
			* Primary CNS Lymphoma
			* Secondary CNS Lymphoma
	* Myeloproliferative Neoplasm (MPN)
		* Myelofibrosis (MF)
		* Primary Polycythaemia
		* Essential Thrombocythaemia (ET)
		* Hypereosinophilic syndrome (HES) / chronic eosinophilic leukemia (CEL)
	* Chronic Lymphoid Leukaemia
		* Chronic Lymphocytic Leukaemia (CLL)
		* Hairy Cell Leukaemia (HCL)
		* T-cell LGL Leukaemia (T-LGL)
		* Prolymphocytic leukaemia (PLL)
			* B-Prolymphocytic leukaemia (B-PLL)
			* T-Prolymphocytic leukaemia (T-PLL)
	* Bone Marrow Failure Syndromes
		* Aplastic Anaemia (AA)
		* Paroxysmal nocturnal hemoglobinuria (PNH)
	* Systemic Mastocytosis
	* Haemophagocytic lymphohistiocytosis (HLH)