import { Component, OnInit } from '@angular/core';
import { ProfileService } from './profile.service';
import { ProfileInfo } from './profile-info.model';
import {InfoService} from "../../shared";

@Component({
    selector: 'jhi-page-ribbon',
    template: `<div class="ribbon" *ngIf="display"><a href="" >{{text}}</a></div>`,
    styleUrls: [
        'page-ribbon.css'
    ]
})
export class PageRibbonComponent implements OnInit {

    profileInfo: ProfileInfo;
    ribbonEnv: string;
    ribbonProperties: any;
    display: boolean;
    text: string;

    constructor(private profileService: ProfileService,
                private infoService: InfoService) {
        this.ribbonProperties = {};

        this.infoService.getRibbonInfo().subscribe(ribbonInfo => {
            this.ribbonProperties.display = ribbonInfo.display;
            this.ribbonProperties.text = ribbonInfo.text;
        });
        this.display = this.ribbonProperties.display;
        this.text = this.ribbonProperties.text;
    }

    ngOnInit() {
        this.profileService.getProfileInfo().then((profileInfo) => {
            this.profileInfo = profileInfo;
            this.ribbonEnv = profileInfo.ribbonEnv;
        });
    }
}
