import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ProfileService } from '../profiles/profile.service'; // FIXME barrel doesnt work here
import { JhiLanguageHelper, Principal, LoginModalService, LoginService } from '../../shared';

import { VERSION, DEBUG_INFO_ENABLED } from '../../app.constants';

@Component({
    selector: 'jhi-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: [
        'sidebar.css'
    ]
})
export class SidebarComponent implements OnInit, OnDestroy {

    inProduction: boolean;
    isSidebarCollapsed: boolean;
    languages: any[];
    swaggerEnabled: boolean;
    modalRef: NgbModalRef;
    version: string;
    eventSubscriber: Subscription;

    constructor(private loginService: LoginService,
                private languageHelper: JhiLanguageHelper,
                private principal: Principal,
                private loginModalService: LoginModalService,
                private profileService: ProfileService,
                private router: Router,
                private eventManager: JhiEventManager) {
        this.version = DEBUG_INFO_ENABLED ? 'v' + VERSION : '';
        this.isSidebarCollapsed = true;
    }

    ngOnInit() {
        this.languageHelper.getAll().then((languages) => {
            this.languages = languages;
        });

        //this.profileService.getProfileInfo().subscribe(profileInfo => {
        //    this.inProduction = profileInfo.inProduction;
        //    this.swaggerEnabled = profileInfo.swaggerEnabled;
        //});
        this.registerSideBarToggle();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    collapseSidebar() {
        this.isSidebarCollapsed = true;
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    logout() {
        this.collapseSidebar();
        this.loginService.logout();
        this.router.navigate(['']);
    }

    toggleNavbar() {
        this.isSidebarCollapsed = !this.isSidebarCollapsed;
        console.log('this.isSidebarCollapsed  = ', this.isSidebarCollapsed);
    }

    registerSideBarToggle() {
        this.eventSubscriber = this.eventManager.subscribe('sideBarToggled', (response) => this.toggleNavbar());
    }

    getImageUrl() {
        return this.isAuthenticated() ? this.principal.getImageUrl() : null;
    }
}
