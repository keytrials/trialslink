import {Component} from "@angular/core";
import {TrialService} from "../../entities/trial";
import {InfoService} from "../../shared";

@Component({
    selector: "feedback",
    templateUrl: "./feedback.component.html",
    styleUrls: [
        'feedback.css'
    ]
})
export class FeedbackComponent {
    appInfo: any;
    useIconOnly: boolean = false;

    constructor(private trialService: TrialService,
                private  infoService: InfoService){
        this.appInfo = {};
        this.infoService.get().subscribe(appInfo => {
            this.appInfo.name = appInfo.name;
            this.appInfo.version = appInfo.version;
            this.appInfo.buildNumber = appInfo.buildNumber;
        });
    }

    doAction(button:string){
        if (button === 'trigger'){
            let emailContext = new Map();
            emailContext.set('mailId','FindAStudy.uclh@nhs.net');
            emailContext.set('subject','Feedback on FindAStudy@UCLH');
            let body = '%0D%0A%0D%0A%0D%0A Keytrials Platform Info: '+JSON.stringify(this.appInfo, null, 2);
            emailContext.set('body',body);
            this.trialService.mailto(emailContext);
        }
    }
}
