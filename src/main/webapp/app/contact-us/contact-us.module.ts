import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from "@angular/core";
import {TrialslinkSharedModule} from "../shared";
import {RouterModule} from "@angular/router";
import {ContactUsRoute} from "./contact-us.route";
import {ContactUsComponent} from "./contact-us.component";

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forChild([ ContactUsRoute ])
    ],
    declarations: [
        ContactUsComponent
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class TrialslinkContactUsModule {

}
