import {Route} from "@angular/router";
import {ContactUsComponent} from "./contact-us.component";

export const ContactUsRoute: Route = {
    path: 'contact-us',
    component: ContactUsComponent,
    data: {
        pageTitle: 'contactUs.title'
    }
};
