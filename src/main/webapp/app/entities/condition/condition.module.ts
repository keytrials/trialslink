import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';

import {
    ConditionService,
    ConditionPopupService,
    ConditionComponent,
    ConditionListComponent,
    ConditionBadgeComponent,
    ConditionDetailComponent,
    ConditionDialogComponent,
    ConditionPopupComponent,
    ConditionDeletePopupComponent,
    ConditionDeleteDialogComponent,
    conditionRoute,
    conditionPopupRoute,
    } from './';

const ENTITY_STATES = [
    ...conditionRoute,
    ...conditionPopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        //RouterModule.forRoot(ENTITY_STATES, {useHash: true})
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ConditionComponent,
        ConditionListComponent,
        ConditionBadgeComponent,
        ConditionDetailComponent,
        ConditionDialogComponent,
        ConditionDeleteDialogComponent,
        ConditionPopupComponent,
        ConditionDeletePopupComponent,
    ],
    entryComponents: [
        ConditionComponent,
        ConditionDetailComponent,
        ConditionDialogComponent,
        ConditionPopupComponent,
        ConditionDeleteDialogComponent,
        ConditionDeletePopupComponent,
    ],
    providers: [
        ConditionService,
        ConditionPopupService,
    ],
    exports:  [
        ConditionListComponent,
        ConditionBadgeComponent,
        ConditionDetailComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkConditionModule {
}
