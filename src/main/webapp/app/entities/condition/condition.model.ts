import { BaseEntity } from './../../shared';

export class Condition implements BaseEntity {
    constructor(
        public id?: string,
        public label?: string,
        public system?: string,
        public code?: string,
        public status?: string,
        public statusCode?: string,
        public trialId?: string
    ) {
    }
}
