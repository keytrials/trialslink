import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Condition } from './condition.model';
import { ConditionService } from './condition.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-condition',
    templateUrl: './condition.component.html'
})
export class ConditionComponent implements OnInit, OnDestroy {
    conditions: Condition[];
    currentAccount: any;
    eventSubscriber: Subscription;

//<<<<<<< HEAD
    constructor(
                private conditionService: ConditionService,
                private alertService: JhiAlertService,
                private eventManager: JhiEventManager,
                private principal: Principal) {
//        this.jhiLanguageService.setLocations(['condition']);
//=======
//    constructor(
//        private conditionService: ConditionService,
//        private jhiAlertService: JhiAlertService,
//        private eventManager: JhiEventManager,
//        private principal: Principal
//    ) {
//>>>>>>> jhipster_upgrade
    }

    loadAll() {
        this.conditionService.query().subscribe(
            (res: ResponseWrapper) => {
                this.conditions = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInConditions();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Condition) {
        return item.id;
    }
//<<<<<<< HEAD
//
//
//=======
//>>>>>>> jhipster_upgrade
    registerChangeInConditions() {
        this.eventSubscriber = this.eventManager.subscribe('conditionListModification', (response) => this.loadAll());
    }

//<<<<<<< HEAD

    private onError(error) {
        this.alertService.error(error.message, null, null);
//=======
//    private onError(error) {
//        this.jhiAlertService.error(error.message, null, null);
//>>>>>>> jhipster_upgrade
    }
}
