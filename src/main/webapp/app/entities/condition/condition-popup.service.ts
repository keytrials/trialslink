import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Condition } from './condition.model';
import { ConditionService } from './condition.service';

@Injectable()
export class ConditionPopupService {
//<<<<<<< HEAD
//    private isOpen = false;
//
//    constructor(private modalService: NgbModal,
//                private router: Router,
//                private conditionService: ConditionService) {
//    }
//
//    open(component: Component, id?: number | any, createNew?: boolean): NgbModalRef {
//        if (this.isOpen) {
//            return;
//        }
//        this.isOpen = true;
//
//        if (!createNew) {
//            this.conditionService.find(id).subscribe(condition => {
//                this.conditionModalRef(component, condition);
//            });
//        } else {
//            let condition = new Condition();
//            condition.trialId = id;
//            return this.conditionModalRef(component, condition);
//        }
//    }
//
//    conditionModalRef(component: Component, condition: Condition): NgbModalRef {
//        let modalRef = this.modalService.open(component, {size: 'lg', backdrop: 'static'});
//        modalRef.componentInstance.condition = condition;
//        modalRef.result.then(result => {
//            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true});
//            this.isOpen = false;
//        }, (reason) => {
//            this.router.navigate([{outlets: {popup: null}}], {replaceUrl: true});
//            this.isOpen = false;
//=======
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private conditionService: ConditionService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any, createNew?: boolean): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (!createNew) {
                this.conditionService.find(id).subscribe((condition) => {
                    this.ngbModalRef = this.conditionModalRef(component, condition);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    let condition = new Condition();
                    condition.trialId = id;
                    this.ngbModalRef = this.conditionModalRef(component, condition);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    conditionModalRef(component: Component, condition: Condition): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.condition = condition;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
//>>>>>>> jhipster_upgrade
        });
        return modalRef;
    }
}
