import { Component, Input } from '@angular/core';

import { Condition } from './condition.model';

@Component({
    selector: 'condition-badge',
    templateUrl: './condition-badge.component.html',
    styleUrls: [
        'condition-badge.css'
    ]
})
export class ConditionBadgeComponent {

    @Input() condition: Condition;

    constructor() {
    }
}
