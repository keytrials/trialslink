import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Condition } from './condition.model';
import { ConditionPopupService } from './condition-popup.service';
import { ConditionService } from './condition.service';

@Component({
    selector: 'jhi-condition-delete-dialog',
    templateUrl: './condition-delete-dialog.component.html'
})
export class ConditionDeleteDialogComponent {

    condition: Condition;

    constructor(
                private conditionService: ConditionService,
                public activeModal: NgbActiveModal,
                private eventManager: JhiEventManager) {
        //this.jhiLanguageService.setLocations(['condition']);
//=======
//    constructor(
//        private conditionService: ConditionService,
//        public activeModal: NgbActiveModal,
//        private eventManager: JhiEventManager
//    ) {
//>>>>>>> jhipster_upgrade
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.conditionService.delete(this.condition.trialId, id).subscribe(response => {
//=======
//    confirmDelete(id: string) {
//        this.conditionService.delete(id).subscribe((response) => {
//>>>>>>> jhipster_upgrade
            this.eventManager.broadcast({
                name: 'conditionListModification',
                content: 'Deleted an condition'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-condition-delete-popup',
    template: ''
})
export class ConditionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(private route: ActivatedRoute,
                private conditionPopupService: ConditionPopupService) {
    }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.conditionPopupService
                .open(ConditionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
