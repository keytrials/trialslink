import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Condition } from './condition.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class ConditionService {

    private resourceUrl = SERVER_API_URL + 'api/conditions';
    private trialsResourceUrl = SERVER_API_URL + 'api/trials';

    constructor(private http: Http) {
    }

    create(condition: Condition): Observable<Condition> {
        const copy = this.convert(condition);
        if (condition.trialId !== undefined) {
            return this.http.post(`${this.trialsResourceUrl}/${condition.trialId}/conditions`, copy).map((res: Response) => {
                const jsonResponse = res.json();
                return this.convertItemFromServer(jsonResponse);
            });
        } else {
            return this.http.post(this.resourceUrl, copy).map((res: Response) => {
                const jsonResponse = res.json();
                return this.convertItemFromServer(jsonResponse);
            });
        }
    }

    update(condition: Condition): Observable<Condition> {
        const copy = this.convert(condition);
        return this.http.post(`${this.trialsResourceUrl}/${condition.trialId}/conditions`, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<Condition> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options);
    }

    conditionsForTrial(id: string, req?: any): Observable<ResponseWrapper> {
        let options = createRequestOption(req);
        return this.http.get(`${this.trialsResourceUrl}/${id}/conditions`, options)
            .map((res: any) => this.convertResponse(res, id))
            ;
    }

    delete(trialId: string, id: string): Observable<Response> {
        return this.http.delete(`${this.trialsResourceUrl}/${trialId}/conditions/${id}`);
    }

    deleteAllForTrial(id: string): Observable<Response> {
        return this.http.delete(`${this.trialsResourceUrl}/${id}/conditions`);
    }

    private convertResponse(res: any, id: string): ResponseWrapper {
        let jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            jsonResponse[i].trialId = id;
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        //res._body = jsonResponse;
        //return res.map((res: Response) => this.convertResponse(res));
        return new ResponseWrapper(res.headers, result, res.status);
    }

    //delete(id: string): Observable<Response> {
    //    return this.http.delete(`${this.resourceUrl}/${id}`);
    //}

    //private convertResponse(res: Response): ResponseWrapper {
    //    const jsonResponse = res.json();
    //    const result = [];
    //    for (let i = 0; i < jsonResponse.length; i++) {
    //        result.push(this.convertItemFromServer(jsonResponse[i]));
    //    }
    //    return new ResponseWrapper(res.headers, result, res.status);
    //}

    /**
     * Convert a returned JSON object to Condition.
     */
    private convertItemFromServer(json: any): Condition {
        const entity: Condition = Object.assign(new Condition(), json);
        return entity;
    }

    /**
     * Convert a Condition to a JSON which can be sent to the server.
     */
    private convert(condition: Condition): Condition {
        const copy: Condition = Object.assign({}, condition);
        return copy;
    }
}
