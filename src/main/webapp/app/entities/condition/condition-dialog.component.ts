import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Condition } from './condition.model';
import { ConditionPopupService } from './condition-popup.service';
import { ConditionService } from './condition.service';

@Component({
    selector: 'jhi-condition-dialog',
    templateUrl: './condition-dialog.component.html'
})
export class ConditionDialogComponent implements OnInit {

    condition: Condition;
    isSaving: boolean;

    constructor(public activeModal: NgbActiveModal,
                //private jhiLanguageService: JhiLanguageService,
                private alertService: JhiAlertService,
                private conditionService: ConditionService,
                private eventManager: JhiEventManager) {
//        this.jhiLanguageService.setLocations(['condition', 'trialCentre', 'trial']);
//=======
//    constructor(
//        public activeModal: NgbActiveModal,
//        private conditionService: ConditionService,
//        private eventManager: JhiEventManager
//    ) {
//>>>>>>> jhipster_upgrade
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.condition.id !== undefined) {
            this.subscribeToSaveResponse(
                this.conditionService.update(this.condition));
        } else {
            this.subscribeToSaveResponse(
                this.conditionService.create(this.condition));
        }
    }

//<<<<<<< HEAD
//    private onSaveSuccess(result: Condition) {
//        this.eventManager.broadcast({name: 'conditionListModification', content: 'OK'});
//=======
    private subscribeToSaveResponse(result: Observable<Condition>) {
        result.subscribe((res: Condition) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: Condition) {
        this.eventManager.broadcast({ name: 'conditionListModification', content: 'OK'});
//>>>>>>> jhipster_upgrade
        this.isSaving = false;
        this.activeModal.dismiss(result);
        this.eventManager.broadcast({ name: 'trialModification', content: 'OK'});
    }

//<<<<<<< HEAD
    private onSaveError(error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
//=======
//    private onSaveError() {
//        this.isSaving = false;
//>>>>>>> jhipster_upgrade
    }
}

@Component({
    selector: 'jhi-condition-popup',
    template: ''
})
export class ConditionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

//<<<<<<< HEAD
    constructor(private route: ActivatedRoute,
                private conditionPopupService: ConditionPopupService) {
    }

    //ngOnInit() {
    //    this.routeSub = this.route.params.subscribe(params => {
    //        if (params['id']) {
    //            this.modalRef = this.conditionPopupService
    //                .open(ConditionDialogComponent, params['id'], false);
    //        } else {
    //            this.modalRef = this.conditionPopupService
    //                .open(ConditionDialogComponent, params['trialId'], true);
//=======
//    constructor(
//        private route: ActivatedRoute,
//        private conditionPopupService: ConditionPopupService
//    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.conditionPopupService
                    .open(ConditionDialogComponent as Component, params['id'], false);
            } else {
                this.conditionPopupService
                    .open(ConditionDialogComponent as Component, params['trialId'], true);
//>>>>>>> jhipster_upgrade
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
