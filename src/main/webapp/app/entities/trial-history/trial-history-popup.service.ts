import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TrialHistory } from './trial-history.model';
import { TrialHistoryService } from './trial-history.service';

@Injectable()
export class TrialHistoryPopupService {
    private isOpen = false;
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private trialHistoryService: TrialHistoryService

    ) {
        this.ngbModalRef = null;
    }

//    open(component: Component, id?: number | any): NgbModalRef {
//        if (this.isOpen) {
//            return;
//        }
//        this.isOpen = true;
//
//        if (id) {
//            this.trialHistoryService.find(id).subscribe((trialHistory) => {
//                this.trialHistoryModalRef(component, trialHistory);
//            });
//        } else {
//            return this.trialHistoryModalRef(component, new TrialHistory());
//        }
//=======
    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.trialHistoryService.find(id).subscribe((trialHistory) => {
                    this.ngbModalRef = this.trialHistoryModalRef(component, trialHistory);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.trialHistoryModalRef(component, new TrialHistory());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    trialHistoryModalRef(component: Component, trialHistory: TrialHistory): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.trialHistory = trialHistory;
        modalRef.result.then((result) => {
//            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
//            this.isOpen = false;
//=======
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
