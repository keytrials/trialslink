import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TrialHistory } from './trial-history.model';
import { TrialHistoryPopupService } from './trial-history-popup.service';
import { TrialHistoryService } from './trial-history.service';

@Component({
    selector: 'jhi-trial-history-delete-dialog',
    templateUrl: './trial-history-delete-dialog.component.html'
})
export class TrialHistoryDeleteDialogComponent {

    trialHistory: TrialHistory;

    constructor(
        private trialHistoryService: TrialHistoryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.trialHistoryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'trialHistoryListModification',
                content: 'Deleted an trialHistory'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-trial-history-delete-popup',
    template: ''
})
export class TrialHistoryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private trialHistoryPopupService: TrialHistoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.trialHistoryPopupService
                .open(TrialHistoryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
