import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TrialHistory } from './trial-history.model';
import { TrialHistoryPopupService } from './trial-history-popup.service';
import { TrialHistoryService } from './trial-history.service';

@Component({
    selector: 'jhi-trial-history-dialog',
    templateUrl: './trial-history-dialog.component.html'
})
export class TrialHistoryDialogComponent implements OnInit {

    trialHistory: TrialHistory;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private trialHistoryService: TrialHistoryService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.trialHistory.id !== undefined) {
            this.subscribeToSaveResponse(
                this.trialHistoryService.update(this.trialHistory));
        } else {
            this.subscribeToSaveResponse(
                this.trialHistoryService.create(this.trialHistory));
        }
    }

    private subscribeToSaveResponse(result: Observable<TrialHistory>) {
        result.subscribe((res: TrialHistory) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: TrialHistory) {
        this.eventManager.broadcast({ name: 'trialHistoryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-trial-history-popup',
    template: ''
})
export class TrialHistoryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private trialHistoryPopupService: TrialHistoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.trialHistoryPopupService
                    .open(TrialHistoryDialogComponent as Component, params['id']);
            } else {
                this.trialHistoryPopupService
                    .open(TrialHistoryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
