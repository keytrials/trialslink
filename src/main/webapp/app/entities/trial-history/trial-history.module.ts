import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';
import {
    TrialHistoryService,
    TrialHistoryPopupService,
    TrialHistoryComponent,
    TrialHistoryDetailComponent,
    TrialHistoryDialogComponent,
    TrialHistoryPopupComponent,
    TrialHistoryDeletePopupComponent,
    TrialHistoryDeleteDialogComponent,
    trialHistoryRoute,
    trialHistoryPopupRoute,
} from './';

const ENTITY_STATES = [
    ...trialHistoryRoute,
    ...trialHistoryPopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TrialHistoryComponent,
        TrialHistoryDetailComponent,
        TrialHistoryDialogComponent,
        TrialHistoryDeleteDialogComponent,
        TrialHistoryPopupComponent,
        TrialHistoryDeletePopupComponent,
    ],
    entryComponents: [
        TrialHistoryComponent,
        TrialHistoryDialogComponent,
        TrialHistoryPopupComponent,
        TrialHistoryDeleteDialogComponent,
        TrialHistoryDeletePopupComponent,
    ],
    providers: [
        TrialHistoryService,
        TrialHistoryPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkTrialHistoryModule {}
