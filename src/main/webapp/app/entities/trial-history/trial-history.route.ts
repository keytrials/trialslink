import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { TrialHistoryComponent } from './trial-history.component';
import { TrialHistoryDetailComponent } from './trial-history-detail.component';
import { TrialHistoryPopupComponent } from './trial-history-dialog.component';
import { TrialHistoryDeletePopupComponent } from './trial-history-delete-dialog.component';

import { Principal } from '../../shared';

export const trialHistoryRoute: Routes = [
    {
        path: 'trial-history',
        component: TrialHistoryComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.trialHistory.home.title'
        }
    }, {
        path: 'trial-history/:id',
        component: TrialHistoryDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.trialHistory.home.title'
        }
    }
];

export const trialHistoryPopupRoute: Routes = [
    {
        path: 'trial-history-new',
        component: TrialHistoryPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.trialHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trial-history/:id/edit',
        component: TrialHistoryPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.trialHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trial-history/:id/delete',
        component: TrialHistoryDeletePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.trialHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
