import { Injectable } from '@angular/core';
import {BaseRequestOptions, Http, Response, URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { TrialHistory } from './trial-history.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class TrialHistoryService {

    private resourceUrl = SERVER_API_URL + 'api/trial-histories';

    constructor(private http: Http) { }

    create(trialHistory: TrialHistory): Observable<TrialHistory> {
        const copy = this.convert(trialHistory);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(trialHistory: TrialHistory): Observable<TrialHistory> {
        const copy = this.convert(trialHistory);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<TrialHistory> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    //private convertResponse(res: any): any {
    //    /*const jsonResponse = res.json();
    //    return new ResponseWrapper(res.headers, jsonResponse, res.status);*/
    //    res._body = res.json();
    //    return res;
    //}

    private convert(trialHistory: TrialHistory): TrialHistory {
        const copy: TrialHistory = Object.assign({}, trialHistory);
        return copy;
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to TrialHistory.
     */
    private convertItemFromServer(json: any): TrialHistory {
        const entity: TrialHistory = Object.assign(new TrialHistory(), json);
        return entity;
    }
}
