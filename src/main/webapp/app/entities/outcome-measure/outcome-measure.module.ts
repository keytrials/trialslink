import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';
import {
    OutcomeMeasureService,
    OutcomeMeasurePopupService,
    OutcomeMeasureComponent,
    OutcomeMeasureDetailComponent,
    OutcomeMeasureDialogComponent,
    OutcomeMeasurePopupComponent,
    OutcomeMeasureDeletePopupComponent,
    OutcomeMeasureDeleteDialogComponent,
    outcomeMeasureRoute,
    outcomeMeasurePopupRoute,
} from './';

const ENTITY_STATES = [
    ...outcomeMeasureRoute,
    ...outcomeMeasurePopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        OutcomeMeasureComponent,
        OutcomeMeasureDetailComponent,
        OutcomeMeasureDialogComponent,
        OutcomeMeasureDeleteDialogComponent,
        OutcomeMeasurePopupComponent,
        OutcomeMeasureDeletePopupComponent,
    ],
    entryComponents: [
        OutcomeMeasureComponent,
        OutcomeMeasureDialogComponent,
        OutcomeMeasurePopupComponent,
        OutcomeMeasureDeleteDialogComponent,
        OutcomeMeasureDeletePopupComponent,
    ],
    providers: [
        OutcomeMeasureService,
        OutcomeMeasurePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkOutcomeMeasureModule {}
