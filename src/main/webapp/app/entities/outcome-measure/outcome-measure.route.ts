import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { OutcomeMeasureComponent } from './outcome-measure.component';
import { OutcomeMeasureDetailComponent } from './outcome-measure-detail.component';
import { OutcomeMeasurePopupComponent } from './outcome-measure-dialog.component';
import { OutcomeMeasureDeletePopupComponent } from './outcome-measure-delete-dialog.component';

export const outcomeMeasureRoute: Routes = [
    {
        path: 'outcome-measure',
        component: OutcomeMeasureComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.outcomeMeasure.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'outcome-measure/:id',
        component: OutcomeMeasureDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.outcomeMeasure.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const outcomeMeasurePopupRoute: Routes = [
    {
        path: 'outcome-measure-new',
        component: OutcomeMeasurePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.outcomeMeasure.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'outcome-measure/:id/edit',
        component: OutcomeMeasurePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.outcomeMeasure.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'outcome-measure/:id/delete',
        component: OutcomeMeasureDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.outcomeMeasure.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
