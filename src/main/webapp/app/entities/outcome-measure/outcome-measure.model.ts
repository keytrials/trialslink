import { BaseEntity } from './../../shared';

export class OutcomeMeasure implements BaseEntity {
    constructor(
        public id?: string,
        public title?: string,
        public description?: string,
        public timeFrame?: string,
    ) {
    }
}
