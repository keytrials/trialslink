import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { OutcomeMeasure } from './outcome-measure.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class OutcomeMeasureService {

    private resourceUrl = SERVER_API_URL + 'api/outcome-measures';

    constructor(private http: Http) { }

    create(outcomeMeasure: OutcomeMeasure): Observable<OutcomeMeasure> {
        const copy = this.convert(outcomeMeasure);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(outcomeMeasure: OutcomeMeasure): Observable<OutcomeMeasure> {
        const copy = this.convert(outcomeMeasure);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<OutcomeMeasure> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to OutcomeMeasure.
     */
    private convertItemFromServer(json: any): OutcomeMeasure {
        const entity: OutcomeMeasure = Object.assign(new OutcomeMeasure(), json);
        return entity;
    }

    /**
     * Convert a OutcomeMeasure to a JSON which can be sent to the server.
     */
    private convert(outcomeMeasure: OutcomeMeasure): OutcomeMeasure {
        const copy: OutcomeMeasure = Object.assign({}, outcomeMeasure);
        return copy;
    }
}
