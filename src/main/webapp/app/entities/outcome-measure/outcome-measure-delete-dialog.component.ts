import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { OutcomeMeasure } from './outcome-measure.model';
import { OutcomeMeasurePopupService } from './outcome-measure-popup.service';
import { OutcomeMeasureService } from './outcome-measure.service';

@Component({
    selector: 'jhi-outcome-measure-delete-dialog',
    templateUrl: './outcome-measure-delete-dialog.component.html'
})
export class OutcomeMeasureDeleteDialogComponent {

    outcomeMeasure: OutcomeMeasure;

    constructor(
        private outcomeMeasureService: OutcomeMeasureService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.outcomeMeasureService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'outcomeMeasureListModification',
                content: 'Deleted an outcomeMeasure'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-outcome-measure-delete-popup',
    template: ''
})
export class OutcomeMeasureDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private outcomeMeasurePopupService: OutcomeMeasurePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.outcomeMeasurePopupService
                .open(OutcomeMeasureDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
