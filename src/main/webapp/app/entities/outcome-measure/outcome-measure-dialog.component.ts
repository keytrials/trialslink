import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { OutcomeMeasure } from './outcome-measure.model';
import { OutcomeMeasurePopupService } from './outcome-measure-popup.service';
import { OutcomeMeasureService } from './outcome-measure.service';

@Component({
    selector: 'jhi-outcome-measure-dialog',
    templateUrl: './outcome-measure-dialog.component.html'
})
export class OutcomeMeasureDialogComponent implements OnInit {

    outcomeMeasure: OutcomeMeasure;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private outcomeMeasureService: OutcomeMeasureService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.outcomeMeasure.id !== undefined) {
            this.subscribeToSaveResponse(
                this.outcomeMeasureService.update(this.outcomeMeasure));
        } else {
            this.subscribeToSaveResponse(
                this.outcomeMeasureService.create(this.outcomeMeasure));
        }
    }

    private subscribeToSaveResponse(result: Observable<OutcomeMeasure>) {
        result.subscribe((res: OutcomeMeasure) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: OutcomeMeasure) {
        this.eventManager.broadcast({ name: 'outcomeMeasureListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-outcome-measure-popup',
    template: ''
})
export class OutcomeMeasurePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private outcomeMeasurePopupService: OutcomeMeasurePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.outcomeMeasurePopupService
                    .open(OutcomeMeasureDialogComponent as Component, params['id']);
            } else {
                this.outcomeMeasurePopupService
                    .open(OutcomeMeasureDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
