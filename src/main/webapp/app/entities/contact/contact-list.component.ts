import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Rx';
import {JhiAlertService, JhiEventManager, JhiParseLinks} from 'ng-jhipster';

import {Contact} from './contact.model';
import {TrialCentre} from '../trial-centre/trial-centre.model';
import {ContactService} from './contact.service';
import {InfoService, ITEMS_PER_PAGE, Principal, ResponseWrapper} from '../../shared';
import {TrialService} from "../trial";

@Component({
    selector: 'jhi-contact-list',
    templateUrl: './contact.component.html'
})
export class ContactListComponent implements OnInit, OnDestroy {

    contacts: Contact[];
    @Input() trialCentre: TrialCentre;
    currentAccount: any;
    eventSubscriber: Subscription;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;
    maskContactDetails: boolean = false;
    defaultMask: string;
    brand: string;

    constructor(
        private contactService: ContactService,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private principal: Principal,
        private infoService: InfoService,
        private trialService: TrialService
    ) {
        this.contacts = [];
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'id';
        this.reverse = true;
        this.infoService.getBrand().subscribe(value => {
            this.brand = value._body;
        });
        //this.jhiLanguageService.setLocations(['trial', 'trialStatus', 'studyType', 'trialCentre', 'contact', 'contactType', 'condition', 'intervention', 'characteristic', 'sex', 'age']);
    }

    loadAll () {
        //if(this.trialCentre) {
        //    this.contacts = this.trialCentre.contacts;
        //}
        this.contactService.contactsForCentre(this.trialCentre.id, {
            page: this.page,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    reset() {
        this.page = 0;
        this.contacts = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    requestDetails(){
        let emailContext: any;
        this.trialService.find(this.trialCentre.trialId).subscribe(value => {
            emailContext = this.trialService.mailContext(value,'Interested%20in%20finding%20out%20more%20about%20-%20');
            this.trialService.mailto(emailContext);
        });
    }

    ngOnInit() {

        this.infoService.getSettings().subscribe(value => {
            this.maskContactDetails = value.contactDetails.mask;
            this.defaultMask = value.contactDetails.text;
        });
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            console.log('account  = ' , account );
        });
        this.registerChangeInContacts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId (index: number, item: Contact) {
        return item.id;
    }

    registerChangeInContacts() {
        this.eventSubscriber = this.eventManager.subscribe('contactAdded', (response) => this.reset());
        this.eventSubscriber = this.eventManager.subscribe('contactRemoved', (id) => this.reset());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        //console.log("data  = " , data );
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        for (let i = 0; i < data.length; i++) {
            data[i]['trialCentreId'] = this.trialCentre.id;
            this.contacts.push(data[i]);
        }
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
