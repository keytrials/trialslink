import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { Contact } from './contact.model';
import { ContactPopupService } from './contact-popup.service';
import { ContactService } from './contact.service';
import { RoleService } from '../role/role.service';

@Component({
    selector: 'jhi-contact-dialog',
    templateUrl: './contact-dialog.component.html'
})
export class ContactDialogComponent implements OnInit {

    contact: Contact;
    authorities: any[];
    roles: Array<string>;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private contactService: ContactService,
        private roleService: RoleService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.roleService.allAsFilter({
                size: 500
            }
        ).subscribe(
            (res: Response) => {
                this.roles = res.json();
            },
            (res: Response) => this.onError(res.json())
        );
    }

    selectedRole(value: any): void {
        this.contact.role.name = value.text;
        this.contact.role.id = value.id;
    }

    removedRole(value: any): void {
        this.contact.role = null;
    }

    clear () {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.contact.id !== undefined) {
            this.subscribeToSaveResponse(
                this.contactService.update(this.contact));
        } else {
            this.subscribeToSaveResponse(
                this.contactService.create(this.contact));
        }
    }

    private subscribeToSaveResponse(result: Observable<Contact>) {
        result.subscribe((res: Contact) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
    }

    private onSaveSuccess(result: Contact) {
        this.eventManager.broadcast({ name: 'contactAdded', content: result});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-contact-popup',
    template: ''
})
export class ContactPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private contactPopupService: ContactPopupService
    ) {}

    ngOnInit() {
        //this.routeSub = this.route.params.subscribe((params) => {
        //    if ( params['id'] ) {
        //        this.modalRef = this.contactPopupService
        //            .open(ContactDialogComponent, params['id'], false);
        //    } else {
        //        this.modalRef = this.contactPopupService
        //            .open(ContactDialogComponent, params['trialCentreId'], true);
        //    }
        //});

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.contactPopupService
                    .open(ContactDialogComponent as Component, params['id'], false);
            } else {
                this.contactPopupService
                    .open(ContactDialogComponent as Component, params['trialCentreId'], true);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
