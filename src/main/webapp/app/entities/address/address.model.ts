import { BaseEntity } from './../../shared';

export class Address implements BaseEntity {
    constructor(
        public id?: string,
        public street?: string,
        public city?: string,
        public county?: string,
        public postalCode?: string,
        public country?: string
    ) {
    }
}
