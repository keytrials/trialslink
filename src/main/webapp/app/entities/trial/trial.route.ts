import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { TrialComponent } from './trial.component';
import { TrialDetailComponent } from './trial-detail.component';
import { TrialPopupComponent } from './trial-dialog.component';
import { TrialDeletePopupComponent } from './trial-delete-dialog.component';

export const trialRoute: Routes = [
    {
        path: 'trial',
        component: TrialComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.trial.home.title'
        }
        //canActivate: [UserRouteAccessService]
    }, {
        path: 'trial/:id',
        component: TrialDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.trial.home.title'
        }
        //canActivate: [UserRouteAccessService]
    }
];

export const trialPopupRoute: Routes = [
  {
    path: 'trial-new',
    component: TrialPopupComponent,
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_PRIVILEGED'],
        pageTitle: 'trialslinkApp.trial.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  },
  {
    path: 'trial/:id/edit',
    component: TrialPopupComponent,
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_PRIVILEGED'],
        pageTitle: 'trialslinkApp.trial.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  },
    {
        path: 'trial/:id/delete',
        component: TrialDeletePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.trial.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
