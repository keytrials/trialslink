import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';
import { JhiDateUtils } from 'ng-jhipster';

import { Trial } from './trial.model';
import { TrialCentre } from '../trial-centre/trial-centre.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import {IdInfo} from "../id-info";
import {InfoService} from "../../shared";

@Injectable()
export class TrialService {
    maskContactDetails: boolean = false;
    defaultMask: string;

    private resourceUrl = SERVER_API_URL + 'api/trials';
    private shortNameUrl = SERVER_API_URL + 'api/trials/shortName';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/trials';
    private omniSearchUrl = SERVER_API_URL + 'api/trials/omni_search';
    private resourceIndexUrl = SERVER_API_URL + 'api/_index/trials';

    constructor(private http: Http, 
                private dateUtils: JhiDateUtils,
                private infoService: InfoService) {
        this.infoService.getSettings().subscribe(value => {
                this.maskContactDetails = value.contactDetails.mask;
                this.defaultMask = value.contactDetails.text;
        });
    }

    create(trial: Trial): Observable<Trial> {
        const copy = this.convert(trial);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(trial: Trial): Observable<Trial> {
        const copy = this.convert(trial);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<Trial> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            let jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    centres(id: number): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrl}/${id}/centres`)
            .map((res: any) => this.convertResponse(res));
    }

    addCentre(id: string, centre: TrialCentre): Observable<TrialCentre> {
        return this.http.post(`${this.resourceUrl}/${id}/centres`, centre).map((res: Response) => {
            let jsonResponse = res.json();
            jsonResponse.startDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.startDate);
            jsonResponse.endDate = this.dateUtils
                .convertDateTimeFromServer(jsonResponse.endDate);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<Response> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertSearchResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    indexAllTrials(): Observable<Response> {
        return this.http.get(`${this.resourceIndexUrl}`);
    }

    search(req?: any): Observable<Response> {
        let options = createRequestOption(req);
        console.log("req.query  = " , req.query );
        return this.http.post(this.resourceSearchUrl, req.query, options)
            .map((res: any) => this.convertSearchResponse(res))
            ;
    }

    findByShortName(shortName: any): Observable<Response> {
        return this.http.get(`${this.shortNameUrl}/${shortName}`);
    }

    annotate(id: string): Observable<Response> {
        return this.http.get(`${this.resourceUrl}/${id}/annotate`);
    }

    omniSearch(req?: any): Observable<Response> {
        let options = createRequestOption(req);
        return this.http.get(this.omniSearchUrl, options)
            .map((res: any) => this.convertSearchResponse(res));
    }

    private convertSearchResponse(res: any): any {
        let jsonResponse = res.json();
        if (res.json().results !== undefined) {
            jsonResponse = jsonResponse.results;
        }
        //const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            jsonResponse[i] = this.convertItemFromServer(jsonResponse[i]);
            //jsonResponse[i].recordVerificationDate = this.dateUtils
            //    .convertDateTimeFromServer(jsonResponse[i].recordVerificationDate);
            //jsonResponse[i].startDate = this.dateUtils
            //    .convertDateTimeFromServer(jsonResponse[i].startDate);
            //jsonResponse[i].primaryCompletionDate = this.dateUtils
            //    .convertDateTimeFromServer(jsonResponse[i].primaryCompletionDate);
            //jsonResponse[i].studyCompletionDate = this.dateUtils
            //    .convertDateTimeFromServer(jsonResponse[i].studyCompletionDate);
            //jsonResponse[i].createdDate = this.dateUtils
            //    .convertDateTimeFromServer(jsonResponse[i].createdDate);
            //jsonResponse[i].lastModifiedDate = this.dateUtils
            //    .convertDateTimeFromServer(jsonResponse[i].lastModifiedDate);
        }
        if (res.json().results !== undefined) {
            res.json().results = jsonResponse;
        } else {
            res._body = jsonResponse;
        }
        return res;
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        //console.log("jsonResponse  = " , jsonResponse );
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Trial.
     */
    private convertItemFromServer(json: any): Trial {
        const entity: Trial = Object.assign(new Trial(), json);
        entity.recordVerificationDate = this.dateUtils
            .convertDateTimeFromServer(json.recordVerificationDate);
        entity.startDate = this.dateUtils
            .convertDateTimeFromServer(json.startDate);
        entity.primaryCompletionDate = this.dateUtils
            .convertDateTimeFromServer(json.primaryCompletionDate);
        entity.studyCompletionDate = this.dateUtils
            .convertDateTimeFromServer(json.studyCompletionDate);
        entity.createdDate = this.dateUtils
            .convertDateTimeFromServer(json.createdDate);
        entity.lastModifiedDate = this.dateUtils
            .convertDateTimeFromServer(json.lastModifiedDate);
        return entity;
    }

    /**
     * Convert a Trial to a JSON which can be sent to the server.
     */
    private convert(trial: Trial): Trial {
        const copy: Trial = Object.assign({}, trial);

        copy.recordVerificationDate = this.dateUtils.toDate(trial.recordVerificationDate);
        copy.startDate = this.dateUtils.toDate(trial.startDate+ ' ');
        copy.primaryCompletionDate = this.dateUtils.toDate(trial.primaryCompletionDate);
        copy.studyCompletionDate = this.dateUtils.toDate(trial.studyCompletionDate);
        copy.createdDate = this.dateUtils.toDate(String(trial.createdDate));
        copy.lastModifiedDate = this.dateUtils.toDate(String(trial.lastModifiedDate));

        return copy;
    }

    /**
     * Service used for the mailto function
     */
    mailContext(trial: Trial, subjectText: string): any{
        let idInfo: IdInfo;
        let emailContext = new Map();
        idInfo=trial.idInfo;
        let identifiers = [];
        if (idInfo.id)
            identifiers.push(idInfo.id);
        if (idInfo.nctId)
            identifiers.push(idInfo.nctId);
        if (idInfo.protocolId)
            identifiers.push(idInfo.protocolId);
        for (let id of idInfo.secondaryIds){
            if (id)
                identifiers.push(id);
        }
        emailContext.set('mailId',this.defaultMask);
        emailContext.set('subject',subjectText+trial.briefTitle);
        let body = '%0D%0A%0D%0A%0D%0A'+trial.briefTitle;
        if (trial.shortName){
            body = body.concat('%0D%0AShort%20Name:%0D%0A'+trial.shortName);
        }
        body = body.concat('%0D%0ATrial%20Identifiers:');
        for(let identifier of identifiers){
            body = body.concat('%0D%0A'+identifier.toString())
        }
        emailContext.set('body',body);
        return emailContext;

    }

    mailto(emailContext: any){
        window.location.href='mailto:'+emailContext.get('mailId')+'?subject='+emailContext.get('subject')+'&body='+emailContext.get('body');
    }
}
