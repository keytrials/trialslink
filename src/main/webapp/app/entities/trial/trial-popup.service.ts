import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Trial } from './trial.model';
import { TrialService } from './trial.service';

@Injectable()
export class TrialPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private trialService: TrialService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.trialService.find(id).subscribe((trial) => {
                    trial.recordVerificationDate = this.datePipe
                        .transform(trial.recordVerificationDate, 'yyyy-MM-ddTHH:mm:ss');
                    trial.startDate = this.datePipe
                        .transform(trial.startDate, 'yyyy-MM-ddTHH:mm:ss');
                    trial.primaryCompletionDate = this.datePipe
                        .transform(trial.primaryCompletionDate, 'yyyy-MM-ddTHH:mm:ss');
                    trial.studyCompletionDate = this.datePipe
                        .transform(trial.studyCompletionDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.trialModalRef(component, trial);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.trialModalRef(component, new Trial());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    trialModalRef(component: Component, trial: Trial): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.trial = trial;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
