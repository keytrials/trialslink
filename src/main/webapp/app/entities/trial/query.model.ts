
export class Query {
    constructor(public locations?:Array<string>,
                public conditions?:Array<string>,
                public genders?:Array<string>,
                public statuses?:Array<string>,
                public siteStatuses?:Array<string>,
                public phases?:Array<string>,
                public types?:Array<string>,
                public areas?:Array<string>,
                public sites?:Array<string>,
                public conditionStatuses?:Array<string>,
                public age?:number,
                public token?:string,
                public includeAncestors?:boolean) {
        this.conditions = [];
        this.locations = [];
        this.genders = [];
        this.statuses = [];
        this.siteStatuses = [];
        this.phases = [];
        this.types = [];
        this.areas = [];
        this.sites = [];
        this.conditionStatuses = [];
    }
}
