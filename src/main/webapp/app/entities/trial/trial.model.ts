import { BaseEntity } from './../../shared';

export const enum TrialStatus {
    'RECRUITING',
    ' SUSPENDED',
    ' STOPPED',
    ' COMPLETED',
    ' DRAFT',
    ' NOT_YET_RECRUITING',
    ' ENROLLING_BY_INVITATION',
    ' ACTIVE_NOT_RECRUITING',
    ' TERMINATED',
    ' WITHDRAWN',
    ' UNKNOWN'
}

export const enum StudyType {
    'INTERVENTIONAL',
    ' OBSERVATIONAL',
    ' PATIENT_REGISTRY'
}


import { Characteristic } from '../characteristic';
import { Organisation } from '../organisation';
import { Practitioner } from '../practitioner';
import { TrialCentre } from '../trial-centre';
import { Condition } from '../condition';
import { Intervention } from '../intervention';
import { OutcomeMeasure } from '../outcome-measure';
import { IdInfo } from '../id-info';
import { Contact } from '../contact/contact.model';

export class Trial  implements BaseEntity {

    constructor(
        public id?: string,
        public title?: string,
        public briefTitle?: string,
        public status?: TrialStatus,
        public type?: StudyType,
        public phases?: string[],
        public hasExpandedAccess?: boolean,
        public category?: string,
        public shortName?: string,
        public focus?: string,
        public keywords?: string[],
        public description?: string,
        public recordVerificationDate?: any,
        public startDate?: any,
        public primaryCompletionDate?: any,
        public studyCompletionDate?: any,
        public createdDate?: Date,
        public createdBy?: String,
        public lastModifiedDate?: Date,
        public reasonStopped?: string,
        public note?: string,
        public briefSummary?: string,
        public detailedDescription?: string,
        public sites?: TrialCentre[],
        public idInfo?: IdInfo,
        public eligibilities?: Characteristic[],
        public interventions?: Intervention[],
        public conditions?: Condition[],
        public chiefInvestigator?: Contact,
        public verified?: boolean
    ) {
        this.hasExpandedAccess = false;
        this.verified = false;
        this.phases = [];
        this.chiefInvestigator = new Contact();
    }
}
