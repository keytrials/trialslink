import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Trial } from './trial.model';
import { TrialPopupService } from './trial-popup.service';
import { TrialService } from './trial.service';

@Component({
    selector: 'jhi-trial-delete-dialog',
    templateUrl: './trial-delete-dialog.component.html'
})
export class TrialDeleteDialogComponent {

    trial: Trial;

    constructor(
        private trialService: TrialService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.trialService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'trialListModification',
                content: 'Deleted an trial'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-trial-delete-popup',
    template: ''
})
export class TrialDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private trialPopupService: TrialPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.trialPopupService
                .open(TrialDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
