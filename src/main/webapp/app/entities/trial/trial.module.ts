import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';
import { TrialslinkTrialCentreModule } from '../trial-centre/trial-centre.module';
import { TrialslinkIdInfoModule } from '../id-info/id-info.module';
import { TrialslinkCharacteristicModule } from '../characteristic/characteristic.module';
import { TrialslinkInterventionModule } from '../intervention/intervention.module';
import { TrialslinkConditionModule } from '../condition/condition.module';
import { TrialslinkContactModule } from '../contact/contact.module';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {
    TrialService,
    TrialPopupService,
    TrialComponent,
    TrialDetailComponent,
    TrialDialogComponent,
    TrialPopupComponent,
    TrialDeletePopupComponent,
    TrialDeleteDialogComponent,
    SearchResultComponent,
    trialRoute,
    trialPopupRoute,
} from './';
import {TitleFirstnameLastnamePipe} from "./title-firstname-lastname.pipe";

const ENTITY_STATES = [
    ...trialRoute,
    ...trialPopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        TrialslinkContactModule,
        TrialslinkCharacteristicModule,
        TrialslinkIdInfoModule,
        TrialslinkConditionModule,
        TrialslinkTrialCentreModule,
        MatSlideToggleModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TrialComponent,
        TrialDetailComponent,
        TrialDialogComponent,
        TrialDeleteDialogComponent,
        TrialPopupComponent,
        SearchResultComponent,
        TrialDeletePopupComponent,
        TitleFirstnameLastnamePipe
    ],
    entryComponents: [
        TrialComponent,
        TrialDialogComponent,
        TrialPopupComponent,
        TrialDeleteDialogComponent,
        TrialDeletePopupComponent,
    ],
    providers: [
        TrialService,
        TrialPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkTrialModule {}
