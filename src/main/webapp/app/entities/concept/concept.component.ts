import { Component, OnInit, OnDestroy } from '@angular/core';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Concept } from './concept.model';
import { ConceptService } from './concept.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-concept',
    templateUrl: './concept.component.html'
})
export class ConceptComponent implements OnInit, OnDestroy {
    concepts: Concept[];
    currentAccount: any;
    links: any;
    itemsPerPage: number;
    page: any;
    totalItems: number;
    eventSubscriber: Subscription;

    constructor(
        private conceptService: ConceptService,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private principal: Principal
    ) {
        this.concepts = [];
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
    }

    loadAll() {
        this.conceptService.query({
            page: this.page,
            size: this.itemsPerPage
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    
    loadPage(page) {
        this.page = page;
        this.loadAll();
    }
    
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInConcepts();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Concept) {
        return item.id;
    }

    indexAll() {
        this.conceptService.indexAll().subscribe(
            (res: Response) => {console.log('Indexed successfully')},
            (res: Response) => this.onError(res.json())
        );
    }

    registerChangeInConcepts() {
        this.eventSubscriber = this.eventManager.subscribe('conceptListModification', (response) => this.loadAll());
    }
    
    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        for (let i = 0; i < data.length; i++) {
            this.concepts.push(data[i]);
        }
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
