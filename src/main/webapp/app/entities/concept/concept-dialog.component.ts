import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Concept } from './concept.model';
import { ConceptPopupService } from './concept-popup.service';
import { ConceptService } from './concept.service';

@Component({
    selector: 'jhi-concept-dialog',
    templateUrl: './concept-dialog.component.html'
})
export class ConceptDialogComponent implements OnInit {

    concept: Concept;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private conceptService: ConceptService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.concept.id !== undefined) {
            this.subscribeToSaveResponse(
                this.conceptService.update(this.concept));
        } else {
            this.subscribeToSaveResponse(
                this.conceptService.create(this.concept));
        }
    }

    private subscribeToSaveResponse(result: Observable<Concept>) {
        result.subscribe((res: Concept) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Concept) {
        this.eventManager.broadcast({ name: 'conceptListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-concept-popup',
    template: ''
})
export class ConceptPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private conceptPopupService: ConceptPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.conceptPopupService
                    .open(ConceptDialogComponent as Component, params['id']);
            } else {
                this.conceptPopupService
                    .open(ConceptDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
