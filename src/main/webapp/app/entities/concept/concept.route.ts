import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ConceptComponent } from './concept.component';
import { ConceptDetailComponent } from './concept-detail.component';
import { ConceptPopupComponent } from './concept-dialog.component';
import { ConceptDeletePopupComponent } from './concept-delete-dialog.component';

export const conceptRoute: Routes = [
    {
        path: 'concept',
        component: ConceptComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.concept.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'concept/:id',
        component: ConceptDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.concept.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const conceptPopupRoute: Routes = [
    {
        path: 'concept-new',
        component: ConceptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.concept.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'concept/:id/edit',
        component: ConceptPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.concept.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'concept/:id/delete',
        component: ConceptDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.concept.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
