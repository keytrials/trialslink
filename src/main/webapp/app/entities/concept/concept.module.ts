import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';
import {
    ConceptService,
    ConceptPopupService,
    ConceptComponent,
    ConceptDetailComponent,
    ConceptDialogComponent,
    ConceptPopupComponent,
    ConceptDeletePopupComponent,
    ConceptDeleteDialogComponent,
    conceptRoute,
    conceptPopupRoute,
} from './';

const ENTITY_STATES = [
    ...conceptRoute,
    ...conceptPopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ConceptComponent,
        ConceptDetailComponent,
        ConceptDialogComponent,
        ConceptDeleteDialogComponent,
        ConceptPopupComponent,
        ConceptDeletePopupComponent,
    ],
    entryComponents: [
        ConceptComponent,
        ConceptDialogComponent,
        ConceptPopupComponent,
        ConceptDeleteDialogComponent,
        ConceptDeletePopupComponent,
    ],
    providers: [
        ConceptService,
        ConceptPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkConceptModule {}
