import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Concept } from './concept.model';
import { ConceptService } from './concept.service';

@Component({
    selector: 'jhi-concept-detail',
    templateUrl: './concept-detail.component.html'
})
export class ConceptDetailComponent implements OnInit, OnDestroy {

    concept: Concept;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private conceptService: ConceptService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInConcepts();
    }

    load(id) {
        this.conceptService.find(id).subscribe((concept) => {
            this.concept = concept;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInConcepts() {
        this.eventSubscriber = this.eventManager.subscribe(
            'conceptListModification',
            (response) => this.load(this.concept.id)
        );
    }
}
