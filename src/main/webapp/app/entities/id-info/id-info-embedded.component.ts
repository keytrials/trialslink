import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { IdInfo } from './id-info.model';
import { IdInfoService } from './id-info.service';
import { Trial } from '../trial/trial.model';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Rx';
import { of } from 'rxjs/observable/of';

@Component({
    selector: 'jhi-id-info-embedded',
    templateUrl: './id-info-embedded.component.html'
})
export class IdInfoEmbeddedComponent implements OnInit, OnDestroy {

    idInfo: IdInfo;
    @Input() trial: Trial;
    private subscription: any;
    dataSource: any;
    displayedColumns: string[];

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private idInfoService: IdInfoService,
        private route: ActivatedRoute
    ) {
        this.displayedColumns = ['name', 'identifier'];
    }

    ngOnInit() {
        //todo this subscription is not needed
        this.subscription = this.route.params.subscribe(params  => {
            this.idInfo = this.trial.idInfo;
            // generate ids by processing this trial's idInfo
            this.dataSource = new IdsDataSource(this.trial);
        });
    }

    load (id) {
        this.idInfoService.find(id).subscribe(idInfo => {
            this.idInfo = idInfo;
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}

export class IdsDataSource extends DataSource<any> {
    constructor(private trial: Trial) {
        super();
    }
    connect(): Observable<Array<any>> {

        let secondaryIds: Array<any> = [];
        Object.keys(this.trial.idInfo.secondaryIds).forEach(key => {
            let value = this.trial.idInfo.secondaryIds[key];
            // only add those ids that actually have a value
            if(value) {
                secondaryIds.push({'name': key, 'identifier': value});
            }
        });

        return of(secondaryIds);
    }

    disconnect() {}
}
