import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IdInfo } from './id-info.model';
import { IdInfoPopupService } from './id-info-popup.service';
import { IdInfoService } from './id-info.service';

@Component({
    selector: 'jhi-id-info-delete-dialog',
    templateUrl: './id-info-delete-dialog.component.html'
})
export class IdInfoDeleteDialogComponent {

    idInfo: IdInfo;

    constructor(
        private idInfoService: IdInfoService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.idInfoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'idInfoListModification',
                content: 'Deleted an idInfo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-id-info-delete-popup',
    template: ''
})
export class IdInfoDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private idInfoPopupService: IdInfoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.idInfoPopupService
                .open(IdInfoDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
