import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { IdInfo } from './id-info.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class IdInfoService {

    private resourceUrl = SERVER_API_URL + 'api/id-infos';

    constructor(private http: Http) { }

    create(idInfo: IdInfo): Observable<IdInfo> {
        const copy = this.convert(idInfo);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(idInfo: IdInfo): Observable<IdInfo> {
        const copy = this.convert(idInfo);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<IdInfo> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to IdInfo.
     */
    private convertItemFromServer(json: any): IdInfo {
        const entity: IdInfo = Object.assign(new IdInfo(), json);
        return entity;
    }

    /**
     * Convert a IdInfo to a JSON which can be sent to the server.
     */
    private convert(idInfo: IdInfo): IdInfo {
        const copy: IdInfo = Object.assign({}, idInfo);
        return copy;
    }
}
