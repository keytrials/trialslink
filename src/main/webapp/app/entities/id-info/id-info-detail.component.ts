import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { IdInfo } from './id-info.model';
import { IdInfoService } from './id-info.service';

@Component({
    selector: 'jhi-id-info-detail',
    templateUrl: './id-info-detail.component.html'
})
export class IdInfoDetailComponent implements OnInit, OnDestroy {

    idInfo: IdInfo;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private idInfoService: IdInfoService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInIdInfos();
    }

    load(id) {
        this.idInfoService.find(id).subscribe((idInfo) => {
            this.idInfo = idInfo;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInIdInfos() {
        this.eventSubscriber = this.eventManager.subscribe(
            'idInfoListModification',
            (response) => this.load(this.idInfo.id)
        );
    }
}
