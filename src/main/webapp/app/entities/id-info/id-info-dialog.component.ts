import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IdInfo } from './id-info.model';
import { IdInfoPopupService } from './id-info-popup.service';
import { IdInfoService } from './id-info.service';

@Component({
    selector: 'jhi-id-info-dialog',
    templateUrl: './id-info-dialog.component.html'
})
export class IdInfoDialogComponent implements OnInit {

    idInfo: IdInfo;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private idInfoService: IdInfoService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.idInfo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.idInfoService.update(this.idInfo));
        } else {
            this.subscribeToSaveResponse(
                this.idInfoService.create(this.idInfo));
        }
    }

    private subscribeToSaveResponse(result: Observable<IdInfo>) {
        result.subscribe((res: IdInfo) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: IdInfo) {
        this.eventManager.broadcast({ name: 'idInfoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-id-info-popup',
    template: ''
})
export class IdInfoPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private idInfoPopupService: IdInfoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.idInfoPopupService
                    .open(IdInfoDialogComponent as Component, params['id']);
            } else {
                this.idInfoPopupService
                    .open(IdInfoDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
