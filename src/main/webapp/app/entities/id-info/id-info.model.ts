import { BaseEntity } from './../../shared';

export class IdInfo implements BaseEntity {
    constructor(
        public id?: string,
        public protocolId?: string,
        public nctId?: string,
        public secondaryIds?: any
    ) {
    }
}
