import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';
import {
    IdInfoService,
    IdInfoPopupService,
    IdInfoComponent,
    IdInfoEmbeddedComponent,
    IdInfoDetailComponent,
    IdInfoDialogComponent,
    IdInfoPopupComponent,
    IdInfoDeletePopupComponent,
    IdInfoDeleteDialogComponent,
    idInfoRoute,
    idInfoPopupRoute,
} from './';

const ENTITY_STATES = [
    ...idInfoRoute,
    ...idInfoPopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        IdInfoComponent,
        IdInfoEmbeddedComponent,
        IdInfoDetailComponent,
        IdInfoDialogComponent,
        IdInfoDeleteDialogComponent,
        IdInfoPopupComponent,
        IdInfoDeletePopupComponent,
    ],
    entryComponents: [
        IdInfoComponent,
        IdInfoEmbeddedComponent,
        IdInfoDialogComponent,
        IdInfoPopupComponent,
        IdInfoDeleteDialogComponent,
        IdInfoDeletePopupComponent,
    ],
    providers: [
        IdInfoService,
        IdInfoPopupService,
    ],
    exports: [
        IdInfoEmbeddedComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkIdInfoModule {}
