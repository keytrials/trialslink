import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IdInfo } from './id-info.model';
import { IdInfoService } from './id-info.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-id-info',
    templateUrl: './id-info.component.html'
})
export class IdInfoComponent implements OnInit, OnDestroy {
idInfos: IdInfo[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private idInfoService: IdInfoService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.idInfoService.query().subscribe(
            (res: ResponseWrapper) => {
                this.idInfos = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInIdInfos();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IdInfo) {
        return item.id;
    }
    registerChangeInIdInfos() {
        this.eventSubscriber = this.eventManager.subscribe('idInfoListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
