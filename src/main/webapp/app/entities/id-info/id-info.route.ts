import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { IdInfoComponent } from './id-info.component';
import { IdInfoDetailComponent } from './id-info-detail.component';
import { IdInfoPopupComponent } from './id-info-dialog.component';
import { IdInfoDeletePopupComponent } from './id-info-delete-dialog.component';

export const idInfoRoute: Routes = [
    {
        path: 'id-info',
        component: IdInfoComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.idInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'id-info/:id',
        component: IdInfoDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.idInfo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const idInfoPopupRoute: Routes = [
    {
        path: 'id-info-new',
        component: IdInfoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.idInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'id-info/:id/edit',
        component: IdInfoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.idInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'id-info/:id/delete',
        component: IdInfoDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.idInfo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
