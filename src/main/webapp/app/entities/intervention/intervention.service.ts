import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Intervention } from './intervention.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class InterventionService {

    private resourceUrl = SERVER_API_URL +'api/interventions';
    private trialsResourceUrl = SERVER_API_URL +'api/trials';

    constructor(private http: Http) { }

    create(intervention: Intervention): Observable<Intervention> {
        let copy: Intervention = Object.assign({}, intervention);
        if (intervention.trialId !== undefined) {
            return this.http.post(`${this.trialsResourceUrl}/${intervention.trialId}/interventions`, copy).map((res: Response) => {
                const jsonResponse = res.json();
                return this.convertItemFromServer(jsonResponse);
            });
        } else {
            return this.http.post(this.resourceUrl, copy).map((res: Response) => {
                const jsonResponse = res.json();
                return this.convertItemFromServer(jsonResponse);
            });
        }
    }

    update(intervention: Intervention): Observable<Intervention> {
        const copy = this.convert(intervention);
        return this.http.put(`${this.trialsResourceUrl}/${intervention.trialId}/interventions`, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<Intervention> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    interventionsForTrial(id: string, req?: any): Observable<ResponseWrapper> {
        let options = createRequestOption(req);
        return this.http.get(`${this.trialsResourceUrl}/${id}/interventions`, options)
            .map((res: any) => this.convertResponseWithTrialId(res, id));
    }


    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponseWithTrialId(res: Response, id: string): ResponseWrapper {
        let jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            jsonResponse[i].trialId = id;
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Intervention.
     */
    private convertItemFromServer(json: any): Intervention {
        const entity: Intervention = Object.assign(new Intervention(), json);
        return entity;
    }

    /**
     * Convert a Intervention to a JSON which can be sent to the server.
     */
    private convert(intervention: Intervention): Intervention {
        const copy: Intervention = Object.assign({}, intervention);
        return copy;
    }
}
