import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Intervention } from './intervention.model';
import { InterventionService } from './intervention.service';

@Injectable()
export class InterventionPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private interventionService: InterventionService

    ) {
        this.ngbModalRef = null;
    }

    //open (component: Component, id?: number | any, createNew?: boolean): NgbModalRef {
    //    if (this.isOpen) {
    //        return;
    //    }
    //    this.isOpen = true;
    //
    //    if (!createNew) {
    //        this.interventionService.find(id).subscribe(intervention => {
    //            this.interventionModalRef(component, intervention);
    //        });
    //    } else {
    //        let intervention = new Intervention();
    //        intervention.trialId = id;
    //        return this.interventionModalRef(component, intervention);
    //    }

    open(component: Component, id?: number | any,  createNew?: boolean): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if  (!createNew) {
                this.interventionService.find(id).subscribe((intervention) => {
                    this.ngbModalRef = this.interventionModalRef(component, intervention);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.interventionModalRef(component, new Intervention());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    interventionModalRef(component: Component, intervention: Intervention): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.intervention = intervention;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
