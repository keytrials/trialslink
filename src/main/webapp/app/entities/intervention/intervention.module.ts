import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';
import {
    InterventionService,
    InterventionPopupService,
    InterventionComponent,
    InterventionListComponent,
    InterventionDetailComponent,
    InterventionDialogComponent,
    InterventionPopupComponent,
    InterventionDeletePopupComponent,
    InterventionDeleteDialogComponent,
    interventionRoute,
    interventionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...interventionRoute,
    ...interventionPopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        InterventionComponent,
        InterventionListComponent,
        InterventionDetailComponent,
        InterventionDialogComponent,
        InterventionDeleteDialogComponent,
        InterventionPopupComponent,
        InterventionDeletePopupComponent,
    ],
    entryComponents: [
        InterventionComponent,
        InterventionListComponent,
        InterventionDialogComponent,
        InterventionPopupComponent,
        InterventionDeleteDialogComponent,
        InterventionDeletePopupComponent,
    ],
    providers: [
        InterventionService,
        InterventionPopupService,
    ],
    exports: [
        InterventionListComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkInterventionModule {}
