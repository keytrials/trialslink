import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Intervention } from './intervention.model';
import { InterventionPopupService } from './intervention-popup.service';
import { InterventionService } from './intervention.service';

@Component({
    selector: 'jhi-intervention-dialog',
    templateUrl: './intervention-dialog.component.html'
})
export class InterventionDialogComponent implements OnInit {

    states = ['Alabama', 'Alaska', 'American Samoa', 'Arizona', 'Arkansas', 'California', 'Colorado',
    'Connecticut', 'Delaware', 'District Of Columbia', 'Federated States Of Micronesia', 'Florida', 'Georgia',
    'Guam', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine',
    'Marshall Islands', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana',
    'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
    'Northern Mariana Islands', 'Ohio', 'Oklahoma', 'Oregon', 'Palau', 'Pennsylvania', 'Puerto Rico', 'Rhode Island',
    'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virgin Islands', 'Virginia',
    'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];

    intervention: Intervention;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private interventionService: InterventionService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.intervention.id !== undefined) {
            this.subscribeToSaveResponse(
                this.interventionService.update(this.intervention));
        } else {
            this.subscribeToSaveResponse(
                this.interventionService.create(this.intervention));
        }
    }

    search = (text$: Observable<string>) =>
        text$
            .debounceTime(200)
            .distinctUntilChanged()
            .map(term => term.length < 2 ? []
                : this.states.filter(v => new RegExp(term, 'gi').test(v)).splice(0, 10));

    private subscribeToSaveResponse(result: Observable<Intervention>) {
        result.subscribe((res: Intervention) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Intervention) {
        this.eventManager.broadcast({ name: 'interventionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-intervention-popup',
    template: ''
})
export class InterventionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private interventionPopupService: InterventionPopupService
    ) {}

    ngOnInit() {
        //this.routeSub = this.route.params.subscribe((params) => {
        //    if ( params['id'] ) {
        //        this.modalRef = this.interventionPopupService
        //            .open(InterventionDialogComponent, params['id'], false);
        //    } else {
        //        this.modalRef = this.interventionPopupService
        //            .open(InterventionDialogComponent, params['trialId'], true);
        //    }
        //});

        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.interventionPopupService
                    .open(InterventionDialogComponent as Component, params['id'], false);
            } else {
                this.interventionPopupService
                    .open(InterventionDialogComponent as Component, params['trialId'], true);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
