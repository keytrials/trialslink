import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import * as _ from 'underscore';
import { SERVER_API_URL } from '../../app.constants';

import { Organisation } from './organisation.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class OrganisationService {

    private resourceUrl = SERVER_API_URL + 'api/organisations';

    constructor(private http: Http) { }

    create(organisation: Organisation): Observable<Organisation> {
        const copy = this.convert(organisation);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(organisation: Organisation): Observable<Organisation> {
        const copy = this.convert(organisation);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<Organisation> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    allAsFilter(req?: any): Observable<Response> {
        let options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: any) => this.convertToFilterResponse(res));
    }


    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertToFilterResponse(res: any): any {
        let jsonResponse = res.json();
        let results = [];
        _.each(jsonResponse, function(item: any){
            let o = <any>{};
            o.id = item.id;
            o.text = item.name;
            results.push(o);
        });
        res._body = results;
        return res;
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Organisation.
     */
    private convertItemFromServer(json: any): Organisation {
        const entity: Organisation = Object.assign(new Organisation(), json);
        return entity;
    }

    /**
     * Convert a Organisation to a JSON which can be sent to the server.
     */
    private convert(organisation: Organisation): Organisation {
        const copy: Organisation = Object.assign({}, organisation);
        return copy;
    }
}
