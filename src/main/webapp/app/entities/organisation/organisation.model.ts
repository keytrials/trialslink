import { Address } from '../address/address.model';
import { BaseEntity } from './../../shared';

export class Organisation implements BaseEntity {
    constructor(
        public id?: string,
        public name?: string,
        public address?: Address
    ) {
    }
}
