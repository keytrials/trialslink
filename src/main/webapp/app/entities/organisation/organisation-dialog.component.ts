import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Organisation } from './organisation.model';
import { OrganisationPopupService } from './organisation-popup.service';
import { OrganisationService } from './organisation.service';

@Component({
    selector: 'jhi-organisation-dialog',
    templateUrl: './organisation-dialog.component.html'
})
export class OrganisationDialogComponent implements OnInit {

    organisation: Organisation;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private organisationService: OrganisationService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.organisation.id !== undefined) {
            this.subscribeToSaveResponse(
                this.organisationService.update(this.organisation));
        } else {
            this.subscribeToSaveResponse(
                this.organisationService.create(this.organisation));
        }
    }

    private subscribeToSaveResponse(result: Observable<Organisation>) {
        result.subscribe((res: Organisation) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Organisation) {
        this.eventManager.broadcast({ name: 'organisationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-organisation-popup',
    template: ''
})
export class OrganisationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private organisationPopupService: OrganisationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.organisationPopupService
                    .open(OrganisationDialogComponent as Component, params['id']);
            } else {
                this.organisationPopupService
                    .open(OrganisationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
