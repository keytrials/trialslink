import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { SavedSearchComponent } from './saved-search.component';
import { SavedSearchDetailComponent } from './saved-search-detail.component';
import { SavedSearchPopupComponent } from './saved-search-dialog.component';
import { SavedSearchDeletePopupComponent } from './saved-search-delete-dialog.component';

@Injectable()
export class SavedSearchResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const savedSearchRoute: Routes = [
    {
        path: 'saved-search',
        component: SavedSearchComponent,
        resolve: {
            'pagingParams': SavedSearchResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.savedSearch.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'saved-search/:id',
        component: SavedSearchDetailComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.savedSearch.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const savedSearchPopupRoute: Routes = [
    {
        path: 'saved-search-new',
        component: SavedSearchPopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.savedSearch.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'saved-search/:id/edit',
        component: SavedSearchPopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.savedSearch.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'saved-search/:id/delete',
        component: SavedSearchDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.savedSearch.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
