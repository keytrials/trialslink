import {Query} from "../trial/query.model";
import { BaseEntity } from './../../shared';

export class SavedSearch implements BaseEntity {
    constructor(
        public id?: string,
        public queryModel?: Query,
        public omniSearchString?: string,
        public createdAt?: any,
        public updatedAt?: any,
        public url?: string,
        public hash?: string,
        public createdBy?: string,
        public shouldNotify?: boolean,
        public title?: string,
    ) {
        this.shouldNotify = false;
    }
}
