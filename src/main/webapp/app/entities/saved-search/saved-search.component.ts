import {Component, OnInit, OnDestroy} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Response } from '@angular/http';
import { Subscription } from 'rxjs/Rx';
import {JhiEventManager, JhiParseLinks, JhiPaginationUtil, JhiAlertService, JhiLanguageService} from 'ng-jhipster';

import { SavedSearch } from './saved-search.model';
import { SavedSearchService } from './saved-search.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import {Observable} from "rxjs/Observable";

@Component({
    selector: 'jhi-saved-search',
    templateUrl: './saved-search.component.html'
})
export class SavedSearchComponent implements OnInit, OnDestroy {

currentAccount: any;
    savedSearches: SavedSearch[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;

    constructor(
        private savedSearchService: SavedSearchService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
        //private languageService: JhiLanguageService,
        //private paginationUtil: PaginationUtil,
        //private paginationConfig: PaginationConfig
    ) {
    //    this.itemsPerPage = ITEMS_PER_PAGE;
    //    this.languageService.setLocations(['savedSearch']);
    //    this.routeData = this.activatedRoute.data.subscribe((data) => {
    //        this.page = data['pagingParams'].page;
    //        this.previousPage = data['pagingParams'].page;
    //        this.reverse = data['pagingParams'].ascending;
    //        this.predicate = data['pagingParams'].predicate;
    //) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
    }

    loadAll() {
        this.savedSearchService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }

    transition() {
        this.router.navigate(['/saved-search'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    toggleNotificationPreference(event, savedSearch: SavedSearch) {
        savedSearch.shouldNotify = event;
        this.subscribeToSaveResponse(this.savedSearchService.update(savedSearch));
    }

    private subscribeToSaveResponse(result: Observable<SavedSearch>) {
        result.subscribe((res: SavedSearch) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(savedSearch: SavedSearch) {
        this.eventManager.broadcast({ name: 'savedSearchListModification', content: 'OK'});
        if (savedSearch.shouldNotify) {
            this.alertService.success("You will now receive notifications for: " + savedSearch.title);
        } else {
            this.alertService.success("You will no longer receive notifications for: " + savedSearch.title);
        }
    }

    private onSaveError(error) {
        this.alertService.error("\"Error saving your notification preferences. Please try again!\"");
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/saved-search', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSavedSearches();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SavedSearch) {
        return item.id;
    }
    registerChangeInSavedSearches() {
        this.eventSubscriber = this.eventManager.subscribe('savedSearchListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.savedSearches = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
