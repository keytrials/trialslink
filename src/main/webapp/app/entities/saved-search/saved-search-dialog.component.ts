import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SavedSearch } from './saved-search.model';
import { SavedSearchPopupService } from './saved-search-popup.service';
import { SavedSearchService } from './saved-search.service';

@Component({
    selector: 'jhi-saved-search-dialog',
    templateUrl: './saved-search-dialog.component.html'
})
export class SavedSearchDialogComponent implements OnInit {

    savedSearch: SavedSearch;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private savedSearchService: SavedSearchService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.savedSearch.id !== undefined) {
            this.subscribeToSaveResponse(
                this.savedSearchService.update(this.savedSearch));
        } else {
            this.subscribeToSaveResponse(
                this.savedSearchService.create(this.savedSearch));
        }
    }

    private subscribeToSaveResponse(result: Observable<SavedSearch>) {
        result.subscribe((res: SavedSearch) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: SavedSearch) {
        this.eventManager.broadcast({ name: 'savedSearchListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-saved-search-popup',
    template: ''
})
export class SavedSearchPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private savedSearchPopupService: SavedSearchPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.savedSearchPopupService
                    .open(SavedSearchDialogComponent as Component, params['id']);
            } else {
                this.savedSearchPopupService
                    .open(SavedSearchDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
