import { Injectable } from '@angular/core';
import {BaseRequestOptions, Http, Response, URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { SavedSearch } from './saved-search.model';
import { SERVER_API_URL } from '../../app.constants';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class SavedSearchService {

    private resourceUrl = SERVER_API_URL + 'api/saved-searches';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(savedSearch: SavedSearch): Observable<SavedSearch> {
        const copy = this.convert(savedSearch);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(savedSearch: SavedSearch): Observable<SavedSearch> {
        const copy = this.convert(savedSearch);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<SavedSearch> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

//    query(req?: any): Observable<Response> {
//        const options = this.createRequestOption(req);
//=======
//            return this.convertItemFromServer(jsonResponse);
//        });
//    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    //private convertResponse(res: Response): Response {
    //    const jsonResponse = res.json();
    //    for (let i = 0; i < jsonResponse.length; i++) {
    //        this.convertItemFromServer(jsonResponse[i]);
    //    }
    //    return res;
    //}

    private convert(savedSearch: SavedSearch): SavedSearch {
        const copy: SavedSearch = Object.assign({}, savedSearch);

        copy.createdAt = this.dateUtils.toDate(savedSearch.createdAt+ ' ');

        copy.updatedAt = this.dateUtils.toDate(savedSearch.updatedAt+ ' ');
        return copy;
    }

//    private createRequestOption(req?: any): BaseRequestOptions {
//        let options: BaseRequestOptions = new BaseRequestOptions();
//        if (req) {
//            let params: URLSearchParams = new URLSearchParams();
//            params.set('page', req.page);
//            params.set('size', req.size);
//            if (req.sort) {
//                params.paramsMap.set('sort', req.sort);
//            }
//            params.set('q', req.query);
//=======
    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to SavedSearch.
     */
    private convertItemFromServer(json: any): SavedSearch {
        const entity: SavedSearch = Object.assign(new SavedSearch(), json);
        entity.createdAt = this.dateUtils
            .convertDateTimeFromServer(json.createdAt);
        entity.updatedAt = this.dateUtils
            .convertDateTimeFromServer(json.updatedAt);
        return entity;
    }
}
