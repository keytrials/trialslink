import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { SavedSearch } from './saved-search.model';
import { SavedSearchService } from './saved-search.service';

@Injectable()
export class SavedSearchPopupService {
    private isOpen = false;
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private savedSearchService: SavedSearchService

    ) {
        this.ngbModalRef = null;
    }

//    open(component: Component, id?: number | any): NgbModalRef {
//        if (this.isOpen) {
//            return;
//        }
//        this.isOpen = true;
//
//        if (id) {
//            this.savedSearchService.find(id).subscribe((savedSearch) => {
//                savedSearch.createdAt = this.datePipe
//                    .transform(savedSearch.createdAt, 'yyyy-MM-ddThh:mm');
//                savedSearch.updatedAt = this.datePipe
//                    .transform(savedSearch.updatedAt, 'yyyy-MM-ddThh:mm');
//                this.savedSearchModalRef(component, savedSearch);
//            });
//        } else {
//            return this.savedSearchModalRef(component, new SavedSearch());
//        }
//=======
    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.savedSearchService.find(id).subscribe((savedSearch) => {
                    savedSearch.createdAt = this.datePipe
                        .transform(savedSearch.createdAt, 'yyyy-MM-ddTHH:mm:ss');
                    savedSearch.updatedAt = this.datePipe
                        .transform(savedSearch.updatedAt, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.savedSearchModalRef(component, savedSearch);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.savedSearchModalRef(component, new SavedSearch());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    savedSearchModalRef(component: Component, savedSearch: SavedSearch): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.savedSearch = savedSearch;
        modalRef.result.then((result) => {
//<<<<<<< HEAD
//            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
//            this.isOpen = false;
//=======
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
