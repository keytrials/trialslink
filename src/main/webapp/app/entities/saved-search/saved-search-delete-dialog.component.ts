import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SavedSearch } from './saved-search.model';
import { SavedSearchPopupService } from './saved-search-popup.service';
import { SavedSearchService } from './saved-search.service';

@Component({
    selector: 'jhi-saved-search-delete-dialog',
    templateUrl: './saved-search-delete-dialog.component.html'
})
export class SavedSearchDeleteDialogComponent {

    savedSearch: SavedSearch;

    constructor(
        private savedSearchService: SavedSearchService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.savedSearchService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'savedSearchListModification',
                content: 'Deleted an savedSearch'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-saved-search-delete-popup',
    template: ''
})
export class SavedSearchDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private savedSearchPopupService: SavedSearchPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.savedSearchPopupService
                .open(SavedSearchDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
