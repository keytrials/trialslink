import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { SecondaryId } from './secondary-id.model';
import { SecondaryIdService } from './secondary-id.service';

@Component({
    selector: 'jhi-secondary-id-detail',
    templateUrl: './secondary-id-detail.component.html'
})
export class SecondaryIdDetailComponent implements OnInit, OnDestroy {

    secondaryId: SecondaryId;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private secondaryIdService: SecondaryIdService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSecondaryIds();
    }

    load(id) {
        this.secondaryIdService.find(id).subscribe((secondaryId) => {
            this.secondaryId = secondaryId;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSecondaryIds() {
        this.eventSubscriber = this.eventManager.subscribe(
            'secondaryIdListModification',
            (response) => this.load(this.secondaryId.id)
        );
    }
}
