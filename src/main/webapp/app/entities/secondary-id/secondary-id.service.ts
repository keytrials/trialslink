import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { SecondaryId } from './secondary-id.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class SecondaryIdService {

    private resourceUrl = SERVER_API_URL + 'api/secondary-ids';

    constructor(private http: Http) { }

    create(secondaryId: SecondaryId): Observable<SecondaryId> {
        const copy = this.convert(secondaryId);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(secondaryId: SecondaryId): Observable<SecondaryId> {
        const copy = this.convert(secondaryId);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<SecondaryId> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to SecondaryId.
     */
    private convertItemFromServer(json: any): SecondaryId {
        const entity: SecondaryId = Object.assign(new SecondaryId(), json);
        return entity;
    }

    /**
     * Convert a SecondaryId to a JSON which can be sent to the server.
     */
    private convert(secondaryId: SecondaryId): SecondaryId {
        const copy: SecondaryId = Object.assign({}, secondaryId);
        return copy;
    }
}
