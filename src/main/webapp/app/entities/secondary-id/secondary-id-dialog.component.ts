import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SecondaryId } from './secondary-id.model';
import { SecondaryIdPopupService } from './secondary-id-popup.service';
import { SecondaryIdService } from './secondary-id.service';

@Component({
    selector: 'jhi-secondary-id-dialog',
    templateUrl: './secondary-id-dialog.component.html'
})
export class SecondaryIdDialogComponent implements OnInit {

    secondaryId: SecondaryId;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private secondaryIdService: SecondaryIdService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.secondaryId.id !== undefined) {
            this.subscribeToSaveResponse(
                this.secondaryIdService.update(this.secondaryId));
        } else {
            this.subscribeToSaveResponse(
                this.secondaryIdService.create(this.secondaryId));
        }
    }

    private subscribeToSaveResponse(result: Observable<SecondaryId>) {
        result.subscribe((res: SecondaryId) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: SecondaryId) {
        this.eventManager.broadcast({ name: 'secondaryIdListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-secondary-id-popup',
    template: ''
})
export class SecondaryIdPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private secondaryIdPopupService: SecondaryIdPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.secondaryIdPopupService
                    .open(SecondaryIdDialogComponent as Component, params['id']);
            } else {
                this.secondaryIdPopupService
                    .open(SecondaryIdDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
