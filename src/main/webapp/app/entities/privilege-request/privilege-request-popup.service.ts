import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { PrivilegeRequest } from './privilege-request.model';
import { PrivilegeRequestService } from './privilege-request.service';

@Injectable()
export class PrivilegeRequestPopupService {
    private isOpen = false;
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private privilegeRequestService: PrivilegeRequestService

    ) {
        this.ngbModalRef = null;
    }

//    open(component: Component, id?: number | any): NgbModalRef {
//        if (this.isOpen) {
//            return;
//        }
//        this.isOpen = true;
//
//        if (id) {
//            this.privilegeRequestService.find(id).subscribe((privilegeRequest) => {
//                privilegeRequest.createdDate = this.datePipe
//                    .transform(privilegeRequest.createdDate, 'yyyy-MM-ddThh:mm');
//                privilegeRequest.lastModifiedDate = this.datePipe
//                    .transform(privilegeRequest.lastModifiedDate, 'yyyy-MM-ddThh:mm');
//                this.privilegeRequestModalRef(component, privilegeRequest);
//            });
//        } else {
//            return this.privilegeRequestModalRef(component, new PrivilegeRequest());
//        }
//=======
    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.privilegeRequestService.find(id).subscribe((privilegeRequest) => {
                    privilegeRequest.createdDate = this.datePipe
                        .transform(privilegeRequest.createdDate, 'yyyy-MM-ddTHH:mm:ss');
                    privilegeRequest.lastModifiedDate = this.datePipe
                        .transform(privilegeRequest.lastModifiedDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.privilegeRequestModalRef(component, privilegeRequest);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.privilegeRequestModalRef(component, new PrivilegeRequest());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    privilegeRequestModalRef(component: Component, privilegeRequest: PrivilegeRequest): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.privilegeRequest = privilegeRequest;
        modalRef.result.then((result) => {
//<<<<<<< HEAD
//            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
//            this.isOpen = false;
//=======
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
