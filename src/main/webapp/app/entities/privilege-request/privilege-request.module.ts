import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';
import {
    PrivilegeRequestService,
    PrivilegeRequestPopupService,
    PrivilegeRequestComponent,
    PrivilegeRequestDetailComponent,
    PrivilegeRequestDialogComponent,
    PrivilegeRequestPopupComponent,
    PrivilegeRequestDeletePopupComponent,
    PrivilegeRequestDeleteDialogComponent,
    privilegeRequestRoute,
    privilegeRequestPopupRoute,
    PrivilegeRequestResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...privilegeRequestRoute,
    ...privilegeRequestPopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        PrivilegeRequestComponent,
        PrivilegeRequestDetailComponent,
        PrivilegeRequestDialogComponent,
        PrivilegeRequestDeleteDialogComponent,
        PrivilegeRequestPopupComponent,
        PrivilegeRequestDeletePopupComponent,
    ],
    entryComponents: [
        PrivilegeRequestComponent,
        PrivilegeRequestDialogComponent,
        PrivilegeRequestPopupComponent,
        PrivilegeRequestDeleteDialogComponent,
        PrivilegeRequestDeletePopupComponent,
    ],
    providers: [
        PrivilegeRequestService,
        PrivilegeRequestPopupService,
        PrivilegeRequestResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkPrivilegeRequestModule {}
