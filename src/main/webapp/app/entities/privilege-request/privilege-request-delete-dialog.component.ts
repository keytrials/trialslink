import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PrivilegeRequest } from './privilege-request.model';
import { PrivilegeRequestPopupService } from './privilege-request-popup.service';
import { PrivilegeRequestService } from './privilege-request.service';

@Component({
    selector: 'jhi-privilege-request-delete-dialog',
    templateUrl: './privilege-request-delete-dialog.component.html'
})
export class PrivilegeRequestDeleteDialogComponent {

    privilegeRequest: PrivilegeRequest;

    constructor(
        private privilegeRequestService: PrivilegeRequestService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.privilegeRequestService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'privilegeRequestListModification',
                content: 'Deleted an privilegeRequest'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-privilege-request-delete-popup',
    template: ''
})
export class PrivilegeRequestDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private privilegeRequestPopupService: PrivilegeRequestPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.privilegeRequestPopupService
                .open(PrivilegeRequestDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
