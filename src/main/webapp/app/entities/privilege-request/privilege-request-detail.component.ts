import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { PrivilegeRequest } from './privilege-request.model';
import { PrivilegeRequestService } from './privilege-request.service';

@Component({
    selector: 'jhi-privilege-request-detail',
    templateUrl: './privilege-request-detail.component.html'
})
export class PrivilegeRequestDetailComponent implements OnInit, OnDestroy {

    privilegeRequest: PrivilegeRequest;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private privilegeRequestService: PrivilegeRequestService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPrivilegeRequests();
    }

    load(id) {
        this.privilegeRequestService.find(id).subscribe((privilegeRequest) => {
            this.privilegeRequest = privilegeRequest;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPrivilegeRequests() {
        this.eventSubscriber = this.eventManager.subscribe(
            'privilegeRequestListModification',
            (response) => this.load(this.privilegeRequest.id)
        );
    }
}
