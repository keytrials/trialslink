import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PrivilegeRequestComponent } from './privilege-request.component';
import { PrivilegeRequestDetailComponent } from './privilege-request-detail.component';
import { PrivilegeRequestPopupComponent } from './privilege-request-dialog.component';
import { PrivilegeRequestDeletePopupComponent } from './privilege-request-delete-dialog.component';

@Injectable()
export class PrivilegeRequestResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const privilegeRequestRoute: Routes = [
    {
        path: 'privilege-request',
        component: PrivilegeRequestComponent,
        resolve: {
            'pagingParams': PrivilegeRequestResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.privilegeRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'privilege-request/:id',
        component: PrivilegeRequestDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.privilegeRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const privilegeRequestPopupRoute: Routes = [
    {
        path: 'privilege-request-new',
        component: PrivilegeRequestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.privilegeRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'privilege-request/:id/edit',
        component: PrivilegeRequestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.privilegeRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'privilege-request/:id/delete',
        component: PrivilegeRequestDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.privilegeRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
