import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { PrivilegeRequest } from './privilege-request.model';
import { PrivilegeRequestPopupService } from './privilege-request-popup.service';
import { PrivilegeRequestService } from './privilege-request.service';

@Component({
    selector: 'jhi-privilege-request-dialog',
    templateUrl: './privilege-request-dialog.component.html'
})
export class PrivilegeRequestDialogComponent implements OnInit {

    privilegeRequest: PrivilegeRequest;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private privilegeRequestService: PrivilegeRequestService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.privilegeRequest.id !== undefined) {
            this.subscribeToSaveResponse(
                this.privilegeRequestService.update(this.privilegeRequest));
        } else {
            this.subscribeToSaveResponse(
                this.privilegeRequestService.create(this.privilegeRequest));
        }
    }

    private subscribeToSaveResponse(result: Observable<PrivilegeRequest>) {
        result.subscribe((res: PrivilegeRequest) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: PrivilegeRequest) {
        this.eventManager.broadcast({ name: 'privilegeRequestListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-privilege-request-popup',
    template: ''
})
export class PrivilegeRequestPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private privilegeRequestPopupService: PrivilegeRequestPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.privilegeRequestPopupService
                    .open(PrivilegeRequestDialogComponent as Component, params['id']);
            } else {
                this.privilegeRequestPopupService
                    .open(PrivilegeRequestDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
