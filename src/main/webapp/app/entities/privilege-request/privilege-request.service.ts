import { Injectable } from '@angular/core';
import {BaseRequestOptions, Http, Response, URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { PrivilegeRequest } from './privilege-request.model';
import { SERVER_API_URL } from '../../app.constants';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PrivilegeRequestService {

    private resourceUrl = SERVER_API_URL + 'api/privilege-requests';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(privilegeRequest: PrivilegeRequest): Observable<PrivilegeRequest> {
        const copy = this.convert(privilegeRequest);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(privilegeRequest: PrivilegeRequest): Observable<PrivilegeRequest> {
        const copy = this.convert(privilegeRequest);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<PrivilegeRequest> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    //private convertResponse(res: Response): Response {
    //    const jsonResponse = res.json();
    //    for (let i = 0; i < jsonResponse.length; i++) {
    //        this.convertItemFromServer(jsonResponse[i]);
    //    }
    //    return res;
    //    // return new Response(res.headers, jsonResponse, res.status);
    //}

    private convertItemFromServer(entity: any) {
        //const entity: PrivilegeRequest = Object.assign(new PrivilegeRequest(), json);
        entity.createdDate = this.dateUtils
            .convertDateTimeFromServer(entity.createdDate);
        entity.lastModifiedDate = this.dateUtils
            .convertDateTimeFromServer(entity.lastModifiedDate);
        return entity;
    }

    private convert(privilegeRequest: PrivilegeRequest): PrivilegeRequest {
        const copy: PrivilegeRequest = Object.assign({}, privilegeRequest);

        copy.createdDate = this.dateUtils.toDate(privilegeRequest.createdDate);

        copy.lastModifiedDate = this.dateUtils.toDate(privilegeRequest.lastModifiedDate);
        return copy;
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }
}
