import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import {PrivilegeRequest, PrivilegeRequestStatus} from './privilege-request.model';
import { PrivilegeRequestService } from './privilege-request.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-privilege-request',
    templateUrl: './privilege-request.component.html'
})
export class PrivilegeRequestComponent implements OnInit, OnDestroy {

currentAccount: any;
    privilegeRequests: PrivilegeRequest[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    privilegeRequestStatus: any;

    constructor(
        private privilegeRequestService: PrivilegeRequestService,
        private parseLinks: JhiParseLinks,
        private alertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager
    ) {
        //this.jhiLanguageService.setLocations(['privilegeRequest', 'privilegeRequestStatus']);
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            //this.page = data['pagingParams'].page;
            //this.previousPage = data['pagingParams'].page;
            //this.reverse = data['pagingParams'].ascending;
            //this.predicate = data['pagingParams'].predicate;
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
    }

    loadAll() {
        this.privilegeRequestService.query({
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort()}).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/privilege-request'], {queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/privilege-request', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }

    approvePrivilegeRequest(privilegeRequest:PrivilegeRequest) {
        privilegeRequest.status = this.privilegeRequestStatus[PrivilegeRequestStatus.APPROVED];
        this.privilegeRequestService.update(privilegeRequest).subscribe(
            (res: PrivilegeRequest) => {
                this.alertService.success("Privilege request for \"" + res.createdBy + "\" has been approved");
                this.registerChangeInPrivilegeRequests();
            }, (error) => {
                this.alertService.error("Error approving privilege request");
            });
    }

    declinePrivilegeRequest(privilegeRequest:PrivilegeRequest) {
        privilegeRequest.status = this.privilegeRequestStatus[PrivilegeRequestStatus.REJECTED];
        this.privilegeRequestService.update(privilegeRequest).subscribe(
            (res: PrivilegeRequest) => {
                this.alertService.success("Privilege request for \"" + res.createdBy + "\" has been declined");
                this.registerChangeInPrivilegeRequests();
            }, (error) => {
                this.alertService.error("Error declining privilege request");
            });
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPrivilegeRequests();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PrivilegeRequest) {
        return item.id;
    }
    registerChangeInPrivilegeRequests() {
        this.eventSubscriber = this.eventManager.subscribe('privilegeRequestListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.privilegeRequests = data;
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
