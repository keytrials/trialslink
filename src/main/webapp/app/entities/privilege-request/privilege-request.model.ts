import { BaseEntity } from './../../shared';

export enum PrivilegeRequestStatus {
    'PENDING',
    'APPROVED',
    'REJECTED'
}

export class PrivilegeRequest implements BaseEntity {
    constructor(
        public id?: string,
        public status?: PrivilegeRequestStatus,
        public createdDate?:any,
        public createdBy?:string,
        public lastModifiedDate?:any,
        public lastModifiedBy?:string
    ) {
    }
}
