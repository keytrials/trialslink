export class Day {
    constructor(public name:string,
                public startTime?:string,
                public endTime?: string) {

    }
}
