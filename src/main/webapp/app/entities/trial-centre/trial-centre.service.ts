import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { TrialCentre } from './trial-centre.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class TrialCentreService {

    private resourceUrl = SERVER_API_URL + 'api/trial-centres';
    private trialsResourceUrl = SERVER_API_URL + 'api/trials';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(trialCentre: TrialCentre): Observable<TrialCentre> {
        //let copy: TrialCentre = Object.assign({}, trialCentre);
        //copy.startDate = this.dateUtils.toDate(trialCentre.startDate);
        //copy.endDate = this.dateUtils.toDate(trialCentre.endDate);
        const copy = this.convert(trialCentre);
        if (trialCentre.trialId !== undefined) {
            return this.http.post(`${this.trialsResourceUrl}/${trialCentre.trialId}/centres`, copy).map((res: Response) => {
                const jsonResponse = res.json();
                return this.convertItemFromServer(jsonResponse);
            });
        } else {
            return this.http.post(this.resourceUrl, copy).map((res: Response) => {
                const jsonResponse = res.json();
                return this.convertItemFromServer(jsonResponse);
            });
        }
    }

    update(trialCentre: TrialCentre): Observable<TrialCentre> {
        const copy = this.convert(trialCentre);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<TrialCentre> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    centresForTrial(id: string, req?: any): Observable<ResponseWrapper> {
        let options = createRequestOption(req);
        return this.http.get(`${this.trialsResourceUrl}/${id}/centres`, options)
            .map((res: any) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to TrialCentre.
     */
    private convertItemFromServer(json: any): TrialCentre {
        const entity: TrialCentre = Object.assign(new TrialCentre(), json);
        entity.startDate = this.dateUtils
            .convertDateTimeFromServer(json.startDate);
        entity.endDate = this.dateUtils
            .convertDateTimeFromServer(json.endDate);
        return entity;
    }

    /**
     * Convert a TrialCentre to a JSON which can be sent to the server.
     */
    private convert(trialCentre: TrialCentre): TrialCentre {
        const copy: TrialCentre = Object.assign({}, trialCentre);

        copy.startDate = this.dateUtils.toDate(trialCentre.startDate);

        copy.endDate = this.dateUtils.toDate(trialCentre.endDate);
        return copy;
    }
}
