import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TrialCentre } from './trial-centre.model';
import { TrialCentrePopupService } from './trial-centre-popup.service';
import { TrialCentreService } from './trial-centre.service';

@Component({
    selector: 'jhi-trial-centre-delete-dialog',
    templateUrl: './trial-centre-delete-dialog.component.html'
})
export class TrialCentreDeleteDialogComponent {

    trialCentre: TrialCentre;

    constructor(
        private trialCentreService: TrialCentreService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.trialCentreService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'trialCentreDeleted',
                content: 'Deleted an trialCentre'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-trial-centre-delete-popup',
    template: ''
})
export class TrialCentreDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private trialCentrePopupService: TrialCentrePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.trialCentrePopupService
                .open(TrialCentreDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
