import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';
import { TrialslinkContactModule } from '../contact/contact.module';

import {
    TrialCentreService,
    TrialCentrePopupService,
    TrialCentreComponent,
    TrialCentreListComponent,
    TrialCentreCardComponent,
    TrialCentreDetailComponent,
    TrialCentreDialogComponent,
    TrialCentrePopupComponent,
    TrialCentreDeletePopupComponent,
    TrialCentreDeleteDialogComponent,
    trialCentreRoute,
    trialCentrePopupRoute,
} from './';

const ENTITY_STATES = [
    ...trialCentreRoute,
    ...trialCentrePopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        TrialslinkContactModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TrialCentreComponent,
        TrialCentreListComponent,
        TrialCentreDetailComponent,
        TrialCentreDialogComponent,
        TrialCentreDeleteDialogComponent,
        TrialCentrePopupComponent,
        TrialCentreDeletePopupComponent,
        TrialCentreCardComponent
    ],
    entryComponents: [
        TrialCentreComponent,
        TrialCentreListComponent,
        TrialCentreCardComponent,
        TrialCentreDialogComponent,
        TrialCentrePopupComponent,
        TrialCentreDeleteDialogComponent,
        TrialCentreDeletePopupComponent,
    ],
    providers: [
        TrialCentreService,
        TrialCentrePopupService,
    ],
    exports: [
        TrialCentreListComponent,
        TrialCentreCardComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkTrialCentreModule {}
