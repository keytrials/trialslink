import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { TrialCentre } from './trial-centre.model';
import { TrialCentreService } from './trial-centre.service';

@Component({
    selector: 'jhi-trial-centre-detail',
    templateUrl: './trial-centre-detail.component.html'
})
export class TrialCentreDetailComponent implements OnInit, OnDestroy {

    trialCentre: TrialCentre;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private trialCentreService: TrialCentreService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTrialCentres();
    }

    load(id) {
        this.trialCentreService.find(id).subscribe((trialCentre) => {
            this.trialCentre = trialCentre;
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTrialCentres() {
        this.eventSubscriber = this.eventManager.subscribe(
            'trialCentreListModification',
            (response) => this.load(this.trialCentre.id)
        );
    }
}
