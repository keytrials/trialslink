import { Component, OnInit, OnDestroy, Input, ViewContainerRef } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { TrialCentre } from './trial-centre.model';
import { Trial } from '../trial/trial.model';
import { TrialCentreService } from './trial-centre.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import * as _ from 'underscore';

@Component({
    selector: 'jhi-trial-centre-list',
    templateUrl: './trial-centre.component.html'
})
export class TrialCentreListComponent implements OnInit, OnDestroy {

    @Input() trial: Trial;
    trialCentres: TrialCentre[];
    currentAccount: any;
    eventSubscriber: Subscription;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;

    constructor(
        //private jhiLanguageService: JhiLanguageService,
        private trialCentreService: TrialCentreService,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private principal: Principal,
        private toastsManager:ToastsManager,
        private vcr: ViewContainerRef
    ) {
        this.trialCentres = [];
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'name';
        this.reverse = true;
        this.toastsManager.setRootViewContainerRef(vcr);
    }

    loadAll () {
         //if(this.trial) {
         //    this.trialCentres = _.sortBy(this.trial.sites, function(o) { return o.name; });
         //}
        // this.trialCentres = this.trial.sites;
        this.trialCentreService.centresForTrial(this.trial.id, {
            page: this.page,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    reset() {
        this.page = 0;
        this.trialCentres = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTrialCentres();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId (index: number, item: TrialCentre) {
        return item.id;
    }

    registerChangeInTrialCentres() {
        this.eventSubscriber = this.eventManager.subscribe('trialCentreAdded', (response) => {
            this.toastsManager.success('Trial Centre created/updated', 'Success!');
            this.reset();
        });
        this.eventSubscriber = this.eventManager.subscribe('trialCentreDeleted', (response) => {
            this.toastsManager.success('Trial Centre removed', 'Success!');
            this.reset();
        });
        this.eventSubscriber = this.eventManager.subscribe('contactAdded', (response) => {
            this.toastsManager.success('Contact added/updated', 'Success!');
        });
        this.eventSubscriber = this.eventManager.subscribe('contactRemoved', (id) => {
            this.toastsManager.success('Contact deleted', 'Success!');
        });
    }

    sort () {
        let result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'name') {
            result.push('name');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        for (let i = 0; i < data.length; i++) {
            this.trialCentres.push(data[i]);
        }
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}
