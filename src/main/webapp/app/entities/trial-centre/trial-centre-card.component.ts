import { Component, OnInit, OnDestroy, Input, ViewContainerRef } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { TrialCentre } from './trial-centre.model';
import { Trial } from '../trial/trial.model';
import { TrialCentreService } from './trial-centre.service';
import { InfoService, ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import * as _ from 'underscore';

@Component({
    selector: 'trial-centre-card',
    templateUrl: './trial-centre.card.html'
})
export class TrialCentreCardComponent {

    @Input() trialCentre: TrialCentre;

    constructor(
        private infoService: InfoService,
        private trialCentreService: TrialCentreService,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private principal: Principal,
        private toastsManager:ToastsManager,
        private vcr: ViewContainerRef
    ) {

    }
}
