import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { TrialCentre } from './trial-centre.model';
import { TrialCentreService } from './trial-centre.service';

@Injectable()
export class TrialCentrePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private trialCentreService: TrialCentreService

    ) {
        this.ngbModalRef = null;
    }

//    open (component: Component, id?: number | any, createNew?: boolean): NgbModalRef {
//        if (this.isOpen) {
//            return;
//        }
//        this.isOpen = true;
//
//        if (!createNew) {
//            this.trialCentreService.find(id).subscribe(trialCentre => {
//                trialCentre.startDate = this.datePipe
//                    .transform(trialCentre.startDate, 'yyyy-MM-ddThh:mm');
//                trialCentre.endDate = this.datePipe
//                    .transform(trialCentre.endDate, 'yyyy-MM-ddThh:mm');
//                this.trialCentreModalRef(component, trialCentre);
//            });
//        } else {
//            let trialCentre = new TrialCentre();
//            trialCentre.trialId = id;
//            return this.trialCentreModalRef(component, trialCentre);
//        }
//=======
    open(component: Component, id?: number | any, createNew?: boolean): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (!createNew) {
                this.trialCentreService.find(id).subscribe((trialCentre) => {
                    trialCentre.startDate = this.datePipe
                        .transform(trialCentre.startDate, 'yyyy-MM-ddTHH:mm:ss');
                    trialCentre.endDate = this.datePipe
                        .transform(trialCentre.endDate, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.trialCentreModalRef(component, trialCentre);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    let trialCentre = new TrialCentre();
                    trialCentre.trialId = id;
                    this.ngbModalRef = this.trialCentreModalRef(component, trialCentre);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    trialCentreModalRef(component: Component, trialCentre: TrialCentre): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.trialCentre = trialCentre;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
