import { Trial, TrialStatus } from '../trial/trial.model';
import { Contact } from '../contact/contact.model';
import { Organisation } from '../organisation/organisation.model';
import { Day } from './day.model';

export class TrialCentre extends Organisation {
    constructor(
        public id?: string,
        public name?: string,
        public allDay?: boolean,
        public startDate?: any,
        public endDate?: any,
        public days?: Day[],
        public trialId?: string,
        public status?: TrialStatus,
        public contacts?: Contact[],
        public principalInvestigator?: Contact
    ) {
        super(name, id);
        this.allDay = false;
        this.days = [];
    }
}
