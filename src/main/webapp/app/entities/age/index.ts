export * from './age.model';
export * from './age-popup.service';
export * from './age.service';
export * from './age-dialog.component';
export * from './age-delete-dialog.component';
export * from './age-detail.component';
export * from './age.component';
export * from './age.route';
