import { BaseEntity } from './../../shared';

export const enum AgeUnit {
    'YEARS',
    'MONTHS',
    'WEEKS',
    'DAYS',
    'HOURS',
    'MINUTES',
    'NA'
}

export class Age implements BaseEntity {
    constructor(
        public id?: string,
        public value?: number,
        public unit?: AgeUnit
    ) {
    }
}
