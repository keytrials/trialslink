import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Age } from './age.model';
import { AgeService } from './age.service';
import { Principal, ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-age',
    templateUrl: './age.component.html'
})
export class AgeComponent implements OnInit, OnDestroy {
ages: Age[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private ageService: AgeService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.ageService.query().subscribe(
            (res: ResponseWrapper) => {
                this.ages = res.json;
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInAges();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Age) {
        return item.id;
    }
    registerChangeInAges() {
        this.eventSubscriber = this.eventManager.subscribe('ageListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
