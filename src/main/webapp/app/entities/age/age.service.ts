import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Age } from './age.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class AgeService {

    private resourceUrl = SERVER_API_URL + 'api/ages';

    constructor(private http: Http) { }

    create(age: Age): Observable<Age> {
        const copy = this.convert(age);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(age: Age): Observable<Age> {
        const copy = this.convert(age);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<Age> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Age.
     */
    private convertItemFromServer(json: any): Age {
        const entity: Age = Object.assign(new Age(), json);
        return entity;
    }

    /**
     * Convert a Age to a JSON which can be sent to the server.
     */
    private convert(age: Age): Age {
        const copy: Age = Object.assign({}, age);
        return copy;
    }
}
