import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Age } from './age.model';
import { AgeService } from './age.service';

@Component({
    selector: 'jhi-age-detail',
    templateUrl: './age-detail.component.html'
})
export class AgeDetailComponent implements OnInit, OnDestroy {

    age: Age;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private ageService: AgeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAges();
    }

    load(id) {
        this.ageService.find(id).subscribe((age) => {
            this.age = age;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAges() {
        this.eventSubscriber = this.eventManager.subscribe(
            'ageListModification',
            (response) => this.load(this.age.id)
        );
    }
}
