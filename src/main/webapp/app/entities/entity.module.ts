import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { TrialslinkContactModule } from './contact/contact.module';
import { TrialslinkAddressModule } from './address/address.module';
import { TrialslinkAgeModule } from './age/age.module';
import { TrialslinkCharacteristicModule } from './characteristic/characteristic.module';
import { TrialslinkConceptModule } from './concept/concept.module';
import { TrialslinkInterventionModule } from './intervention/intervention.module';
import { TrialslinkIdInfoModule } from './id-info/id-info.module';
import { TrialslinkOrganisationModule } from './organisation/organisation.module';
import { TrialslinkOutcomeMeasureModule } from './outcome-measure/outcome-measure.module';
import { TrialslinkPractitionerModule } from './practitioner/practitioner.module';
import { TrialslinkRoleModule } from './role/role.module';
import { TrialslinkSecondaryIdModule } from './secondary-id/secondary-id.module';
import { TrialslinkTrialModule } from './trial/trial.module';
import { TrialslinkTrialCentreModule } from './trial-centre/trial-centre.module';
import { TrialslinkConditionModule } from './condition/condition.module';
import { TrialslinkTrialImportModule } from './trial-import/trial-import.module';
import { TrialslinkSavedSearchModule } from './saved-search/saved-search.module';
import { TrialslinkPrivilegeRequestModule } from './privilege-request/privilege-request.module';
import { TrialslinkTrialHistoryModule } from './trial-history/trial-history.module';
import {TrialslinkReviewerModule} from "./trial-reviewer/trial-reviewer.module";
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        TrialslinkContactModule,
        TrialslinkAddressModule,
        TrialslinkAgeModule,
        TrialslinkCharacteristicModule,
        TrialslinkConceptModule,
        TrialslinkInterventionModule,
        TrialslinkIdInfoModule,
        TrialslinkOrganisationModule,
        TrialslinkOutcomeMeasureModule,
        TrialslinkPractitionerModule,
        TrialslinkRoleModule,
        TrialslinkSecondaryIdModule,
        TrialslinkTrialModule,
        TrialslinkTrialCentreModule,
        TrialslinkConditionModule,
        TrialslinkTrialImportModule,
        TrialslinkReviewerModule,
        TrialslinkSavedSearchModule,
        TrialslinkPrivilegeRequestModule,
        TrialslinkTrialHistoryModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkEntityModule {}
