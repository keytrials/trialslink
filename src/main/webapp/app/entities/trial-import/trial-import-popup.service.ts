import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TrialImport } from './trial-import.model';
import { TrialImportService } from './trial-import.service';

@Injectable()
export class TrialImportPopupService {
    private isOpen = false;
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private trialImportService: TrialImportService

    ) {
        this.ngbModalRef = null;
    }

//    open(component: Component, id?: number | any): NgbModalRef {
//        if (this.isOpen) {
//            return;
//        }
//        this.isOpen = true;
//
//        if (id) {
//            this.trialImportService.find(id).subscribe((trialImport) => {
//                this.trialImportModalRef(component, trialImport);
//            });
//        } else {
//            return this.trialImportModalRef(component, new TrialImport());
//        }
//=======
    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.trialImportService.find(id).subscribe((trialImport) => {
                    this.ngbModalRef = this.trialImportModalRef(component, trialImport);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.trialImportModalRef(component, new TrialImport());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    trialImportModalRef(component: Component, trialImport: TrialImport): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.trialImport = trialImport;
        modalRef.result.then((result) => {
//<<<<<<< HEAD
//            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
//            this.isOpen = false;
//=======
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
