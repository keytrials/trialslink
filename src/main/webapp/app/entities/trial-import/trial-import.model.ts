import { BaseEntity } from './../../shared';
import {Trial} from "../trial/trial.model";
export enum TrialImportSource {
    'NIHR',
    'CLINICALTRIALS_GOV'
}

export enum TrialImportStatus {
    'IN_PROGRESS',
    'COMPLETE',
    'FAILED'
}

//export class TrialImport {
//
//export const enum TrialImportSource {
//    'NIHR',
//    ' CLINICALTRIALS_GOV'
//}
//
//export const enum TrialImportStatus {
//    'IN_PROGRESS',
//    ' COMPLETE',
//    ' FAILED'
//}

export class TrialImport implements BaseEntity {
    constructor(
        public id?: string,
        public source?: TrialImportSource,
        public sourceIdentifier?: string,
        public status?: TrialImportStatus,
        public filePath?: string,
        public trials ?: Trial[],
        public failedTrials ?: string[]
    ) {
    }
}
