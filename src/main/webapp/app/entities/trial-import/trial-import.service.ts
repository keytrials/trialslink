import { Injectable } from '@angular/core';
import {BaseRequestOptions, Http, Response, URLSearchParams} from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { TrialImport } from './trial-import.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class TrialImportService {

    private resourceUrl = SERVER_API_URL + 'api/trial-imports';

    constructor(private http: Http) { }

    create(trialImport: TrialImport): Observable<TrialImport> {
        const copy = this.convert(trialImport);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(trialImport: TrialImport): Observable<TrialImport> {
        const copy = this.convert(trialImport);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<TrialImport> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    //private convert(trialImport: TrialImport): TrialImport {
    //    const copy: TrialImport = Object.assign({}, trialImport);
    //    return copy;
    //}

    //private convertResponse(res: any): any {
    //    /*let jsonResponse = res.json();
    //    res._body = jsonResponse;*/
    //    res._body = res.json();
    //    return res;
    //}

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to TrialImport.
     */
    private convertItemFromServer(json: any): TrialImport {
        const entity: TrialImport = Object.assign(new TrialImport(), json);
        return entity;
    }

    /**
     * Convert a TrialImport to a JSON which can be sent to the server.
     */
    private convert(trialImport: TrialImport): TrialImport {
        const copy: TrialImport = Object.assign({}, trialImport);
        return copy;
    }
}
