import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { TrialImportComponent } from './trial-import.component';
import { TrialImportDetailComponent } from './trial-import-detail.component';
import { TrialImportPopupComponent } from './trial-import-dialog.component';
import { TrialImportDeletePopupComponent } from './trial-import-delete-dialog.component';

@Injectable()
export class TrialImportResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const trialImportRoute: Routes = [
    {
        path: 'trial-import',
        component: TrialImportComponent,
        resolve: {
            'pagingParams': TrialImportResolvePagingParams
        },
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.trialImport.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'trial-import/:id',
        component: TrialImportDetailComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.trialImport.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const trialImportPopupRoute: Routes = [
    {
        path: 'trial-import-new',
        component: TrialImportPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.trialImport.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trial-import/:id/edit',
        component: TrialImportPopupComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.trialImport.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trial-import/:id/delete',
        component: TrialImportDeletePopupComponent,
        data: {
            authorities: ['ROLE_ADMIN', 'ROLE_PRIVILEGED'],
            pageTitle: 'trialslinkApp.trialImport.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
