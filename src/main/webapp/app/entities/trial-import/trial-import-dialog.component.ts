import {Component, OnInit, OnDestroy, ChangeDetectorRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import {JhiEventManager, JhiAlertService, JhiLanguageService} from 'ng-jhipster';

import {TrialImport, TrialImportSource} from './trial-import.model';
import { TrialImportPopupService } from './trial-import-popup.service';
import { TrialImportService } from './trial-import.service';
import {FileItem, FileUploader, ParsedResponseHeaders} from 'ng2-file-upload';

@Component({
    selector: 'jhi-trial-import-dialog',
    templateUrl: './trial-import-dialog.component.html'
})
export class TrialImportDialogComponent implements OnInit {

    source: string;
    uploader: FileUploader;
    trialImport: TrialImport;
    isSaving: boolean;
    detector: ChangeDetectorRef;

    constructor(
        public activeModal: NgbActiveModal,
        public router: Router,
        private alertService: JhiAlertService,
        private trialImportService: TrialImportService,
        private eventManager: JhiEventManager,
        private changeDetector: ChangeDetectorRef
    ) {
        this.router = router;
        this.uploader = new FileUploader({
            url: 'https://file.io'
        });
        this.configureUploader();
        this.detector = changeDetector;
    }

    ngOnInit() {
        this.isSaving = false;
        //this.source = this.trialImport.source.toString();
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.trialImport.source.toString() === TrialImportSource[TrialImportSource.NIHR]) {
            // defer to file uploader for NIHR trial import
            this.uploader.uploadAll();
            return;
        }
        this.persistTrialImport();
    }

    private configureUploader() {
        this.uploader.onAfterAddingFile = (fileItem: FileItem): any => {
            console.log(fileItem);
        };
        this.uploader.onBeforeUploadItem = (item) => {
            item.withCredentials = false;
        };
        this.uploader.onSuccessItem = (item: FileItem, response: string, status: number, headers: ParsedResponseHeaders): any => {
            this.saveTrialImportOnSuccessfulFileUpload(item, response);
        };
        this.uploader.onErrorItem = () => {
            this.handleFileUploadError();
        };
        this.uploader.onProgressItem = () => {
            // trigger angular to refresh the values of the uploader progress model
            this.detector.detectChanges();
        };
    }

    private saveTrialImportOnSuccessfulFileUpload (item: FileItem, response: string): any {
        // remove item from upload queue
        item.remove();

        let parsedResponse = JSON.parse(response);
        this.trialImport.filePath = parsedResponse.link;

        // persist trialImport to database
        this.persistTrialImport();
    }

    private handleFileUploadError () {
        this.isSaving = false;
        this.onError({message: 'Error encountered while uploading file'});
    }

    private persistTrialImport() {
        this.subscribeToSaveResponse(
            this.trialImportService.create(this.trialImport)
        );
    }

    private subscribeToSaveResponse(result: Observable<TrialImport>) {
        result.subscribe((res: TrialImport) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: TrialImport) {
        this.eventManager.broadcast({ name: 'trialImportListModification', content: 'OK'});
        this.isSaving = false;
        this.router.navigate(['/trial-import/' + result.id]).then(() => {
            this.activeModal.dismiss(result);
        });
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }
    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-trial-import-popup',
    template: ''
})
export class TrialImportPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private trialImportPopupService: TrialImportPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.trialImportPopupService
                    .open(TrialImportDialogComponent as Component, params['id']);
            } else {
                this.trialImportPopupService
                    .open(TrialImportDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
