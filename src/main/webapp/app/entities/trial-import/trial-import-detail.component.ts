import { Component, OnInit, OnDestroy } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import {JhiEventManager, JhiLanguageService} from 'ng-jhipster';

import { TrialImport } from './trial-import.model';
import { TrialImportService } from './trial-import.service';
import {Angular2Csv} from "angular2-csv";

@Component({
    selector: 'jhi-trial-import-detail',
    templateUrl: './trial-import-detail.component.html'
})
export class TrialImportDetailComponent implements OnInit, OnDestroy {

    trialImport: TrialImport;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    private pollerId: number;

    constructor(
        public router: Router,
        private jhiLanguageService: JhiLanguageService,
        private eventManager: JhiEventManager,
        private trialImportService: TrialImportService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.startPolling();
        this.registerChangeInTrialImports();
    }

    load(id) {
        this.trialImportService.find(id).subscribe((trialImport) => {
            this.trialImport = trialImport;
        });
    }

    startPolling() {
        if (!this.shouldAutoRefresh()) {
            return;
        }
        this.pollerId = window.setInterval(() => {
            this.refresh();
        }, 3000);
    }

    stopPolling() {
        window.clearInterval(this.pollerId);
    }

    shouldAutoRefresh() {
        if (!this.trialImport) return true;
        return this.trialImport.status.toString() === "IN_PROGRESS";
    }

    refresh() {
        // ensure that automatic refresh is done ONLY if the trialImport is still in progress
        if (!this.shouldAutoRefresh()) {
            this.stopPolling();
        }
        this.eventManager.broadcast({
            name: 'trialImportListModification',
            content: 'Refreshing a trialImport'
        });
    }

    gotoListView() {
        this.router.navigate(['/trial-import']);
    }

    downloadFailedTrials() {
        if (this.trialImport.failedTrials.length <= 0) {
            return;
        }
        let filename:string = "failed__trials__" + this.trialImport.id;
        new Angular2Csv(this.trialImport.failedTrials, filename, {showLabels: true});
    }

    ngOnDestroy() {
        this.stopPolling();
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTrialImports() {
        this.eventSubscriber = this.eventManager.subscribe('trialImportListModification', (response) => {
            if (this.trialImport) {
                this.load(this.trialImport.id);
            }
        });
    }
}
