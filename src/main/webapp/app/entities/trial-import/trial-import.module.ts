import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';
import {
    TrialImportService,
    TrialImportPopupService,
    TrialImportComponent,
    TrialImportDetailComponent,
    TrialImportDialogComponent,
    TrialImportPopupComponent,
    TrialImportDeletePopupComponent,
    TrialImportDeleteDialogComponent,
    trialImportRoute,
    trialImportPopupRoute,
    TrialImportResolvePagingParams,
} from './';
import {FileUploadModule} from 'ng2-file-upload';

const ENTITY_STATES = [
    ...trialImportRoute,
    ...trialImportPopupRoute,
];

@NgModule({
    imports: [
        FileUploadModule,
        TrialslinkSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TrialImportComponent,
        TrialImportDetailComponent,
        TrialImportDialogComponent,
        TrialImportDeleteDialogComponent,
        TrialImportPopupComponent,
        TrialImportDeletePopupComponent,
    ],
    entryComponents: [
        TrialImportComponent,
        TrialImportDialogComponent,
        TrialImportPopupComponent,
        TrialImportDeleteDialogComponent,
        TrialImportDeletePopupComponent,
    ],
    providers: [
        TrialImportService,
        TrialImportPopupService,
        TrialImportResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkTrialImportModule {}
