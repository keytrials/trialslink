import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Characteristic } from './characteristic.model';
import { CharacteristicPopupService } from './characteristic-popup.service';
import { CharacteristicService } from './characteristic.service';

@Component({
    selector: 'jhi-characteristic-dialog',
    templateUrl: './characteristic-dialog.component.html'
})
export class CharacteristicDialogComponent implements OnInit {

    characteristic: Characteristic;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private characteristicService: CharacteristicService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.characteristic.id !== undefined) {
            this.subscribeToSaveResponse(
                this.characteristicService.update(this.characteristic));
        } else {
            this.subscribeToSaveResponse(
                this.characteristicService.create(this.characteristic));
        }
    }

    private subscribeToSaveResponse(result: Observable<Characteristic>) {
        result.subscribe((res: Characteristic) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Characteristic) {
        this.eventManager.broadcast({ name: 'characteristicListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-characteristic-popup',
    template: ''
})
export class CharacteristicPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private characteristicPopupService: CharacteristicPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
            //    this.modalRef = this.characteristicPopupService
            //        .open(CharacteristicDialogComponent, params['id'], false);
            //} else {
            //    this.modalRef = this.characteristicPopupService
            //        .open(CharacteristicDialogComponent, params['trialId'], true);
//=======
                this.characteristicPopupService
                    .open(CharacteristicDialogComponent as Component, params['id']);
            } else {
                this.characteristicPopupService
                    .open(CharacteristicDialogComponent as Component);
//>>>>>>> jhipster_upgrade
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
