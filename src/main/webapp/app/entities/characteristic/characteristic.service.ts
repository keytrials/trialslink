import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Characteristic } from './characteristic.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class CharacteristicService {

    private resourceUrl = SERVER_API_URL + 'api/characteristics';
    private trialsResourceUrl = SERVER_API_URL + 'api/trials';

    constructor(private http: Http) { }

    create(characteristic: Characteristic): Observable<Characteristic> {
        let copy: Characteristic = Object.assign({}, characteristic);
        if (characteristic.trialId !== undefined) {
            return this.http.post(`${this.trialsResourceUrl}/${characteristic.trialId}/criteria`, copy).map((res: Response) => {
                return res.json();
            });
        } else {
            return this.http.post(this.resourceUrl, copy).map((res: Response) => {
                return res.json();
            });
        }
    }

    update(characteristic: Characteristic): Observable<Characteristic> {

        const copy = this.convert(characteristic);
        return this.http.put(`${this.trialsResourceUrl}/${characteristic.trialId}/criteria`, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<Characteristic> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    criteriaForTrial(id: string, req?: any): Observable<Response> {
        let options = createRequestOption(req);
        return this.http.get(`${this.trialsResourceUrl}/${id}/criteria`, options);
    }

    delete(trialId: string, id: string): Observable<Response> {
        return this.http.delete(`${this.trialsResourceUrl}/${trialId}/criteria/${id}`);
    }

    deleteAllForTrial(id: string): Observable<Response> {
        return this.http.delete(`${this.trialsResourceUrl}/${id}/criteria`)
    }

    //delete(id: string): Observable<Response> {
    //    return this.http.delete(`${this.resourceUrl}/${id}`);
    //}

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Characteristic.
     */
    private convertItemFromServer(json: any): Characteristic {
        const entity: Characteristic = Object.assign(new Characteristic(), json);
        return entity;
    }

    /**
     * Convert a Characteristic to a JSON which can be sent to the server.
     */
    private convert(characteristic: Characteristic): Characteristic {
        const copy: Characteristic = Object.assign({}, characteristic);
        return copy;
    }
}
