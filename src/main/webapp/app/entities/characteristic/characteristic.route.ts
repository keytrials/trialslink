import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { CharacteristicComponent } from './characteristic.component';
import { CharacteristicDetailComponent } from './characteristic-detail.component';
import { CharacteristicPopupComponent } from './characteristic-dialog.component';
import { CharacteristicDeletePopupComponent } from './characteristic-delete-dialog.component';

export const characteristicRoute: Routes = [
    {
        path: 'characteristic',
        component: CharacteristicComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.characteristic.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'characteristic/:id',
        component: CharacteristicDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.characteristic.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const characteristicPopupRoute: Routes = [
  {
    path: 'characteristic-new/:trialId/create',
    component: CharacteristicPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.characteristic.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'characteristic/:id/edit',
    component: CharacteristicPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'trialslinkApp.characteristic.home.title'
//=======
//    {
//        path: 'characteristic-new',
//        component: CharacteristicPopupComponent,
//        data: {
//            authorities: ['ROLE_USER'],
//            pageTitle: 'trialslinkApp.characteristic.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
//>>>>>>> jhipster_upgrade
    },
    {
        path: 'characteristic/:id/edit',
        component: CharacteristicPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.characteristic.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'characteristic/:id/delete',
        component: CharacteristicDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.characteristic.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
