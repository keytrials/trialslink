import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Characteristic } from './characteristic.model';
import { CharacteristicPopupService } from './characteristic-popup.service';
import { CharacteristicService } from './characteristic.service';

@Component({
    selector: 'jhi-characteristic-delete-dialog',
    templateUrl: './characteristic-delete-dialog.component.html'
})
export class CharacteristicDeleteDialogComponent {

    characteristic: Characteristic;

    constructor(
        private characteristicService: CharacteristicService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete (id: string) {
        this.characteristicService.delete(this.characteristic.trialId, id).subscribe(response => {
//=======
//    confirmDelete(id: string) {
//        this.characteristicService.delete(id).subscribe((response) => {
//>>>>>>> jhipster_upgrade
            this.eventManager.broadcast({
                name: 'characteristicListModification',
                content: 'Deleted an characteristic'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-characteristic-delete-popup',
    template: ''
})
export class CharacteristicDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private characteristicPopupService: CharacteristicPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.characteristicPopupService
                .open(CharacteristicDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
