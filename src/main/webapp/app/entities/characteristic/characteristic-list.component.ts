import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Response } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiLanguageService, JhiAlertService } from 'ng-jhipster';

import { Characteristic } from './characteristic.model';
import { CharacteristicService } from './characteristic.service';
import { ITEMS_PER_PAGE, Principal } from '../../shared';
import { PaginationConfig } from '../../blocks/config/uib-pagination.config';
import { Trial } from '../trial/trial.model';

@Component({
    selector: 'jhi-characteristic-list',
    templateUrl: './characteristic-list.component.html'
})
export class CharacteristicListComponent implements OnInit, OnDestroy {

    @Input() trial: Trial;
    characteristics: Characteristic[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        //private jhiLanguageService: JhiLanguageService,
        private characteristicService: CharacteristicService,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
        //this.jhiLanguageService.setLocations(['trial', 'trialStatus', 'studyType', 'trialCentre', 'contact', 'condition', 'intervention', 'characteristic', 'sex', 'age']);
    }

    loadAll() {
        this.characteristicService.criteriaForTrial(this.trial.id).subscribe(
            (res: Response) => {
                this.characteristics = res.json();
            },
            (res: Response) => this.onError(res.json())
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCharacteristics();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId (index: number, item: Characteristic) {
        return item.id;
    }

    registerChangeInCharacteristics() {
        this.eventSubscriber = this.eventManager.subscribe('characteristicListModification', (response) => this.loadAll());
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}
