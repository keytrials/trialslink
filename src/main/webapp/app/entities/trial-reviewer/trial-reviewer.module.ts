import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from "@angular/core";
import {TrialslinkSharedModule} from "../../shared";
import {RouterModule} from "@angular/router";
import {ABOUT_ROUTE, AboutComponent} from "../../about";
import {TrialReviewerComponent} from "./trial-reviewer.component";
import {TrialReviewerService} from "./trial-reviewer.service";
import {FileUploadModule} from "ng2-file-upload";
import {trialReviewerRoute} from "./trial-reviewer.route";
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
    imports: [
        FileUploadModule,
        TrialslinkSharedModule,
        MatProgressSpinnerModule,
        RouterModule.forChild([trialReviewerRoute])
    ],
    declarations: [
        TrialReviewerComponent,
    ],
    entryComponents: [
    ],
    providers: [
        TrialReviewerService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkReviewerModule {}
