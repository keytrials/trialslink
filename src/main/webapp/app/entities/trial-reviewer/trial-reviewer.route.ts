import {Route} from "@angular/router";
import {SettingsComponent} from "../../account";
import {UserRouteAccessService} from "../../shared";
import {TrialReviewerComponent} from "./trial-reviewer.component";

export const trialReviewerRoute: Route = {
    path: 'trial-reviewer',
    component: TrialReviewerComponent,
    data: {
        authorities: ['ROLE_ADMIN', 'ROLE_DATA_MANAGER'],
        pageTitle: 'trial-reviewer'
    },
    canActivate: [UserRouteAccessService]
};
