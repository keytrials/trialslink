import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs";
import {SERVER_API_URL} from "../../app.constants";
import {FileItem} from "ng2-file-upload";
import {TrialImport} from "../trial-import";

@Injectable()
export class TrialReviewerService {

    private resourceUrl = SERVER_API_URL + 'api/_review/trials';
    private trialsImportUrl = SERVER_API_URL + 'api/_import/trials';

    constructor(private http: Http) {
    }

    review(): Observable<any>{
        return this.http.get(this.resourceUrl).map((res: Response) => res.json());
    }

    import(reviewedTrials: any):Observable<any>{
        return this.http.post(this.trialsImportUrl, reviewedTrials).map((res: Response) => res.json());
    }

}
