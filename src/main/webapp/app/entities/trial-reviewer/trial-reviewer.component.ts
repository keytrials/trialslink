import {Component, OnInit, ɵViewFlags} from "@angular/core";
import {FileItem, FileUploader} from "ng2-file-upload";
import {TrialReviewerService} from "./trial-reviewer.service";
import {SERVER_API_URL} from "../../app.constants";
import { MatSpinner } from '@angular/material';
const URL = SERVER_API_URL + 'api/_import/file';

@Component({
    selector: 'jhi-trial-reviewer',
    templateUrl: './trial-reviewer.component.html'
})
export class TrialReviewerComponent implements OnInit{

    public uploader:FileUploader = new FileUploader({url: URL});
    public hasBaseDropZoneOver:boolean = false;
    trialReviewer: any;
    toBeAdded: any;
    toBeUpdated: any;
    toBeDeleted: any;
    showToBeAdded: boolean;
    showToBeUpdated: boolean;
    showToBeDeleted: boolean;
    uploaded: boolean;
    imported: boolean;
    showSpinner: boolean;

    constructor(
        private trialReviewerService: TrialReviewerService
    ) {
        this.showToBeAdded = true;
        this.showToBeUpdated = false;
        this.showToBeDeleted = false;
        this.uploaded = false;
    }

    ngOnInit(): void {
        //this.uploader.onAfterAddingFile = (file) => { this.review() };
    }

    public fileOverBase(e:any):void {
        this.hasBaseDropZoneOver = e;
    }


    upload(item: FileItem){
        item.upload();
        this.uploaded = true;
        console.log(this.uploaded)
    }

    review(){
        this.trialReviewerService.review().subscribe(value => {
            console.log('2: ',value);
            this.trialReviewer = value;
            console.log('4: ',this.trialReviewer);
            this.toBeAdded = this.trialReviewer['add'];
            this.toBeUpdated = this.trialReviewer['update'];
            this.toBeDeleted = this.trialReviewer['delete'];
            console.log('added: ',this.toBeAdded);
            this.uploaded = false;
        });
    }


    showToBeAddedButton(){
        this.showToBeAdded = true;
        this.showToBeUpdated = false;
        this.showToBeDeleted = false;
    }

    showToBeUpdatedButton(){
        this.showToBeAdded = false;
        this.showToBeUpdated = true;
        this.showToBeDeleted = false;
    }

    showToBeDeletedButton(){
        this.showToBeAdded = false;
        this.showToBeUpdated = false;
        this.showToBeDeleted = true;
    }

    import(){
        this.showSpinner = true;
        let reviewedMap = {add: this.toBeAdded, update: this.toBeUpdated, delete: this.toBeDeleted}
        this.trialReviewerService.import(reviewedMap).subscribe(value => {
            this.imported = value;
        });
    }

    reject(mapName: string,key: string){
        if (mapName === 'add'){
            delete this.toBeAdded[key];
        }
        else if (mapName === 'update'){
            delete this.toBeUpdated[key];
        }
        else {
            delete this.toBeDeleted[key];
        }
    }

    rejectAll(){
        this.toBeAdded = [];
        this.toBeUpdated = [];
        this.toBeDeleted = [];
    }

}
