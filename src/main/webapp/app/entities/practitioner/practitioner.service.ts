import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { Practitioner } from './practitioner.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PractitionerService {

    private resourceUrl = SERVER_API_URL + 'api/practitioners';

    constructor(private http: Http) { }

    create(practitioner: Practitioner): Observable<Practitioner> {
        const copy = this.convert(practitioner);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(practitioner: Practitioner): Observable<Practitioner> {
        const copy = this.convert(practitioner);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: string): Observable<Practitioner> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: string): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Practitioner.
     */
    private convertItemFromServer(json: any): Practitioner {
        const entity: Practitioner = Object.assign(new Practitioner(), json);
        return entity;
    }

    /**
     * Convert a Practitioner to a JSON which can be sent to the server.
     */
    private convert(practitioner: Practitioner): Practitioner {
        const copy: Practitioner = Object.assign({}, practitioner);
        return copy;
    }
}
