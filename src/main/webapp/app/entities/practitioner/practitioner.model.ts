import { BaseEntity } from './../../shared';

export const enum Gender {
    'MALE',
    ' FEMALE',
    ' OTHER',
    ' UNKNOWN'
}

export class Practitioner implements BaseEntity {
    constructor(
        public id?: string,
        public name?: string,
        public gender?: Gender,
    ) {
    }
}
