import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Practitioner } from './practitioner.model';
import { PractitionerPopupService } from './practitioner-popup.service';
import { PractitionerService } from './practitioner.service';

@Component({
    selector: 'jhi-practitioner-dialog',
    templateUrl: './practitioner-dialog.component.html'
})
export class PractitionerDialogComponent implements OnInit {

    practitioner: Practitioner;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private practitionerService: PractitionerService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.practitioner.id !== undefined) {
            this.subscribeToSaveResponse(
                this.practitionerService.update(this.practitioner));
        } else {
            this.subscribeToSaveResponse(
                this.practitionerService.create(this.practitioner));
        }
    }

    private subscribeToSaveResponse(result: Observable<Practitioner>) {
        result.subscribe((res: Practitioner) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Practitioner) {
        this.eventManager.broadcast({ name: 'practitionerListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-practitioner-popup',
    template: ''
})
export class PractitionerPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private practitionerPopupService: PractitionerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.practitionerPopupService
                    .open(PractitionerDialogComponent as Component, params['id']);
            } else {
                this.practitionerPopupService
                    .open(PractitionerDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
