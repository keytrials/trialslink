import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Practitioner } from './practitioner.model';
import { PractitionerPopupService } from './practitioner-popup.service';
import { PractitionerService } from './practitioner.service';

@Component({
    selector: 'jhi-practitioner-delete-dialog',
    templateUrl: './practitioner-delete-dialog.component.html'
})
export class PractitionerDeleteDialogComponent {

    practitioner: Practitioner;

    constructor(
        private practitionerService: PractitionerService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.practitionerService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'practitionerListModification',
                content: 'Deleted an practitioner'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-practitioner-delete-popup',
    template: ''
})
export class PractitionerDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private practitionerPopupService: PractitionerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.practitionerPopupService
                .open(PractitionerDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
