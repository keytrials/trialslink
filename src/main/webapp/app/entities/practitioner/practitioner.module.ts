import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TrialslinkSharedModule } from '../../shared';
import {
    PractitionerService,
    PractitionerPopupService,
    PractitionerComponent,
    PractitionerDetailComponent,
    PractitionerDialogComponent,
    PractitionerPopupComponent,
    PractitionerDeletePopupComponent,
    PractitionerDeleteDialogComponent,
    practitionerRoute,
    practitionerPopupRoute,
} from './';

const ENTITY_STATES = [
    ...practitionerRoute,
    ...practitionerPopupRoute,
];

@NgModule({
    imports: [
        TrialslinkSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        PractitionerComponent,
        PractitionerDetailComponent,
        PractitionerDialogComponent,
        PractitionerDeleteDialogComponent,
        PractitionerPopupComponent,
        PractitionerDeletePopupComponent,
    ],
    entryComponents: [
        PractitionerComponent,
        PractitionerDialogComponent,
        PractitionerPopupComponent,
        PractitionerDeleteDialogComponent,
        PractitionerDeletePopupComponent,
    ],
    providers: [
        PractitionerService,
        PractitionerPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrialslinkPractitionerModule {}
