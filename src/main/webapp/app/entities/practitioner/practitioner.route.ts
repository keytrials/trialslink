import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PractitionerComponent } from './practitioner.component';
import { PractitionerDetailComponent } from './practitioner-detail.component';
import { PractitionerPopupComponent } from './practitioner-dialog.component';
import { PractitionerDeletePopupComponent } from './practitioner-delete-dialog.component';

export const practitionerRoute: Routes = [
    {
        path: 'practitioner',
        component: PractitionerComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.practitioner.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'practitioner/:id',
        component: PractitionerDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.practitioner.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const practitionerPopupRoute: Routes = [
    {
        path: 'practitioner-new',
        component: PractitionerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.practitioner.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'practitioner/:id/edit',
        component: PractitionerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.practitioner.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'practitioner/:id/delete',
        component: PractitionerDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'trialslinkApp.practitioner.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
