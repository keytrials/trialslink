import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';
import {NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import { NgbDateMomentParserFormatter } from '../blocks/config/ngb-datepicker-parser-formatter';
import { MaterialModule } from './material.module';
import 'hammerjs';
import 'cookieconsent';

import {
    TrialslinkSharedLibsModule,
    TrialslinkSharedCommonModule,
    CSRFService,
    AuthServerProvider,
    AccountService,
    InfoService,
    UserService,
    StateStorageService,
    LoginService,
    LoginModalService,
    JhiLoginModalComponent,
    Principal,
    HasAnyAuthorityDirective,
    JhiSocialComponent,
    SocialService,
} from './';

@NgModule({
    imports: [
        TrialslinkSharedLibsModule,
        TrialslinkSharedCommonModule,
        MaterialModule
    ],
    declarations: [
        JhiSocialComponent,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective
    ],
    providers: [
        LoginService,
        LoginModalService,
        AccountService,
        InfoService,
        StateStorageService,
        Principal,
        CSRFService,
        AuthServerProvider,
        SocialService,
        UserService,
        DatePipe,
        {
            provide: NgbDateParserFormatter,
            useFactory: () => { return new NgbDateMomentParserFormatter('DD-MM-YYYY'); }
        }
    ],
    entryComponents: [JhiLoginModalComponent],
    exports: [
        TrialslinkSharedCommonModule,
        MaterialModule,
        JhiSocialComponent,
        JhiLoginModalComponent,
        HasAnyAuthorityDirective,
        DatePipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class TrialslinkSharedModule {}
