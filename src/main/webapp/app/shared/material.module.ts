import { NgModule } from '@angular/core';

import {
    MatButtonModule,
    MatTabsModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatExpansionModule,
    MatChipsModule,
    MatListModule,
    MatTableModule
    } from '@angular/material';

@NgModule({
    imports: [
        MatButtonModule,
        MatTabsModule,
        MatToolbarModule,
        MatIconModule,
        MatCardModule,
        MatExpansionModule,
        MatChipsModule,
        MatListModule,
        MatTableModule
    ],
    exports: [
        MatButtonModule,
        MatTabsModule,
        MatToolbarModule,
        MatIconModule,
        MatCardModule,
        MatExpansionModule,
        MatChipsModule,
        MatListModule,
        MatTableModule
    ]
})
export class MaterialModule {}
