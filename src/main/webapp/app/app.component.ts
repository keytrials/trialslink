import { Component } from '@angular/core';
import {Router, NavigationEnd} from '@angular/router'; // import Router and NavigationEnd

  // declare ga as a function to set and sent the events
 declare let ga: Function;

@Component({
  selector: 'jhi-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  title = '';

   constructor(public router: Router) {

    // subscribe to router events and send page views to google analytics
    this.router.events.subscribe(event => {

      if (event instanceof NavigationEnd) {
        ga('set', 'page', event.urlAfterRedirects);
        ga('send', 'pageview');

      }

    });

  }

}
