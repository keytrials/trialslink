import './vendor.ts';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Ng2Webstorage } from 'ngx-webstorage';

import { TrialslinkSharedModule, UserRouteAccessService } from './shared';
import { TrialslinkAppRoutingModule} from './app-routing.module';
import { TrialslinkHomeModule } from './home/home.module';
import { TrialslinkAboutModule } from './about/about.module';
import { TrialslinkAdminModule } from './admin/admin.module';
import { TrialslinkAccountModule } from './account/account.module';
import { TrialslinkEntityModule } from './entities/entity.module';
import { customHttpProvider } from './blocks/interceptor/http.provider';
import { PaginationConfig } from './blocks/config/uib-pagination.config';
import {AppComponent} from './app.component';

// jhipster-needle-angular-add-module-import JHipster will add new module here

import {
    JhiMainComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    ProfileService,
    PageRibbonComponent,
    ActiveMenuDirective,
    ErrorComponent
} from './layouts';
import {TrialslinkContactUsModule} from "./contact-us";
import {MatButtonModule, MatIconModule} from "@angular/material";
import {EcoFabSpeedDialModule} from "@ecodev/fab-speed-dial"
import {FeedbackComponent} from "./layouts/feedback/feedback.component";
import {FileSelectDirective} from "ng2-file-upload";

@NgModule({
    imports: [
        BrowserModule,
        EcoFabSpeedDialModule,
        MatButtonModule,
        MatIconModule,
        Ng2Webstorage.forRoot({ prefix: 'jhi', separator: '-'}),
        TrialslinkSharedModule,
        TrialslinkHomeModule,
        TrialslinkAboutModule,
        TrialslinkContactUsModule,
        TrialslinkAdminModule,
        TrialslinkAccountModule,
        TrialslinkEntityModule,
        TrialslinkAppRoutingModule
        // jhipster-needle-angular-add-module JHipster will add new module here
    ],
    declarations: [
        JhiMainComponent,
        NavbarComponent,
        SidebarComponent,
        ErrorComponent,
        PageRibbonComponent,
        ActiveMenuDirective,
        FooterComponent,
        AppComponent,
        FeedbackComponent/*,
        FileSelectDirective*/
    ],
    providers: [
        ProfileService,
        customHttpProvider(),
        PaginationConfig,
        UserRouteAccessService
    ],
    bootstrap: [ JhiMainComponent ]
})
export class TrialslinkAppModule {}
