import { Component, OnInit } from '@angular/core';
import { Response} from '@angular/http';
import { Router } from '@angular/router';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { NgcCookieConsentService } from 'ngx-cookieconsent';
import {JhiEventManager, JhiLanguageService} from 'ng-jhipster';
import * as _ from 'underscore';
import {LocalStorageService, LocalStorage} from 'ngx-webstorage';
import { ConceptService } from '../entities/concept/concept.service';
import { TrialService } from '../entities/trial/trial.service';
import { NgcInitializeEvent, NgcStatusChangeEvent } from 'ngx-cookieconsent';
import { Subscription }   from 'rxjs/Subscription';

import {Account, InfoService, LoginModalService, Principal} from '../shared';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: [
        'home.css'
    ]

})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    single:any[];
    pieData:any[];
    conditions: any[];
    conditionsLookup: any;

    // pie options
    showLegend = false;
    view:any[] = [800, 800];
    colorScheme = {
        domain: ['#006064','#80deea']
    };
    showLabels = true;
    explodeSlices = false;
    doughnut = false;
    // options
    showXAxis = true;
    showYAxis = true;
    gradient = false;
    showXAxisLabel = true;
    xAxisLabel = 'Number of studies';
    showYAxisLabel = true;
    yAxisLabel = 'Areas';
    trimYAxisTicks = true;
    maxYAxisTickLength = 40;
    brand: string;
    useLocalStorage: boolean;

    //keep refs to subscriptions to be able to unsubscribe later
    private popupOpenSubscription: Subscription;
    private popupCloseSubscription: Subscription;
    private initializeSubscription: Subscription;
    private statusChangeSubscription: Subscription;
    private revokeChoiceSubscription: Subscription;

    constructor(
        private principal: Principal,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private trialService: TrialService,
        private conceptService: ConceptService,
        private localStorageService:LocalStorageService,
        private router: Router,
        private languageService: JhiLanguageService,
        private infoService: InfoService,
        private ccService: NgcCookieConsentService,
        private translateService:TranslateService,
        private cookieConsentService: NgcCookieConsentService
    ) {

        this.pieData = [
            {
                "name": "Lymphoma",
                "value": 4
            },
            {
                "name": "Leukaemia",
                "value": 3
            }
        ];
    }

    ngOnInit() {

        // subscribe to cookieconsent observables to react to main events
        this.popupOpenSubscription = this.ccService.popupOpen$.subscribe(
            () => {
                // you can use this.ccService.getConfig() to do stuff...
            });

        this.popupCloseSubscription = this.ccService.popupClose$.subscribe(
            () => {
                // you can use this.ccService.getConfig() to do stuff...
            });

        this.initializeSubscription = this.ccService.initialize$.subscribe(
            (event: NgcInitializeEvent) => {
                // you can use this.ccService.getConfig() to do stuff...
            });


        this.statusChangeSubscription = this.ccService.statusChange$.subscribe(
            (event: NgcStatusChangeEvent) => {
                // you can use this.ccService.getConfig() to do stuff...
            });

        this.revokeChoiceSubscription = this.ccService.revokeChoice$.subscribe(
            () => {
                // you can use this.ccService.getConfig() to do stuff...
            });

        this.principal.identity().then((account) => {
            this.account = account;
        });
        this.registerAuthenticationSuccess();
        // load conditions from local storage or server if needed
        if (this.useLocalStorage) {
            this.conditions = this.localStorageService.retrieve('conditions');
            this.conditionsLookup = this.localStorageService.retrieve('conditionsLookup');
        }
        if(!this.conditions || !this.conditionsLookup) {
            this.conceptService.allAsFilter({
                    size: 500
                }
            ).subscribe(
                (res: Response) => {
                    this.conditions = res.json().filter(function(c){return c.type !== 'status'});
                    this.conditions = _.sortBy(this.conditions, function(c) {
                        return c.text;});
                    let allConditions = this.conditions;
                    this.conditionsLookup = _.indexBy(allConditions, 'id');
                    // store in local storage
                    if (this.useLocalStorage) {
                        this.localStorageService.store('conditions', this.conditions);
                        this.localStorageService.store('conditionsLookup', this.conditionsLookup);
                    }
                },
                (res: Response) => this.onError(res.json())
            );
        }

        // load all trials
        this.trialService.query({
            page: 0,
            size: 10,
            sort: ['id' + ',' + 'asc']
        }).subscribe(
            (res: Response) => this.onSuccess(res.json(), res.headers),
            (res: Response) => this.onError(res.json())
        );
        this.infoService.getBrand().subscribe(brand => {
            this.brand = brand._body;
            if (this.brand==='uclh') {
                this.languageService.changeLanguage('uclh');
                this.colorScheme={
                domain:['#006064', '#80deea']
                };
            }
        });
        this.infoService.useLocalStorage().subscribe(value => {
            this.useLocalStorage = value;
        });

    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
            });
        });
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    private onSuccess(data, headers) {
        if(data.results !== undefined) {
            // process categories
            let v = [];
            let m = [];
            let p = [];
            let pl = this.conditionsLookup;
            _.mapObject(data.categories, function (value, key) {
                // process as single if key is procedures
                if ('areas' == key) {
                    _.each(value, function (item) {
                        p.push({name: pl[item['type']].text, value: item['count'], extra: item['type']});
                    });
                    // sort p by name, for display
                    p = _.sortBy(p, 'name');
                }
            });
            this.pieData = p;
        }
    }

    private onError (error) {
        console.log("error  = " , error );
    }

    ngOnDestroy() {
        // unsubscribe to cookieconsent observables to prevent memory leaks
        this.popupOpenSubscription.unsubscribe();
        this.popupCloseSubscription.unsubscribe();
        this.initializeSubscription.unsubscribe();
        this.statusChangeSubscription.unsubscribe();
        this.revokeChoiceSubscription.unsubscribe();
      }

    onSelect(event:any) {
        console.log(event);
        this.router.navigate(['trial'], { queryParams: { areas: event } });
    }
}
