package com.trialslink.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String PRIVILEGED = "ROLE_PRIVILEGED";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String DATA_MANAGER = "ROLE_DATA_MANAGER";

    private AuthoritiesConstants() {
    }
}
