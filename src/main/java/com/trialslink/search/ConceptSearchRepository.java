package com.trialslink.search;

import com.trialslink.domain.Concept;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Concept entity.
 */
public interface ConceptSearchRepository extends ElasticsearchRepository<Concept, String> {
}
