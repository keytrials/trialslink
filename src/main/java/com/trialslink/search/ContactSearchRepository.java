package com.trialslink.search;

import com.trialslink.domain.Contact;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link com.trialslink.domain.Contact} entity.
 */
public interface ContactSearchRepository extends ElasticsearchRepository<Contact, String> {
}
