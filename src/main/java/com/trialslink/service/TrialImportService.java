package com.trialslink.service;

import com.trialslink.domain.TrialImport;
import com.trialslink.domain.TrialImportItem;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * Service Interface for managing TrialImport.
 */
public interface TrialImportService {

    /**
     * Save a trialImport.
     *
     * @param trialImport the entity to save
     * @return the persisted entity
     */
    TrialImport save(TrialImport trialImport);

    /**
     *  Get all the trialImports.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<TrialImport> findAll(Pageable pageable);

    /**
     * Get the "id" trialImport.
     *
     * @param id the id of the entity
     * @return the entity
     */
    TrialImport findOne(String id);

    /**
     * Delete the "id" trialImport.
     *
     * @param id the id of the entity
     */
    void delete(String id);

    void deleteAllByIdNotNull();

    /**
     *  Get all the trialImports belong to current user.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<TrialImport> findAllByUserId(String userId, Pageable pageable);

    /**
     * Method that imports trials from a specified location/path passed as a {@link Resource}
     * @param resource the file system resource object
     * @return the result of the import - true when trials have been imported successfully.
     */
    boolean importTrials(Resource resource, Map<String, List<TrialImportItem>> reviwedTrials);

    Map<String, List<TrialImportItem>> reviewTrials(Resource resource);

}
