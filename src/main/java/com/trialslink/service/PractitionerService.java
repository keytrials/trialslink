package com.trialslink.service;

import com.trialslink.domain.Practitioner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Practitioner.
 */
public interface PractitionerService {

    /**
     * Save a practitioner.
     *
     * @param practitioner the entity to save
     * @return the persisted entity
     */
    Practitioner save(Practitioner practitioner);

    /**
     * Get all the practitioners.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Practitioner> findAll(Pageable pageable);

    /**
     * Get the "id" practitioner.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Practitioner findOne(String id);

    /**
     * Delete the "id" practitioner.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
