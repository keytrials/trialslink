package com.trialslink.service;

import com.trialslink.domain.TrialHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing TrialHistory.
 */
public interface TrialHistoryService {

    /**
     * Save a trialHistory.
     *
     * @param trialHistory the entity to save
     * @return the persisted entity
     */
    TrialHistory save(TrialHistory trialHistory);

    /**
     * Get all the trialHistories.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<TrialHistory> findAll(Pageable pageable);

    /**
     * Get the "id" trialHistory.
     *
     * @param id the id of the entity
     * @return the entity
     */
    TrialHistory findOne(String id);

    /**
     * Delete the "id" trialHistory.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
