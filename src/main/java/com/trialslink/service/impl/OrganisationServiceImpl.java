package com.trialslink.service.impl;

import com.trialslink.domain.Organisation;
import com.trialslink.repository.OrganisationRepository;
import com.trialslink.search.OrganisationSearchRepository;
import com.trialslink.service.OrganisationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;

/**
 * Service Implementation for managing Organisation.
 */
@Service
public class OrganisationServiceImpl implements OrganisationService{

    private final Logger log = LoggerFactory.getLogger(OrganisationServiceImpl.class);

    private final OrganisationRepository organisationRepository;

    private final OrganisationSearchRepository organisationSearchRepository;

    public OrganisationServiceImpl(OrganisationRepository organisationRepository,
                                   OrganisationSearchRepository organisationSearchRepository) {
        this.organisationRepository = organisationRepository;
        this.organisationSearchRepository = organisationSearchRepository;
    }

    /**
     * Save a organisation.
     *
     * @param organisation the entity to save
     * @return the persisted entity
     */
    @Override
    public Organisation save(Organisation organisation) {
        log.debug("Request to save Organisation : {}", organisation);
        // reuse existing id if org with same name exists
        Organisation existing = findOneByName(organisation.getName());
        if(existing != null){
            organisation.setId(existing.getId());
        }
        Organisation result = organisationRepository.save(organisation);
        organisationSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the organisations.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<Organisation> findAll(Pageable pageable) {
        log.debug("Request to get all Organisations");
        return organisationRepository.findAll(pageable);
    }


    /**
     * Get all the Organisation as List
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Organisation> findAllAsList() {
        log.debug("Request to get all Conditions");
        List<Organisation> result = organisationRepository.findAll();
        return result;
    }

    /**
     * Get one organisation by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Organisation findOne(String id) {
        log.debug("Request to get Organisation : {}", id);
        return organisationRepository.findOne(id);
    }

    /**
     *  Get one organisation by name.
     *
     *  @param name the name of the entity
     *  @return the entity
     */
    @Override
    public Organisation findOneByName(String name) {
        log.debug("Request to get Organisation by name : {}", name);
        Organisation organisation = organisationRepository.findOneByName(name);
        return organisation;
    }

    /**
     *  Delete the  organisation by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Organisation : {}", id);
        organisationRepository.delete(id);
        organisationSearchRepository.delete(id);
    }


    /**
     * Search for the organisation corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Organisation> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Organisations for query {}", query);
        Page<Organisation> result = organisationSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
