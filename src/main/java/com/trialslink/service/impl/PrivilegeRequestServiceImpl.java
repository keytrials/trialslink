package com.trialslink.service.impl;

import com.trialslink.domain.PrivilegeRequest;
import com.trialslink.domain.User;
import com.trialslink.domain.enumeration.PrivilegeRequestStatus;
import com.trialslink.repository.PrivilegeRequestRepository;
import com.trialslink.repository.UserRepository;
import com.trialslink.security.SecurityUtils;
import com.trialslink.service.PrivilegeRequestService;
import com.trialslink.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service Implementation for managing PrivilegeRequest.
 */
@Service
public class PrivilegeRequestServiceImpl implements PrivilegeRequestService {

    private final Logger log = LoggerFactory.getLogger(PrivilegeRequestServiceImpl.class);

    private final PrivilegeRequestRepository privilegeRequestRepository;

    private final UserRepository userRepository;

    private final UserService userService;

    public PrivilegeRequestServiceImpl(PrivilegeRequestRepository privilegeRequestRepository, UserRepository userRepository, UserService userService) {
        this.privilegeRequestRepository = privilegeRequestRepository;
        this.userRepository = userRepository;
        this.userService = userService;
    }

    /**
     * Save a privilegeRequest.
     *
     * @param privilegeRequest the entity to save
     * @return the persisted entity
     */
    @Override
    public PrivilegeRequest save(PrivilegeRequest privilegeRequest) {
        log.debug("Request to save PrivilegeRequest : {}", privilegeRequest);
        User user = findCurrentUser();
        if (user.hasPendingPrivilegeRequest()) {
            return privilegeRequest;
        }
        user.setPendingPrivilegeRequest(true);
        userRepository.save(user);
        return privilegeRequestRepository.save(privilegeRequest);
    }

    private User findCurrentUser(){
        log.debug("Request to get current user by username");
        Optional<User> user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin().get());
        log.debug("user = {}", user);
        return user.get();
    }

    /**
     * Get all the privilegeRequests.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<PrivilegeRequest> findAll(Pageable pageable) {
        log.debug("Request to get all PrivilegeRequests");
        return privilegeRequestRepository.findAll(pageable);
    }

    /**
     * Get one privilegeRequest by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public PrivilegeRequest findOne(String id) {
        log.debug("Request to get PrivilegeRequest : {}", id);
        return privilegeRequestRepository.findOne(id);
    }

    /**
     * Delete the  privilegeRequest by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete PrivilegeRequest : {}", id);
        privilegeRequestRepository.delete(id);
    }

    /**
     * Update the privilegeRequest.
     *
     * @param privilegeRequest the entity
     */
    @Override
    public PrivilegeRequest handleRequest(PrivilegeRequest privilegeRequest) {
        log.debug("Request to update PrivilegeRequest : {}", privilegeRequest);
        if (privilegeRequest.getStatus() == PrivilegeRequestStatus.APPROVED) {
            log.debug("Approving PrivilegeRequest : {}", privilegeRequest);
            userService.approvePrivilege(privilegeRequest.getCreatedBy());
        }

        return privilegeRequestRepository.save(privilegeRequest);
    }
}
