package com.trialslink.service.impl;

import com.trialslink.domain.SavedSearch;
import com.trialslink.domain.User;
import com.trialslink.repository.SavedSearchRepository;
import com.trialslink.repository.UserRepository;
import com.trialslink.security.SecurityUtils;
import com.trialslink.service.SavedSearchService;
import com.trialslink.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;


/**
 * Service Implementation for managing SavedSearch.
 */
@Service
public class SavedSearchServiceImpl implements SavedSearchService{

    private final Logger log = LoggerFactory.getLogger(SavedSearchServiceImpl.class);

    private final SavedSearchRepository savedSearchRepository;
    private final UserRepository userRepository;

    private final UserService userService;

    public SavedSearchServiceImpl(SavedSearchRepository savedSearchRepository, UserRepository userRepository,UserService userService) {
        this.savedSearchRepository = savedSearchRepository;
        this.userRepository = userRepository;
        this.userService = userService;
    }

    /**
     * Save a savedSearch.
     *
     * @param savedSearch the entity to save
     * @return the persisted entity
     */
    @Override
    public SavedSearch save(SavedSearch savedSearch) {
        log.debug("Request to save SavedSearch : {}", savedSearch);

        ZonedDateTime now = ZonedDateTime.now();

        if (savedSearch.getId() == null) {
            // this is a create operation, since no ID is set
            User user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin().get()).get();

            // set created_by field before computing hash since it is used in `SavedSearch.hashCode()`
            savedSearch.setCreatedBy(user.getId());
            String hashValue = Integer.toString(savedSearch.hashCode());

            SavedSearch existingSavedSearch = savedSearchRepository.findOneByHash(hashValue);
            if (existingSavedSearch != null) {
                existingSavedSearch.setUpdatedAt(now);
                return savedSearchRepository.save(existingSavedSearch);
            }

            // first time saving, set up hash value
            savedSearch.setHash(hashValue);
            savedSearch.setCreatedAt(now);
        }

        savedSearch.setUpdatedAt(now);
        return savedSearchRepository.save(savedSearch);
    }

    /**
     *  Get all the savedSearches.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    public Page<SavedSearch> findAll(Pageable pageable) {
        log.debug("Request to get all SavedSearches");
        User user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin().get()).get();

        return savedSearchRepository.findAllByCreatedBy(user.getId(), pageable);
    }

    /**
     * Get one savedSearch by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public SavedSearch findOne(String id) {
        log.debug("Request to get SavedSearch : {}", id);
        return savedSearchRepository.findOne(id);
    }

    /**
     * Delete the savedSearch by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete SavedSearch : {}", id);
        savedSearchRepository.delete(id);
    }
}
