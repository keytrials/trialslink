package com.trialslink.service.impl;

import com.trialslink.service.PractitionerService;
import com.trialslink.domain.Practitioner;
import com.trialslink.repository.PractitionerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing Practitioner.
 */
@Service
public class PractitionerServiceImpl implements PractitionerService{

    private final Logger log = LoggerFactory.getLogger(PractitionerServiceImpl.class);

    private final PractitionerRepository practitionerRepository;

    public PractitionerServiceImpl(PractitionerRepository practitionerRepository) {
        this.practitionerRepository = practitionerRepository;
    }

    /**
     * Save a practitioner.
     *
     * @param practitioner the entity to save
     * @return the persisted entity
     */
    @Override
    public Practitioner save(Practitioner practitioner) {
        log.debug("Request to save Practitioner : {}", practitioner);
        return practitionerRepository.save(practitioner);
    }

    /**
     * Get all the practitioners.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<Practitioner> findAll(Pageable pageable) {
        log.debug("Request to get all Practitioners");
        return practitionerRepository.findAll(pageable);
    }

    /**
     * Get one practitioner by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Practitioner findOne(String id) {
        log.debug("Request to get Practitioner : {}", id);
        return practitionerRepository.findOne(id);
    }

    /**
     * Delete the practitioner by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Practitioner : {}", id);
        practitionerRepository.delete(id);
    }
}
