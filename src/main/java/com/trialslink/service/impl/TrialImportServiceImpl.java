package com.trialslink.service.impl;

import com.csvreader.CsvReader;
import com.trialslink.TrialslinkApp;
import com.trialslink.domain.Trial;
import com.trialslink.domain.TrialImport;
import com.trialslink.domain.TrialImportItem;
import com.trialslink.domain.enumeration.TrialImportSource;
import com.trialslink.domain.enumeration.TrialImportStatus;
import com.trialslink.repository.TrialImportRepository;
import com.trialslink.repository.UserRepository;
import com.trialslink.service.TrialImportService;
import com.trialslink.service.TrialService;
import com.trialslink.service.TrialVisitorService;
import com.trialslink.trialimport.utils.CSVParser;
import com.trialslink.trialimport.utils.ExcelImporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;


/**
 * Service Implementation for managing TrialImport.
 */
@Service
public class TrialImportServiceImpl implements TrialImportService {

    private final Logger log = LoggerFactory.getLogger(TrialImportServiceImpl.class);

    private final TrialImportRepository trialImportRepository;
    private final UserRepository userRepository;
    private final RabbitTemplate rabbitTemplate;
    private final TrialService trialService;
    private final TrialVisitorService trialVisitorService;
    private final CSVParser csvParser;
    private final ExcelImporter excelImporter;

    public TrialImportServiceImpl(
        TrialImportRepository trialImportRepository,
        UserRepository userRepository,
        RabbitTemplate rabbitTemplate,
        TrialService trialService,
        TrialVisitorService trialVisitorService,
        CSVParser csvParser, ExcelImporter excelImporter
    ) {
        this.rabbitTemplate = rabbitTemplate;
        this.trialImportRepository = trialImportRepository;
        this.userRepository = userRepository;
        this.trialService = trialService;
        this.trialVisitorService = trialVisitorService;
        this.csvParser = csvParser;
        this.excelImporter = excelImporter;
    }

    /**
     * Save a trialImport.
     *
     * @param trialImport the entity to save
     * @return the persisted entity
     */
    @Override
    public TrialImport save(TrialImport trialImport) {
        log.debug("Request to save TrialImport : {}", trialImport);

        if (trialImport.getId() == null) {
            // this is a create operation and should be sent to the message queue
            trialImport.setStatus(TrialImportStatus.IN_PROGRESS);
        }
        trialImport = trialImportRepository.save(trialImport);
        queueTrialImport(trialImport);

        return trialImport;
    }

    /**
     *  Get all the trialImports.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    public Page<TrialImport> findAll(Pageable pageable) {
        log.debug("Request to get all TrialImports");
        return trialImportRepository.findAll(pageable);
    }

    /**
     * Get one trialImport by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public TrialImport findOne(String id) {
        log.debug("Request to get TrialImport : {}", id);
        return trialImportRepository.findOne(id);
    }

    /**
     * Delete the trialImport by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete TrialImport : {}", id);
        trialImportRepository.delete(id);
    }

    @Override
    public void deleteAllByIdNotNull() {
        log.debug("Request to delete all TrialImport");
        trialImportRepository.deleteAllByIdNotNull();
    }

    /**
     *  Get trialImport by userID.
     *
     *  @param pageable the pagination information
     *  @return the entity
     */
    @Override
    public Page<TrialImport> findAllByUserId(String userId, Pageable pageable) {
        log.debug("Request to get TrialImport by userId : {}", userId);
        return trialImportRepository.findAllByCreatedBy(userId, pageable);
    }

    private void queueTrialImport(TrialImport trialImport) {

        log.debug("Sending trial import to queue ", trialImport);
        if (trialImport.getSource() == TrialImportSource.NIHR) {
            // delay the queuing of NIHR trial imports as the uploaded files might still be undergoing processing on file.io
            rabbitTemplate.convertAndSend(TrialslinkApp.trialsQueue, trialImport, new MessagePostProcessor() {
                @Override
                public Message postProcessMessage(Message message) throws AmqpException {
                    message.getMessageProperties().setDelay(15000);
                    return message;
                }
            });
        } else {
            rabbitTemplate.convertAndSend(TrialslinkApp.trialsQueue, trialImport);
        }
    }

    /**
     * Method that imports trials from a specified location/path passed as a {@link Resource}
     * @param resource the file system resource object
     * @return the result of the import - true when trials have been imported successfully.
     */
    @Override
    public boolean importTrials(Resource resource, Map<String, List<TrialImportItem>> reviewedTrials) {

        boolean success = false;
        if(resource.getFilename().toLowerCase().endsWith(".csv")){
            try (InputStream inputStream = resource.getInputStream()) {
                CsvReader csvReader = new CsvReader(inputStream, Charset.defaultCharset());
                csvReader.setDelimiter('\t');
                csvReader.readHeaders();
                String[] csvHeaders = csvReader.getHeaders();
                log.info("csvHeaders = {}", csvHeaders);

                while (csvReader.readRecord()) {
                    try {
                        Trial trial = csvParser.readRow(csvReader);
                        log.info("trial = {}", trial);
                        trial = trialService.save(trial);
                        trialVisitorService.lungTrialsVisit(trial);
                        success = true;
                    } catch (IOException e) {
                        log.error("Error reading single trial from source file...", e);
                    }
                }
            } catch (NullPointerException | IOException e) {
                log.error("Unable to read trials data from class path. Nested exception is : ", e);
            }
        } else if(resource.getFilename().toLowerCase().endsWith(".xls") || resource.getFilename().toLowerCase().endsWith(".xlsx")){
            try{
                excelImporter.excelRead(resource.getFile(),reviewedTrials);
                success = true;
            } catch (NullPointerException | IOException e) {
                log.error("Unable to read trials data from class path. Nested exception is : ", e);
            }
        }
        trialService.indexAll();
        return success;
    }


    @Override
    public Map<String, List<TrialImportItem>> reviewTrials(Resource resource){
        try {
            Map<String, List<TrialImportItem>> map = excelImporter.trialReviewMap(resource.getFile());
            return map;
        } catch (IOException e) {
            log.error("Error generating trials for review from resource. Nested exception is : ", e);
        }
        return null;
    }

}
