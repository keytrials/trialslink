package com.trialslink.service.impl;

import com.trialslink.domain.Role;
import com.trialslink.repository.RoleRepository;
import com.trialslink.service.RoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Service Implementation for managing Role.
 */
@Service
public class RoleServiceImpl implements RoleService{

    private final Logger log = LoggerFactory.getLogger(RoleServiceImpl.class);

    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    /**
     * Save a role.
     *
     * @param role the entity to save
     * @return the persisted entity
     */
    @Override
    public Role save(Role role) {
        log.debug("Request to save Role : {}", role);
        return roleRepository.save(role);
    }

    /**
     * Get all the roles.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<Role> findAll(Pageable pageable) {
        log.debug("Request to get all Roles");
        return roleRepository.findAll(pageable);
    }

    /**
     * Get all the Roles as List
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Role> findAllAsList() {
        log.debug("Request to get all Conditions");
        List<Role> result = roleRepository.findAll();
        return result;
    }

    /**
     *  Get one role by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Role findOne(String id) {
        log.debug("Request to get Role : {}", id);
        return roleRepository.findOne(id);
    }

    /**
     * Delete the role by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Role : {}", id);
        roleRepository.delete(id);
    }
}
