package com.trialslink.service.impl;

import com.trialslink.domain.Characteristic;
import com.trialslink.domain.Trial;
import com.trialslink.repository.CharacteristicRepository;
import com.trialslink.service.CharacteristicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service Implementation for managing Characteristic.
 */
@Service
public class CharacteristicServiceImpl implements CharacteristicService{

    private final Logger log = LoggerFactory.getLogger(CharacteristicServiceImpl.class);

    private final CharacteristicRepository characteristicRepository;

    public CharacteristicServiceImpl(CharacteristicRepository characteristicRepository) {
        this.characteristicRepository = characteristicRepository;
    }

    /**
     * Save a characteristic.
     *
     * @param characteristic the entity to save
     * @return the persisted entity
     */
    @Override
    public Characteristic save(Characteristic characteristic) {
        log.debug("Request to save Characteristic : {}", characteristic);
        return characteristicRepository.save(characteristic);
    }

    /**
     * Get all the characteristics.
     *
     * @return the list of entities
     */
    @Override
    public List<Characteristic> findAll() {
        log.debug("Request to get all Characteristics");
        return characteristicRepository.findAll();
    }

    /**
     *  Get all the trialCentres for a given trial.
     *
     *  @param trial the trial
     *  @return the list of entities
     */
    @Override
    public Page<Characteristic> findAllForTrial(Trial trial, Pageable pageable) {
        log.debug("Request to get all TrialCentres");
        Page<Characteristic> result = characteristicRepository.findByTrialId(trial.getId(), pageable);
        return result;
    }

    /**
     *  Get one characteristic by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Characteristic findOne(String id) {
        log.debug("Request to get Characteristic : {}", id);
        return characteristicRepository.findOne(id);
    }

    /**
     * Delete the characteristic by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Characteristic : {}", id);
        characteristicRepository.delete(id);
    }

    /**
     * Delete all criteria for trial
     */
    @Override
    public void deleteAllForTrial(String trialId) {
        log.debug("Request to delete all criteria for trial {}", trialId);
        characteristicRepository.deleteByTrialId(trialId);
    }

}
