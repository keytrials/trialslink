package com.trialslink.service.impl;

import com.trialslink.domain.Contact;
import com.trialslink.repository.ContactRepository;
import com.trialslink.search.ContactSearchRepository;
import com.trialslink.service.ContactService;
import org.elasticsearch.index.IndexNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Service;

/**
 * Service Implementation for managing Contact.
 */
@Service
public class ContactServiceImpl implements ContactService{

    private final Logger log = LoggerFactory.getLogger(ContactServiceImpl.class);

    private final ContactRepository contactRepository;
    private final ContactSearchRepository contactSearchRepository;
    private final ElasticsearchTemplate elasticsearchTemplate;

    public ContactServiceImpl(ContactRepository contactRepository, ContactSearchRepository contactSearchRepository,
                              ElasticsearchTemplate elasticsearchTemplate) {
        this.contactRepository = contactRepository;
        this.contactSearchRepository = contactSearchRepository;
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    /**
     * Save a contact.
     *
     * @param contact the entity to save
     * @return the persisted entity
     */
    @Override
    public Contact save(Contact contact) {
        log.debug("Request to save Contact : {}", contact);
        return contactRepository.save(contact);
    }

    /**
     * Get all the contacts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<Contact> findAll(Pageable pageable) {
        log.debug("Request to get all Contacts");
        return contactRepository.findAll(pageable);
    }

    /**
     *  Get all the contacts for given trial centre.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    public Page<Contact> findAllForTrialCentre(String id, Pageable pageable) {
        log.debug("Request to get all Contacts for trial centre: {}", id);
        Page<Contact> result = contactRepository.findByTrialCentreId(id, pageable);
        return result;
    }

    /**
     *  Get one contact by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Contact findOne(String id) {
        log.debug("Request to get Contact : {}", id);
        return contactRepository.findOne(id);
    }

    /**
     * Delete the contact by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Contact : {}", id);
        contactRepository.delete(id);
    }


    /**
     *  Index all the contacts.
     *
     *  @return the boolean that represents the success of the index action
     */
    @Override
    public boolean indexAll() {
        log.debug("Request to index all Contacts");
        boolean result = false;

        // delete existing indices
        try {
            elasticsearchTemplate.deleteIndex(Contact.class);
        } catch (IndexNotFoundException e) {
            log.error("Error deleting contact index. Assuming index does not exist.");
        }

        try {
            contactRepository.findAll().forEach(contactSearchRepository::save);
            result = true;
        } catch (Exception e) {
            log.error("Error bulk indexing concepts. Nested exception is : ", e);
        }

        return result;
    }
}
