package com.trialslink.service.impl;

import com.trialslink.TrialslinkApp;
import com.trialslink.domain.Condition;
import com.trialslink.domain.Trial;
import com.trialslink.repository.TrialRepository;
import com.trialslink.service.AnnotationService;
import com.trialslink.service.ConditionService;
import com.trialslink.service.TrialService;
import com.trialslink.service.util.TermlexRestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A concrete implementation of {@link com.trialslink.service.AnnotationService} that annotates {@link Trial}s.
 */
@Service
@ConfigurationProperties(prefix = "annotator", ignoreUnknownFields = false)
public class AnnotationServiceImpl implements AnnotationService {

    int maxTrials = 800;
    String schedule = "0 0 1 * * ?";
    String sanitiseSchedule;
//    @Value("${annotator.conceptsToPurge}")
    List<String> conceptsToPurge;
    boolean processOnlyEmptyTrials = false;
    private final ConditionService conditionService;
    private final TrialService trialService;
    private final Logger log = LoggerFactory.getLogger(AnnotationServiceImpl.class);
    private final TermlexRestClient termlexRestClient;
    private final RabbitTemplate rabbitTemplate;
    private final TrialRepository trialRepository;
    private final Environment env;

    public AnnotationServiceImpl(ConditionService conditionService,
                                 TrialService trialService,
                                 TermlexRestClient termlexRestClient,
                                 RabbitTemplate rabbitTemplate,
                                 TrialRepository trialRepository, Environment environment) {
        this.conditionService = conditionService;
        this.trialService = trialService;
        this.termlexRestClient = termlexRestClient;
        this.rabbitTemplate = rabbitTemplate;
        this.trialRepository = trialRepository;
        this.env = environment;

        log.info("Setting schedule for annotate job to = {}", env.getProperty("annotator.schedule"));
    }

    /**
     *  Annotate all the trials.
     *  This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Override
    @Transactional(readOnly = false)
    @Scheduled(cron = "${annotator.schedule}")
    public void annotateAll() {
        log.debug("Request to annotate all Trials. Task schedule set to : ", schedule);

        int counter = 0;
        for(Trial trial : trialRepository.findAll()) {
            if(counter < maxTrials) {
                if(processOnlyEmptyTrials) {
                    if (trial.getConditions() != null && trial.getConditions().isEmpty() && !trial.isVerified()) {
                        rabbitTemplate.convertAndSend(TrialslinkApp.exchange, "annotation", trial);
                    }
                } else {
                    rabbitTemplate.convertAndSend(TrialslinkApp.exchange, "annotation", trial);
                }
            }
            // increment counter
            counter++;
        }
    }

    /**
     *  Sanitises existing trials by purging all conditions that have black listed concepts.
     *  This is scheduled to get fired everyday, at 04:00 (am).
     */
    @Override
    @Transactional(readOnly = false)
    @Scheduled(cron = "${annotator.sanitiseSchedule}")
    public void sanitiseAllTrials() {
        log.debug("Request to sanitise all Trials. Task schedule set to : ", schedule);

        trialRepository.findAll().forEach(trial -> {
            Set<Condition> conditions = new HashSet<>();
            trial.getConditions().forEach(condition -> {
                if(conceptsToPurge.contains(condition.getCode())) {
                    conditions.add(condition);
                }
            });
            // now remove conditions if they exist
            if(!conditions.isEmpty()) {
                trial.getConditions().removeAll(conditions);
                // conditions should also be removed from condition collection too - but sometimes conditions have no ids
                conditions.forEach(condition -> {
                    if (condition.getId() != null) {
                        conditionService.delete(condition.getId());
                    }
                });
                // save trial using trialService, which will also update the search index
                trialService.save(trial);
            }
        });
    }

    /**
     * A method that annotates a given trial
     * @param trial the trial to annotate
     * @return a reponse entity that returns OK if trial was annotated without issues.
     */
    @Override
    public ResponseEntity annotateTrial(Trial trial) {
        ResponseEntity<List> responseEntity = null;
        try {

        String text = trial.getTitle();
        responseEntity = termlexRestClient.processText(text);
        log.debug("responseEntity.getBody() = {}", responseEntity.getBody());
        if(HttpStatus.OK == responseEntity.getStatusCode()) {
            log.info("Successfully processed text {}", text);
            List<Map<String, Object>> matches = responseEntity.getBody();
            final Trial finalTrial = trial;
            if (matches != null && !matches.isEmpty()) {
                // get all existing codes associated with conditions of trial
                Set<String> existingCodes = trial.getConditions().stream().map(Condition::getCode).collect(Collectors.toSet());
                matches.forEach(match -> {
                    /*
                     within each match, we need to get the codes that are from SCT. From these we remove ones
                     that are already exist (existingCodes). The ones left over after removing will be the new ones to add
                     */
                    List<Map> codes = (List<Map>) match.get("codes");
                    List<Map> sctCodes = codes.stream().filter(codeMap -> {
                        String source = String.valueOf(codeMap.get("source"));
                        String id = String.valueOf(codeMap.get("id"));
                        return ("SCT".equalsIgnoreCase(source) && !existingCodes.contains(id));
                    }).collect(Collectors.toList());
                    log.info("sctCodes: ",sctCodes);

                    // now process any new codes contained in sctCodes
                    if (!sctCodes.isEmpty()) {
//                        List<String> conceptIds = new ArrayList<>();
//                        sctCodes.forEach(disease -> {
//                            conceptIds.add(String.valueOf(disease.get("id")));
//                        });
//                        Predicate<Condition> conditionPredicate = c -> conceptIds.contains(c.getCode());
//                        boolean exist = trial.getConditions().stream().allMatch(conditionPredicate);
//                        if (!exist) {
                            sctCodes.forEach(disease -> {
                            /*String conceptId = String.valueOf(disease.get("id"));
                            boolean existing = trial.getConditions().stream().filter(c -> c.getCode().equalsIgnoreCase(conceptId)).findAny().isPresent();*/

                                // create and add condition to trial
                                Condition condition = new Condition().code(String.valueOf(disease.get("id"))).label(String.valueOf(disease.get("preferredTerm")));
                                condition.setTrialId(finalTrial.getId());
                                condition = conditionService.save(condition);
                                finalTrial.addConditions(condition);
                            });
                            trialService.save(finalTrial);
//                        }
                    }
                });
            }

        } else {
            log.error("Unable to process text {}", text);
        }
        }catch (Exception e){
            throw new AmqpRejectAndDontRequeueException(e.getMessage());
        }
        return responseEntity;
    }

    public void setMaxTrials(int maxTrials) {
        this.maxTrials = maxTrials;
    }

    public void setProcessOnlyEmptyTrials(boolean processOnlyEmptyTrials) {
        this.processOnlyEmptyTrials = processOnlyEmptyTrials;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getSanitiseSchedule() {
        return sanitiseSchedule;
    }

    public void setSanitiseSchedule(String sanitiseSchedule) {
        this.sanitiseSchedule = sanitiseSchedule;
    }

    public List<String> getConceptsToPurge() {
        return conceptsToPurge;
    }

    public void setConceptsToPurge(List<String> conceptsToPurge) {
        this.conceptsToPurge = conceptsToPurge;
    }
}
