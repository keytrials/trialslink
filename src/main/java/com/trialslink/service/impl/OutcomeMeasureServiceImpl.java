package com.trialslink.service.impl;

import com.trialslink.service.OutcomeMeasureService;
import com.trialslink.domain.OutcomeMeasure;
import com.trialslink.repository.OutcomeMeasureRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


/**
 * Service Implementation for managing OutcomeMeasure.
 */
@Service
public class OutcomeMeasureServiceImpl implements OutcomeMeasureService{

    private final Logger log = LoggerFactory.getLogger(OutcomeMeasureServiceImpl.class);

    private final OutcomeMeasureRepository outcomeMeasureRepository;

    public OutcomeMeasureServiceImpl(OutcomeMeasureRepository outcomeMeasureRepository) {
        this.outcomeMeasureRepository = outcomeMeasureRepository;
    }

    /**
     * Save a outcomeMeasure.
     *
     * @param outcomeMeasure the entity to save
     * @return the persisted entity
     */
    @Override
    public OutcomeMeasure save(OutcomeMeasure outcomeMeasure) {
        log.debug("Request to save OutcomeMeasure : {}", outcomeMeasure);
        return outcomeMeasureRepository.save(outcomeMeasure);
    }

    /**
     * Get all the outcomeMeasures.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    public Page<OutcomeMeasure> findAll(Pageable pageable) {
        log.debug("Request to get all OutcomeMeasures");
        return outcomeMeasureRepository.findAll(pageable);
    }

    /**
     * Get one outcomeMeasure by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public OutcomeMeasure findOne(String id) {
        log.debug("Request to get OutcomeMeasure : {}", id);
        return outcomeMeasureRepository.findOne(id);
    }

    /**
     * Delete the outcomeMeasure by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete OutcomeMeasure : {}", id);
        outcomeMeasureRepository.delete(id);
    }
}
