package com.trialslink.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.trialslink.domain.Organisation;
import com.trialslink.domain.Trial;
import com.trialslink.domain.TrialHistory;
import com.trialslink.repository.TrialHistoryRepository;
import com.trialslink.repository.TrialRepository;
import com.trialslink.search.TrialSearchRepository;
import com.trialslink.security.SecurityUtils;
import com.trialslink.service.TrialService;
import com.trialslink.service.UserService;
import com.trialslink.service.util.LookupGenerator;
import com.trialslink.service.util.TermlexRestClient;
import com.trialslink.web.rest.util.QueryModel;
import org.elasticsearch.index.IndexNotFoundException;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.FacetedPage;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Trial.
 */
@Service
public class TrialServiceImpl implements TrialService {

    private final Logger log = LoggerFactory.getLogger(TrialServiceImpl.class);

    private final TrialRepository trialRepository;
    private final TrialSearchRepository trialSearchRepository;
    private final TrialHistoryRepository trialHistoryRepository;
    private final ElasticsearchTemplate elasticsearchTemplate;
    private final UserService userService;
    private final TermlexRestClient termlexRestClient;
    private final Pattern idPattern = Pattern.compile("\\d{6,18}\\D*");
    private final Map<String, Object> options = new HashMap<>();

    public TrialServiceImpl(
        TrialRepository trialRepository,
        TrialSearchRepository trialSearchRepository,
        TrialHistoryRepository trialHistoryRepository,
        ElasticsearchTemplate elasticsearchTemplate,
        LookupGenerator lookupGenerator,
        UserService userService,
        TermlexRestClient termlexRestClient
    ) {
        this.trialRepository = trialRepository;
        this.trialSearchRepository = trialSearchRepository;
        this.trialHistoryRepository = trialHistoryRepository;
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.userService = userService;
        this.termlexRestClient = termlexRestClient;

        // create optionsMap for doing a look up of ids
        int[] conceptTypes = {404684003, 64572001};
        options.put("conceptType", conceptTypes);
        options.put("maxResultsSize", 100);
        options.put("maxInstantResultsSize", 100);
    }

    /**
     * Save a trial.
     *
     * @param trial the entity to save
     * @return the persisted entity
     */
    @Override
    @Transactional
    public Trial save(Trial trial) {
        log.debug("Request to save Trial : {}", trial);
        Trial result = trialRepository.save(trial);
        this.saveHistory(result);
        trialSearchRepository.delete(result);
        trialSearchRepository.save(result);
        return result;
    }

    @Transactional
    private void saveHistory(Trial trial) {
        TrialHistory trialHistory = trialHistoryRepository.findOne(trial.getId());
        if (trialHistory == null) {
            trialHistory = new TrialHistory();
            trialHistory.setId(trial.getId());
        }
        trialHistory.addSnapshot(Instant.now().getEpochSecond(), trial);
        trialHistoryRepository.save(trialHistory);
    }

    /**
     *  Get all the trials.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Trial> findAll(Pageable pageable) {
        log.debug("Request to get all Trials");
        return trialRepository.findAll(pageable);
    }

    /**
     * Get one trial by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public Trial findOne(String id) {
        log.debug("Request to get Trial : {}", id);
        return trialRepository.findOne(id);
    }

    /**
     *  Get trial for shortName.
     *
     *  @param shortName the shortName of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public List<Trial> findByShortName(String shortName) {
        log.debug("Request to get Trial by short name: {}", shortName);
        List<Trial> trials = trialSearchRepository.findByShortName(shortName);
        return trials;
    }

    /**
     *  Delete the  trial by id.
     *
     * @param id the id of the entity
     */
    @Override
    @Transactional
    public void delete(String id) {
        log.debug("Request to delete Trial : {}", id);
        trialRepository.delete(id);
        trialSearchRepository.delete(id);
    }

    /**
     * Verify ownership of trial
     *
     * @param id the id of the trial
     * @return the trial if user is owner or has ADMIN role - otherwise returns null
     */
    @Override
    @Transactional(readOnly = true)
    public Trial verifyOwnershipAndGet(String id) {
        log.debug("Request to verify ownership and get Trial : {}", id);
        Trial operino = trialRepository.findOneByCreatedByAndId(SecurityUtils.getCurrentUserLogin().get(), id);
        if (operino != null) {
            return operino;
        }
        else if (userService.isAdmin()) {
            return trialRepository.findOne(id);
        }
        else {
            return null;
        }
    }

    /**
     *  Index all the trials.
     *
     *  @return the boolean that represents the success of the index action
     */
    @Override
    @Transactional(readOnly = true)
    public boolean indexAll() {
        log.debug("Request to get all Trials");
        boolean result = false;

        // delete existing indices
//        try {
//            log.debug("Request to get all -------------- {}", elasticsearchTemplate.getMapping(Trial.class));
//
//            elasticsearchTemplate.deleteIndex(Trial.class);
//            elasticsearchTemplate.deleteIndex(Organisation.class);
//        } catch (IndexNotFoundException e) {
//            log.error("Error deleting indices. Assuming index does not exist.");
//        }

        try {
            // set sites only for lung trials
            trialRepository.findAll().forEach(trial -> trial.getSites().forEach(site -> {
                // deleting each trial and saving
                trialSearchRepository.delete(trial);
                trialSearchRepository.save(trial);
            }));
            result = true;
            log.debug("trial index mapping {}", elasticsearchTemplate.getMapping(Trial.class));

        } catch (Exception e) {
            log.error("Error bulk indexing trials.Nested exception is : ", e);
        }

        return result;
    }

    /**
     * Search for the trial corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public FacetedPage<Trial> search(QueryModel query, Pageable pageable) {
        log.debug("Request to search for a page of Trials for query {}", query);

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

        BoolQueryBuilder conceptTypesQueryBuilder = QueryBuilders.boolQuery();
        // verify if any passed conditions are non-SNOMED CT ids // test for non digits
        List<String> conditions = query.getConditions();

        Set<String> maps = new HashSet<>();
        for (String condition : conditions) {
            Matcher matcher = idPattern.matcher(condition);
            if(!matcher.find()) {
                // fix for codes with dot
                condition = condition.replaceAll("\\.", "");
                options.put("term", condition);
                ResponseEntity<Map> responseEntity = termlexRestClient.getSearchResults(options);
                if(responseEntity.getStatusCode() == HttpStatus.OK) {
                    List<Map> results = (List<Map>) responseEntity.getBody().get("results");
                    log.debug("results = {}", results);
                    if (results != null) {
                        maps.addAll(results.stream().map(result -> String.valueOf(result.get("id"))).collect(Collectors.toList()));
                    }
                } else {
                    log.error("Unable to SNOMED CT concepts for given conditions. Termlex response is : []", responseEntity.getBody());
                }
            }
        }
        // if any SNOMED CT codes were returned by looking up ICD map, then we add to conditions
        if(!maps.isEmpty()) {
            conditions.addAll(maps);
        }

        // appending the concept code with concept name(eg: '363346000|Cancer') as below - #408 issue https://gitlab.com/keytrials/trialslink/issues/408
        for (String condition : conditions) {
            if (condition.indexOf('|') > 0) {
                // the below line gets only the concept codes for searching
                String[] condition1 = condition.split(Pattern.quote("|"));
                conditions.set(conditions.indexOf(condition), condition1[0]);
            }
        }
        // expand query to include both ancestors and descendants - only if conditions are not empty
        if (!conditions.isEmpty()) {
            Set<String> expandedIds = new HashSet<>();
            try {
                if(query.getIncludeAncestors()) {
                    expandedIds.addAll(termlexRestClient.getAncestors(conditions));
                }
                expandedIds.addAll(termlexRestClient.getDescendants(conditions));
                // adding the conditions itself to the array
                expandedIds.addAll(conditions);
            } catch (JsonProcessingException e) {
                log.error("Unable to get ancestors/descendants for concepts {}. Nested exception is : []", e);
            }

            if(! expandedIds.isEmpty()) {
                conceptTypesQueryBuilder.should(QueryBuilders.termsQuery("conditions.code", expandedIds));
            }
        }

        BoolQueryBuilder sourcesQueryBuilder = QueryBuilders.boolQuery();
        for(String location : query.getLocations()) {
            sourcesQueryBuilder.should(QueryBuilders.matchQuery("sites.orgId", location));
        }

        BoolQueryBuilder statusQueryBuilder = QueryBuilders.boolQuery();
        for(String status : query.getStatuses()) {
            statusQueryBuilder.should(QueryBuilders.matchQuery("status", status));
        }

        BoolQueryBuilder siteStatusQueryBuilder = QueryBuilders.boolQuery();
        for(String status : query.getSiteStatuses()) {
            siteStatusQueryBuilder.should(QueryBuilders.matchQuery("sites.status", status));
        }

        BoolQueryBuilder conditionStatusQueryBuilder = QueryBuilders.boolQuery();
        for(String status : query.getConditionStatuses()) {
            conditionStatusQueryBuilder.should(QueryBuilders.matchQuery("conditions.statusCode", status));
        }

        BoolQueryBuilder phaseQueryBuilder = QueryBuilders.boolQuery();
        for(String phase : query.getPhases()) {
            phaseQueryBuilder.should(QueryBuilders.matchQuery("phases", phase));
        }

        BoolQueryBuilder typeQueryBuilder = QueryBuilders.boolQuery();
        for(String type : query.getTypes()) {
            typeQueryBuilder.should(QueryBuilders.matchQuery("type", type));
        }

        BoolQueryBuilder areaQueryBuilder = QueryBuilders.boolQuery();
        for(String type : query.getAreas()) {
            areaQueryBuilder.should(QueryBuilders.multiMatchQuery(type, "category").type(MultiMatchQueryBuilder.Type.PHRASE));
        }

        BoolQueryBuilder siteQueryBuilder = QueryBuilders.boolQuery();
        for(String type : query.getSites()) {
            siteQueryBuilder.should(QueryBuilders.multiMatchQuery(type, "focus").type(MultiMatchQueryBuilder.Type.PHRASE));
        }

        BoolQueryBuilder genderQueryBuilder = QueryBuilders.boolQuery();
        for(String gender : query.getGenders()) {
            genderQueryBuilder.should(QueryBuilders.matchQuery("eligibilities.sex", gender));
        }

        if(query.getAge() != null) {
            boolQueryBuilder.must(
                    QueryBuilders.boolQuery().should(QueryBuilders.rangeQuery("eligibilities.minAge.value").lte(query.getAge()))
                    .should(QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery("eligibilities.minAge.value")))
            );
            boolQueryBuilder.must(
                    QueryBuilders.boolQuery().should(QueryBuilders.rangeQuery("eligibilities.maxAge.value").gte(query.getAge()))
                    .should(QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery("eligibilities.maxAge.value")))
            );
        }

        // we only add conditions clause if there are more than 1 condition specified
        if(query.getConditions().size() > 0){
            boolQueryBuilder.must(conceptTypesQueryBuilder);
        }

        // we only add locations clause if there are more than 1 locations specified
        if(query.getLocations().size() > 0){
            boolQueryBuilder.must(sourcesQueryBuilder);
        }

        // we only add phases clause if there are more than 1 phase specified
        if (query.getPhases().size() > 0){
            boolQueryBuilder.must(phaseQueryBuilder);
        }

        // we only add status clause if there are more than 1 status specified
        if(query.getStatuses().size() > 0){
            boolQueryBuilder.must(statusQueryBuilder);
        }

        // we only add site status clause if there are more than 1 site status specified
        if(query.getSiteStatuses().size() > 0){
            boolQueryBuilder.must(siteStatusQueryBuilder);
        }

        // we only add condition status clause if there are more than 1 condition status specified
        if(query.getConditionStatuses().size() > 0){
            boolQueryBuilder.must(conditionStatusQueryBuilder);
        }

        // we only add types clause if there are more than 1 type specified
        if(query.getTypes().size() > 0){
            boolQueryBuilder.must(typeQueryBuilder);
        }

        // we only add areas clause if there are more than 1 area specified
        if(query.getAreas().size() > 0){
            boolQueryBuilder.must(areaQueryBuilder);
        }

        // we only add sites clause if there are more than 1 site specified
        if(query.getSites().size() > 0){
            boolQueryBuilder.must(siteQueryBuilder);
        }

        // we only add gender clause if there are more than 1 gender specified
        if (query.getGenders().size() > 0){
            boolQueryBuilder.must(genderQueryBuilder);
        }

        // we only add token clause if there is a token specified
        if (query.getToken() != null && query.getToken().length() > 2){
            BoolQueryBuilder tokenBuilder = QueryBuilders.boolQuery();
            tokenBuilder.should(QueryBuilders.multiMatchQuery(query.getToken(), "idInfo.*", "keywords", "id").type(MultiMatchQueryBuilder.Type.PHRASE));
            tokenBuilder.should(QueryBuilders.multiMatchQuery(query.getToken(), "title", "briefTitle", "shortName",
                "chiefInvestigator.personName", "sites.principalInvestigator.personName").operator(MatchQueryBuilder.Operator.AND));
            tokenBuilder.should(QueryBuilders.multiMatchQuery(query.getToken(), "title", "briefTitle", "shortName",
                "chiefInvestigator.personName", "sites.principalInvestigator.personName").type(MultiMatchQueryBuilder.Type.PHRASE_PREFIX));

            boolQueryBuilder.must(tokenBuilder);
        }
        log.debug("boolQueryBuilder = " + boolQueryBuilder);
        // build and return boolean query
        return getFacetedPageForQuery(boolQueryBuilder, pageable);
    }



    /**
         * Omni search for the trial corresponding to the query but not on conditions or sites.
         *
         *  @param query the query of the search
         *  @return the list of entities
         */
    @Override
    @Transactional(readOnly = true)
    public FacetedPage<Trial> omniSearch(String query, Pageable pageable) {
        log.debug("Request to search for a page of Trials for query {}", query);

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.should(QueryBuilders.multiMatchQuery(query, "idInfo.*", "shortName", "id").type(MultiMatchQueryBuilder.Type.PHRASE_PREFIX));
        boolQueryBuilder.should(QueryBuilders.multiMatchQuery(query, "keywords", "title", "briefTitle").type(MultiMatchQueryBuilder.Type.PHRASE));
        log.debug("boolQueryBuilder = " + boolQueryBuilder);
        // build and return boolean query
        return getFacetedPageForQuery(boolQueryBuilder,pageable);
    }

    /**
     * Return all trials corresponding to the page including all categories.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public FacetedPage<Trial> findAllWithCategories(Pageable pageable) {
        log.debug("Request to search for a page of Trials for page {}", pageable);
        // build and return match all query
        return getFacetedPageForQuery(QueryBuilders.matchAllQuery(), pageable);
    }

    private FacetedPage<Trial> getFacetedPageForQuery(QueryBuilder queryBuilder, Pageable pageable) {
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(queryBuilder)
                .withSort(getSortParameters(pageable))
                .withPageable(pageable)
                .addAggregation(new TermsBuilder("types").field("type").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("statuses").field("status").size(10).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("siteStatuses").field("sites.status").size(10).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("areas").field("category").size(100).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("sites").field("focus").size(100).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("phases").field("phases").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("locations").field("sites.orgId").size(250).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("conditions").field("conditions.code").size(250).order(Terms.Order.term(true)))
                // genders filter removed at client request case #170
//                .addAggregation(new TermsBuilder("genders").field("eligibilities.sex").size(5).order(Terms.Order.term(true)))
                .addAggregation(new TermsBuilder("conditionStatuses").field("conditions.statusCode").size(10).order(Terms.Order.term(true)))
                .build();

        FacetedPage<Trial> page = elasticsearchTemplate.queryForPage(searchQuery, Trial.class);

        return page;
    }

    private FieldSortBuilder getSortParameters(Pageable pageable) {
        List<Sort.Order> orders = new ArrayList<>() ;
        if (pageable != null && pageable.getSort() != null) {
            for (Sort.Order order : pageable.getSort()) {
                String encapsulatedProperty = "("+order.getProperty() + ")";
                orders.add(new Sort.Order(order.getDirection(), encapsulatedProperty));
            }
        }

        SortOrder sortOrder = SortOrder.ASC;
        String sortField = "briefTitle";

        if (!orders.isEmpty()) {
            Sort.Order order = orders.get(0);
            if(order != null) {
                if(order.getProperty() != null) {
                    sortField = order.getProperty();
                }
                if(order.isDescending()) {
                    sortOrder = SortOrder.DESC;
                }
            }
        }

        return SortBuilders.fieldSort(sortField).order(sortOrder).unmappedType("string");
    }
}
