package com.trialslink.service.util;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.reflection.AbstractReflectionConverter;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.trialslink.domain.*;
import com.trialslink.domain.enumeration.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;

/**
 * A custom converter for {@link com.trialslink.domain.Trial} from ClinicalTrials.gov data
 */
public class TrialXmlConverter implements Converter {

    private final Logger log = LoggerFactory.getLogger(TrialXmlConverter.class);
    boolean skipConditions = true;
    boolean skipInterventions = true;
    boolean skipLocations = true;

    @Override
    public void marshal(Object o, HierarchicalStreamWriter hierarchicalStreamWriter, MarshallingContext marshallingContext) {
        // not implemented
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        Trial trial = new Trial();

        while (reader.hasMoreChildren()) {
            reader.moveDown();
            String nodeName = reader.getNodeName();
            if ("id_info".equals(nodeName)) {
                IdInfo idInfo = (IdInfo)context.convertAnother(trial, IdInfo.class);
                trial.setIdInfo(idInfo);
            } else if ("brief_title".equals(nodeName)) {
                trial.setBriefTitle(reader.getValue());
            } else if ("overall_official".equals(nodeName)) {
                reader.moveDown();
                Contact contact = new Contact();
                if(reader.getNodeName().equalsIgnoreCase("last_name")){
                    contact.setPersonName(reader.getValue());
                }
                reader.moveUp();
                trial.setChiefInvestigator(contact);
            } else if ("official_title".equals(nodeName)) {
                trial.setTitle(reader.getValue());
            }
            else if ("condition".equals(nodeName)) {
                if (!skipConditions) {
                    trial.addConditions(new Condition().label(reader.getValue()));
                }
            } else if ("brief_summary".equals(nodeName)) {
                reader.moveDown();
                trial.setBriefSummary(reader.getValue());
                reader.moveUp();
            } else if ("detailed_description".equals(nodeName)) {
                reader.moveDown();
                trial.setDetailedDescription(reader.getValue());
                reader.moveUp();
            }
            else if ("intervention".equals(nodeName)) {
                if (!skipInterventions) {
                    try {
                        Intervention intervention = (Intervention) context.convertAnother(trial, Intervention.class);
                        trial = trial.addInterventions(intervention);
                    } catch (AbstractReflectionConverter.DuplicateFieldException e) {
                       log.warn("Skipping import of intervention. Nested error : ", e);
                    }
                }
            } else if ("sponsors".equals(nodeName)) {
                reader.moveDown();
                if(reader.getNodeName().equalsIgnoreCase("lead_sponsor")){
                    reader.moveDown();
                    if(reader.getNodeName().equalsIgnoreCase("agency")){
                        trial.setSponsor(new Organisation().name(reader.getValue()));
                    }
                    reader.moveUp();
                }
                reader.moveUp();
            }
            else if ("overall_status".equals(nodeName)) {
                trial.setStatus(TrialStatus.valueOf(reader.getValue().toUpperCase().replace("STATUS", "").replace(",", "").trim().replace(" ", "_")));
            }
            else if ("phase".equals(nodeName)) {
                String phaseString = reader.getValue();
                if(phaseString == null) {
                    trial.setPhases(Collections.singleton(Phase.UNKNOWN));
                } else {
                    // we can not do if else - there might be more than one phase in this string!!!
                    if(phaseString.contains("1")){
                        trial.getPhases().add(Phase.ONE);
                    }
                    if(phaseString.contains("2")){
                        trial.getPhases().add(Phase.TWO);
                    }
                    if(phaseString.contains("3")){
                        trial.getPhases().add(Phase.THREE);
                    }
                    if(phaseString.contains("4")){
                        trial.getPhases().add(Phase.FOUR);
                    }
                    if(phaseString.contains("0")){
                        trial.getPhases().add(Phase.ZERO);
                    }
                }
            }
            else if ("study_type".equals(nodeName)) {
                trial.setType(StudyType.valueOf(reader.getValue().toUpperCase()));
            } else if ("has_expanded_access".equals(nodeName)) {
                if("No".equalsIgnoreCase(reader.getValue())){
                    trial.hasExpandedAccess(false);
                } else {
                    trial.hasExpandedAccess(true);
                }
            } else if ("brief_title".equals(nodeName)) {
                trial.setBriefTitle(reader.getValue());
            } else if ("brief_title".equals(nodeName)) {
                trial.setBriefTitle(reader.getValue());
            } else if ("brief_title".equals(nodeName)) {
                trial.setBriefTitle(reader.getValue());
            } else if ("brief_title".equals(nodeName)) {
                trial.setBriefTitle(reader.getValue());
            } else if ("brief_title".equals(nodeName)) {
                trial.setBriefTitle(reader.getValue());
            } else if ("brief_title".equals(nodeName)) {
                trial.setBriefTitle(reader.getValue());
            }
            else if ("eligibility".equals(nodeName)) {
                Characteristic characteristic = new Characteristic();
                while(reader.hasMoreChildren()){
                    reader.moveDown();
                    if(reader.getNodeName().equalsIgnoreCase("criteria")){
                        reader.moveDown();
                        if(reader.getNodeName().equalsIgnoreCase("textblock")){
                            characteristic.setCriteria(reader.getValue());
                        }
                        reader.moveUp();
                    } else if(reader.getNodeName().equalsIgnoreCase("gender")){
                        characteristic.setSex(Sex.valueOf(reader.getValue().toUpperCase()));
                    } else if(reader.getNodeName().equalsIgnoreCase("minimum_age")){
                        characteristic.setMinAge(inferAge(reader.getValue()));
                    } else if(reader.getNodeName().equalsIgnoreCase("maximum_age")){
                        characteristic.setMaxAge(inferAge(reader.getValue()));
                    } else if(reader.getNodeName().equalsIgnoreCase("healthy_volunteers")){
                        characteristic.setAcceptHealthyVolunteers(!reader.getValue().equalsIgnoreCase("no"));
                    }
                    reader.moveUp();
                }
                trial.addEligibility(characteristic);
            }
            else if ("location".equals(nodeName)) {
                if (!skipLocations) {
                    TrialCentre trialCentre = new TrialCentre();
                    while (reader.hasMoreChildren()) {
                        reader.moveDown();
                        if(reader.getNodeName().equalsIgnoreCase("facility")){
                            Organisation organisation = (Organisation)context.convertAnother(trial, Organisation.class);
                            trialCentre.setAddress(organisation.getAddress());
                            trialCentre.setName(organisation.getName());
                        } else if(reader.getNodeName().equalsIgnoreCase("status")){
                            trialCentre.setStatus(TrialStatus.valueOf(reader.getValue().toUpperCase().replace("STATUS", "").replace(",", "").trim().replace(" ", "_")));
                        } else if(reader.getNodeName().equalsIgnoreCase("contact")){
                            Contact contact = (Contact)context.convertAnother(trial, Contact.class);
                            // hack for email empty field
                            if(contact.getEmail() == null){
                                contact.setEmail("");
                            }
                            if(contact.getPhone() == null){
                                contact.setPhone("");
                            }
                            trialCentre.addContact(contact);
                        }
                        reader.moveUp();
                    }
                    trial.addSite(trialCentre);
                }
            }
            reader.moveUp();
        }
        return trial;
    }

    public boolean isSkipConditions() {
        return skipConditions;
    }

    public void setSkipConditions(boolean skipConditions) {
        this.skipConditions = skipConditions;
    }

    public boolean isSkipInterventions() {
        return skipInterventions;
    }

    public void setSkipInterventions(boolean skipInterventions) {
        this.skipInterventions = skipInterventions;
    }

    public boolean isSkipLocations() {
        return skipLocations;
    }

    public void setSkipLocations(boolean skipLocations) {
        this.skipLocations = skipLocations;
    }

    private Age inferAge(String ageString){
        Age age = new Age();
        for(AgeUnit unit: AgeUnit.values()){
            int index = ageString.toLowerCase().indexOf(unit.name().toLowerCase());
            if(index > 0){
                age.setUnit(unit);
                ageString = ageString.substring(0, index).trim();
                age.setValue(Integer.valueOf(ageString));
                break;
            }
        }

        return age;
    }

    @Override
    public boolean canConvert(Class clazz) {
        return Trial.class == clazz;
    }
}
