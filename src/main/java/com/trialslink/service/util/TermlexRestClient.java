package com.trialslink.service.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A naive rest client for Gate Cloud
 */
@Service
@ConfigurationProperties(prefix = "termlexclient", ignoreUnknownFields = false)
public class TermlexRestClient {

    String userName;
    String password;
    String baseUrl;
    String apiToken;
    final RestTemplate restTemplate;
    final ObjectMapper objectMapper;
    private final Logger log = LoggerFactory.getLogger(TermlexRestClient.class);

    public TermlexRestClient(RestTemplate restTemplate, ObjectMapper objectMapper) {
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
    }

    public ResponseEntity<List> processText(String text) {

        HttpEntity<Object> request = new HttpEntity<>(text, getAdminHeaders());
        log.debug("request = " + request);

        ResponseEntity<List> responseEntity = null;
        try {
            responseEntity = restTemplate.postForEntity(baseUrl + "annotate", request, List.class);
        } catch (HttpServerErrorException e) {
            log.error("Error annotating text via Termlex. Nested exception is : ", e);
        }
        log.debug("responseEntity = {}", responseEntity);
        return responseEntity;
    }

    public Set<String> getDescendants(List<String> conceptIds) throws JsonProcessingException {

        Set<String> descendants = new HashSet<>();
        HttpEntity<Object> request = new HttpEntity<>(objectMapper.writeValueAsString(conceptIds), getAdminHeaders());
        log.debug("request = " + request);

        ResponseEntity<Set> responseEntity = restTemplate.exchange(
                baseUrl+"hierarchy/version/null/descendants",
                HttpMethod.POST,
                request,
                Set.class
        );
        log.debug("responseEntity = {}", responseEntity);

        //process response
        if(HttpStatus.OK == responseEntity.getStatusCode()) {
            descendants = responseEntity.getBody();
        }

        return descendants;
    }

    public Set<String> getAncestors(List<String> conceptIds) throws JsonProcessingException {

        Set<String> ancestors = new HashSet<>();
        HttpEntity<Object> request = new HttpEntity<>(objectMapper.writeValueAsString(conceptIds), getAdminHeaders());
        log.debug("request = " + request);

        ResponseEntity<Set> responseEntity = restTemplate.exchange(
                baseUrl+"hierarchy/version/null/ancestors",
                HttpMethod.POST,
                request,
                Set.class
        );
        log.debug("responseEntity = {}", responseEntity);

        //process response
        if(HttpStatus.OK == responseEntity.getStatusCode()) {
            ancestors = responseEntity.getBody();
        }

        return ancestors;
    }

    public ResponseEntity<Map> getSearchResults(Map<String, Object> options) {

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "search/sct")
                .queryParam("term", options.get("term"))
                .queryParam("maxInstantResultsSize", options.get("maxInstantResultsSize"))
                .queryParam("conceptType", options.get("conceptType"))
                .queryParam("maxResultsSize", options.get("maxResultsSize"));
        HttpEntity<Object> request = new HttpEntity<>(options, getAdminHeaders());
        log.debug("request = " + request);
        log.debug("builder.build().encode().toUri() = " + builder.build().encode().toUri());

        ResponseEntity<Map> responseEntity = restTemplate.exchange(
                builder.build().encode().toUri(),
                HttpMethod.GET,
                request,
                Map.class
        );
        log.debug("responseEntity = " + responseEntity);

        return responseEntity;
    }

    private HttpHeaders getAdminHeaders() {
        // set headers
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + apiToken);
        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }
}
