package com.trialslink.service.util;

import com.trialslink.domain.Concept;
import com.trialslink.service.ConceptService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Utility class for generating lookups
 */
@Service
public class LookupGenerator implements InitializingBean {

    private final ConceptService conceptService;
    private final Logger log = LoggerFactory.getLogger(LookupGenerator.class);
    private Map<String, Set<String>> descendantsLookup = new HashMap<>();
    private Map<String, Set<String>> ancestorsLookup = new HashMap<>();

    public LookupGenerator(ConceptService conceptRepository) {
        this.conceptService = conceptRepository;
    }

    public Set<String> getDescendants(String id){

        Set<String> descendantIds = new HashSet<>();
        Concept c = conceptService.findOne(id);
        if(c == null){
            log.warn("No concept found with id {}", id);
        } else {
            c.getChildren().forEach(childId -> {
                descendantIds.add(childId);
                // recursive call
                descendantIds.addAll(getDescendants(childId));
            });
        }

        return descendantIds;
    }

    public Set<String> getAncestors(String id){

        Set<String> ancestorIds = new HashSet<>();
        Concept c = conceptService.findOne(id);
        if(c == null){
            log.warn("No concept found with id {}" , id);
        } else {
            c.getParents().forEach(parentId -> {
                ancestorIds.add(parentId);
                // recursive call
                ancestorIds.addAll(getAncestors(parentId));
            });
        }

        return ancestorIds;
    }

    public Map<String, Set<String>> getDescendantsLookup() {
        return descendantsLookup;
    }

    public Map<String, Set<String>> getAncestorsLookup() {
        return ancestorsLookup;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
     populateLookups();
    }

    public void populateLookups() {
        // we need to generate lookup maps - save them in memory for faster execution
        conceptService.findAllAsList().forEach(concept -> {
            ancestorsLookup.put(concept.getId(), this.getAncestors(concept.getId()));
            descendantsLookup.put(concept.getId(), this.getDescendants(concept.getId()));
        });
        log.info("Ancestors lookup generated for {} concepts " + ancestorsLookup.keySet().size());
        log.info("Descendants lookup generated for {} concepts " + descendantsLookup.keySet().size());
    }
}
