package com.trialslink.service.notifications;

import com.trialslink.domain.SavedSearch;
import com.trialslink.domain.Trial;
import com.trialslink.domain.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by pigiotisk on 27/07/2017.
 */
public class UserNotification implements Serializable, Notifications {

    private static final long serialVersionUID = 1L;

    private ArrayList<Trial> trials;
    private SavedSearch savedSearch;

    @Override
    public SavedSearch getSavedSearch() {
        return savedSearch;
    }

    @Override
    public void setSavedSearch(SavedSearch savedSearch) {
        this.savedSearch = savedSearch;
    }

    @Override
    public ArrayList<Trial> getTrials() {
        return trials;
    }

    @Override
    public void setTrials(ArrayList<Trial> trials) {
        this.trials = trials;
    }
}
