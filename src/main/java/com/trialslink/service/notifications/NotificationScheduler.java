package com.trialslink.service.notifications;

import com.trialslink.TrialslinkApp;
import com.trialslink.domain.SavedSearch;
import com.trialslink.domain.Trial;
import com.trialslink.repository.SavedSearchRepository;
import com.trialslink.service.TrialService;
import com.trialslink.web.rest.util.QueryModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.elasticsearch.core.FacetedPage;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by pigiotisk on 25/07/2017.
 */
@Component
@ConfigurationProperties(prefix = "notifier", ignoreUnknownFields = false)
public class NotificationScheduler {

    private final RabbitTemplate rabbitTemplate;
    private final SavedSearchRepository savedSearchRepository;
    private final TrialService trialService;
    private final String cronTime = "*/60 * * * * *";
    private boolean enabled = false;

    private static final Logger log = LoggerFactory.getLogger(NotificationScheduler.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    public NotificationScheduler(RabbitTemplate rabbitTemplate, SavedSearchRepository savedSearchRepository, TrialService trialService) {
        this.rabbitTemplate = rabbitTemplate;
        this.savedSearchRepository = savedSearchRepository;
        this.trialService = trialService;
    }

    /**
     *
     * This method will run periodically to check if new trials
     * much any saved search criteria, and send an email to the user
     */
    @Scheduled(cron = cronTime)
    public void generateSavedSearchNotifications() {
        log.debug("The time is now {}", dateFormat.format(new Date()));
        // skip if not enabled
        if(!enabled) {
            log.debug("Skipping notifications as notifier is disabled");
            return;
        }

        List<SavedSearch> allSavedSearches = savedSearchRepository.findAll();
        if (allSavedSearches.size() == 0) return;

        for (SavedSearch savedSearch:allSavedSearches) {
            // check that the user has specified to receive notifications for this saved search
            if (! savedSearch.isShouldNotify()) {
                continue;
            }

            ArrayList<Trial> trials = this.getTrialsMatchingSavedSearch(savedSearch);
            // if no trials found for this search return
            if (trials.isEmpty()) continue;

            Notifications notification = createSavedSearchNotification(savedSearch, trials);

            // update the saved search object to reflect that notifications have been generated
            savedSearch.setUpdatedAt(ZonedDateTime.now());
            savedSearchRepository.save(savedSearch);

            rabbitTemplate.convertAndSend(TrialslinkApp.exchange, "notification", notification);
        }
    }

    private Notifications createSavedSearchNotification(SavedSearch savedSearch, ArrayList<Trial> trials) {
        Notifications notification = new UserNotification();
        log.debug("Notification for user {}: ", savedSearch.getCreatedBy());

        notification.setTrials(trials);
        notification.setSavedSearch(savedSearch);
        return notification;
    }

    private ArrayList<Trial> getTrialsMatchingSavedSearch(SavedSearch savedSearch) {
        ArrayList<Trial> trials = new ArrayList<>();
        FacetedPage<Trial> trialSearchResults = null;

        QueryModel query = savedSearch.getQueryModel();
        if (query != null) {
            trialSearchResults = trialService.search(query, null);
        }

        if (trialSearchResults != null) {
            // if trial updated time is newer than last time saved search was updated then add it to the notification
            for (Trial trial:trialSearchResults){
                if (trial.getLastModifiedDate().isAfter(savedSearch.getUpdatedAt().toInstant())) {
                    trials.add(trial);
                }
            }
        }

        return trials;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
