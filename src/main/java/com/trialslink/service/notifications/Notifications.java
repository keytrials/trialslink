package com.trialslink.service.notifications;

import com.trialslink.domain.SavedSearch;
import com.trialslink.domain.Trial;
import com.trialslink.domain.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by pigiotisk on 27/07/2017.
 */
public interface Notifications {
    SavedSearch getSavedSearch();
    void setSavedSearch(SavedSearch savedSearch);
    ArrayList<Trial> getTrials();
    void setTrials(ArrayList<Trial> trials);
}
