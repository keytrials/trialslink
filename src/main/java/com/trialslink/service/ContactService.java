package com.trialslink.service;

import com.trialslink.domain.Contact;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Contact.
 */
public interface ContactService {

    /**
     * Save a contact.
     *
     * @param contact the entity to save
     * @return the persisted entity
     */
    Contact save(Contact contact);

    /**
     * Get all the contacts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Contact> findAll(Pageable pageable);

    Page<Contact> findAllForTrialCentre(String id, Pageable pageable);

    /**
     * Get the "id" contact.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Contact findOne(String id);

    /**
     * Delete the "id" contact.
     *
     * @param id the id of the entity
     */
    void delete(String id);

    boolean indexAll();
}
