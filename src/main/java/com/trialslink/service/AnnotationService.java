package com.trialslink.service;

import com.trialslink.domain.Trial;
import org.springframework.http.ResponseEntity;

/**
 * An interface for service that annotates {@link Trial}s
 * Created by jay on 07/06/19.
 */
public interface AnnotationService {

    /**
     *  Annotate all the trials as a batch process
     *
     */
    void annotateAll();

    void sanitiseAllTrials();

    /**
     * A method that annotates a given trial
     * @param trial the trial to annotate
     * @return a reponse entity that returns OK if trial was annotated without issues.
     */
    ResponseEntity annotateTrial(Trial trial);
}
