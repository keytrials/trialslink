package com.trialslink.queue;

import com.trialslink.TrialslinkApp;
import com.trialslink.domain.TrialImport;
import com.trialslink.factories.TrialImporterFactory;
import com.trialslink.repository.TrialImportRepository;
import com.trialslink.trialimport.TrialImporterInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * A handler for importing {@link com.trialslink.domain.Trial}s.
 *
 * Created by pigiotisk on 25/07/2017.
 */
@Component
@RabbitListener(bindings = @QueueBinding(value = @Queue(value = TrialslinkApp.trialsQueue, durable = "true"), exchange = @Exchange(value = TrialslinkApp.exchange, autoDelete = "true"), key = "trial"))
public class TrialImportHandler {

    private final Logger log = LoggerFactory.getLogger(TrialImportHandler.class);
    private final TrialImportRepository trialImportRepository;
    private final TrialImporterFactory trialImporterFactory;

    public TrialImportHandler(TrialImporterFactory trialImporterFactory, TrialImportRepository trialImportRepository) {
        this.trialImporterFactory = trialImporterFactory;
        this.trialImportRepository = trialImportRepository;
    }

    @RabbitHandler
    public void receiveMessage(@Payload TrialImport trialImport){

        log.debug("Received request to import trial from {}" , trialImport.getSource());
        try {
            TrialImporterInterface trialImporter = trialImporterFactory.getTrialImporter(trialImport);
            trialImport = trialImporter.importTrial(trialImport);
            log.info("Successfully imported trial via trialImport = {}", trialImport);
        } catch (Exception e) {
            log.error(
                "Trial import with id: " + trialImport.getId() +
                    " from <"  + trialImport.getSource() + "> has failed with exception \n", e
            );
        }

        // save trial import object on successful import operation
        trialImportRepository.save(trialImport);
    }

}
