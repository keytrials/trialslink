package com.trialslink.queue;

import com.trialslink.TrialslinkApp;
import com.trialslink.service.notifications.NotificationService;
import com.trialslink.service.notifications.UserNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * A handler for notifications queue
 * Created by pigiotisk on 25/07/2017.
 */
@Component
@RabbitListener(bindings = @QueueBinding(value = @org.springframework.amqp.rabbit.annotation.Queue(value = TrialslinkApp.notificationsQueue, durable = "true"), exchange = @Exchange(value = TrialslinkApp.exchange, autoDelete = "true"), key = "notification"))
public class NotificationsHandler {

    private final Logger log = LoggerFactory.getLogger(NotificationsHandler.class);
    private final NotificationService notificationService;

    public NotificationsHandler(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @RabbitHandler
    public void receiveMessage(@Payload UserNotification userNotification){
        //send the email
        notificationService.processNotification(userNotification);
    }
}
