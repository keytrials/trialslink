package com.trialslink.domain;

import com.trialslink.web.rest.util.QueryModel;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A SavedSearch.
 */
@Document(collection = "saved_search")
public class SavedSearch implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String id;

    @Field("created_at")
    private ZonedDateTime createdAt;

    @Field("updated_at")

    private ZonedDateTime updatedAt;

    @Field("url")
    private String url;

    @Field("hash")
    private String hash;

    @Field("created_by")
    private String createdBy;

    @Field("should_notify")
    private Boolean shouldNotify;

    @Field("omni_search_string")
    private String omniSearchString;

    @NotNull
    @Field("title")
    public String title;

    @Field("query_model")
    private QueryModel queryModel;

    public QueryModel getQueryModel() {
        return queryModel;
    }

    public void setQueryModel(QueryModel queryModel) {
        this.queryModel = queryModel;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOmniSearchString() {
        return omniSearchString;
    }

    public void setOmniSearchString(String omniSearchString) {
        this.omniSearchString = omniSearchString;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public SavedSearch createdAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(ZonedDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public ZonedDateTime getUpdatedAt() {
        return updatedAt;
    }

    public SavedSearch updatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(ZonedDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUrl() {
        return url;
    }

    public SavedSearch url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHash() {
        return hash;
    }

    public SavedSearch hash(String hash) {
        this.hash = hash;
        return this;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public SavedSearch createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Boolean isShouldNotify() {
        return shouldNotify;
    }

    public SavedSearch shouldNotify(Boolean shouldNotify) {
        this.shouldNotify = shouldNotify;
        return this;
    }

    public void setShouldNotify(Boolean shouldNotify) {
        this.shouldNotify = shouldNotify;
    }

    public String getTitle() {
        return title;
    }

    public SavedSearch title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SavedSearch savedSearch = (SavedSearch) o;
        if (savedSearch.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), savedSearch.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(queryModel, omniSearchString, createdBy);
    }

    @Override
    public String toString() {
        return "SavedSearch{" +
            "id=" + getId() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            ", queryModel=" + getQueryModel() + "'" +
            ", omniSearchString=" + getOmniSearchString() + "'" +
            ", url='" + getUrl() + "'" +
            ", hash='" + getHash() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", shouldNotify='" + isShouldNotify() + "'" +
            ", title='" + getTitle() + "'" +
            "}";
    }
}
