package com.trialslink.domain;

/**
 * A generic class for holding a trial while it is in the process of being reviewed before import
 *
 * Created by jay on 01/09/19.
 */
public class TrialImportItem {

    String id;

    String title;

    public TrialImportItem(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public TrialImportItem() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
