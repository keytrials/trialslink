package com.trialslink.domain.enumeration;

/**
 * The PrivilegeRequestStatus enumeration.
 */
public enum PrivilegeRequestStatus {
    PENDING,  APPROVED,  REJECTED
}
