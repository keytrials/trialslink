package com.trialslink.domain.enumeration;

/**
 * The TrialImportStatus enumeration.
 */
public enum TrialImportStatus {
    IN_PROGRESS,  COMPLETE,  FAILED
}
