package com.trialslink.domain.enumeration;

/**
 * The StudyType enumeration.
 */
public enum StudyType {
    INTERVENTIONAL, OBSERVATIONAL, BOTH, PATIENT_REGISTRY
}
