package com.trialslink.domain.enumeration;

/**
 * The TrialImportSource enumeration.
 */
public enum TrialImportSource {
    NIHR,  CLINICALTRIALS_GOV
}
