package com.trialslink.domain;

import com.trialslink.domain.enumeration.TrialStatus;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A TrialCentre.
 */
@Document(collection = "trial_centre")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "trial_centre")
public class TrialCentre extends Organisation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Field("all_day")
    Boolean allDay;

    @Field("start_date")
    ZonedDateTime startDate;

    @Field("end_date")
    ZonedDateTime endDate;

    @Field("days")
    Set<Day> days;

    @DBRef
    @org.springframework.data.elasticsearch.annotations.Field(type = FieldType.Nested)
    Set<Contact> contacts = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    @Field
    String trialId;

    @Field
    String orgId;

    @Field
    TrialStatus status;

    @Field
    Contact principalInvestigator;

    public Boolean isAllDay() {
        return allDay;
    }

    public TrialCentre allDay(Boolean allDay) {
        this.allDay = allDay;
        return this;
    }

    public void setAllDay(Boolean allDay) {
        this.allDay = allDay;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public TrialCentre startDate(ZonedDateTime startDate) {
        this.startDate = startDate;
        return this;
    }

    public ZonedDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate;
    }

    public TrialCentre endDate(ZonedDateTime endDate) {
        this.endDate = endDate;
        return this;
    }

    public Set<Day> getDays() {
        return days;
    }

    public void setDays(Set<Day> days) {
        this.days = days;
    }

    public TrialCentre days(Set<Day> days) {
        this.days = days;
        return this;
    }

    public Set<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Set<Contact> contacts) {
        this.contacts = contacts;
    }

    public TrialCentre contacts(Set<Contact> contacts) {
        this.contacts = contacts;
        return this;
    }

    public TrialCentre addContact(Contact contact) {
        this.contacts.add(contact);
        return this;
    }

    public TrialCentre removeContact(Contact contact) {
        this.contacts.remove(contact);
        return this;
    }

    public Organisation asOrganisation(){
        Organisation organisation = new Organisation();
        organisation.address = getAddress();
        organisation.name = getName();
        organisation.id = getOrgId();

        return organisation;
    }

    public void fromOrganisation(Organisation organisation){
        this.address = organisation.address;
        this.name = organisation.name;
        this.orgId = organisation.id;
    }

    public TrialStatus getStatus() {
        return status;
    }

    public void setStatus(TrialStatus status) {
        this.status = status;
    }

    public String getTrialId() {
        return trialId;
    }

    public void setTrialId(String trialId) {
        this.trialId = trialId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Contact getPrincipalInvestigator() {
        return principalInvestigator;
    }

    public void setPrincipalInvestigator(Contact principalInvestigator) {
        this.principalInvestigator = principalInvestigator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TrialCentre trialCentre = (TrialCentre) o;
        if (trialCentre.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), trialCentre.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TrialCentre{" +
            "id=" + id +
            ", name='" + name + "'" +
            ", allDay='" + allDay + "'" +
            ", startDate='" + startDate + "'" +
            ", endDate='" + endDate + "'" +
            ", days='" + days + "'" +
            ", trialId='" + trialId + "'" +
            '}';
    }
}
