package com.trialslink.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.*;

/**
 * A IdInfo.
 */
@Document(collection = "id_info")
@XStreamAlias(value = "id_info")
public class IdInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("protocol_id")
    @XStreamAlias("org_study_id")
    private String protocolId;

    @Field("nct_id")
    @XStreamAlias(value = "nct_id")
    private String nctId;

    @XStreamImplicit(itemFieldName = "secondary_id")
    private Map< String,String > secondaryIds = new HashMap<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProtocolId() {
        return protocolId;
    }

    public void setProtocolId(String protocolId) {
        this.protocolId = protocolId;
    }

    public IdInfo protocolId(String protocolId) {
        this.protocolId = protocolId;
        return this;
    }

    public String getNctId() {
        return nctId;
    }

    public void setNctId(String nctId) {
        this.nctId = nctId;
    }

    public IdInfo nctId(String nctId) {
        this.nctId = nctId;
        return this;
    }

    public Map<String, String> getSecondaryIds() {
        return secondaryIds;
    }

    public void setSecondaryIds(Map<String, String> secondaryIds) {
        this.secondaryIds = secondaryIds;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IdInfo idInfo = (IdInfo) o;
        if (idInfo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), idInfo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "IdInfo{" +
            "id=" + getId() +
            ", protocolId='" + getProtocolId() + "'" +
            ", nctId='" + getNctId() + "'" +
            ", nctId='" + getSecondaryIds() + "'" +
            "}";
    }
}
