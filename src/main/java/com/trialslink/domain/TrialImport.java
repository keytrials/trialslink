package com.trialslink.domain;

import com.trialslink.domain.enumeration.TrialImportSource;
import com.trialslink.domain.enumeration.TrialImportStatus;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A TrialImport.
 */
@Document(collection = "trial_import")
public class TrialImport extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("source")
    private TrialImportSource source;

    @Field("source_identifier")
    private String sourceIdentifier;

    @Field("status")
    private TrialImportStatus status;

    @Field("file_path")
    private String filePath;

    @DBRef
    private Set<Trial> trials = new HashSet<>();

    @Field("failed_trials")
    private Set<HashMap<String, String>> failedTrials = new HashSet<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TrialImportSource getSource() {
        return source;
    }

    public TrialImport source(TrialImportSource source) {
        this.source = source;
        return this;
    }

    public void setSource(TrialImportSource source) {
        this.source = source;
    }

    public String getSourceIdentifier() {
        return sourceIdentifier;
    }

    public TrialImport sourceIdentifier(String sourceIdentifier) {
        this.sourceIdentifier = sourceIdentifier;
        return this;
    }

    public void setSourceIdentifier(String sourceIdentifier) {
        this.sourceIdentifier = sourceIdentifier;
    }

    public TrialImportStatus getStatus() {
        return status;
    }

    public TrialImport status(TrialImportStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(TrialImportStatus status) {
        this.status = status;
    }

    public String getFilePath() {
        return filePath;
    }

    public TrialImport filePath(String filePath) {
        this.filePath = filePath;
        return this;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public Set<Trial> getTrials() {
        return trials;
    }

    public void setTrials(Set<Trial> trials) {
        this.trials = trials;
    }

    public TrialImport trials(Set<Trial> trials) {
        this.trials= trials;
        return this;
    }

    public TrialImport addTrial(Trial trial) {
        this.trials.add(trial);
        return this;
    }

    public TrialImport removeTrial(Trial trial) {
        this.trials.remove(trial);
        return this;
    }

    public TrialImport updateTrials(String trialId, Trial updatedTrial) {
        Trial existingTrial = null;
        for (Trial trial : getTrials()) {
            if (trial.getId().equalsIgnoreCase(trialId)) {
                existingTrial = trial;
                break;
            }
        }
        if (existingTrial != null) {
            getTrials().remove(existingTrial);
        }

        this.trials.add(updatedTrial);
        return this;
    }

    public Set<HashMap<String, String>> getFailedTrials() { return this.failedTrials; }

    public TrialImport addFailedTrial(HashMap<String, String> failedTrial) {
        this.failedTrials.add(failedTrial);
        return this;
    }

    public void setFailedTrials(Set<HashMap<String, String>> failedTrials) {
        this.failedTrials = failedTrials;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TrialImport trialImport = (TrialImport) o;
        if (trialImport.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), trialImport.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TrialImport{" +
            "id=" + getId() +
            ", source='" + getSource() + "'" +
            ", sourceIdentifier='" + getSourceIdentifier() + "'" +
            ", status='" + getStatus() + "'" +
            ", filePath='" + getFilePath() + "'" +
            "}";
    }
}
