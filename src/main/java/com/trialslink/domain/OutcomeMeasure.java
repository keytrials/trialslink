package com.trialslink.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A OutcomeMeasure.
 */
@Document(collection = "outcome_measure")
public class OutcomeMeasure implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("title")
    private String title;

    @NotNull
    @Size(max = 999)
    @Field("description")
    private String description;

    @NotNull
    @Field("time_frame")
    private String timeFrame;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public OutcomeMeasure title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public OutcomeMeasure description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTimeFrame() {
        return timeFrame;
    }

    public OutcomeMeasure timeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
        return this;
    }

    public void setTimeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OutcomeMeasure outcomeMeasure = (OutcomeMeasure) o;
        if (outcomeMeasure.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), outcomeMeasure.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OutcomeMeasure{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", timeFrame='" + getTimeFrame() + "'" +
            "}";
    }
}
