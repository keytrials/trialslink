package com.trialslink.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashMap;
import java.util.Objects;
import java.util.Set;

/**
 * A TrialHistory.
 */
@Document(collection = "trial_history")
public class TrialHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field
    private HashMap<Long, Trial> snapshots = new HashMap<>();

    // jhipster-needle-entity-add-field - Jhipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public HashMap<Long, Trial> getSnapshots() {
        return snapshots;
    }

    public void setSnapshots(HashMap<Long, Trial> snapshots) {
        this.snapshots = snapshots;
    }

    public void addSnapshot(Long timeStamp, Trial snapshot) {
        this.snapshots.put(timeStamp, snapshot);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TrialHistory trialHistory = (TrialHistory) o;
        if (trialHistory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), trialHistory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TrialHistory{" +
            "id=" + getId() +
            "}";
    }
}
