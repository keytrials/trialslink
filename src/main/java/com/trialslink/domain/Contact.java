package com.trialslink.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Contact.
 */
@Document(collection = "contact")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "contact", type = "contact")
public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("phone")
    @XStreamAlias(value = "phone")
    private String phone;

    @Field("phone_ext")
    @XStreamAlias(value = "phone_ext")
    private String phoneExt;

    @NotNull
    @Field("person_name")
    @XStreamAlias(value = "last_name")
    private String personName;

    @NotNull
    @Field("email")
    @XStreamAlias(value = "email")
    private String email;

    @Field("role")
    private Role role;

    @Field
    private String trialCentreId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public Contact phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPersonName() {
        return personName;
    }

    public Contact personName(String personName) {
        this.personName = personName;
        return this;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getEmail() {
        return email;
    }

    public Contact email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public Contact role(Role role) {
        this.role = role;
        return this;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getTrialCentreId() {
        return trialCentreId;
    }

    public void setTrialCentreId(String trialCentreId) {
        this.trialCentreId = trialCentreId;
    }

    public String getPhoneExt() {
        return phoneExt;
    }

    public void setPhoneExt(String phoneExt) {
        this.phoneExt = phoneExt;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Contact contact = (Contact) o;
        if (contact.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), contact.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Contact{" +
            "id=" + id +
            ", phone='" + phone + "'" +
            ", personName='" + personName + "'" +
            ", email='" + email + "'" +
            ", role='" + role + "'" +
            '}';
    }
}
