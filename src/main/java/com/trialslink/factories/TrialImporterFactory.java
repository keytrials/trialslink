package com.trialslink.factories;

import com.trialslink.domain.TrialImport;
import com.trialslink.domain.enumeration.TrialImportSource;
import com.trialslink.trialimport.TrialImporterInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;


/**
 * Created by pigiotisk on 14/07/2017.
 */
@Component
public class TrialImporterFactory {

    private final Logger log = LoggerFactory.getLogger(TrialImporterFactory.class);

    @Autowired
    @Qualifier("CTgovImporter")
    private TrialImporterInterface ctGovImporter;

    @Autowired
    @Qualifier("NIHRImporter")
    private TrialImporterInterface nihrImporter;

    /**
     *
     * @param trialImport :is the trialImport object that is created when a user imports a trial
     *                    from either NIHR or clinicaltrials.gov.
     * @return : The factory can detect and return the appropriate importer class. Factory method design pattern.
     */
    public TrialImporterInterface getTrialImporter(TrialImport trialImport) {

        if (trialImport.getSource().equals(TrialImportSource.CLINICALTRIALS_GOV)) {
             return ctGovImporter;
        } else if (trialImport.getSource().equals(TrialImportSource.NIHR)) {
             return nihrImporter;
        } else {
            log.error("Unable to return appropriate importer for source {} : ", trialImport.getSource());
            return null;
        }
        // further importing mechanisms in the future
        // you can add the additional classes only and the importing mechanism will pick the correct one
    }
}
