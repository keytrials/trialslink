package com.trialslink.web.rest.util;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * A class that represents queries passed to {@link com.trialslink.web.rest.TrialResource}
 */
@JsonDeserialize(using = QueryModelDeserializer.class)
public class QueryModel implements Serializable{


    boolean includeAncestors;
    List<String> conditions = new ArrayList<>();
    List<String> locations = new ArrayList<>();
    List<String> statuses = new ArrayList<>();
    List<String> siteStatuses = new ArrayList<>();
    List<String> conditionStatuses = new ArrayList<>();
    List<String> phases = new ArrayList<>();
    List<String> types = new ArrayList<>();
    List<String> areas = new ArrayList<>();
    List<String> sites = new ArrayList<>();
    List<String> genders = new ArrayList<>();
    Integer age;
    String token;


    public QueryModel() {

    }

    public List<String> getConditions() {
        return conditions;
    }

    public void setConditions(List<String> conditions) {
        this.conditions = conditions;
    }

    public List<String> getLocations() {
        return locations;
    }

    public void setLocations(List<String> locations) {
        this.locations = locations;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public List<String> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<String> statuses) {
        this.statuses = statuses;
    }

    public List<String> getPhases() {
        return phases;
    }

    public void setPhases(List<String> phases) {
        this.phases = phases;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public List<String> getAreas() {
        return areas;
    }

    public void setAreas(List<String> areas) {
        this.areas = areas;
    }

    public List<String> getSites() {
        return sites;
    }

    public void setSites(List<String> sites) {
        this.sites = sites;
    }

    public List<String> getGenders() {
        return genders;
    }

    public void setGenders(List<String> genders) {
        this.genders = genders;
    }

    public List<String> getConditionStatuses() {
        return conditionStatuses;
    }

    public void setConditionStatuses(List<String> conditionStatuses) {
        this.conditionStatuses = conditionStatuses;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<String> getSiteStatuses() {
        return siteStatuses;
    }

    public void setSiteStatuses(List<String> siteStatuses) {
        this.siteStatuses = siteStatuses;
    }

    public boolean getIncludeAncestors() {
        return includeAncestors;
    }

    public void setIncludeAncestors(boolean includeAncestors) {
        this.includeAncestors = includeAncestors;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("QueryModel{");
        sb.append("conditions=").append(conditions);
        sb.append(", locations=").append(locations);
        sb.append(", statuses=").append(statuses);
        sb.append(", siteStatuses=").append(siteStatuses);
        sb.append(", conditionStatuses=").append(conditionStatuses);
        sb.append(", phases=").append(phases);
        sb.append(", types=").append(types);
        sb.append(", areas=").append(areas);
        sb.append(", sites=").append(sites);
        sb.append(", age=").append(age);
        sb.append(", token=").append(token);
        sb.append(", includeAncestors=").append(includeAncestors);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int hashCode() {
        Collections.sort(conditions);
        Collections.sort(locations);
        Collections.sort(statuses);
        Collections.sort(siteStatuses);
        Collections.sort(phases);
        Collections.sort(types);
        Collections.sort(areas);
        Collections.sort(sites);
        Collections.sort(genders);

        return Objects.hash(conditions, locations, statuses, siteStatuses, phases, types, areas, sites, genders, age);
    }
}
