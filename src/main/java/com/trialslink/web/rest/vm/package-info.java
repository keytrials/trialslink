/**
 * View Models used by Spring MVC REST controllers.
 */
package com.trialslink.web.rest.vm;
