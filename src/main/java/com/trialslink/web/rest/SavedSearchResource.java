package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.trialslink.domain.SavedSearch;
import com.trialslink.service.SavedSearchService;
import com.trialslink.web.rest.errors.BadRequestAlertException;
import com.trialslink.web.rest.util.HeaderUtil;
import com.trialslink.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SavedSearch.
 */
@RestController
@RequestMapping("/api")
public class SavedSearchResource {

    private final Logger log = LoggerFactory.getLogger(SavedSearchResource.class);

    private static final String ENTITY_NAME = "savedSearch";

    private final SavedSearchService savedSearchService;

    public SavedSearchResource(SavedSearchService savedSearchService) {
        this.savedSearchService = savedSearchService;
    }

    /**
     * POST  /saved-searches : Create a new savedSearch.
     *
     * @param savedSearch the savedSearch to create
     * @return the ResponseEntity with status 201 (Created) and with body the new savedSearch, or with status 400 (Bad Request) if the savedSearch has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/saved-searches")
    @Timed
    public ResponseEntity<SavedSearch> createSavedSearch(@Valid @RequestBody SavedSearch savedSearch) throws URISyntaxException {
        log.debug("REST request to save SavedSearch : {}", savedSearch);
        if (savedSearch.getId() != null) {
            throw new BadRequestAlertException("A new savedSearch cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SavedSearch result = savedSearchService.save(savedSearch);
        return ResponseEntity.created(new URI("/api/saved-searches/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /saved-searches : Updates an existing savedSearch.
     *
     * @param savedSearch the savedSearch to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated savedSearch,
     * or with status 400 (Bad Request) if the savedSearch is not valid,
     * or with status 500 (Internal Server Error) if the savedSearch couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/saved-searches")
    @Timed
    public ResponseEntity<SavedSearch> updateSavedSearch(@Valid @RequestBody SavedSearch savedSearch) throws URISyntaxException {
        log.debug("REST request to update SavedSearch : {}", savedSearch);
        if (savedSearch.getId() == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "Cannot update savedSearch without having an ID")).body(null);
        }
        SavedSearch result = savedSearchService.save(savedSearch);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, savedSearch.getId().toString()))
            .body(result);
    }

    /**
     * GET  /saved-searches : get all the savedSearches.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of savedSearches in body
     */
    @GetMapping("/saved-searches")
    @Timed
    public ResponseEntity<List<SavedSearch>> getAllSavedSearches(Pageable pageable) {
        log.debug("REST request to get a page of SavedSearches");
        Page<SavedSearch> page = savedSearchService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/saved-searches");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /saved-searches/:id : get the "id" savedSearch.
     *
     * @param id the id of the savedSearch to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the savedSearch, or with status 404 (Not Found)
     */
    @GetMapping("/saved-searches/{id}")
    @Timed
    public ResponseEntity<SavedSearch> getSavedSearch(@PathVariable String id) {
        log.debug("REST request to get SavedSearch : {}", id);
        SavedSearch savedSearch = savedSearchService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(savedSearch));
    }

    /**
     * DELETE  /saved-searches/:id : delete the "id" savedSearch.
     *
     * @param id the id of the savedSearch to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/saved-searches/{id}")
    @Timed
    public ResponseEntity<Void> deleteSavedSearch(@PathVariable String id) {
        log.debug("REST request to delete SavedSearch : {}", id);
        savedSearchService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
