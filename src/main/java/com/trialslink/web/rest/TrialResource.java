package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.trialslink.domain.Trial;
import com.trialslink.domain.TrialImportItem;
import com.trialslink.security.AuthoritiesConstants;
import com.trialslink.service.AnnotationService;
import com.trialslink.service.TrialImportService;
import com.trialslink.service.TrialService;
import com.trialslink.web.rest.errors.BadRequestAlertException;
import com.trialslink.web.rest.util.HeaderUtil;
import com.trialslink.web.rest.util.PaginationUtil;
import com.trialslink.web.rest.util.QueryModel;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.FacetedPage;
import org.springframework.data.elasticsearch.core.facet.result.Term;
import org.springframework.data.elasticsearch.core.facet.result.TermResult;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * REST controller for managing Trial.
 */
@RestController
@RequestMapping("/api")
public class TrialResource {

    private static final String ENTITY_NAME = "trial";
    private final Logger log = LoggerFactory.getLogger(TrialResource.class);
    private final TrialService trialService;
    private final AnnotationService annotationService;
    private final TrialImportService trialImportService;
    File tempFile;
    Resource resource;

    public TrialResource(TrialService trialService, AnnotationService annotationService,
                         TrialImportService trialImportService) {
        this.trialService = trialService;
        this.annotationService = annotationService;
        this.trialImportService = trialImportService;
    }

    /**
     * POST  /trials : Create a new trial.
     *
     * @param trial the trial to create
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trials")
    @Timed
    @Secured({AuthoritiesConstants.ADMIN, AuthoritiesConstants.PRIVILEGED})
    public ResponseEntity<Trial> createTrial(@Valid @RequestBody Trial trial) throws URISyntaxException {
        log.debug("REST request to save Trial : {}", trial);
        if (trial.getId() != null) {
            throw new BadRequestAlertException("A new trial cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Trial result = trialService.save(trial);
        return ResponseEntity.created(new URI("/api/trials/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * PUT  /trials : Updates an existing trial.
     *
     * @param trial the trial to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated trial,
     * or with status 400 (Bad Request) if the trial is not valid,
     * or with status 500 (Internal Server Error) if the trial couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trials")
    @Timed
    public ResponseEntity<Trial> updateTrial(@Valid @RequestBody Trial trial) throws URISyntaxException {
        log.debug("REST request to update Trial : {}", trial);

        if (trial.getId() == null) {
            return createTrial(trial);
        }

        Trial verifiedTrial = trialService.verifyOwnershipAndGet(trial.getId());
        if (verifiedTrial == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "ownershipError", "Cannot update trials not created by the current user")).body(null);
        }
        else {
            Trial result = trialService.save(trial);
            return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, trial.getId())).body(result);
        }
    }

    /**
     * GET  /trials : get all the trials.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of trials in body
     */
    @GetMapping("/trials")
    @Timed
    public ResponseEntity<Map<String, Object>> getAllTrials(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Trials");
        FacetedPage<Trial> page = trialService.findAllWithCategories(pageable);
        // wrap results page in a response entity with faceted results turned into a map
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/trials");
        return new ResponseEntity<>(getResultMapMapForResults(page), headers, HttpStatus.OK);
    }

    /**
     * GET  /trials/:id : get the "id" trial.
     *
     * @param id the id of the trial to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the trial, or with status 404 (Not Found)
     */
    @GetMapping("/trials/{id}")
    @Timed
    public ResponseEntity<Trial> getTrial(@PathVariable String id) {
        log.debug("REST request to get Trial : {}", id);
        Trial trial = trialService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(trial));
    }

    /**
     * GET  /trials/:id/annotate : annotate the "id" trial.
     *
     * @param id the id of the trial to annotate
     * @return the ResponseEntity with status 200 (OK) and with body the trial, or with status 404 (Not Found)
     */
    @GetMapping("/trials/{id}/annotate")
    @Timed
    public ResponseEntity annotateTrial(@PathVariable String id) {
        log.debug("REST request to get Trial : {}", id);
        Trial trial = trialService.findOne(id);
        if(trial != null) {
            return annotationService.annotateTrial(trial);
        } else {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "Annotation Error", "Trial is missing")).body(null);
        }
    }

    /**
     * GET  /trials/shortName/:shortName : get the "shortName" trial.
     *
     * @param shortName the shortName of the trial to check. Returns true if match exists
     * @return the ResponseEntity with status 200 (OK) and boolean value of present or absent
     */
    @GetMapping("/trials/shortName/{shortName}")
    @Timed
    public ResponseEntity<Boolean> getTrialByShortName(@PathVariable String shortName) {
        log.debug("REST request to get Trial : {}", shortName);
        List<Trial> trials = trialService.findByShortName(shortName);
        return ResponseEntity.ok(!trials.isEmpty());
    }

    /**
     * DELETE  /trials/:id : delete the "id" trial.
     *
     * @param id the id of the trial to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/trials/{id}")
    @Timed
    public ResponseEntity<Void> deleteTrial(@PathVariable String id) {
        log.debug("REST request to delete Trial : {}", id);
        Trial verifiedTrial = trialService.verifyOwnershipAndGet(id);
        if (verifiedTrial == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "ownershipError", "Cannot update trials not created by the current user")).body(null);
        }
        else {
            trialService.delete(id);
            return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
        }
    }

    /**
     * SEARCH  /_search/trials?query=:query : search for the trial corresponding
     * to the query.
     *
     * @param query the query of the trial search
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @PostMapping("/_search/trials")
    @Timed
    public ResponseEntity<Map<String, Object>> searchTrials(@RequestBody QueryModel query, @ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search for a page of Trials for query {}", query);
        FacetedPage<Trial> page = trialService.search(query, pageable);
        // wrap results page in a response entity with faceted results turned into a map
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query.getConditions().toString(), page, "/api/_search/trials");
        return new ResponseEntity<>(getResultMapMapForResults(page), headers, HttpStatus.OK);
    }

    /**
     * SEARCH  /trials/omni_search?q=:query : search for the trial corresponding
     * to the query.
     *
     * @param query the query of the trial search
     * @param pageable the pagination information
     * @return the result of the search
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/trials/omni_search")
    @Timed
    public ResponseEntity<Map<String, Object>> omniSearchTrials(@RequestParam(name = "q") String query, @ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to perform omni search for a page of Trials for query {}", query);
        FacetedPage<Trial> page = trialService.omniSearch(query, pageable);
        // wrap results page in a response entity with faceted results turned into a map
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/trials/omni_search");
        return new ResponseEntity<>(getResultMapMapForResults(page), headers, HttpStatus.OK);
    }

    /**
     * Utility private method for transforming a {@link FacetedPage} into a {@link Map} object with results
     * and categories.
     * @param page the page of results
     * @return results as a Map
     */
    private Map<String, Object> getResultMapMapForResults(FacetedPage<Trial> page) {

        Set<String> items = new HashSet<>();
        items.add("types");
        items.add("statuses");
        items.add("siteStatuses");
        items.add("phases");
        items.add("locations");
        items.add("conditions");
        items.add("areas");
        items.add("sites");
        // genders filter removed at client request case #170
        //  items.add("genders");
        items.add("conditionStatuses");
        Map<String, Set<Map<String, Object>>> facetsMap = new HashMap<>();
        for (String key : items) {
            log.info("Processed facet key : {}", key);
            // get search categories via facets
            Set<Map<String, Object>> mapSet = new HashSet<>();
            TermResult pageFacet = (TermResult) page.getFacet(key);
//            Aggregation pageFacet = page.getAggregation(key);

            if (pageFacet != null) {
                for (Term bucket : pageFacet.getTerms()) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("type", bucket.getTerm());
                    map.put("count", bucket.getCount());
                    mapSet.add(map);
                }
            }

            // add to facets map
            facetsMap.put(key, mapSet);
        }
        log.debug("facetsMap {}", facetsMap);
        Map<String, Object> resultsMap = new HashMap<>();
        resultsMap.put("results", page.getContent());
        resultsMap.put("categories", facetsMap);

        return resultsMap;
    }

    /**
     * INDEX  /_index/trials : index all trials
     *
     * @return the result of the index action
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/_index/trials")
    @Timed
    public ResponseEntity<Boolean> indexTrials() throws URISyntaxException {
        log.debug("REST request to bulk index all trials");
        Boolean result = trialService.indexAll();
        if (result) {
            return ResponseEntity.ok().body(true);
        }
        else {
            return ResponseEntity.badRequest().body(result);
        }
    }

    /**
     * Annotate  /_annotate/trials : annotate all trials
     *
     */
    @GetMapping("/_annotate/trials")
    @Timed
    public ResponseEntity annotateTrials(){
        log.debug("REST request to bulk annotate all trials");
        annotationService.annotateAll();
        return ResponseEntity.ok().build();
    }

    /**
     * Sanitise  /_sanitise/trials : sanitise all trials by removing conditions with black listed concepts
     *
     */
    @GetMapping("/_sanitise/trials")
    @Timed
    public ResponseEntity sanitiseTrials(){
        log.debug("REST request to bulk sanitise all trials");
        annotationService.sanitiseAllTrials();
        return ResponseEntity.ok().build();
    }

    /**
     * Import  /_import/trials : import all trials from the excel file
     *
     */
    @PostMapping("/_import/file")
    @Timed
    public ResponseEntity<Boolean> importTrialsFile(@RequestParam(value = "file") final MultipartFile file) throws IOException, URISyntaxException {
        log.debug("REST request to bulk import all trials in file : ", file);
        String fileName = file.getOriginalFilename();
        tempFile = new File(System.getProperty("java.io.tmpdir")+System.getProperty("file.separator")+fileName);
        file.transferTo(tempFile);
        log.info("tempFile.getAbsolutePath() = {}", tempFile.getAbsolutePath());
        Boolean result = tempFile.exists();
        if (result) {
            return ResponseEntity.ok().body(true);
        }
        else {
            return ResponseEntity.badRequest().body(result);
        }
    }

    @GetMapping("/_review/trials")
    @Timed
    public ResponseEntity<Map<String, List<TrialImportItem>>> reviewTrials(){
        resource = new FileSystemResource(tempFile);
        Map<String, List<TrialImportItem>> reviewedTrials = trialImportService.reviewTrials(resource);
        // delete file //todo - we need to add proper logic here for delete
        // tempFile.delete();
        return ResponseEntity.ok(reviewedTrials);
    }

    @PostMapping("/_import/trials")
    @Timed
    public ResponseEntity<Boolean> importReviewedTrials(@RequestBody Map<String, List<TrialImportItem>> reviewedMap){
        Boolean result = trialImportService.importTrials(resource,reviewedMap);
        tempFile.delete();  //todo - handle this cleanly, what happens to errors??
        log.info("success: "+result);
        if (result) {
            return ResponseEntity.ok().body(true);
        }
        else {
            return ResponseEntity.badRequest().body(result);
        }
    }
}
