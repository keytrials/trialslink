package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.trialslink.config.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * REST controller for application information.
 */
@RestController
@RequestMapping("/api")
public class AppResource {

    private final Logger log = LoggerFactory.getLogger(AppResource.class);

    private final Environment env;
    @Autowired
    public ApplicationProperties applicationProperties;

    public AppResource(Environment env) {
        this.env = env;
    }

    /**
     * GET  /info : get application information
     *
     * @return the ResponseEntity with status 200 (OK) and application version and other information
     */
    @GetMapping("/info")
    @Timed
    public ResponseEntity<Map<String, Object>> getAppInfo() {
        log.debug("REST request to get app");
        Map<String, Object> appInfo = new HashMap<>();
        appInfo.put("name", env.getProperty("spring.application.name"));
        appInfo.put("version", env.getProperty("info.project.version"));
        appInfo.put("buildNumber", env.getProperty("info.project.buildNumber"));
        appInfo.put("buildTime", env.getProperty("info.project.buildTime"));
        return ResponseEntity.ok(appInfo);
    }

    @GetMapping("/brand")
    @Timed
    public ResponseEntity<String> getBrandInfo() {
        log.debug("REST request to get app");
        return ResponseEntity.ok(env.getProperty("application.branding"));
    }

    @GetMapping("/omnisearch")
    @Timed
    public ResponseEntity<Boolean> isPreferOmnisearch() {
        log.debug("REST request to get app");
        return ResponseEntity.ok(Boolean.parseBoolean(env.getProperty("application.preferOmnisearch")));
    }

    /**
     * GET  /registrations : helps to enable/disable registrations for keytrials
     *
     */
    @GetMapping("/registrations")
    @Timed
    public ResponseEntity<Boolean> getRegistrationInfo() {
        log.debug("REST request to get app");
        return ResponseEntity.ok(Boolean.parseBoolean(env.getProperty("application.allowRegistrations")));
    }

    /**
     * GET  /registrations : helps to enable/disable language switcher for keytrials
     *
     */
    @GetMapping("/languageswitcher")
    @Timed
    public ResponseEntity<Boolean> getLanguageSwitcherInfo() {
        log.debug("REST request to get app");
        return ResponseEntity.ok(Boolean.parseBoolean(env.getProperty("application.showLanguageSwitcher")));
    }

    /**
     * GET  /registrations : helps to enable/disable language switcher for keytrials
     *
     */
    @GetMapping("/ribbon")
    @Timed
    public ResponseEntity<Map<String,Object>> getRibbonInfo() {
        log.debug("REST request to get app");
        Map<String, Object> ribbonInfo = new HashMap<>();
        ribbonInfo.put("display", env.getProperty("application.ribbon.display"));
        ribbonInfo.put("text", env.getProperty("application.ribbon.text"));
        return ResponseEntity.ok(ribbonInfo);
    }

    /**
     * GET  /registrations : helps to enable/disable the usage of local storage for keytrials
     *
     */
    @GetMapping("/localstorage")
    @Timed
    public ResponseEntity<Boolean> getLocalStorageInfo() {
        log.debug("REST request to get app");
        return ResponseEntity.ok(Boolean.parseBoolean(env.getProperty("application.useLocalStorage")));
    }

    /**
     * GET  /settings : gets platform settings for this keytrials instance
     *
     */
    @GetMapping("/settings")
    @Timed
    public ResponseEntity<Map<String,Object>> getSettings() {
        log.debug("REST request to get platform settings");
        Map<String, Object> contactDetails = new HashMap<>();
        contactDetails.put("mask", env.getProperty("application.contactDetails.mask"));
        contactDetails.put("text", env.getProperty("application.contactDetails.defaultMask"));

        Map<String, Object> settings = new HashMap<>();
        // add contact details info
        settings.put("contactDetails", contactDetails);
        // setting for specifying if this instance is a single site instance, so we can modify behaviour in UI
        settings.put("singleSiteInstance", Boolean.parseBoolean(env.getProperty("application.singleSiteInstance")));
        settings.put("allowedFilters", applicationProperties.getAllowedFilters());

        return ResponseEntity.ok(settings);
    }

}
