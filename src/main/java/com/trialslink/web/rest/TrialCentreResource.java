package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.trialslink.domain.Contact;
import com.trialslink.domain.Trial;
import com.trialslink.domain.TrialCentre;
import com.trialslink.service.ContactService;
import com.trialslink.service.TrialCentreService;
import com.trialslink.service.TrialService;
import com.trialslink.web.rest.errors.BadRequestAlertException;
import com.trialslink.web.rest.util.HeaderUtil;
import com.trialslink.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing TrialCentre.
 */
@RestController
@RequestMapping("/api")
public class TrialCentreResource {

    private static final String ENTITY_NAME = "trialCentre";
    private static final String CONTACT_ENTITY_NAME = "contact";
    private final Logger log = LoggerFactory.getLogger(TrialCentreResource.class);
    private final TrialCentreService trialCentreService;
    private final ContactService contactService;
    private final TrialService trialService;

    public TrialCentreResource(TrialCentreService trialCentreService, ContactService contactService,
                               TrialService trialService) {
        this.trialCentreService = trialCentreService;
        this.contactService = contactService;
        this.trialService = trialService;
    }

    /**
     * POST  /trial-centres : Create a new trialCentre.
     *
     * @param trialCentre the trialCentre to create
     * @return the ResponseEntity with status 201 (Created) and with body the new trialCentre, or with status 400 (Bad Request) if the trialCentre has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trial-centres")
    @Timed
    public ResponseEntity<TrialCentre> createTrialCentre(@Valid @RequestBody TrialCentre trialCentre) throws URISyntaxException {
        log.debug("REST request to save TrialCentre : {}", trialCentre);
        if (trialCentre.getId() != null) {
            throw new BadRequestAlertException("A new trialCentre cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TrialCentre result = trialCentreService.save(trialCentre);
        return ResponseEntity.created(new URI("/api/trial-centres/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * PUT  /trial-centres : Updates an existing trialCentre.
     *
     * @param trialCentre the trialCentre to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated trialCentre,
     * or with status 400 (Bad Request) if the trialCentre is not valid,
     * or with status 500 (Internal Server Error) if the trialCentre couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trial-centres")
    @Timed
    public ResponseEntity<TrialCentre> updateTrialCentre(@Valid @RequestBody TrialCentre trialCentre) throws URISyntaxException {
        log.debug("REST request to update TrialCentre : {}", trialCentre);
        if (trialCentre.getId() == null) {
            return createTrialCentre(trialCentre);
        }
        TrialCentre result = trialCentreService.save(trialCentre);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, trialCentre.getId()))
            .body(result);
    }

    /**
     * GET  /trial-centres : get all the trialCentres.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of trialCentres in body
     */
    @GetMapping("/trial-centres")
    @Timed
    public ResponseEntity<List<TrialCentre>> getAllTrialCentres(Pageable pageable) {
        log.debug("REST request to get a page of TrialCentres");
        Page<TrialCentre> page = trialCentreService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/trial-centres");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /trial-centres/id/contacts : get all contacts for given trialCentre.
     *
     * @param pageable the pagination information
     * @param id the id of the trialCentre
     * @return the ResponseEntity with status 200 (OK) and the list of contacts in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/trial-centres/{id}/contacts")
    @Timed
    public ResponseEntity<List<Contact>> getAllTrialCentres(@PathVariable String id, @ApiParam Pageable pageable) {
        log.debug("REST request to get contacts for TrialCentre : {}", id);
        TrialCentre trialCentre = trialCentreService.findOne(id);
        if (trialCentre == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial centre found with given id")).build();
        }
        Page<Contact> page = new PageImpl<>(new ArrayList<>(trialCentre.getContacts()), pageable, trialCentre.getContacts().size());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/contacts");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * POST  /trial-centres/id/contacts : Add a new contact for trial centre.
     *
     * @param contact the contact to create
     * @param id the id of the trialCentre
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trial-centres/{id}/contacts")
    @Timed
    public ResponseEntity<Contact> addContactForTrialCentre(@PathVariable String id, @Valid @RequestBody Contact contact) throws URISyntaxException {
        log.debug("REST request to add contact : {} for TrialCentre : {}", contact, id);
        TrialCentre trialCentre = trialCentreService.findOne(id);
        if (trialCentre == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial centre found with given id")).build();
        }

        Contact result = contactService.save(contact);
        // now also save trial centre
        trialCentre = trialCentre.addContact(result);
        trialCentreService.save(trialCentre);
        return ResponseEntity.created(new URI("/api/contacts/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(CONTACT_ENTITY_NAME, result.getId()))
                .body(result);
    }

    /**
     * DELETE  /trial-centres/trialCentreId/contacts/id : Delete a contact with 'id' for trial centre with 'trialCentreId'
     *
     * @param trialCentreId the id of the trialCentre
     * @param id the id of the contact
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @DeleteMapping("/trial-centres/{trialCentreId}/contacts/{id}")
    @Timed
    public ResponseEntity<Void> deleteContactForTrialCentre(@PathVariable String trialCentreId, @PathVariable String id) throws URISyntaxException {
        log.debug("REST request to delete contact : {} for TrialCentre : {}", id, trialCentreId);
        TrialCentre trialCentre = trialCentreService.findOne(trialCentreId);
        if (trialCentre == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial centre found with given id")).build();
        }

        Contact result = contactService.findOne(id);
        if (result == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(CONTACT_ENTITY_NAME, "missing", "No contact found with given id")).build();
        }
        // now also save trial centre
        trialCentre = trialCentre.removeContact(result);
        trialCentreService.save(trialCentre);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(CONTACT_ENTITY_NAME, id)).build();
    }

    /**
     * GET  /trial-centres/:id : get the "id" trialCentre.
     *
     * @param id the id of the trialCentre to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the trialCentre, or with status 404 (Not Found)
     */
    @GetMapping("/trial-centres/{id}")
    @Timed
    public ResponseEntity<TrialCentre> getTrialCentre(@PathVariable String id) {
        log.debug("REST request to get TrialCentre : {}", id);
        TrialCentre trialCentre = trialCentreService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(trialCentre));
    }

    /**
     * DELETE  /trial-centres/:id : delete the "id" trialCentre.
     *
     * @param id the id of the trialCentre to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/trial-centres/{id}")
    @Timed
    public ResponseEntity<Void> deleteTrialCentre(@PathVariable String id) {
        log.debug("REST request to delete TrialCentre : {}", id);
        trialCentreService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * GET  /trial:id/centres : get the "id" trial to get the trialCentres.
     *
     * @param id the id of the trial to retrieve
     * @return the ResponseEntity with status 200 (OK) and the list of trialCentres in body
     */
    @GetMapping("/trials/{id}/centres")
    @Timed
    public ResponseEntity<List<TrialCentre>> getCentresForTrial(@PathVariable String id, @ApiParam Pageable pageable) {
        log.debug("REST request to get TrialCentres for Trial : {}", id);
        Page<TrialCentre> page = trialCentreService.findAllForTrial(trialService.findOne(id), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/trial-centres");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * POST  /trials/id/centre : Add a new trial centre.
     *
     * @param trialCentre the centre to create
     * @param id the id of the trial
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trials/{id}/centres")
    @Timed
    public ResponseEntity<TrialCentre> addCentreForTrial(@PathVariable String id, @Valid @RequestBody TrialCentre trialCentre) throws URISyntaxException {
        log.debug("REST request to add centre : {} for Trial : {}", trialCentre, id);
        Trial trial = trialService.findOne(id);
        if (trial == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial found with given id")).build();
        }
        trialCentre.setTrialId(trial.getId());
        TrialCentre result = trialCentreService.save(trialCentre);
        // now also save trial
        trial.addSite(result);
        trialService.save(trial);
        return ResponseEntity.created(new URI("/api/trials-centres/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
                .body(result);
    }

    /**
     * PUT  /trials/id/centre : Add a new trial centre.
     *
     * @param trialCentre the centre to create
     * @param id the id of the trial
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trials/{id}/centres")
    @Timed
    public ResponseEntity<TrialCentre> updateCentreForTrial(@PathVariable String id, @Valid @RequestBody TrialCentre trialCentre) throws URISyntaxException {
        log.debug("REST request to add centre : {} for Trial : {}", trialCentre, id);
        Trial trial = trialService.findOne(id);
        if (trial == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial found with given id")).build();
        }

        TrialCentre result = trialCentreService.findOne(trialCentre.getId());
        if (result == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial centre found for id")).build();
        }

        trialCentre.setTrialId(trial.getId());
        result = trialCentreService.save(trialCentre);
        // now also save trial with update centre
        trial.addSite(result);
        trialService.save(trial);
        return ResponseEntity.created(new URI("/api/trials-centres/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
                .body(result);
    }

    /**
     * DELETE  /trials/id/centre/centreId : Delete trial centre for given trial.
     *
     * @param trialCentreId the id centre to delete
     * @param id the id of the trial
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @DeleteMapping("/trials/{id}/centres")
    @Timed
    public ResponseEntity<TrialCentre> deleteCentreForTrial(@PathVariable String id, @PathVariable String trialCentreId) throws URISyntaxException {
        log.debug("REST request to delete centre : {} for Trial : {}", trialCentreId, id);
        Trial trial = trialService.findOne(id);
        if (trial == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial found with given id")).build();
        }

        TrialCentre result = trialCentreService.findOne(trialCentreId);
        if (result == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial centre found for id")).build();
        }

        // now remove site and save trial
        trial.removeSite(result);
        trialService.save(trial);
        // finally delete centre
        trialCentreService.delete(result.getId());
        return ResponseEntity.created(new URI("/api/trials-centres/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
                .body(result);
    }
}
