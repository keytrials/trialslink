package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.trialslink.domain.PrivilegeRequest;
import com.trialslink.domain.enumeration.PrivilegeRequestStatus;
import com.trialslink.security.AuthoritiesConstants;
import com.trialslink.security.SecurityUtils;
import com.trialslink.service.PrivilegeRequestService;
import com.trialslink.web.rest.errors.BadRequestAlertException;
import com.trialslink.web.rest.util.HeaderUtil;
import com.trialslink.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PrivilegeRequest.
 */
@RestController
@RequestMapping("/api")
public class PrivilegeRequestResource {

    private final Logger log = LoggerFactory.getLogger(PrivilegeRequestResource.class);

    private static final String ENTITY_NAME = "privilegeRequest";

    private final PrivilegeRequestService privilegeRequestService;

    public PrivilegeRequestResource(PrivilegeRequestService privilegeRequestService) {
        this.privilegeRequestService = privilegeRequestService;
    }

    /**
     * POST  /privilege-requests : Create a new privilegeRequest.
     *
     * @param privilegeRequest the privilegeRequest to create
     * @return the ResponseEntity with status 201 (Created) and with body the new privilegeRequest, or with status 400 (Bad Request) if the privilegeRequest has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/privilege-requests")
    @Timed
    public ResponseEntity<PrivilegeRequest> createPrivilegeRequest(@Valid @RequestBody PrivilegeRequest privilegeRequest) throws URISyntaxException {
        log.debug("REST request to save PrivilegeRequest : {}", privilegeRequest);
        if (privilegeRequest.getId() != null) {
            throw new BadRequestAlertException("A new privilegeRequest cannot already have an ID", ENTITY_NAME, "idexists");
        }
        boolean isPrivilegedUser = SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.PRIVILEGED) || SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN);
        if (isPrivilegedUser) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "forbidden", "A new privilegeRequest cannot be created by a privileged user")).body(null);
        }
        // ensure privilege status is always set to PENDING
        privilegeRequest.setStatus(PrivilegeRequestStatus.PENDING);
        PrivilegeRequest result = privilegeRequestService.save(privilegeRequest);
        if (result.getId() == null) {
            // ID not being set means that the privilege request was not persisted to the database
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "userhaspendingrequest", "A pending privilege request already exists for this user")).body(null);
        }
        return ResponseEntity.created(new URI("/api/privilege-requests/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * PUT  /privilege-requests : Updates an existing privilegeRequest.
     *
     * @param privilegeRequest the privilegeRequest to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated privilegeRequest,
     * or with status 400 (Bad Request) if the privilegeRequest is not valid,
     * or with status 500 (Internal Server Error) if the privilegeRequest couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/privilege-requests")
    @Timed
    public ResponseEntity<PrivilegeRequest> updatePrivilegeRequest(@Valid @RequestBody PrivilegeRequest privilegeRequest) throws URISyntaxException {
        log.debug("REST request to update PrivilegeRequest : {}", privilegeRequest);
        if (privilegeRequest.getId() == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idnonexists", "The privilegeRequest must already have an ID")).body(null);
        }

        PrivilegeRequest result = privilegeRequestService.handleRequest(privilegeRequest);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, privilegeRequest.getId()))
            .body(result);
    }

    /**
     * GET  /privilege-requests : get all the privilegeRequests.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of privilegeRequests in body
     */
    @GetMapping("/privilege-requests")
    @Timed
    public ResponseEntity<List<PrivilegeRequest>> getAllPrivilegeRequests(Pageable pageable) {
        log.debug("REST request to get a page of PrivilegeRequests");
        Page<PrivilegeRequest> page = privilegeRequestService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/privilege-requests");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /privilege-requests/:id : get the "id" privilegeRequest.
     *
     * @param id the id of the privilegeRequest to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the privilegeRequest, or with status 404 (Not Found)
     */
    @GetMapping("/privilege-requests/{id}")
    @Timed
    public ResponseEntity<PrivilegeRequest> getPrivilegeRequest(@PathVariable String id) {
        log.debug("REST request to get PrivilegeRequest : {}", id);
        PrivilegeRequest privilegeRequest = privilegeRequestService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(privilegeRequest));
    }

    /**
     * DELETE  /privilege-requests/:id : delete the "id" privilegeRequest.
     *
     * @param id the id of the privilegeRequest to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    /*@DeleteMapping("/privilege-requests/{id}")
    @Timed
    public ResponseEntity<Void> deletePrivilegeRequest(@PathVariable String id) {
        // log.debug("REST request to delete PrivilegeRequest : {}", id);
        // privilegeRequestService.delete(id);
        // return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }*/
}
