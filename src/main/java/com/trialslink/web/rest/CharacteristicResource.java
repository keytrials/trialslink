package com.trialslink.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.trialslink.domain.Characteristic;
import com.trialslink.domain.Trial;
import com.trialslink.service.CharacteristicService;
import com.trialslink.service.TrialService;
import com.trialslink.web.rest.errors.BadRequestAlertException;
import com.trialslink.web.rest.util.HeaderUtil;
import com.trialslink.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Characteristic.
 */
@RestController
@RequestMapping("/api")
public class CharacteristicResource {

    private final Logger log = LoggerFactory.getLogger(CharacteristicResource.class);

    private static final String ENTITY_NAME = "characteristic";

    private final CharacteristicService characteristicService;
    private final TrialService trialService;

    public CharacteristicResource(CharacteristicService characteristicService, TrialService trialService) {
        this.characteristicService = characteristicService;
        this.trialService = trialService;
    }

    /**
     * POST  /characteristics : Create a new characteristic.
     *
     * @param characteristic the characteristic to create
     * @return the ResponseEntity with status 201 (Created) and with body the new characteristic, or with status 400 (Bad Request) if the characteristic has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/characteristics")
    @Timed
    public ResponseEntity<Characteristic> createCharacteristic(@Valid @RequestBody Characteristic characteristic) throws URISyntaxException {
        log.debug("REST request to save Characteristic : {}", characteristic);
        if (characteristic.getId() != null) {
            throw new BadRequestAlertException("A new characteristic cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Characteristic result = characteristicService.save(characteristic);
        return ResponseEntity.created(new URI("/api/characteristics/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * PUT  /characteristics : Updates an existing characteristic.
     *
     * @param characteristic the characteristic to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated characteristic,
     * or with status 400 (Bad Request) if the characteristic is not valid,
     * or with status 500 (Internal Server Error) if the characteristic couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/characteristics")
    @Timed
    public ResponseEntity<Characteristic> updateCharacteristic(@Valid @RequestBody Characteristic characteristic) throws URISyntaxException {
        log.debug("REST request to update Characteristic : {}", characteristic);
        if (characteristic.getId() == null) {
            return createCharacteristic(characteristic);
        }
        Characteristic result = characteristicService.save(characteristic);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, characteristic.getId()))
            .body(result);
    }

    /**
     * GET  /characteristics : get all the characteristics.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of characteristics in body
     */
    @GetMapping("/characteristics")
    @Timed
    public List<Characteristic> getAllCharacteristics() {
        log.debug("REST request to get all Characteristics");
        return characteristicService.findAll();
        }

    /**
     * GET  /characteristics/:id : get the "id" characteristic.
     *
     * @param id the id of the characteristic to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the characteristic, or with status 404 (Not Found)
     */
    @GetMapping("/characteristics/{id}")
    @Timed
    public ResponseEntity<Characteristic> getCharacteristic(@PathVariable String id) {
        log.debug("REST request to get Characteristic : {}", id);
        Characteristic characteristic = characteristicService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(characteristic));
    }

    /**
     * DELETE  /characteristics/:id : delete the "id" characteristic.
     *
     * @param id the id of the characteristic to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/characteristics/{id}")
    @Timed
    public ResponseEntity<Void> deleteCharacteristic(@PathVariable String id) {
        log.debug("REST request to delete Characteristic : {}", id);
        characteristicService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }


    /**
     * GET  /trial:id/criteria : get the "id" trial to get the trialCentres.
     *
     * @param id the id of the trial to retrieve
     * @return the ResponseEntity with status 200 (OK) and the list of trialCentres in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/trials/{id}/criteria")
    @Timed
    public ResponseEntity<List<Characteristic>> getCharacteristicsForTrial(@PathVariable String id, @ApiParam Pageable pageable) {
        log.debug("REST request to get TrialCentres for Trial : {}", id);
        Page<Characteristic> page = characteristicService.findAllForTrial(trialService.findOne(id), pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/characteristics");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * POST  /trials/id/criteria : Add a new criteria for trial.
     *
     * @param characteristic the criteria to create
     * @param id the id of the trial
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/trials/{id}/criteria")
    @Timed
    public ResponseEntity<Characteristic> addCharacteristicForTrial(@PathVariable String id, @Valid @RequestBody Characteristic characteristic) throws URISyntaxException {
        log.debug("REST request to add characteristic : {} for Trial : {}", characteristic, id);
        Trial trial = trialService.findOne(id);
        if (trial == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial found with given id")).build();
        }
        characteristic.setTrialId(trial.getId());
        Characteristic result = characteristicService.save(characteristic);
        // now also save trial
        trial.addEligibility(result);
        trialService.save(trial);
        return ResponseEntity.created(new URI("/api/characteristics/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId()))
                .body(result);
    }

    /**
     * PUT  /trials/id/criteria : Add a new criteria for trial.
     *
     * @param characteristic the criteria to create
     * @param id the id of the trial
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/trials/{id}/criteria")
    @Timed
    public ResponseEntity<Characteristic> updateCharacteristicForTrial(@PathVariable String id, @Valid @RequestBody Characteristic characteristic) throws URISyntaxException {
        log.debug("REST request to add characteristic : {} for Trial : {}", characteristic, id);
        Trial trial = trialService.findOne(id);
        if (trial == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial found with given id")).build();
        }

        Characteristic result = characteristicService.findOne(characteristic.getId());
        if (result == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No criteria found with given id")).build();
        }

        result = characteristicService.save(characteristic);
        // now also save trial
        trial = trial.updateEligibility(result.getId(), result);
        trialService.save(trial);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, characteristic.getId()))
                .body(result);
    }

    /**
     * DELETE  /trials/id/criteria/characteristicId : Remove criteria for trial.
     *
     * @param characteristicId the criteria id to remove
     * @param id the id of the trial
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @DeleteMapping("/trials/{id}/criteria/{characteristicId}")
    @Timed
    public ResponseEntity<Characteristic> removeCharacteristicForTrial(@PathVariable String id, @PathVariable String characteristicId) throws URISyntaxException {
        log.debug("REST request to delete characteristic : {} for Trial : {}", characteristicId, id);
        Trial trial = trialService.findOne(id);
        if (trial == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial found with given id")).build();
        }

        Characteristic result = characteristicService.findOne(characteristicId);
        if (result == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No criteria found with given id")).build();
        }
        // now also save trial
        trial.removeEligibility(result);
        trialService.save(trial);
        characteristicService.delete(characteristicId);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * DELETE  /trials/id/criteria : Removes all criteria for trial.
     *
     * @param id the id of the trial
     * @return the ResponseEntity with status 201 (Created) and with body the new trial, or with status 400 (Bad Request) if the trial has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @DeleteMapping("/trials/{id}/criteria")
    @Timed
    public ResponseEntity<Characteristic> removeAllCharacteristicsForTrial(@PathVariable String id) throws URISyntaxException {
        log.debug("REST request to delete all characteristics for Trial : {}", id);
        Trial trial = trialService.findOne(id);
        if (trial == null) {
            return ResponseEntity.notFound().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "missing", "No trial found with given id")).build();
        }

        characteristicService.deleteAllForTrial(id);
        // reset all elibility criteria for trial
        trial.setEligibilities(new HashSet<>());
        trialService.save(trial);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
