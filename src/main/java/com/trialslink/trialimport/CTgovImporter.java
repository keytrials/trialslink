
/**
 * Created by pigiotisk on 14/07/2017.
 */

package com.trialslink.trialimport;

import com.trialslink.domain.*;
import com.trialslink.repository.*;
import com.trialslink.service.TrialCentreService;
import com.trialslink.service.TrialService;
import com.trialslink.service.util.TrialXmlConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.xstream.XStreamMarshaller;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import static com.trialslink.domain.enumeration.TrialImportStatus.COMPLETE;
import static com.trialslink.domain.enumeration.TrialImportStatus.FAILED;

@Component(value = "CTgovImporter")
public class CTgovImporter implements TrialImporterInterface {

    private final Logger log = LoggerFactory.getLogger(CTgovImporter.class);

    private final TrialService trialService;
    private final ContactRepository contactRepository;
    private final ConditionRepository conditionRepository;
    private final InterventionRepository interventionRepository;
    private final TrialCentreService trialCentreService;
    private final CharacteristicRepository characteristicRepository;
    private final XStreamMarshaller xStreamMarshaller;
    private final TrialXmlConverter trialXmlConverter = new TrialXmlConverter();
    private final TrialRepository trialRepository;

    public CTgovImporter (
        TrialService trialService,
        XStreamMarshaller xStreamMarshaller,
        TrialCentreService trialCentreService,
        CharacteristicRepository characteristicRepository,
        ContactRepository contactRepository,
        ConditionRepository conditionRepository,
        InterventionRepository interventionRepository,
        TrialRepository trialRepository
    ) {
        this.trialService = trialService;
        this.xStreamMarshaller = xStreamMarshaller;
        this.trialCentreService = trialCentreService;
        this.characteristicRepository = characteristicRepository;
        this.contactRepository = contactRepository;
        this.conditionRepository = conditionRepository;
        this.interventionRepository = interventionRepository;
        this.trialRepository = trialRepository;
    }

    /**
     *
     * @param trialImport : is the trialImport object that is created when a user imports a trial
     *                    from either NIHR or clinicaltrials.gov. In this case is from clinicaltrials.gov
     * @return :update the trialImport object and set its status either COMPLETE or FAILED
     */
    @Override
    public TrialImport importTrial(TrialImport trialImport) {
        // logic for importing trials
        String nctId = trialImport.getSourceIdentifier();

        // check if a trial with this nctId already exists in the database
        Trial trial = trialRepository.findOneByIdInfoNctId(nctId);

        if (trial == null) {
            try {
                trial = importTrialFromClinicalTrailsGov(nctId);
            } catch (IOException e) {
                trialImport.setStatus(FAILED);
                log.error("Error importing trial with nctId {}. Nested exception is : {}", nctId, e);
                return trialImport;
            }
        }

        trialImport.setStatus(COMPLETE);
        trialImport.addTrial(trial);

        return trialImport;
    }

    /**
     *
     * @param id : is the NCT number from the clinicaltrials.gov website
     * @return : it saves & return the trial.
     * @throws IOException
     */
    public Trial importTrialFromClinicalTrailsGov(String id) throws IOException {
        log.debug("Attempting to import from CTGOV with identifier: ", id);

        String url = "https://clinicaltrials.gov/show/" + id + "?displayxml=true";
        xStreamMarshaller.getXStream().alias("clinical_study", Trial.class);
        xStreamMarshaller.getXStream().registerConverter(trialXmlConverter);

        Trial trial = (Trial) xStreamMarshaller.unmarshalInputStream(new URL(url).openStream());
        log.debug("trial: {}", trial);

        // save characteristics
        Set<Characteristic> characteristics =  new HashSet<>();
        characteristics.addAll(trial.getEligibilities());
        trial.setEligibilities(new HashSet<>());
        characteristics.forEach(characteristic -> {
            trial.addEligibility(characteristicRepository.save(characteristic));
        });

        // collect and verify conditions and then finally add to trial
        Set<Condition> conditions = new HashSet<>();
        for (Condition condition : trial.getConditions()) {
            Condition existing = conditionRepository.findOneByLabel(condition.getLabel());
            if (existing == null) {
                existing = conditionRepository.save(condition);
            }
            conditions.add(existing);
        }
        trial.setConditions(conditions);

        // collect and verify interventions and then finally add to trial
        Set<Intervention> interventions = new HashSet<>();
        for (Intervention intervention : trial.getInterventions()) {
            Intervention existing = interventionRepository.findOneByLabel(intervention.getLabel());
            if (existing == null) {
                existing = interventionRepository.save(intervention);
            }
            interventions.add(existing);
        }
        trial.setInterventions(interventions);

        // we have to save sites explicitly - or db refs wont work
        Set<TrialCentre> sites = new HashSet<>();
        sites.addAll(trial.getSites());
        trial.setSites(new HashSet<>());
        Trial result = trialService.save(trial);
        sites.forEach(site -> {
            site.setTrialId(result.getId());
            TrialCentre centre = trialCentreService.save(site);
            centre.getContacts().forEach(contact -> {
                contact.setTrialCentreId(centre.getId());
                contactRepository.save(contact);
            });
            trial.addSite(centre);
        });

        log.debug("trial id info: {}", trial.getIdInfo());
        // now we need to update characteristics with trial id
        trial.getEligibilities().forEach(characteristic -> {
            characteristic.setTrialId(trial.getId());
            characteristicRepository.save(characteristic);
        });

        return trialService.save(trial);
    }
}
