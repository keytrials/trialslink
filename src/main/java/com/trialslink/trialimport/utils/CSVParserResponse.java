package com.trialslink.trialimport.utils;

import com.trialslink.domain.Trial;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class CSVParserResponse {
    private final Set<Trial> importedTrials = new HashSet<>();
    private final Set<HashMap<String, String>> failedTrials = new HashSet<>();

    public Set<Trial> getImportedTrials() {
        return importedTrials;
    }

    public Set<HashMap<String, String>> getFailedTrials() {
        return failedTrials;
    }

    public boolean addTrial(Trial trial) {
        return this.importedTrials.add(trial);
    }

    public boolean addFailedTrial(HashMap<String, String> failedTrial) {
        return this.failedTrials.add(failedTrial);
    }
}
