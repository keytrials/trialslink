package com.trialslink.trialimport.utils;

import com.trialslink.domain.TrialImportItem;

import java.io.File;
import java.util.List;
import java.util.Map;

public interface TrialReviewer {

//    /**
//     * Creates a list of trials that should be added to the repository from the import file
//     *
//     * @param sheet import file excel sheet
//     * @return list of trials that should be added to the repository
//     */
//    Map<String, String> toBeAddedList(Sheet sheet);
//
//    /**
//     * Creates a list of trials that are already in the repository, thus needs to be updated
//     *
//     * @param existingTrials list of trials that exists in the repository
//     * @param edgeIds that are taken from the import file trials
//     * @return list of trials that should be updated to the repository
//     */
//    Map<String, String> toBeUpdatedList(List<Trial> existingTrials, List<String> edgeIds);
//
//    /**
//     * Creates a list of trials That should be deleted from the repository as they doesn't exists in the latest import file
//     *
//     * @param existingTrials list of trials that exists in the repository
//     * @param edgeIds that are taken from the import file trials
//     * @return list of trials that should be deleted to the repository
//     */
//    Map<String, String> toBeDeletedList(List<Trial> existingTrials, List<String> edgeIds);

    /**
     * Creates a Map which contains the trials which should be added/updated/deleted from the repository
     *
     * @param file import file
     * @return Map that contains all the trials that should be reviewed
     */
    Map<String, List<TrialImportItem>> trialReviewMap(File file);

}
