package com.trialslink.trialimport.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.*;
import java.net.URL;

/**
 * Created by pigiotisk on 18/07/2017.
 * https://stackoverflow.com/questions/34704943/how-to-download-a-file-from-a-url-in-java
 */

@Component
public class CSVDownloader {

    private final Logger log = LoggerFactory.getLogger(CSVDownloader.class);

    /**
     *
     * @param url is the expected url of the file that was saved form the front-end
     * @param file is the file that will be created after the download finished
     * @throws IOException
     */
    public void download(URL url, String file) throws IOException {
        log.debug("Downloading started...");

        //if url or file null then throw an exception
        if (url == null || file == null) throw new NullPointerException();

        byte[] buffer = new byte[256];
        InputStream in = url.openStream();
        FileOutputStream fos = new FileOutputStream(new File(file));

        int length;
        while ((length = in.read(buffer)) > -1) {
            fos.write(buffer, 0, length);
        }

        fos.close();
        in.close();

        log.debug("Download finished successfully!");
    }

}
