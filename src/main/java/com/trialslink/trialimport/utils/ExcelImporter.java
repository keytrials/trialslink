package com.trialslink.trialimport.utils;

import com.trialslink.domain.*;
import com.trialslink.domain.enumeration.Sex;
import com.trialslink.repository.TrialRepository;
import com.trialslink.service.CharacteristicService;
import com.trialslink.service.TrialService;
import com.trialslink.service.TrialVisitorService;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@ConfigurationProperties(prefix = "excelImport", ignoreUnknownFields = false)
public class ExcelImporter extends AbstractImporter implements TrialReviewer {

    private final Logger log = LoggerFactory.getLogger(ExcelImporter.class);
    private  final TrialVisitorService trialVisitorService;
    private final TrialService trialService;
    private final CharacteristicService characteristicService;
    private final TrialRepository trialRepository;
    private boolean filter;
    private int filterColumnNumber;
    private List<String> filterValues;
    private int sheetNumber;
    Workbook workbook;
    Sheet sheet;

    public ExcelImporter(TrialVisitorService trialVisitorService,
                         TrialService trialService,
                         CharacteristicService characteristicService,
                         TrialRepository trialRepository) {
        this.trialVisitorService = trialVisitorService;
        this.trialService = trialService;
        this.characteristicService = characteristicService;
        this.trialRepository = trialRepository;
    }

    public boolean excelRead(File file, Map<String, List<TrialImportItem>> reviewedTrials) {

        log.debug("Request to bulk import all trials in file : ", file);
        int count = 0;
        boolean success = false;
        try {
            workbook = Workbook.getWorkbook(file);
            sheet = workbook.getSheet(sheetNumber);
            importTrials(reviewedTrials);
            // set success to true if everything went off smoothly
            success = true;

        } catch (IOException | BiffException e){
            log.error("Unable to parse the excel sheet. Nested exception is : ",e);
        }
        log.info("Trials imported count = " , count);
        workbook.close();
        return success;
    }

    public void readRows(Sheet sheet, int rowNumber, Trial trial){

        try {
            String trialTitle = sheet.getCell(34, rowNumber).getContents();
            log.info("trialTitle = {}", trialTitle);
            if (trialTitle == null || trialTitle.equals("")) {
                trialTitle = "Oops! I don't have a title... Please fix me!";
            }
            trial.setTitle(trialTitle);
            trial.setBriefTitle(trialTitle);
            trial.setShortName(sheet.getCell(38, rowNumber).getContents());

            // add Disease area as a concept and use id for category
            String diseaseArea = sheet.getCell(11, rowNumber).getContents();
            if (diseaseArea != null && !diseaseArea.isEmpty()) {
                Concept daConcept = trialVisitorService.verifyAndGetConcept(diseaseArea, "area", null);
                trial.setCategory(daConcept.getId());
            }

            // add Disease site as a concept and use id for focus
            String diseaseSite = sheet.getCell(12, rowNumber).getContents();
            if (diseaseSite != null && !diseaseSite.isEmpty()) {
                Concept dsConcept = trialVisitorService.verifyAndGetConcept(diseaseSite, "site", null);
                trial.setFocus(dsConcept.getId());
            }

            String briefSummary = sheet.getCell(42, rowNumber).getContents();
            if (briefSummary == null) {
                briefSummary = "";
            }
            trial.setBriefSummary(briefSummary);

            // get the status of the trial
            String studyStatus = sheet.getCell(40, rowNumber).getContents();
            trial.setStatus(getTrialStatus(studyStatus));

            // determine which phases the trial in
            String phaseString = sheet.getCell(37, rowNumber).getContents();

            trial.setPhases(getTrialPhase(phaseString));

            //determine the type of the study design
            trial.setType(getTrialStudyDesign(sheet.getCell(32, rowNumber).getContents()));

            // set start date
            trial.setStartDate(dateFormatting(sheet.getCell(53, rowNumber).getContents()));

            //set default verified flag
            try {
                if (trial.isVerified() == true)
                    trial.setVerified(true);
            }catch (NullPointerException e){
                trial.setVerified(false);
            }

            // set primary completion date
            String primaryCompletionDate = sheet.getCell(25, rowNumber).getContents();
            trial.setPrimaryCompletionDate(dateFormatting(primaryCompletionDate));

            // set study completion date
            String studyCompletionDate = sheet.getCell(13, rowNumber).getContents();
            trial.setStudyCompletionDate(dateFormatting(studyCompletionDate));

            // criteria
            trial.setEligibilities(getCharacteristicSet(sheet, rowNumber));

            // id info
            IdInfo idinfo = new IdInfo();
            idinfo.setId(sheet.getCell(55, rowNumber).getContents());
            idinfo.setNctId(sheet.getCell(5, rowNumber).getContents());
            Map<String,String> otherIds = new HashMap<>();
            otherIds.put("NIHR Portfolio Study ID",sheet.getCell(22, rowNumber).getContents());
            otherIds.put("ISRCTN",sheet.getCell(19, rowNumber).getContents());
            otherIds.put("EudraCT",sheet.getCell(15, rowNumber).getContents());
            otherIds.put("IRAS Number",sheet.getCell(18, rowNumber).getContents());
            otherIds.put("MREC Number",sheet.getCell(21, rowNumber).getContents());
            otherIds.put("Local project reference",sheet.getCell(20, rowNumber).getContents());
            otherIds.put("Project ID",sheet.getCell(39, rowNumber).getContents());
            idinfo.setSecondaryIds(otherIds);
            trial.setIdInfo(idinfo);

            // contact details
            Contact contact = new Contact();
            contact.setEmail(sheet.getCell(2, rowNumber).getContents());
            contact.setPersonName(sheet.getCell(0, rowNumber).getContents());
            contact.setPhone(sheet.getCell(4, rowNumber).getContents());
            // todo - set contact organisation & address
            trial.setChiefInvestigator(contact);

            // set project site and add details
            log.info("Project ID = {}", sheet.getCell(39, rowNumber).getContents());
            trial = trialVisitorService.checkAndAddDefaultSite(trial);
            int finalI = rowNumber;
            trial.getSites().forEach(site -> {

                try {
                    site.endDate(dateFormatting(sheet.getCell(14, finalI).getContents()));
                    site.setStartDate(dateFormatting(sheet.getCell(54, finalI).getContents()));
                    // add project site status
                    site.setStatus(getTrialStatus(sheet.getCell(41, finalI).getContents()));

                    // add principal investigator
                    Contact principalInvestigator = new Contact();
                    principalInvestigator.setEmail(sheet.getCell(29, finalI).getContents());
                    principalInvestigator.setPersonName(sheet.getCell(27, finalI).getContents());
                    principalInvestigator.setPhone(sheet.getCell(31, finalI).getContents());
                    site.setPrincipalInvestigator(principalInvestigator);

                    // now save update trial centre
                    trialVisitorService.addContactsAndUpdateSite(site);
                } catch (Exception e) {
                    log.error("Error processing fields for site. Nested exception is : ", e);
                }
            });
        } catch (Exception e) {
            log.error("Unable to parse the excel sheet. Nested exception is : ", e);
        }
    }

    public Set<Characteristic> getCharacteristicSet(Sheet sheet, int row) throws IOException{
        Set<Characteristic> characteristicSet = new HashSet<>();
        Characteristic characteristicInclusion = new Characteristic();
        // process gender info
        String genderString = sheet.getCell(35, row).getContents();
        if ("Male".equalsIgnoreCase(genderString)) {
            characteristicInclusion.setSex(Sex.MALE);
            log.info("male");
        } else if ("Female".equalsIgnoreCase(genderString)) {
            characteristicInclusion.setSex(Sex.FEMALE);
            log.info("female");
        } else {
            // todo - this needs better resolution!
            characteristicInclusion.setSex(Sex.ALL);
            log.info("all");
        }
        // process ages
        String maxAgeString = sheet.getCell(23, row).getContents();
        if (maxAgeString != null && !"".equals(maxAgeString)) {
            try {
                int maxAge = Integer.parseInt(maxAgeString);
                characteristicInclusion.setMaxAge(new Age(maxAge));
            } catch (NumberFormatException e) {
                log.error("Error processing max age. Nested exception is : ", e);
            }
        }

        String minAgeString = sheet.getCell(24,row).getContents();
        if (minAgeString != null && !"".equals(minAgeString)) {
            try {
                int minAge = Integer.parseInt(minAgeString);
                characteristicInclusion.setMinAge(new Age(minAge));
            } catch (NumberFormatException e) {
                log.error("Error processing min age. Nested exception is : ", e);
            }
        }

        characteristicInclusion.setCriteria(sheet.getCell(17, row).getContents());
        characteristicInclusion.setExclusive(false);
        Characteristic characteristicExclusion = new Characteristic();
        characteristicExclusion.setCriteria(sheet.getCell(16, row).getContents());
        characteristicExclusion.setSex(Sex.ALL);
        characteristicExclusion.setExclusive(true);
        characteristicService.save(characteristicExclusion);
        characteristicService.save(characteristicInclusion);
        characteristicSet.add(characteristicInclusion);
        characteristicSet.add(characteristicExclusion);

        return characteristicSet;
    }


    public boolean isFilter() {
        return filter;
    }

    public void setFilter(boolean filter) {
        this.filter = filter;
    }

    public int getFilterColumnNumber() {
        return filterColumnNumber;
    }

    public void setFilterColumnNumber(int filterColumnNumber) {
        this.filterColumnNumber = filterColumnNumber;
    }

    public List<String> getFilterValues() {
        return filterValues;
    }

    public void setFilterValues(List<String> filterValues) {
        this.filterValues = filterValues;
    }

    public int getSheetNumber() {
        return sheetNumber;
    }

    public void setSheetNumber(int sheetNumber) {
        this.sheetNumber = sheetNumber;
    }

    @Override
    public Map<String, List<TrialImportItem>> trialReviewMap(File file) {
        try {
            workbook = Workbook.getWorkbook(file);
        } catch (IOException | BiffException e) {
            log.error("Error reading file passed. Nested exception is : ", e);
        }
        sheet = workbook.getSheet(sheetNumber);
        Map<String, List<TrialImportItem>> trialsToBeReviewed = new HashMap<>();
        List<Trial> existingTrials = trialRepository.findAll();
        Map<String, String> edgeData = new HashMap<>();
        for (int i = 1; i <= sheet.getRows()-1; i++){
            String edgeId = sheet.getCell(55, i).getContents();
            String trialTitle = sheet.getCell(34, i).getContents();
            edgeData.put(edgeId, trialTitle);
        }
        log.info("edgeIds count: "+ edgeData.size());
        log.info("existing count: "+ existingTrials.size());

        // now generate changes by comparing with existing trials
        List<TrialImportItem> toBeAdded = new ArrayList<>();
        List<TrialImportItem> toBeUpdated = new ArrayList<>();
        List<TrialImportItem> toBeDeleted = new ArrayList<>();

        // create a list of processedIds, which we can use later
        Set<String> existingTrialIds = new HashSet<>();
        existingTrials.forEach(trial -> {
            String id = trial.getIdInfo().getId();
            if (edgeData.keySet().contains(id)) {
                toBeUpdated.add(new TrialImportItem(id, trial.getTitle()));
            }
            else {
                toBeDeleted.add(new TrialImportItem(id, trial.getTitle()));
            }
            // add to existing trial ids
            existingTrialIds.add(id);
        });
        /*
         now compare edge data with toBeUpdated and toBeDeleted lists, if they don't contain the relevant key,
         then it is a trial to be added/imported
        */
        edgeData.forEach((key, value) -> {
            if (!existingTrialIds.contains(key)) {
                toBeAdded.add(new TrialImportItem(key, value));
            }
        });

        // now populate final map
        trialsToBeReviewed.put("add", toBeAdded);
        trialsToBeReviewed.put("update", toBeUpdated);
        trialsToBeReviewed.put("delete", toBeDeleted);
        return trialsToBeReviewed;
    }

    private void addOrUpdateTrial(TrialImportItem importItem) {

        // locate cell that corresponds to this item - using the id
        Cell cell = sheet.findCell(importItem.getId(),55,0,55,sheet.getRows(),false);
        log.info("cd "+sheet.findCell(importItem.getId(),55,0,55,sheet.getRows(),false).getContents());
        // verify if trial exists
        Trial trial = trialRepository.findOneByIdInfoId(importItem.getId());
        if(trial == null) {
            trial = new Trial();
        }
        if (filter) {
            if (filterValues.contains(sheet.getCell(filterColumnNumber, cell.getRow()).getContents())) {
                readRows(sheet,cell.getRow(),trial);
            }
        } else {
            readRows(sheet,cell.getRow(),trial);
        }
        Trial trial1 = trialService.save(trial);
        trialVisitorService.lungTrialsVisit(trial1);
    }

    public void deleteTrials(List<TrialImportItem> tobeDeletedTrials){
        tobeDeletedTrials.forEach(importItem -> {
            // todo - this really needs to be service thing, because there might no no trial by that id!!!
            Trial trial = trialRepository.findOneByIdInfoId(importItem.getId());
            trialService.delete(trial.getId());
        });
    }

    public void importTrials(Map<String, List<TrialImportItem>> reviewedTrials){
        reviewedTrials.forEach((key, value) -> {
            if ("delete".equalsIgnoreCase(key)){
                deleteTrials(value);
            } else {
                value.forEach(this::addOrUpdateTrial);
            }
        });

    }
}
