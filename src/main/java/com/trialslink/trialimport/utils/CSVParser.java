package com.trialslink.trialimport.utils;

import com.csvreader.CsvReader;
import com.trialslink.domain.*;
import com.trialslink.domain.enumeration.Sex;
import com.trialslink.service.CharacteristicService;
import com.trialslink.service.TrialVisitorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.*;

@Component
public class CSVParser extends AbstractImporter{

    private final Logger log = LoggerFactory.getLogger(CSVParser.class);
    private final TrialVisitorService trialVisitorService;
    private final CharacteristicService characteristicService;

    public CSVParser(TrialVisitorService trialVisitorService,
                     CharacteristicService characteristicService    ) {
        this.trialVisitorService = trialVisitorService;
        this.characteristicService = characteristicService;
    }

    public CSVParserResponse parse(String filePath) {
        log.debug("CSVParser start reading trials from file: " + filePath);

        CSVParserResponse parserResponse = new CSVParserResponse();
        try {
            CsvReader csvReader = new CsvReader(filePath);
            parserResponse = this.readTrials(csvReader, parserResponse);
        } catch (IOException e) {
            log.debug("Unable to read trials from file: <" + filePath + ">", e);
        }
        return parserResponse;
    }

    public CSVParserResponse readTrials(CsvReader csvReader, CSVParserResponse csvParserResponse) throws IOException {
        csvReader.readHeaders();
        String[] csvHeaders = csvReader.getHeaders();

        while (csvReader.readRecord()) {
            Trial trial;
            try {
                trial = this.readRow(csvReader);
            } catch (IOException e) {
                log.debug("Error reading single trial from source file...", e);
                csvParserResponse.addFailedTrial(buildFailedTrial(csvReader, csvHeaders));
                continue;
            }
            if (trial == null) {
                csvParserResponse.addFailedTrial(buildFailedTrial(csvReader, csvHeaders));
                continue;
            }
            csvParserResponse.addTrial(trial);
        }

        return csvParserResponse;
    }

    public Trial readRow(CsvReader csvReader) throws IOException {
        // add stuff from csv file to trial; missing of brief name. Use short name temporally.
        Trial trial = new Trial();
        Organisation sponsor = new Organisation();

        String trialTitle = csvReader.get("Project Full title");
        log.info("trialTitle = {}", trialTitle);
        if (trialTitle == null || trialTitle.equals("")) {
            trialTitle = "Oops! I don't have a title... Please fix me!";
        }
        trial.setTitle(trialTitle);
        trial.setBriefTitle(trialTitle);
        trial.setShortName(csvReader.get("Project Short title"));

        // add Disease area as a concept and use id for category
        String diseaseArea = csvReader.get("Disease area");
        if (diseaseArea != null && !diseaseArea.isEmpty()){
            Concept daConcept = trialVisitorService.verifyAndGetConcept(diseaseArea, "area", null);
            trial.setCategory(daConcept.getId());
        }

        // add Disease site as a concept and use id for focus
        String diseaseSite = csvReader.get("Disease site");
        if (diseaseSite != null && !diseaseSite.isEmpty()){
            Concept dsConcept = trialVisitorService.verifyAndGetConcept(diseaseSite, "site", null);
            trial.setFocus(dsConcept.getId());
        }

        String briefSummary = csvReader.get("Project Summary");
        if (briefSummary == null) {
            briefSummary = "";
        }
        trial.setBriefSummary(briefSummary);

        // get the status of the trial
        String studyStatus = csvReader.get("Project status");
        trial.setStatus(getTrialStatus(studyStatus));

        // determine which phases the trial in
        String phaseString = csvReader.get("Project phase");
        trial.setPhases(getTrialPhase(phaseString));

        // link trial to condition
        String condition = csvReader.get("Condition");
        if(condition != null && ! condition.isEmpty()) {
            trialVisitorService.linkOrAddCondition(trial, condition);
        } else {
            log.info("Unable to import condition with label {}", condition);
        }
        // determine the type of the study design
        trial.setType(getTrialStudyDesign(csvReader.get("Project designs")));

        // determine the live study status
        trial.setHasExpandedAccess(getTrialExpandedAccess(csvReader));

        // set start date
        String startDate = csvReader.get("Start Date");
        trial.setStartDate(dateFormatting(startDate));

        // set record verification date
        String recordVerificationDate = csvReader.get("First Participant UK");
        trial.setRecordVerificationDate(dateFormatting(recordVerificationDate));

        // set primary completion date
        String primaryCompletionDate = csvReader.get("Planned End Date");
        trial.setPrimaryCompletionDate(dateFormatting(primaryCompletionDate));

        // set study completion date
        String studyCompletionDate = csvReader.get("End Date");
        trial.setStudyCompletionDate(dateFormatting(studyCompletionDate));

        // this is the sponsor's name
        sponsor.setName(csvReader.get("Sponsors"));
        trial.setSponsor(sponsor);

        // intervention details
        String interventionDetail = csvReader.get("Intervention Detail");
        trial.setInterventions(getInterventions(interventionDetail));

        // criteria
        trial.setEligibilities(getCharacteristicSet(csvReader));

        // id info
        IdInfo idinfo = new IdInfo();
        idinfo.setId(csvReader.get("Project site Edge ID"));
        idinfo.setProtocolId(csvReader.get("Protocol ID"));
        idinfo.setNctId(csvReader.get("Clinical Trials Gov"));
        Map< String, String > otherIds = new HashMap<>();
        otherIds.put("NIHR Portfolio Study ID",csvReader.get("NIHR Portfolio Study ID"));
        otherIds.put("ISRCTN",csvReader.get("ISRCTN"));
        otherIds.put("EudraCT",csvReader.get("EudraCT"));
        otherIds.put("IRAS Number",csvReader.get("IRAS Number"));
        otherIds.put("MREC Number",csvReader.get("MREC Number"));
        otherIds.put("Local project reference",csvReader.get("Local project reference"));
        otherIds.put("Project ID",csvReader.get("Project ID"));
        idinfo.setSecondaryIds(otherIds);
        trial.setIdInfo(idinfo);

        // contact details
        Contact contact = new Contact();
        contact.setEmail(csvReader.get("Chief Investigator Email"));
        contact.setPersonName(csvReader.get("Chief Investigator"));
        contact.setPhone(csvReader.get("Chief Investigator Phone"));
        // todo - set contact organisation & address
        trial.setChiefInvestigator(contact);

        // set project site and add details
        log.info("Project ID = {}", csvReader.get("Project ID"));
        trial = trialVisitorService.checkAndAddDefaultSite(trial);
        trial.getSites().forEach(site -> {

            try {
                site.endDate(dateFormatting(csvReader.get("Project site Planned closing date")));
                site.setStartDate(dateFormatting(csvReader.get("Project site Start date (NHS Permission)")));
                // add project site status
                site.setStatus(getTrialStatus(csvReader.get("Project site status")));

                // add key contact
                Contact keyContact = new Contact();
                keyContact.setEmail(csvReader.get("Key staff email"));
                keyContact.setPersonName(csvReader.get("Key staff"));
                keyContact.setPhone(csvReader.get("Key staff phone"));
                site.addContact(keyContact);

                // add principal investigator
                Contact principalInvestigator = new Contact();
                principalInvestigator.setEmail(csvReader.get("Principal Investigator Email"));
                principalInvestigator.setPersonName(csvReader.get("Principal Investigator"));
                principalInvestigator.setPhone(csvReader.get("Principal Investigator Phone"));
                site.setPrincipalInvestigator(principalInvestigator);

                // now save update trial centre
                trialVisitorService.addContactsAndUpdateSite(site);
            }
            catch (IOException e) {
                log.error("Error processing fields for site. Nested exception is : ", e);
            }
        });


        // add the new trial to the trial set
        log.debug("Finished reading Trial: " + trial.toString() + " from source file...\n");

        return trial;
    }

    public HashMap<String, String> buildFailedTrial(CsvReader csvReader, String[] csvHeaders) {
        HashMap<String, String> failedTrial = new HashMap<>();

        for (String header: csvHeaders) {
            String fieldValue;
            try {
                fieldValue = csvReader.get(header);
            } catch (IOException e) {
                fieldValue = "";
            }
            failedTrial.put(header, fieldValue);
        }

        return failedTrial;
    }

    public Set<Characteristic> getCharacteristicSet(CsvReader csvReader) throws IOException{
        Set<Characteristic> characteristicSet = new HashSet<>();
        Characteristic characteristicInclusion = new Characteristic();
        // process gender info
        String genderString = csvReader.get("Project Genders");
        if("Male".equalsIgnoreCase(genderString)) {
            characteristicInclusion.setSex(Sex.MALE);
            log.info("male");
        } else if ("Female".equalsIgnoreCase(genderString)) {
            characteristicInclusion.setSex(Sex.FEMALE);
            log.info("female");
        } else {
            // todo - this needs better resolution!
            characteristicInclusion.setSex(Sex.ALL);
            log.info("all");
        }
        // process ages
        String maxAgeString = csvReader.get("Patient maximum age");
        if(maxAgeString != null && !"".equals(maxAgeString)) {
            try {
                int maxAge = Integer.parseInt(maxAgeString);
                characteristicInclusion.setMaxAge(new Age(maxAge));
            } catch (NumberFormatException e) {
                log.error("Error processing max age. Nested exception is : ", e);
            }
        }

        String minAgeString = csvReader.get("Patient minimum age");
        if(minAgeString != null && !"".equals(minAgeString)) {
            try {
                int minAge = Integer.parseInt(minAgeString);
                characteristicInclusion.setMinAge(new Age(minAge));
            } catch (NumberFormatException e) {
                log.error("Error processing min age. Nested exception is : ", e);
            }
        }

        characteristicInclusion.setCriteria(csvReader.get("Inclusion Criteria"));
        characteristicInclusion.setExclusive(false);
        Characteristic characteristicExclusion = new Characteristic();
        characteristicExclusion.setCriteria(csvReader.get("Exclusion Criteria"));
        characteristicExclusion.setSex(Sex.ALL);
        characteristicExclusion.setExclusive(true);
        characteristicService.save(characteristicExclusion);
        characteristicService.save(characteristicInclusion);
        characteristicSet.add(characteristicInclusion);
        characteristicSet.add(characteristicExclusion);
        return characteristicSet;

    }

    public Set<Intervention> getInterventions(String interventionDetail) {
        Set<Intervention> interventionSet = new HashSet<>();
        StringTokenizer st1 = new StringTokenizer(interventionDetail,",");
        String[] interventionDetailArray = new String[st1.countTokens()];
        int counter1 = 0;
        while (st1.hasMoreElements()){
            interventionDetailArray[counter1++] = st1.nextToken();
        }

        for(int i=0;i<interventionDetailArray.length;i++){
            Intervention intervention = new Intervention();
            intervention.setType(interventionDetail);
            interventionDetailArray[i] = interventionDetailArray[i].trim();
            intervention.setDescription(interventionDetailArray[i]);
            interventionSet.add(intervention);
        }
        return interventionSet;

    }

    public Boolean getTrialExpandedAccess(CsvReader csvReader) throws IOException {
        return csvReader.get("Live Study").equals("Yes");
    }

}

