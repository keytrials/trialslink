package com.trialslink.trialimport.utils;

import com.trialslink.domain.enumeration.Phase;
import com.trialslink.domain.enumeration.StudyType;
import com.trialslink.domain.enumeration.TrialStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import static com.trialslink.domain.enumeration.Phase.*;
import static com.trialslink.domain.enumeration.StudyType.BOTH;
import static com.trialslink.domain.enumeration.StudyType.INTERVENTIONAL;
import static com.trialslink.domain.enumeration.StudyType.OBSERVATIONAL;
import static com.trialslink.domain.enumeration.TrialStatus.*;
import static com.trialslink.domain.enumeration.TrialStatus.UNKNOWN;

public class AbstractImporter {
    private final Logger log = LoggerFactory.getLogger(AbstractImporter.class);
    @Autowired
    Environment env;

    public TrialStatus getTrialStatus(String studyStatus) {
        //determine which status the trial in.
        TrialStatus ts = null;
        if(studyStatus.equalsIgnoreCase("Open to Recruitment") || studyStatus.equalsIgnoreCase("Recruiting")
            || studyStatus.equalsIgnoreCase("Open")){
            ts = RECRUITING;
        } else if(studyStatus.contains("Setup")){
            ts = NOT_YET_RECRUITING;
        } else if(studyStatus.contains("Withdraw")){
            ts = WITHDRAWN;
        } else if(studyStatus.contains("Complete")){
            ts = COMPLETED;
        } else if(studyStatus.contains("Suspend")){
            ts = SUSPENDED;
        } else if(studyStatus.contains("Feasibility")){
            ts = FEASIBILITY;
        } else if(studyStatus.equalsIgnoreCase("In Follow Up") || ("Closed to recruitment - in follow up").equalsIgnoreCase(studyStatus)){
            ts = FOLLOWUP;
        } else if(studyStatus.contains("No Follow Up") || "Closed to recruitment - no follow up".equalsIgnoreCase(studyStatus)){
            ts = TERMINATED;
        } else {
            ts = UNKNOWN;
            log.error("Unrecognised trial status found - {}", studyStatus);
        }
        return ts;
    }

    public Set<Phase> getTrialPhase(String phaseString) {
        StringTokenizer phaseStringTokenizer = new StringTokenizer(phaseString, ",/");
        String[] sourceStrArray = new String[phaseStringTokenizer.countTokens()];
        int counter = 0;
        while (phaseStringTokenizer.hasMoreElements()) {
            sourceStrArray[counter++] = phaseStringTokenizer.nextToken();
        }
        Set<Phase> phaseSet = new HashSet<>();

        for (int i = 0; i < sourceStrArray.length; i++) {
            sourceStrArray[i] = sourceStrArray[i].trim();

            Phase phase = null;
            if ("N/A".equals(sourceStrArray[i]) || "Not Applicable".equals(sourceStrArray[i])) {
                log.info("Not applicable");
            }
            else if("Pilot/Feasibility".equals(sourceStrArray[i])) {
                phase = ZERO;
            }
            else if ("I".equals(sourceStrArray[i]) ||
                "Ia".equals(sourceStrArray[i]) ||
                "Ib".equals(sourceStrArray[i]) ||
                "Phase I".equals(sourceStrArray[i]) ||
                "Phase Ia".equals(sourceStrArray[i]) ||
                "Phase Ib".equals(sourceStrArray[i])) {
                phase = ONE;
            }
            else if ("II".equals(sourceStrArray[i]) ||
                "IIa".equals(sourceStrArray[i]) ||
                "IIb".equals(sourceStrArray[i]) ||
                "Phase II".equals(sourceStrArray[i]) ||
                "Phase IIa".equals(sourceStrArray[i]) ||
                "Phase IIb".equals(sourceStrArray[i])) {
                phase = TWO;
            }
            else if ("III".equals(sourceStrArray[i]) ||
                "IIIa".equals(sourceStrArray[i]) ||
                "IIIb".equals(sourceStrArray[i]) ||
                "Phase III".equals(sourceStrArray[i]) ||
                "Phase IIIa".equals(sourceStrArray[i]) ||
                "Phase IIIb".equals(sourceStrArray[i])) {
                phase = THREE;
            }
            else if ("IV".equals(sourceStrArray[i]) ||
                "IVa".equals(sourceStrArray[i]) ||
                "IVb".equals(sourceStrArray[i]) ||
                "Phase IV".equals(sourceStrArray[i]) ||
                "Phase IVa".equals(sourceStrArray[i]) ||
                "Phase IVb".equals(sourceStrArray[i])) {
                phase = FOUR;
            }
            else {
                log.info(" Cannot add {}!",sourceStrArray.toString());
            }
            if (phase != null) {
                phaseSet.add(phase);
            }
        }
        return phaseSet;
    }

    public ZonedDateTime dateFormatting(String dateString){
        String pattern = env.getProperty("application.dateFormat");
        ZonedDateTime date = null;
        if(!"".equals(dateString)) {
            LocalDate localDate = null;
            // we try two different date formats
            try {
                localDate = LocalDate.parse(dateString, DateTimeFormatter.ofPattern("M/d/yy"));
            } catch (Exception e) {
                localDate = LocalDate.parse(dateString, DateTimeFormatter.ofPattern(pattern));

            } finally {
                if(localDate != null) {
                    date = localDate.atStartOfDay(ZoneOffset.UTC);
                }
            }
        }
        return date;
    }

    public StudyType getTrialStudyDesign(String studyDesignType) throws IOException {
        //determine the type of the study design
        switch (studyDesignType) {
            case "Interventional":
                return INTERVENTIONAL;
            case "Observational":
                return OBSERVATIONAL;
            default:
                return BOTH;
        }
    }
}
