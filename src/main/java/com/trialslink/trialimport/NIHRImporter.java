package com.trialslink.trialimport;

import com.trialslink.domain.Trial;
import com.trialslink.domain.TrialImport;
import com.trialslink.repository.TrialRepository;
import com.trialslink.service.TrialService;
import com.trialslink.trialimport.utils.CSVDownloader;
import com.trialslink.trialimport.utils.CSVParser;
import com.trialslink.trialimport.utils.CSVParserResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static com.trialslink.domain.enumeration.TrialImportStatus.COMPLETE;
import static com.trialslink.domain.enumeration.TrialImportStatus.FAILED;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by pigiotisk on 14/07/2017.
 */
@Component(value = "NIHRImporter")
public class NIHRImporter implements TrialImporterInterface {
    private final Logger log = LoggerFactory.getLogger(NIHRImporter.class);

    private final TrialService trialService;
    private final TrialRepository trialRepository;
    private final CSVDownloader csvDownloader;
    private final CSVParser csvParser;

    public NIHRImporter(
        TrialService trialService,
        TrialRepository trialRepository,
        CSVDownloader csvDownloader,
        CSVParser csvParser
    ) {
        this.trialService = trialService;
        this.trialRepository = trialRepository;
        this.csvDownloader = csvDownloader;
        this.csvParser = csvParser;
    }

    /**
     * @param trialImport : is the trialImport object that is created when a user imports a trial
     *                    from either NIHR or clinicaltrials.gov . In this case is from NIHR
     * @return :update the trialImport object and set its status either COMPLETE or FAILED
     */
    @Override
    public TrialImport importTrial(TrialImport trialImport) {
        String trialSourceFilePath = this.downloadTrialImportSourceFile(trialImport);
        if (trialSourceFilePath == null) {
            trialImport.setStatus(FAILED);
            return trialImport;
        }

        Set<Trial> importedTrials;
        Set<HashMap<String, String>> failedTrials;

        CSVParserResponse csvParserResponse = this.csvParser.parse(trialSourceFilePath);
        importedTrials = csvParserResponse.getImportedTrials();
        failedTrials = csvParserResponse.getFailedTrials();

        for (Trial trial:importedTrials){
            try {
                Trial existingTrial = trialRepository.findOneByShortName(trial.getShortName());
                if (existingTrial == null) {
                    // trial doesn't already exist in the database, so save it as a new trial
                    trialService.save(trial);
                }
            } catch (Exception e) {
                log.debug("Unable to save imported trial: " + trial);
                //TODO: convert the unsaved trial object into a failedTrial
                continue;
            }
            trialImport.addTrial(trial);
        }

        if (failedTrials.size() > 0) {
            trialImport.setFailedTrials(failedTrials);
            trialImport.setStatus(FAILED);
        } else {
            trialImport.setStatus(COMPLETE);
        }

        log.debug("Finished reading trials...");
        log.debug("Number of successful trials: " +  importedTrials.size());
        log.debug("Number of failed trials: " +  failedTrials.size());

        return trialImport;
    }

    private String downloadTrialImportSourceFile(TrialImport trialImport) {
        URL url;
        String downloadFilePath;

        try {
            url = new URL(trialImport.getFilePath());
        } catch (MalformedURLException e) {
            log.debug("Unable to create download URL for TrialImport<" + trialImport.getId() + ">", e);
            return null;
        }
        try {
            downloadFilePath = "data/trial_import_" + trialImport.getId() + ".csv";
            this.csvDownloader.download(url, downloadFilePath);
        } catch (IOException e) {
            log.debug("Failed to download source file for TrialImport<" + trialImport.getId() + ">", e);
            return null;
        }
        return downloadFilePath;
    }
}
