package com.trialslink.repository;

import com.trialslink.domain.Age;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Age entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AgeRepository extends MongoRepository<Age, String> {

}
