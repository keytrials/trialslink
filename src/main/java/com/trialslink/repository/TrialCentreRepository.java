package com.trialslink.repository;

import com.trialslink.domain.TrialCentre;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Set;

/**
 * Spring Data MongoDB repository for the TrialCentre entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrialCentreRepository extends MongoRepository<TrialCentre,String> {
    Page<TrialCentre> findByTrialId(String trialId, Pageable pageable);

    Set<TrialCentre> findByName(String name);
}
