package com.trialslink.repository;

import com.trialslink.domain.OutcomeMeasure;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the OutcomeMeasure entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OutcomeMeasureRepository extends MongoRepository<OutcomeMeasure, String> {

}
