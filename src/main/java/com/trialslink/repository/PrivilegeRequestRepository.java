package com.trialslink.repository;

import com.trialslink.domain.PrivilegeRequest;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the PrivilegeRequest entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PrivilegeRequestRepository extends MongoRepository<PrivilegeRequest,String> {

}
