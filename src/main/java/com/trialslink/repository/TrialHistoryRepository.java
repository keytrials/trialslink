package com.trialslink.repository;

import com.trialslink.domain.TrialHistory;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the TrialHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TrialHistoryRepository extends MongoRepository<TrialHistory,String> {

}
