package com.trialslink.repository;

import com.trialslink.domain.Concept;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Spring Data MongoDB repository for the Concept entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConceptRepository extends MongoRepository<Concept, String> {

    Concept findOneByLabel(String label);

    Concept findOneByCode(String code);

    Concept findOneByLabelAndType(String label, String type);

    Concept findOneByIdOrLabel(String id, String label);

    List<Concept> findAllByType(String type);

    Page<Concept> findAllByType(Pageable pageable, String type);
}
