package com.trialslink.repository;

import com.trialslink.domain.Authority;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Authority entity.
 */
public interface AuthorityRepository extends MongoRepository<Authority, String> {

    Optional<Authority> findOneByName(String authority);
}
