package com.trialslink.repository;

import com.trialslink.domain.Intervention;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Intervention entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InterventionRepository extends MongoRepository<Intervention, String> {

    Intervention findOneByLabel(String label);
}
