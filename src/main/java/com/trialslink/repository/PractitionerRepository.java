package com.trialslink.repository;

import com.trialslink.domain.Practitioner;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Practitioner entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PractitionerRepository extends MongoRepository<Practitioner, String> {

}
